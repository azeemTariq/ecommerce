-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2021 at 04:54 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abc`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `our_mission` varchar(1000) DEFAULT NULL,
  `our_vision` varchar(1000) DEFAULT NULL,
  `our_goal` varchar(1000) DEFAULT NULL,
  `our_journy` varchar(1000) DEFAULT NULL,
  `created_by` varchar(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(11) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `our_mission`, `our_vision`, `our_goal`, `our_journy`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 'Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular e-markets whereas customized niches. Assertively envisioneer sticky intellectual capital for user-centric leadership skills.', 'Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular e-markets whereas customized niches. Assertively envisioneer sticky intellectual capital for user-centric leadership skills.', 'Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular e-markets whereas customized niches. Assertively envisioneer sticky intellectual capital for user-centric leadership skills.', 'Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular e-markets whereas customized niches. Assertively envisioneer sticky intellectual capital for user-centric leadership skills.', '32', '2021-06-21 20:18:15', '32', '2021-07-08 20:48:14');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `bannes_name` varchar(255) DEFAULT NULL,
  `target_url` int(11) NOT NULL DEFAULT 0,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(3000) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT 0,
  `created_by` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(100) NOT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `page_url` varchar(256) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT 1,
  `banner` varchar(500) DEFAULT NULL,
  `showincollection` int(11) NOT NULL DEFAULT 1,
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(255) NOT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `page_url`, `description`, `image`, `is_active`, `banner`, `showincollection`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(83, 'Perfume', 'pre-v', 'Perfume', '', 1, NULL, 1, '32', '2021-06-07 15:25:55', '32', '2021-06-16 16:16:14'),
(84, 'my Category', 'myategory', '', '', 1, 'a1624811162.jpg', 1, '32', '2021-06-27 16:26:02', '32', '2021-06-27 16:29:09'),
(85, 'Doloribus nisi dolor', 'epudiandaeiustomo', 'Quia quod et facilis', '', 1, 'a1624812667.jpg', 1, '32', '2021-06-27 16:51:07', '32', '2021-06-27 16:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE `faq_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(100) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `modified_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`id`, `title`, `is_active`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(1, 'Importance of System', 1, '2021-07-01 21:13:51', '1', NULL, NULL),
(2, 'Opinion', 1, '2021-07-01 21:14:10', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `shop` varchar(300) DEFAULT NULL,
  `phone` varchar(233) DEFAULT NULL,
  `address` varchar(233) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `roleId` int(11) NOT NULL,
  `add` int(2) NOT NULL DEFAULT 0,
  `edit` int(2) NOT NULL DEFAULT 0,
  `delete` int(2) NOT NULL DEFAULT 0,
  `view` int(2) NOT NULL DEFAULT 0,
  `fcm_token` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `aaa` tinyint(2) NOT NULL DEFAULT 0,
  `is_customer` tinyint(2) NOT NULL DEFAULT 1,
  `created_by` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(100) NOT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `name`, `email`, `shop`, `phone`, `address`, `password`, `roleId`, `add`, `edit`, `delete`, `view`, `fcm_token`, `is_active`, `aaa`, `is_customer`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(32, 'admin', 'Azeem Tariq', 'az@gmail.com', 'Head Office Admin', '030300300300', 'abc school', 'admin', 0, 0, 0, 0, 0, NULL, 1, 0, 0, '', '2021-06-07 20:24:06', '', NULL),
(34, 'adm', 'Azeem Tariq', 'az@gmail.com', 'Head Office Admin', '030300300300', 'abc school', 'adm', 0, 0, 0, 0, 0, NULL, 1, 0, 1, '', '2021-06-07 20:24:06', '', NULL),
(35, 'Yardley', 'Yardley Roy', 'gytubetu@mailinator.com', NULL, '27', NULL, '@#$#FGFFF447', 0, 0, 0, 0, 0, NULL, 1, 0, 1, '', '2021-06-17 23:28:05', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_CreateTicketsTable', 1),
(2, '2014_10_12_100000_CreateTicketsTable2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `cust_id` varchar(100) DEFAULT '0',
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cell` varchar(255) DEFAULT NULL,
  `ship_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ship_country` varchar(255) DEFAULT NULL,
  `ship_postal` varchar(255) DEFAULT NULL,
  `bill_address` varchar(255) DEFAULT NULL,
  `bill_country` varchar(255) DEFAULT NULL,
  `bill_postal` varchar(255) DEFAULT NULL,
  `massage` varchar(255) DEFAULT NULL,
  `items` varchar(255) NOT NULL DEFAULT '0',
  `totalAmount` varchar(255) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT 1,
  `payment_method` varchar(20) DEFAULT NULL,
  `payment_status` int(11) DEFAULT 0,
  `payment_date` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `code`, `year`, `date`, `time`, `cust_id`, `first_name`, `last_name`, `email`, `cell`, `ship_address`, `ship_country`, `ship_postal`, `bill_address`, `bill_country`, `bill_postal`, `massage`, `items`, `totalAmount`, `status`, `payment_method`, `payment_status`, `payment_date`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(12, 'ES-2100011', '2021', '2021-06-16', '16:08:44', '0', 'Virginia', 'Peterson', 'gise@mailinator.com', 'Molestiae recusandae', 'Et illo officia volu', NULL, NULL, NULL, NULL, NULL, NULL, '1', '250', 4, NULL, 1, '2021-06-20 16:00:04', NULL, '2021-06-16 16:08:44', 32, '2021-06-20 21:00:04'),
(13, 'ES-2100012', '2021', '2021-06-17', '13:32:36', '0', 'Xandra', 'Hull', 'jixasix@mailinator.com', '40', 'Reiciendis animi ad', '1', 'xutycipizi@mailinator.com', 'Non consectetur dui', '1', 'nusesafubi@mailinator.com', NULL, '1', '250', 2, '1', 1, '2021-06-20 16:05:18', NULL, '2021-06-17 13:32:36', 32, '2021-06-20 21:09:02'),
(14, 'ES-2100013', '2021', '2021-06-17', '18:28:05', '35', 'Yardley', 'Roy', 'gytubetu@mailinator.com', '27', 'Eaque aliqua Sed de', '1', 'benoweva@mailinator.com', 'Aspernatur alias ass', '1', 'vekuqa@mailinator.com', NULL, '1', '250', 1, '1', 0, NULL, NULL, '2021-06-17 18:28:05', NULL, NULL);

--
-- Triggers `orders`
--
DELIMITER $$
CREATE TRIGGER `orders` BEFORE INSERT ON `orders` FOR EACH ROW BEGIN
    UPDATE 
     `sequence` 
    SET
     `executed_record` = @tempVariable := executed_record + 1 
    WHERE table_name = 'orders' 
     AND tbl_year = NEW.year;
        IF (ROW_COUNT() < 1) THEN 
         INSERT INTO sequence SET table_name = 'orders',tbl_year = NEW.year,executed_record = 1;
         SET @tempVariable = 1;
    END IF;
     SET NEW.code = CONCAT('ES-',DATE_FORMAT(NOW(),'%y'),LPAD((@tempVariable),5, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `OrderDetailId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `ItemId` int(11) NOT NULL,
  `ItemQty` int(11) NOT NULL,
  `suggestion` varchar(300) DEFAULT NULL,
  `AddedOn` int(11) NOT NULL,
  `AddedBy` int(11) NOT NULL,
  `modified_by` varchar(11) NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT 1,
  `is_done` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`OrderDetailId`, `OrderId`, `code`, `year`, `ItemId`, `ItemQty`, `suggestion`, `AddedOn`, `AddedBy`, `modified_by`, `modified_date`, `Status`, `is_done`) VALUES
(18, 12, 'ES-2100011', '2021', 569, 1, '', 0, 0, '32', '2021-06-16 16:09:28', 1, 0),
(19, 13, 'ES-2100012', '2021', 569, 1, '', 0, 0, '', NULL, 1, 0),
(20, 14, 'ES-2100013', '2021', 570, 1, '', 0, 0, '', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameter`
--

CREATE TABLE `parameter` (
  `id` int(11) NOT NULL,
  `par_code` varchar(255) NOT NULL,
  `par_data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameter`
--

INSERT INTO `parameter` (`id`, `par_code`, `par_data`) VALUES
(1, 'UPDATE', '30');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE `policy` (
  `id` int(11) NOT NULL,
  `add_policy` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `policy`
--

INSERT INTO `policy` (`id`, `add_policy`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(1, '', '2021-06-22 19:32:42', '32', NULL, NULL),
(2, 'azeem', '2021-06-22 19:32:42', '32', NULL, NULL),
(3, 'azeem tariq', '2021-06-22 19:32:42', '32', NULL, NULL),
(4, 'azeem', '2021-06-22 19:32:42', '32', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `cat_id` int(255) NOT NULL,
  `subCatID` int(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `prod_date` date DEFAULT NULL,
  `year` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `retail_price` int(11) NOT NULL DEFAULT 0,
  `product_code` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `long_description` text DEFAULT NULL,
  `banner` varchar(256) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT 0,
  `image1` varchar(1000) DEFAULT NULL,
  `image2` varchar(1000) DEFAULT NULL,
  `image3` varchar(1000) DEFAULT NULL,
  `image4` varchar(1000) DEFAULT NULL,
  `image5` varchar(1000) DEFAULT NULL,
  `image6` varchar(255) NOT NULL,
  `image7` varchar(255) NOT NULL,
  `image8` varchar(255) NOT NULL,
  `image9` varchar(255) NOT NULL,
  `image10` varchar(255) NOT NULL,
  `des_status` int(11) NOT NULL DEFAULT 0,
  `created_by` varchar(100) NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `code`, `cat_id`, `subCatID`, `product_name`, `prod_date`, `year`, `description`, `price`, `retail_price`, `product_code`, `image`, `long_description`, `banner`, `is_active`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`, `image7`, `image8`, `image9`, `image10`, `des_status`, `created_by`, `modified_date`, `modified_by`, `created_date`) VALUES
(569, '01021442', 83, 177, 'Sania Mirza', '2021-06-07', '2021', 'IjxiPjxpPjx1PnRoaXMgaXMgbWFnbmlmaWNlbnQgcHJvZHVjdDxcL3U_PLUS_PFwvaT48XC9iPiI_EQUALS_', '250', 200, '', '', NULL, NULL, 1, 'IklId29lci5qcGcxNjIzMDg0MjIyIg_EQUALS__EQUALS_.jpg', '', '', '', '', '', '', '', '', '', 1, '32', '2021-06-07 16:43:42', '32', '2021-06-07 15:29:16'),
(570, '01021443', 83, 177, 'Sania Mirza', '2021-06-07', '2021', 'IjxiPjxpPjx1PnRoaXMgaXMgbWFnbmlmaWNlbnQgcHJvZHVjdDxcL3U_PLUS_PFwvaT48XC9iPiI_EQUALS_', '250', 200, '', '', NULL, NULL, 1, 'IklId29lci5qcGcxNjIzMDg0MjIyIg_EQUALS__EQUALS_.jpg', '', '', '', '', '', '', '', '', '', 1, '32', '2021-06-07 16:43:42', '32', '2021-06-07 15:29:16'),
(571, '01021444', 83, 177, 'Sania Mirza\'s', '2021-06-27', '2021', 'IjxiPjxpPjx1PnRoaXMgaXMgbWFnbmlmaWNlbnQgcHJvZHVjdDxcL3U_PLUS_PFwvaT48XC9iPiI_EQUALS_', '250', 200, '123', '', '', 'a1624809466.png', 1, 'IklId29lci5qcGcxNjIzMDkxNjkwIg_EQUALS__EQUALS_.jpg', '', '', '', '', '', '', '', '', '', 1, '32', '2021-06-27 15:57:46', '32', '2021-06-07 15:29:16'),
(572, '01021445', 83, 177, 'aaaaaaaaaaa', '2021-06-15', '2021', 'ImhoaGQi', '100', 200, '', '', NULL, NULL, 0, 'IklId29lci5qcGcxNjIzNzc2ODI5Ig_EQUALS__EQUALS_.jpg', '', '', '', '', '', '', '', '', '', 1, '32', '2021-06-15 18:35:07', '32', '2021-06-15 17:07:09');

--
-- Triggers `product`
--
DELIMITER $$
CREATE TRIGGER `product` BEFORE INSERT ON `product` FOR EACH ROW BEGIN
    UPDATE 
     `sequence` 
    SET
     `executed_record` = @tempVariable := executed_record + 1 
    WHERE table_name = 'product' 
     AND tbl_year = NEW.year;
        IF (ROW_COUNT() < 1) THEN 
         INSERT INTO sequence SET table_name = 'product',tbl_year = NEW.year,executed_record = 1;
         SET @tempVariable = 1;
    END IF;
     SET NEW.code = CONCAT('010',DATE_FORMAT(NEW.prod_date,'%y'),LPAD((@tempVariable),3, '0'));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`, `description`, `is_active`) VALUES
(1, 'Admin User ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sequence`
--

CREATE TABLE `sequence` (
  `id` int(11) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `executed_record` int(11) NOT NULL,
  `tbl_year` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sequence`
--

INSERT INTO `sequence` (`id`, `table_name`, `executed_record`, `tbl_year`) VALUES
(1, 'product', 445, '2021'),
(1, 'orders', 13, '2021');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL DEFAULT 1,
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` varchar(100) NOT NULL,
  `modified_date` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `subCategoryName` varchar(255) DEFAULT NULL,
  `page_url` varchar(256) DEFAULT NULL,
  `cat_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `banner` varchar(500) DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` int(100) NOT NULL,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `subCategoryName`, `page_url`, `cat_id`, `description`, `is_active`, `banner`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(177, 'High Perfume', NULL, 83, 'High Perfume', 1, NULL, '32', '2021-06-07 15:26:17', 32, '2021-06-15 16:59:40'),
(178, 'azeem tester', NULL, 0, 'azeem tester', 1, NULL, '32', '2021-06-15 16:59:13', 0, NULL),
(179, 'Digital', 'abc', 0, 'Digital', 1, 'a1624812291.jpg', '32', '2021-06-27 16:40:06', 32, '2021-06-27 16:44:51'),
(180, 'My  Banners', 'mypp', 0, 'My  Banners', 1, 'a1624812621.jpg', '32', '2021-06-27 16:50:21', 32, '2021-06-27 16:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL,
  `ques` varchar(500) NOT NULL,
  `answer` varchar(500) NOT NULL,
  `category` varchar(100) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(100) NOT NULL,
  `modified_date` varchar(100) NOT NULL,
  `modified_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`id`, `ques`, `answer`, `category`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(1, 'Dolor cupiditate lab', 'Minus doloribus volu', '1', '2021-07-01 16:48:43', '32', '', ''),
(2, 'Dolor cupiditate lab', 'Minus doloribus volu', '2', '2021-07-01 16:48:43', '32', '', ''),
(3, 'Dolor cupiditate lab', 'Minus doloribus volu', '1', '2021-07-01 16:48:43', '32', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `terms_condition`
--

CREATE TABLE `terms_condition` (
  `id` int(11) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_url` varchar(255) NOT NULL,
  `page` varchar(255) DEFAULT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT 1,
  `created_date` datetime NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `modified_date` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms_condition`
--

INSERT INTO `terms_condition` (`id`, `content`, `page_title`, `page_url`, `page`, `is_active`, `created_date`, `created_by`, `modified_date`, `modified_by`) VALUES
(1, '[\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repu', 'Itaque dolor accusam', 'terms', 'terms', 1, '0000-00-00 00:00:00', '1', '2021-07-08 21:21:29', '32'),
(2, '[\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \"]', 'Soluta numquam in vo', 'untquivelabsed', 'policy', 1, '2021-06-28 00:00:00', '1', '2021-07-08 21:20:53', '32'),
(3, '[\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \"]', 'Refund Policy', 'Refund  Policy', 'refund_policy', 1, '2021-06-28 00:00:00', '1', '2021-07-08 21:22:07', '32'),
(4, '[\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical Conveniently repurpose web-enabled supply chains after technical \",\"Conveniently repurpose web-enabled supply chains after technical Conveniently repu', 'Aperiam pariatur Vo', 'Sunt qui vel ab sed ', 'prod_ship_policy', 1, '2021-06-28 00:00:00', '1', '2021-07-08 21:20:33', '32');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test2`
--

CREATE TABLE `test2` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `roleId` int(11) NOT NULL,
  `shop` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`OrderDetailId`);

--
-- Indexes for table `parameter`
--
ALTER TABLE `parameter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_condition`
--
ALTER TABLE `terms_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test2`
--
ALTER TABLE `test2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `OrderDetailId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `parameter`
--
ALTER TABLE `parameter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `policy`
--
ALTER TABLE `policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=573;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `terms_condition`
--
ALTER TABLE `terms_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test2`
--
ALTER TABLE `test2`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
