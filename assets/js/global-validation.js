
if(fields!='nojson'){


$("form").on('submit', function(e){

    var datastring = $(this).serializeArray();

    //$(".has-worn").text('');
    //$(".has-worn").html('');

        $.each( datastring, function( key, value ) {

            var fieldName = value.name;
            var fieldValue = value.value;

            var patt  = '';
            var label  = '';
            var patternmessage = 'Pattern Mismatch';
            var minlength = '';
            var maxlength = '';
            var AppendToFieldName = '';

            if(fields.hasOwnProperty(fieldName)){

                patt  = '';
                label  = '';
                patternmessage = 'Pattern Mismatch';
                minlength = fields[fieldName].minlength;
                maxlength = fields[fieldName].maxlength;

                if(fields[fieldName].hasOwnProperty('label')){
                    label = fields[fieldName].label;
                }
                if(fields[fieldName].hasOwnProperty('pattern')){
                    patt = fields[fieldName].pattern;
                }
                if(fields[fieldName].hasOwnProperty('patternmessage')){
                    patternmessage = fields[fieldName].patternmessage;
                }

                var regx = new RegExp(patt);
                if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {

                    if($( "[name='"+fieldName+"']").parent('div').next('.has-worn').text().length === 0){
                        $( "[name='"+fieldName+"']" ).parent('div').after('<div class="has-worn">'+patternmessage+'</div>');
                    }
                    e.preventDefault();
                }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){

                    if($( "[name='"+fieldName+"']").parent('div').next('.has-worn').text().length === 0) {
                        $("[name='" + fieldName + "']").parent('div').after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
                    }
                    e.preventDefault();
                }

            }else if(fieldNameLikeExistInJson2(fieldName,fields,'nameGroup_')){

                AppendToFieldName = fieldName;
                fieldName = fieldNameLikeExistInJson2(fieldName,fields,'nameGroup_');

                patt  = '';
                label  = '';
                patternmessage = 'Pattern Mismatch';
                minlength = fields[fieldName].minlength;
                maxlength = fields[fieldName].maxlength;

                if(fields[fieldName].hasOwnProperty('label')){
                    label = fields[fieldName].label;
                }
                if(fields[fieldName].hasOwnProperty('pattern')){
                    patt = fields[fieldName].pattern;
                }
                if(fields[fieldName].hasOwnProperty('patternmessage')){
                    patternmessage = fields[fieldName].patternmessage;
                }


                var regx = new RegExp(patt);
                if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {

                    if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
                        $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + patternmessage + '</div>');
                    }

                    e.preventDefault();
                    ReturnErr = true;

                }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){

                    if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
                        $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
                    }
                    e.preventDefault();
                    ReturnErr = true;
                }
            }

            // else if(fieldNameLikeExistInJson2(fieldName,fields,'sup_')){
            //
            //     AppendToFieldName = fieldName;
            //     fieldName = fieldNameLikeExistInJson2(fieldName,fields,'sup_');
            //
            //     patt  = '';
            //     label  = '';
            //     patternmessage = 'Pattern Mismatch';
            //     minlength = fields[fieldName].minlength;
            //     maxlength = fields[fieldName].maxlength;
            //
            //     if(fields[fieldName].hasOwnProperty('label')){
            //         label = fields[fieldName].label;
            //     }
            //     if(fields[fieldName].hasOwnProperty('pattern')){
            //         patt = fields[fieldName].pattern;
            //     }
            //     if(fields[fieldName].hasOwnProperty('patternmessage')){
            //         patternmessage = fields[fieldName].patternmessage;
            //     }
            //
            //     var regx = new RegExp(patt);
            //     if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {
            //
            //         if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
            //             $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + patternmessage + '</div>');
            //         }
            //
            //         e.preventDefault();
            //         ReturnErr = true;
            //
            //     }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){
            //
            //         if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
            //             $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
            //         }
            //         e.preventDefault();
            //         ReturnErr = true;
            //     }
            // }
        });



});


function globalValidate(e,thiss){

    var ReturnErr = false;
    var datastring = thiss.serializeArray();

        $.each( datastring, function( key, value ) {

            var fieldName = value.name;
            var fieldValue = value.value;

            var patt  = '';
            var label  = '';
            var patternmessage = 'Pattern Mismatch';
            var minlength = '';
            var maxlength = '';
            var regx = '';
            var AppendToFieldName = '';

            if(fields.hasOwnProperty(fieldName)){

                minlength = fields[fieldName].minlength;
                maxlength = fields[fieldName].maxlength;

                if(fields[fieldName].hasOwnProperty('label')){
                    label = fields[fieldName].label;
                }
                if(fields[fieldName].hasOwnProperty('pattern')){
                    patt = fields[fieldName].pattern;
                }
                if(fields[fieldName].hasOwnProperty('patternmessage')){
                    patternmessage = fields[fieldName].patternmessage;
                }

                regx = new RegExp(patt);
                if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {

                    if($( "[name='"+fieldName+"']").parent('div').next('.has-worn').text().length === 0) {
                        $("[name='" + fieldName + "']").parent('div').after('<div class="has-worn">' + patternmessage + '</div>');
                    }
                    e.preventDefault();
                    ReturnErr = true;

                }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){

                    if($( "[name='"+fieldName+"']").parent('div').next('.has-worn').text().length === 0) {
                        $("[name='" + fieldName + "']").parent('div').after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
                    }
                    e.preventDefault();
                    ReturnErr = true;
                }
            }else if(fieldNameLikeExistInJson2(fieldName,fields,'nameGroup_')){

                AppendToFieldName = fieldName;
                fieldName = fieldNameLikeExistInJson2(fieldName,fields,'nameGroup_');

                minlength = fields[fieldName].minlength;
                maxlength = fields[fieldName].maxlength;

                if(fields[fieldName].hasOwnProperty('label')){
                    label = fields[fieldName].label;
                }
                if(fields[fieldName].hasOwnProperty('pattern')){
                    patt = fields[fieldName].pattern;
                }
                if(fields[fieldName].hasOwnProperty('patternmessage')){
                    patternmessage = fields[fieldName].patternmessage;
                }


                regx = new RegExp(patt);
                if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {

                    if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
                        $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + patternmessage + '</div>');
                    }

                    e.preventDefault();
                    ReturnErr = true;

                }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){

                    if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
                        $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
                    }
                    e.preventDefault();
                    ReturnErr = true;
                }
            }

            // else if(fieldNameLikeExistInJson2(fieldName,fields,'sup_')){
            //
            //     AppendToFieldName = fieldName;
            //     fieldName = fieldNameLikeExistInJson2(fieldName,fields,'sup_');
            //
            //     minlength = fields[fieldName].minlength;
            //     maxlength = fields[fieldName].maxlength;
            //
            //     if(fields[fieldName].hasOwnProperty('label')){
            //         label = fields[fieldName].label;
            //     }
            //     if(fields[fieldName].hasOwnProperty('pattern')){
            //         patt = fields[fieldName].pattern;
            //     }
            //     if(fields[fieldName].hasOwnProperty('patternmessage')){
            //         patternmessage = fields[fieldName].patternmessage;
            //     }
            //
            //     regx = new RegExp(patt);
            //     if (!regx.test(fieldValue) && fieldValue.length>0 && patt!=='') {
            //
            //         if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
            //             $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + patternmessage + '</div>');
            //         }
            //
            //         e.preventDefault();
            //         ReturnErr = true;
            //
            //     }else if((!(fieldValue.length>=minlength && fieldValue.length<=maxlength  )) && fieldValue.length>0){
            //
            //         if($( "[name='"+AppendToFieldName+"']").next('.has-worn').text().length === 0) {
            //             $("[name='" + AppendToFieldName + "']").after('<div class="has-worn">' + label + ' must be in between ' + minlength + ' to ' + maxlength + ' characters in length</div>');
            //         }
            //         e.preventDefault();
            //         ReturnErr = true;
            //     }
            // }
        });



    return ReturnErr;
}



    function fieldNameLikeExistInJson2(fieldName,fields,prefix){

        var f = fieldName.substr(fieldName.indexOf(prefix)+prefix.length);

        if(typeof f!=='undefined'){
            f = f.substring(1, f.length);//[qty1]
            f = prefix.concat(f);//nameGroup_[qty1]
            if(fields.hasOwnProperty(f)){
                return f;
            }
        }
        return false;
    }


}//endif