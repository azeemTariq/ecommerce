<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct(){
        log_message('info', 'Model Class Initialized');
	}
	public function LSQ($log =true){
	    if(!$log){
	         $this->db->last_query();
	    }else{
	        logd('query model ', $this->db->last_query());
	    }
	}
	public function inventory_validation($item , $location=null, $date=null,$analysis=null,$round='SUM(view_trans_in_out.quantity)'){
        if (empty($date)) {
        $date = date('Y-m-d ',strtotime('now'));
        }
        $sub_inventoryPOS=$sub_inventoryAllWHR='';
        $quantitySum= $round.' Qty_2' ;
        if($this->uri->segment(1)=='Purchase_RequisitionMaster'){
            $sub_inventoryPOS='  locators.si_code= 000014 AND locators.`code` LIKE "01%" ';
            $sub_inventoryAllWHR=' locators.si_code= 000015 AND locators.`code` NOT LIKE  "01%" ';
            $quantitySum= ' SUM(if('.$sub_inventoryPOS.',view_trans_in_out.quantity,0)) pos_shop,
            SUM(if('.$sub_inventoryAllWHR.',view_trans_in_out.quantity,0)) all_whr,
            SUM(if('.$sub_inventoryAllWHR.',view_trans_in_out.containers,0)) all_whr_1' ;
        }
        $anWhere = [];
        $locationclous="";
        if(!empty($location)){
        $locationclous="((view_trans_in_out.location_code = '".$location ."')) AND";
        }
		$this->db->select($quantitySum.",(si_items.itm_itemdesc) itm_itemdesc,(si_items.itm_itemcode) itm_itemcode,(locators.title) Location_title ,(locators.code) Location_code ");
		$this->db->from("si_items,view_trans_in_out,locators");
		$this->db->where("
			 		((view_trans_in_out.bg_code = ".$this->session->userdata('bg_code').")) AND
					((view_trans_in_out.le_code = ".$this->session->userdata('le_code').")) AND
					((view_trans_in_out.ou_code = ".$this->session->userdata('ou_code').")) AND
					( view_trans_in_out.item_code = si_items.`itm_itemcode`) AND
                    ( view_trans_in_out.`Location_Code` = locators.`code`)AND
					( view_trans_in_out.`bg_code` = locators.`bg_code`)AND
					((view_trans_in_out.item_code = '".$item ."'))AND
					".$locationclous."				
					 si_items.itm_itemcode = view_trans_in_out.item_code and si_items.bg_code = view_trans_in_out.bg_code ");
        if($date !=null){
            $this->db->where("trans_date <= '$date'");
        }
        if($analysis!=null){
            $anGroup ='';
            if(isset($analysis['invd_cc_id_1']) && !empty($analysis['invd_cc_id_1'])){
                $anWhere['view_trans_in_out.invd_cc_id_1'] = $analysis['invd_cc_id_1'];
                $anGroup.=',invd_cc_id_1';
            }
            if(isset($analysis['invd_cc_id_2']) && !empty($analysis['invd_cc_id_2'])){
                $anWhere['view_trans_in_out.invd_cc_id_2'] = $analysis['invd_cc_id_2'];
                $anGroup.=',invd_cc_id_2';
            }
            if(isset($analysis['invd_cc_id_3']) && !empty($analysis['invd_cc_id_3'])){
                $anWhere['view_trans_in_out.invd_cc_id_3'] = $analysis['invd_cc_id_3'];
                $anGroup.=',invd_cc_id_3';
            }
            if(isset($analysis['invd_cc_id_4']) && !empty($analysis['invd_cc_id_4'])){
                $anWhere['view_trans_in_out.invd_cc_id_4'] = $analysis['invd_cc_id_4'];
                $anGroup.=',invd_cc_id_4';
            }
            if(isset($analysis['invd_cc_id_5']) && !empty($analysis['invd_cc_id_5'])){
                $anWhere['view_trans_in_out.invd_cc_id_5'] = $analysis['invd_cc_id_5'];
                $anGroup.=',invd_cc_id_5';
            }
            if(count($anWhere) > 0){
            $this->db->where($anWhere);
            }
            $this->db->group_by("view_trans_in_out.item_code, view_trans_in_out.location_code".$anGroup);
        }
        if(!empty($location) && $analysis==null){
        $this->db->group_by("view_trans_in_out.item_code,view_trans_in_out.location_code");
        }
		
		 return  $this->db->get()->result_array();
		
	}
	public function GetMaskParameter($mask, $status = null) {
		$data = $this->db->get_where ( 'si_parameter', array (
				'par_code' => $mask,
				'is_active' => $status ,
		        'bg_code'  => $this->session->userdata('bg_code')
		) )->result_array ();
		if ($data) {
			return $data [0] ['par_data'];
		} else {
			return false;
		}
	}
	public function EvaluationRete($item, $loc, $doctyp_code, $docs_doccode, $invm_date, $invm_no=null) {
		$SaiPar = $this->GetMaskParameter ( '0000000445', 1 );
		$SrnPar = $this->GetMaskParameter ( '0000000447', 1 );
		$loc = '';
		if(true == false){
		    $loc  ="( view_trans_in_out.location_code  = '" . $loc . "') AND";
		}
		$docCond = "";
		// if($invm_no != null ){
		//     $docCond  = "AND NOT (view_trans_in_out.doc_code = '".$docs_doccode."' AND view_trans_in_out.doc_no =  '".$invm_no."' AND view_trans_in_out.doc_type_code =  '".$doctyp_code."')"; 
		// }
		$this->db->select ( "
		  view_trans_in_out.bg_code,   
         view_trans_in_out.le_code,   
         view_trans_in_out.ou_code,   
         view_trans_in_out.c_year,   
         view_trans_in_out.c_period,   
         view_trans_in_out.doc_code,   
         view_trans_in_out.doc_type_code,   
         view_trans_in_out.doc_no,   
         view_trans_in_out.trans_date,   
         view_trans_in_out.location_code,   
		 view_trans_in_out.item_code,
         view_trans_in_out.containers,   
         view_trans_in_out.c_size,
		 view_trans_in_out.trans_type,   
        (CASE view_trans_in_out.doc_code WHEN 'D.C' THEN view_trans_in_out.cost WHEN 'SRN' THEN view_trans_in_out.cost WHEN 'CRN' THEN view_trans_in_out.cost ELSE view_trans_in_out.rate END) rate,   
         view_trans_in_out.quantity,   
         view_trans_in_out.storingunit,   
         view_trans_in_out.created_when,
		(view_trans_in_out.quantity * 0) inv_act_amount" );
		$this->db->from ( "view_trans_in_out" );
		$this->db->where ( "( view_trans_in_out.trans_date <= '" . date ( 'Y-m-d ', strtotime ( 'now' ) ) . "' ) AND  
        ( view_trans_in_out.item_code = '" . $item . "' ) AND
( view_trans_in_out.doc_code !=  'POS' ) AND
		$loc
		( view_trans_in_out.trans_type = 'O' ) AND
        ( view_trans_in_out.quantity < 0 ) AND NOT
         ( view_trans_in_out.doc_code ='GRN'  AND view_trans_in_out.rate = 0) AND
		( view_trans_in_out.bg_code = '" . $this->session->userdata ( 'bg_code' ) . "' ) AND  
        ( view_trans_in_out.le_code = '" . $this->session->userdata ( 'le_code' ) . "' ) AND  
        ( view_trans_in_out.ou_code = '" . $this->session->userdata ( 'ou_code' ) . "' ) $docCond");
		$this->db->get ();
		$query1 = $this->db->last_query ();
		
		$this->db->select ( "  view_trans_in_out.bg_code,   
         view_trans_in_out.le_code,   
         view_trans_in_out.ou_code,   
         view_trans_in_out.c_year,   
         view_trans_in_out.c_period,   
         view_trans_in_out.doc_code,   
         view_trans_in_out.doc_type_code,   
         view_trans_in_out.doc_no,   
         view_trans_in_out.trans_date,   
         view_trans_in_out.location_code,   
	 	 view_trans_in_out.item_code,
         view_trans_in_out.containers,   
         view_trans_in_out.c_size,  
		 view_trans_in_out.trans_type, 
         (CASE view_trans_in_out.doc_code WHEN 'D.C' THEN view_trans_in_out.cost WHEN 'SRN' THEN view_trans_in_out.cost ELSE view_trans_in_out.rate END) rate ,
         view_trans_in_out.quantity,   
         view_trans_in_out.storingunit,   
         view_trans_in_out.created_when,
		(CASE view_trans_in_out.doc_code WHEN 'IOB' THEN view_trans_in_out.quantity * view_trans_in_out.rate ELSE view_trans_in_out.quantity * 0 END) inv_act_amount" );
		
		$this->db->from ( "view_trans_in_out" );
		$this->db->where ( "
	     ((view_trans_in_out.trans_date <= '" . date ( 'Y-m-d ', strtotime ( 'now' ) ) . "' AND
	   	 (view_trans_in_out.doc_code <> 'SAI' OR  $SaiPar = 0) ) OR 
		 (view_trans_in_out.trans_date < '" . date ( 'Y-m-d ', strtotime ( 'now' ) ) . "' AND 
		  view_trans_in_out.doc_code = 'SAI' AND  $SaiPar = 1)) AND  
         ( view_trans_in_out.trans_date <= '" . date ( 'Y-m-d ', strtotime ( 'now' ) ) . "' ) AND  
         ( view_trans_in_out.item_code = '" . $item . "' )  AND
( view_trans_in_out.doc_code !=  'POS' ) AND
         $loc
         ( view_trans_in_out.trans_type = 'I' ) AND
         ( view_trans_in_out.quantity > 0 ) AND NOT
		 ( view_trans_in_out.doc_code ='GRN'  AND view_trans_in_out.rate = 0) AND
		 ( view_trans_in_out.bg_code = '" . $this->session->userdata ( 'bg_code' ) . "' ) AND  
		 ( view_trans_in_out.le_code = '" . $this->session->userdata ( 'le_code' ) . "' ) AND  
		 ( view_trans_in_out.ou_code = '" . $this->session->userdata ( 'ou_code' ) . "' ) AND
		 ($SaiPar =0 OR ($SaiPar = 1   AND view_trans_in_out.doc_code <> 'SAI')) AND
		 ($SrnPar =$SrnPar OR (0 = 1   AND view_trans_in_out.doc_code <> 'SRN')) $docCond ");
		$this->db->get();
		$query2 = $this->db->last_query ();
		$query = $this->db->query ( $query1 . " UNION ALL " . $query2 );
	//	error_log ( 're 2 ' . print_r ( $this->db->last_query (), true ) );
		return $query->result_array();
	
	}
    public function _get_max_global($parameter,$table, $bg_code){
            
            
            $query = $this->db->query("SELECT MAX(`$parameter`) as '$parameter' FROM `$table` WHERE `bg_code` = '$bg_code' ");
//          $this->db->select_max($parameter);          
//          $this->db->like('bg_code',$bg_code);
//          $this->db->from($table);
            $result =  $query->result();
        
            return $result[0]->$parameter;
            
        }
    public function debug_r($_array,$exit){
        echo '<pre>';print_r($_array);
        echo '</pre>';
        if($exit > 0){
            exit;
        }
    }
    public function product_attr_title_code(){
        return $this->db_select('pat_title,patt_code,patt_attribute','product_attributes',array('is_active'=>1));
    }
    public function _get_max_result($columns, $table, $where){
        $this->db->select_max($columns);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get();
         
        if($query->result()){
            return   $query->result()[0]->$columns+1;
        }else{
            return 1;
        }
    }
    public function db_command($data, $id ,$table){
        $now = date('Y-m-d H:i:s');
        
        if($id != null){
            $data['modified_date']  = $now;
            $data['modified_by']    = $this->session->userdata('id');
        }else{
            
            $data['created_date'] = $now;
            $data['created_by']   = $this->session->userdata('id');
        }
        
        // Insert
        if (empty($id)){
        	$this->db->trans_start();
           $save = $this->db->insert($table,$data); 
           if ($this->db->trans_status() === FALSE){
           	$this->db->trans_rollback();
           }else{
           $id = $this->db->insert_id();         
            $this->db->trans_complete();
           }
              
                 
        }else { 
        	$this->db->trans_start();        	
        	$this->db->where('id', $id);
        	$this->db->update($table, $data);         
      		$this->db->trans_complete();        		
    
        }
        return $id;  
    }
    public function db_delete($id,$table){
        $this->db->where($this->_primary_code, $id);
        $delete =  $this->db->delete($table);
        if($delete){
            return 1;
        }else{
            return 0;
        }
    }

    public function db_insert_last_id($table, $data){
        $this->db->trans_start();
            $this->db->insert($table, $data);
            $insert  = $this->db->insert_id();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
            if($insert){
                return $insert;
            }else{
                return 0;
            }
        }
    }   
    
    public function db_insert($table, $data){
        $this->db->trans_start();
        $insert = $this->db->insert($table, $data);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
            if($insert){
                return 1;
            }else{
                return 0;
            }
        }
    }
    public function db_update($where, $table, $data){
        $this->db->trans_start();
        $this->db->where($where);
        $update =   $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
            if($update){
                return 1;
            }else{
                return 0;
            }
        }
    }
    public function db_select($columns,$table,$where = null,$or_where = null, $wherein=null, $orderby=null, $groupby=null,$having=null,$limit=null){
        $this->db->select($columns);
        $this->db->from($table);
        if($where!=null){
            $this->db->where($where);
        }
        if($or_where!=null){
            $this->db->or_where($or_where);
        }
        if($wherein!=null){
            $this->db->where_in($wherein);
        }
        if($orderby!=null){
            $this->db->order_by($orderby);
        }
        if($groupby!=null){
            $this->db->group_by($groupby);
        }
        if($having!=null){
            $this->db->having($having);
        }
        if($limit!=null){
            $this->db->limit($limit);
        }
        $query = $this->db->get()->result();
        return  $query;        
    }


 public function db_select_single_row($columns,$table,$where = null,$or_where = null, $wherein=null, $orderby=null, $groupby=null){
    
        $this->db->select($columns);
        $this->db->from($table);
        if($where!=null){
            $this->db->where($where);
        }
        if($or_where!=null){
            $this->db->or_where($or_where);
        }
        if($wherein!=null){
            $this->db->where_in($wherein);
        }
        if($orderby!=null){
            $this->db->order_by($orderby);
        }
        if($groupby!=null){
            $this->db->group_by($groupby);
        }
        $query = $this->db->get()->row_array();
        return $query;        
    }



    public function db_select_array($columns,$table,$where = null,$or_where = null, $wherein=null, $orderby=null, $groupby=null,$having=null,$limit=null){
        $this->db->select($columns);
        $this->db->from($table);
        if($where!=null){
            $this->db->where($where);
        }
        if($or_where!=null){
            $this->db->or_where($or_where);
        }
        if($wherein!=null){
            $this->db->where_in($wherein);
        }
        if($orderby!=null){
            $this->db->order_by($orderby);
        }
        if($groupby!=null){
            $this->db->group_by($groupby);
        }
        if($having!=null){
            $this->db->having($having);
        }
        if($limit!=null){
            $this->db->limit($limit);
        }
        $query = $this->db->get()->result_array();
        return $query;        
    }

    public function db_single_join($column, $table, $tbl_join, $condition, $joinType=null, $where=null,$orderBy=null){
        $this->db->select($column);
        $this->db->from($table);
        $this->db->join($tbl_join,$condition,$joinType);
        if($where!=null){
            $this->db->where($where);
        }
        $this->db->order_by($orderBy);
        $query = $this->db->get()->result();
        return $query;
    }
    public function db_get_multiple_join($columns, $from, $tbl_first_join, $first_join, $tbl_sec_join, $sec_join, $where, $groupby = null){
        $this->db->select($columns);
        $this->db->from($from);
        $this->db->join($tbl_first_join,$first_join);
        $this->db->join($tbl_sec_join,$sec_join);
        $this->db->where($where);
        if($groupby != null){
            $this->db->group_by($groupby);
        }
        $query = $this->db->get()->result();
        return $query;
    }
    public function db_delete_row($where, $table, $data){
        $this->db->where($where);
        $delete = $this->db->update($table, $data);
        if($delete){
            return 1;
        }else{
            return 0;
        }
    }
    public function db_change_status($where,$table,$data){
        $this->db->trans_start();
        $this->db->where($where);
        $update =   $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
            if($update){
                return 1;
            }else{
                return 0;
            }
        }
    }
    public function _get_code($columns, $table, $where = null, $array){
        $this->db->select($columns);
        if($where != null){
            $this->db->where($where);
        }
        $result =  $this->db->get($table)->result();
        if(count($result)){
            foreach($result as $res){
                $array[$res->code] = $res->title;
            }
        }
        return $array; 
    }
    
    public function _get_tbl_code($columns, $table, $array,$orderby=null){
        $this->db->select($columns);
        if($array){
        $this->db->where($array);
        }
        if($orderby!=null){
            $this->db->order_by($orderby);
        }
        $result =  $this->db->get($table)->result();
        return $result; 
    }
    
    public function _get_code_list($columns, $table, $where = null){

    	$this->db->select($columns);
        if($where !=null) {
    	   $this->db->where($where);
        }
    	return  $this->db->get($table)->result_array();
    	
    }
    public function _get_code_byparent($columns, $table ,$code ,$paramname){
        
    	$this->db->select($columns);
//     	$this->db->where('is_active',1);
    	$this->db->where($paramname,$code);
    	return  $this->db->get($table)->result_array();
    	
    }
    
    public function _get_code_array($values, $array){
        $select = '';
        // get column values and add comma sepereted values
        foreach($values['column'] as $colum_key => $column){
            $select.= $column.',';
        }
        // remove last comma sepperated value from array
        $this->db->select(rtrim($select,','));
        if(!$values['where']==null){
            $this->db->where($values['where']);
        }
        // fetch as array
        $result =  $this->db->get($values['table'])->result_array();
        // return if value is greater than 0
        if(count($result)){
            foreach($result as $res_key => $value){
                // get Columns from Controller
                $code  = $values['code'];
                $title = $values['title'];
                // code array
                $code  = $value[$code];
                // title array
                $title = $value[$title];
                
                $array[$code] = $title;
            }
        }
        return $array; 
    }
    // row Count
    public function RowCount($table, $where){
        $this->db->group_start();
        $this->db->where($where);
        $this->db->group_end();
        $results= $this->db->count_all_results($table);
        return $results;
    }
    // user log
    public function checkinRecord($data){
    	$this->db->where($data);
    	$result =  $this->db->count_all_results('user_operation_log');
    	if($result>0){
    		return 0;
    	}else{
    		return 1;
    	}
    }
    
    public function _get_max($parameter, $table, $where = null){
    	$this->db->select_max($parameter);
    	$this->db->from($table);
        if($where !=null){
            $this->db->where($where);
        }
    	$query=$this->db->get();
    	$result =  $query->result_array();
    	return $result[0][$parameter];
    	
    }
    public function _get_max_plus($parameter,$table,$length,$where=null){
        $this->db->select_max($parameter);
        $this->db->from($table);
        if(!empty($where)){
            $this->db->Where($where);
        }
        $result=$this->db->get()->row();
        
        if(isset($result->$parameter) && $result->$parameter >0 ){
        return str_pad ( $result->$parameter+1, $length, '0', STR_PAD_LEFT );
        }else{
            return str_pad ( 1, $length, '0', STR_PAD_LEFT );
        }
        
    }
    public function _get_max_child($parameter,$length){
       return str_pad ( $parameter, $length, '0', STR_PAD_LEFT );
    }
    public function _get_city_list(){
    	 $this->db->select('*');    	
    	 $this->db->from('city_list');
    	 $this->db->where('country_id',$this->session->userdata('country_id'));
    	$query = $this->db->get()->result();
    	return $query;   
    }
    public function GetSalesPoint(){
    	$this->db->select(array('pos_desc','pos_code'));
    	$this->db->where(array(
            'pos_activetag' =>1, 
            'bg_code'       => $this->session->bg_code,
            'le_code'       => $this->session->le_code,
            'ou_code'       => $this->session->ou_code

        ));
    	return $this->db->get('si_point_of_sales')->result();
    }



     public function get_relation_All($table,$column,$input,$key){
        $this->db->select($column);
        return $this->db->where(array($key=>$input,'is_active'=>1))->get($table)->result_array();
        
        
    }
    
     public function GateById($keys,$tbl,$key){
    	$this->db->select($keys);
    	$this->db->where($key);
    	return $this->db->get($tbl)->result_array();
    }

    public function product_stock($itm_itemcode,$locators_code){
    $stock  =  $this->Mdl_prequisitionmaster->db_select('SUM(quantity) as stock','view_trans_in_out', array('item_code'=>$itm_itemcode,'location_code'=>$locators_code));
    return $stock[0]->stock;
    }
    public function latest_transaction($itm_itemcode,$locators_code){
    $this->db->select("MAX(si_inventorymaster.invm_date) as date");
    $this->db->from("si_inventorymaster, si_inventorydetail,
            view_allowed_doctypes,view_allowed_locations");
    $this->db->where("
     view_allowed_doctypes.doctyp_code = si_inventorymaster.doctyp_code
    AND `view_allowed_doctypes`.`bg_code` = si_inventorymaster.bg_code
    AND `view_allowed_doctypes`.`docs_doccode` = si_inventorymaster.docs_doccode
    AND `view_allowed_doctypes`.`usr_id` = ".$this->session->userdata('id')." 
    AND si_inventorydetail.bg_code = si_inventorymaster.bg_code 
    AND si_inventorydetail.le_code = si_inventorymaster.le_code
    AND si_inventorydetail.ou_code = si_inventorymaster.ou_code
    AND si_inventorydetail.invm_year = si_inventorymaster.invm_year
    AND si_inventorydetail.invm_period = si_inventorymaster.invm_period
    AND si_inventorydetail.docs_doccode = si_inventorymaster.docs_doccode
    AND si_inventorydetail.doctyp_code = si_inventorymaster.doctyp_code
    AND si_inventorydetail.invm_no = si_inventorymaster.invm_no
    AND ( si_inventorydetail.docs_doccode = 'GRN' OR
        si_inventorydetail.docs_doccode = 'D.P' )
   AND si_inventorydetail.itm_itemcode='".$itm_itemcode."'
   AND view_allowed_locations.locator_code = si_inventorydetail.loc_code 
   AND view_allowed_locations.locator_bgcode = si_inventorydetail.bg_code
   AND view_allowed_locations.usr_id = ".$this->session->userdata('id')."
   GROUP BY si_inventorydetail.itm_itemcode");

    $query = $this->db->get()->result();
    $this->db->select("
        si_inventorydetail.invm_no,invm_date,invd_qty,invd_rate,
        si_supplier.sup_name,sup_phone1");
        $this->db->from("si_inventorydetail,si_inventorymaster,si_supplier,view_allowed_doctypes,view_allowed_locations");
        $this->db->where("
         view_allowed_doctypes.doctyp_code = si_inventorymaster.doctyp_code
        AND `view_allowed_doctypes`.`bg_code` = si_inventorymaster.bg_code
        AND `view_allowed_doctypes`.`docs_doccode` = si_inventorymaster.docs_doccode
        AND `view_allowed_doctypes`.`usr_id` = ".$this->session->userdata('id')." 
        AND ( si_inventorymaster.bg_code = si_inventorydetail.bg_code )
        AND ( si_inventorymaster.le_code = si_inventorydetail.le_code )
        AND ( si_inventorymaster.ou_code = si_inventorydetail.ou_code ) 
        AND  ( si_inventorymaster.invm_year = si_inventorydetail.invm_year ) 
        AND ( si_inventorymaster.invm_period = si_inventorydetail.invm_period ) 
        AND ( si_inventorymaster.docs_doccode = si_inventorydetail.docs_doccode ) 
        AND  ( si_inventorymaster.doctyp_code = si_inventorydetail.doctyp_code ) 
        AND ( si_inventorymaster.invm_no = si_inventorydetail.invm_no ) 
        AND  ( ( si_inventorydetail.docs_doccode = 'GRN' OR
             si_inventorydetail.docs_doccode = 'D.P' ) 
        AND ( si_inventorydetail.itm_itemcode = '".$itm_itemcode."' )
        AND ( si_inventorymaster.invm_date = '".$query[0]->date."')

        AND view_allowed_locations.locator_code = si_inventorydetail.loc_code 
        AND view_allowed_locations.locator_bgcode = si_inventorydetail.bg_code
        AND view_allowed_locations.usr_id = ".$this->session->userdata('id')." ) 
        AND si_inventorymaster.bg_code = si_supplier.bg_code
        AND si_inventorymaster.le_code = si_supplier.le_code
        AND si_inventorymaster.sup_code = si_supplier.sup_code 
        ORDER BY si_inventorymaster.invm_date DESC,
        si_inventorymaster.invm_createdwhen DESC");

        $query=$this->db->get()->result();
        return $query;
    }


    /// Update batch ///
    function Update_batch($table , $data , $where , $start = 1,$condition=null){
      //$data=$this->dateFormateUpdate($data);
        $return = 0;
        for ($i=$start; $i <= count($data); $i++) { 
            # code...
            if(!empty($data[$i])){
          $return =  $this->db->update($table, $data[$i],$where[$i]);
            }
        }
        return $return;
      
    }
    /// Update batch ///
    // Check Child Data against parent and restric deletion
    //Start
    public function RestricDelete($table , $parameter , $DeleteStatus =null, $blo = null){
        if($DeleteStatus!=""){
            $Where = array_merge($parameter,array($DeleteStatus => 1));
        }else{
            $Where = $parameter;
        }
        $Pkeys=[
            'bg_code'          => $this->session->userdata('bg_code'),
            'le_code'          => $this->session->userdata('le_code'),
            'ou_code'          => $this->session->userdata('ou_code'),
        ];
        $this->db->select("count(*)");
        $this->db->from($table);
        $this->db->Where((($blo == true) ? $Where : array_merge($Pkeys,$Where)));
        return $this->db->count_all_results();
    }
    //End
    //Document Alert
    public function insert_alert($docno,$doctype_code,$doccode,$date,$status,$master_child,$comment=null,$usr_id=[]){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $this->insert_alert_email($docno,$doctype_code,$doccode,$date,$status,$master_child,$comment,$usr_id) );
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
        curl_close($ch);
    }
    public function insert_alert_fundTransfer($array){
        $users   =  $this->db_select('id','login',array('bg_code'=>$this->session->userdata("bg_code"),'dvn_code'=>$array['dvn_code']));
        $message=$array['pageTitle']."  - ".$array['doc_no']." ";
        $message.=" 'Dated ".$array['date']."' Has Been Created";
        if(!empty($users) && count($users)){
            foreach ($users as $key => $value) {

               $this->db_insert('si_alert_log',
                    array(
                        'bg_code' => $this->session->userdata("bg_code"),
                        'usr_id'  => $value->id,
                        'al_message' => $message,
                        'al_status' =>  0,
                        'al_createdby' => $this->session->userdata("id"),
                        'al_createdwhen' => $array['date']
                        )
                );
            }
        }
    }

    /* document closing notification to user who create the document */
    public function insert_alert_closing($value,$where,$table_name,$closeDate,$dateKey){

        $doctyp_desc=$this->db_select('doctyp_desc','si_doctype',array('doctyp_code'=>$value['doctype']))[0]->doctyp_desc;
        $created_by = $this->db_select_single_row($dateKey,$table_name,$where)[$dateKey];
        $message = $doctyp_desc."  - ".$value['docno']." ";
        $message.=" 'Dated ".$closeDate."' Has Been Closed By ".$_SESSION['name'];
        $this->db_insert('si_alert_log',
            array(
                'bg_code' => $this->session->userdata("bg_code"),
                'usr_id'  => $created_by,
                'al_message' => $message,
                'al_status' =>  0,
                'al_createdby' => $this->session->userdata("id"),
                'al_createdwhen' => $closeDate
            )
        );
    }
    /* document closing notification to user who create the document */

    public function insert_alert_email($docno,$doctype_code,$doccode,$date,$status,$master_child,$comment,$usr_id){
        $alert_user= array_merge(array_filter($usr_id),$this->check_alert_user($doctype_code,$status));
        if(!empty($alert_user)){
            $alert_log_email= $this->alert_log($alert_user,$doccode,$doctype_code,$docno,$status,$date,$master_child,$comment);
        }
    }
    public function check_alert_user($doctyp_code,$status){
        $check_user=$this->db_select('*','si_alert_preferences',array
            ( 'doctyp_code' =>$doctyp_code,
               'ap_doc_action_type' =>$status,
               'bg_code'            =>$this->session->userdata('bg_code')
            ));
        return $check_user;
    }
    public function alert_log($alert_user,$doc_code,$doctype_code,$doc_no,$status,$date,$master_child,$comment){
        $username   =   $this->db_select('name','login',array('id'=>$this->session->userdata("id")))[0]->name;
        $doctyp_desc=$this->db_select('doctyp_desc','si_doctype',array('doctyp_code'=>$doctype_code))[0]->doctyp_desc;
        $currentDate=date('Y-m-d H:i:s');
        switch ($status) {
        case "INSERT":
            $msgStatus="Created";
            break;
        case "UPDATE":
            $msgStatus= "Modified";
            break;
        case "DELETE":
            $msgStatus= "Deleted";
            break;
        case "VERIFY":
            $msgStatus= "Verified";
            break;
        case "REJECT":
            $msgStatus= "Rejected";
            break;
        default:
           $msgStatus = "Un-Verified";
        }
        $lastID =   1;
        $message=$doctyp_desc." - ".$doc_no." ";
        $message.=" 'Dated ".$date."' ";
        $subject=$message.$msgStatus;
        if(!empty($master_child))
        { $message.=$master_child.' ';}
        $message.=" '".$msgStatus."' by '".$username."' on '";
        $message.=$currentDate."'";
        if($comment!=null){
            $message.=" Comment: ".$comment;
        }
        
//        $seqNo      =   $this->db_select('*','si_alert_log',array('bg_code'=>$this->session->userdata("bg_code")));
//        if(isset($seqNo)){
//            $res    =   count($seqNo);
//            $lastID =   $res+1;
//        }
//        $al_code     =   str_pad($lastID, 10, '0', STR_PAD_LEFT);

        $year_period = $this->db->select('cl_gl_year,cl_gl_period,cl_title')->where(['cl_startdate <='=>$date,'cl_enddate >='=>$date])->get('si_calendar')->row();
        foreach ($alert_user as $value){
            $insert = $this->db_insert('si_alert_log',
                array(
                    'bg_code' => $this->session->userdata("bg_code"),
                  //  'al_code' => $al_code,
                    'usr_id'  => $value->usr_id,
                    'al_subject' => $subject,
                    'al_message' => $message,
                    'al_status' =>  0,
                    'al_createdby' => $this->session->userdata("id"),
                    'al_createdwhen' => $currentDate,
                    'parent'        => encrypt(['controller' => $this->uri->segment(1),'bg_code' => $this->session->userdata("bg_code"),'le_code' => $this->session->userdata("le_code"),'ou_code' => $this->session->userdata("ou_code"),'msg' => $msgStatus,'year' => $year_period->cl_gl_year,'period' => $year_period->cl_gl_period,'docs_doccode' => $doc_code,'doctyp_code' => $doctype_code,'no' => $doc_no,'date' => $date,'extra' => $comment])
                    ));
        }
    }
    public function find_user_email($usr_id){
      return $this->db_select('email','login',array('bg_code'=>$this->session->userdata("bg_code"),'id'=>$usr_id))[0]->email ?? FALSE;
    } 
    public function send_mail($email,$subject,$message,$attachment=null)
    {
     $this->load->library('email');
     $config['protocol']     = 'smtp';
     $config['smtp_host']    = 'smtp.gmail.com';
     $config['smtp_port']    = '587';
     $config['smtp_crypto']  = 'tls';
     $config['smtp_user']    = 'erp.noreply@dawateislami.net';
     $config['smtp_pass']    = 'uNae<4Jberp';
     $config['charset']      = 'utf-8';
     $config['newline']      = "\r\n";
     $config['wordwrap']     = TRUE;
     $config['mailtype']     = 'html';
     
     
     
     $this->email->initialize($config);
     
     $this->email->from('erp.noreply@dawateislami.net','ERP Alert Notification');
     $this->email->bcc($email);
     
     $this->email->subject($subject);     
     $this->email->message($message);
     if($attachment!=null){
        $this->email->attach($attachment, 'attachment', 'report.pdf');
     }
        $this->email->send();

    }
    public function alert_child_item($docno,$doctype,$doccode,$period,$year,$tbl){
    $item=$this->db_select('itm_itemcode',$tbl,array(
        'invm_no'       => $docno,
        'doctyp_code'   => $doctype,
        'docs_doccode'  => $doccode,
        'invm_period'   => $period,
        'invm_year'     => $year,
        'ou_code'       => $this->session->userdata('bg_code'),
        'le_code'       => $this->session->userdata('le_code'),
        'bg_code'       => $this->session->userdata('ou_code')
        ));
        $pids=array();
        foreach ($item as  $value) {
            $pids[] = array('itm' => $value->itm_itemcode);
        }
        $this->insert_alert_misc(multi_unique($pids));
    }
    public function check_stock($item,$loc_code=null){
        $this->db->select("SUM(view_trans_in_out.quantity) Qty_2,(si_items.itm_itemdesc) itm_itemdesc,(si_items.itm_itemcode) itm_itemcode");
        $this->db->from("si_items,view_trans_in_out");
        $this->db->where("
                    ((view_trans_in_out.bg_code = ".$this->session->userdata('bg_code').")) AND
                    ((view_trans_in_out.le_code = ".$this->session->userdata('le_code').")) AND
                    ((view_trans_in_out.ou_code = ".$this->session->userdata('ou_code').")) AND
                    ( view_trans_in_out.bg_code = si_items.bg_code AND 
                    si_items.itm_itemcode = view_trans_in_out.item_code ) AND   
                    ((view_trans_in_out.item_code = '".$item ."')) ");
        if($loc_code>0){
            $this->db->where("view_trans_in_out.Location_Code = $loc_code");
        }
         return  $this->db->get()->result_array();
    }
    //Miscellaneous Alert
    public function insert_alert_misc($arrcode){
        foreach ($arrcode as $value) {
        $record_lvl=$this->db_select('itm_reordlevel','si_items','itm_itemcode ="'.$value['itm'].'" AND bg_code = '.$this->session->userdata("bg_code"))[0]->itm_reordlevel;
        $InvReturn = $this->check_stock($value['itm']);
        if(!empty($InvReturn[0]['Qty_2']) && $InvReturn[0]['Qty_2']<=$record_lvl){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,  $this->insert_alert_email_misc($InvReturn[0]['Qty_2'],$InvReturn[0]['itm_itemdesc'],$record_lvl) );
            curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
            curl_close($ch);
        }
      }
    }
    public function insert_alert_email_misc($stock_qty,$desc,$qty){
     $alert_user=$this->check_alert_user_misc();
        if(!empty($alert_user)){
        $all_email=array();
         foreach ($alert_user as $value){
           $alert_log_email= $this->alert_log_product(
                $value->usr_id,
                $stock_qty,$desc,$qty);
           $email   = $this->find_user_email($value->usr_id);
           $subject = $alert_log_email[0];
           $message = $alert_log_email[1];
           array_push($all_email,$email);
            }if(!empty($all_email)){
                $this->send_mail($all_email,$subject,$message);
                
            }
        }
    }
    public function check_alert_user_misc(){
        $check_user=$this->db_select('*','si_alert_preferences',array('ap_type' =>'Inventory Reorder LEVEL'));
        if(count($check_user)>0){
            return $check_user;
        }
        return 0;
    }
    public function alert_log_product($user_id,$stock_qty,$prod_desc,$qty){
        $currentDate=date('Y-m-d H:i:s');
        $lastID =   1;
        $message="'".$prod_desc."' with Stock ".$stock_qty." has dropped FROM its Re-ORDER LIMIT of Qty ".$qty." ";
        $subject="'".$prod_desc."' has dropped FROM its Re-ORDER LIMIT";
        //$seqNo      =   $this->db_select('*','si_alert_log',array('bg_code'=>$this->session->userdata("bg_code")));
        //if(isset($seqNo)){
	//$res    =   count($seqNo);
        //$lastID =   $res+1;
        //}
        //$al_code     =   str_pad($lastID, 10, '0', STR_PAD_LEFT);
        $insert = $this->db_insert('si_alert_log',
            array(
                'bg_code' => $this->session->userdata("bg_code"),
                //'al_code' => $al_code,
                'usr_id'  => $user_id,
                'al_message' => $message,
                'al_status' =>  0,
                'al_createdby' => $this->session->userdata("id"),
                'al_createdwhen' => $currentDate
                ));
        return array($subject,$message);
    }

    function get_field($table){
        $result = $this->db->field_data($table);
        // $fields = $this->db->list_fields($table);
        $fields = array();
        foreach($result as $field){
            if($field->type == 'int' || $field->type == 'double'){
                $fields[$field->name]= 0;
            }else if($field->type == 'timestamp' || $field->type == 'date'){
                $fields[$field->name]= '';
                // $fields[$field->name]= date('Y-m-d');
            }else{
                $fields[$field->name]= "";
            }
        }
        return $fields;
    }

    public function _get_max_Where($parameter,$table,$where){
        $this->db->select($parameter);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get();
        $result =  $query->result_array();
        if(!empty($result)){
          return $result[0][$parameter];
        }
        
    }
    public function insert_rate($add_data){
        $data = array_merge(array(
            'bg_code'=>$this->session->userdata('bg_code'),
            'le_code'=>$this->session->userdata('le_code')),$add_data);
        if(exploadcomma($data['rate']) > 0){
            $this->db->query("INSERT INTO itm_rate (bg_code,le_code, itm_itemcode,rate,rate_bef_conv) VALUES ('".$data['bg_code']."', '".$data['le_code']."','".$data['itm_itemcode']."', '".exploadcomma($data['rate'])."', '".$data['rate_bef_conv']."')
            ON DUPLICATE KEY UPDATE rate = '".exploadcomma($data['rate'])."',rate_bef_conv = '".$data['rate_bef_conv']."'");
        }
        $this->db_insert('rate_history',[
            'bg_code'      => $this->session->userdata('bg_code'),
            'le_code'      => $this->session->userdata('le_code'),
            'ou_code'      => $this->session->userdata('ou_code'),
            'itm_itemcode' => $data['itm_itemcode'],
            'docs_doccode' => $data['docs_doccode'],
            'buy_price'    => exploadcomma($data['rate'])
        ]);

    }
    public function GetGlCode($type,$primaryDatecolumn,$postedDate,$docno,$table,$doctype){

                ////// Doc No Genrate in the base of year month daily bases and never
                $docno_date = $this->_get_max_Where($primaryDatecolumn,$table,array_merge($doctype,array('vm_dt' => $postedDate)));

                $date                  = $postedDate;
                $Current_year          = date('Y', strtotime($date));
                $docno_date_year       = date('Y', strtotime($docno_date));
                $Current_month         = date('m', strtotime($date));
                $docno_date_month      = date('m', strtotime($docno_date));
               // getLastQuery(false,'test');
                if($type==0)
                {
                    if(!empty($docno_date))
                    {
                        if($Current_year==$docno_date_year)
                        {
                            $vm_No = $this->_get_max_plus($docno,$table,10,array_merge($doctype,array($primaryDatecolumn => $docno_date)));      
                        }
                        else 
                        {
                          $vm_No = '0000000001';
                        }
                    }  
                    else 
                    {
                         $vm_No = '0000000001';
                    }
                }
                else if($type==1)
                {  
                    if(!empty($docno_date))
                    { 
                        if($Current_month==$docno_date_month && $Current_year==$docno_date_year)
                        {
                            $vm_No = $this->_get_max_plus($docno,$table,10,array_merge($doctype,array($primaryDatecolumn => $docno_date)));
                            //getLastQuery(true,'test');
                        }
                         else 
                        {
                          $vm_No = '0000000001';
                        }
                    }  
                    else 
                    {
                      $vm_No = '0000000001';
                    }
                   
                }
                else if($type==3)
                {   
                    if($docno_date==$date){
                        $vm_No = $this->_get_max_plus($docno,$table,10, array_merge($doctype,array($primaryDatecolumn => $date)));  
                    }
                    else
                    {
                        $vm_No = '0000000001';
                    }
                }
                else
                {
                   $vm_No = $this->_get_max_plus($docno,$table,10,$doctype);     
                }
            ////// Doc No Genrate in the base of year month daily bases and never
         
            return $vm_No;

    }

    //** Get Single Column **//
    public function _get_column($columns,$table,$where){
        return $this->db->select($columns)->get_where($table,$where)->row()->$columns ?? '';
    }

    public function selectSum($columns,$table,$where){
        return $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->group_by(array_keys($where))
        ->get()->result_array();

    }

    public function get_row($table,$where){
        $this->db->where($where);
       return $this->db->get($table)->row();
    }
    public function roleWiseMenuList($role_id,$controller='',$function=''){
        $record=[];
        if($role_id!=''){
            $data=array(
                'role_id'   =>$role_id,
                'controller'=>$controller,
            
            );
            if($function != ''){
               $data['function'] =$function;
            }
            $record=$this->db_select_single_row(
                'menu_id,controller,function',
                'rolewisemenu',$data
            )['menu_id'];
        }
        return $record;
    }

//    public function _change_status($id,$data,$table){
//        if($id != null){
//            $update =  $this->db_update($data,$id,$table);
//            if($update){
//                return 1;
//            }else{
//                return 0;
//            }
//        }
//    }
//   
//    
//     

public function _getrow($columns,$table,$where,$returnType = 'array')
{
    $query = $this->db->select($columns, FALSE)->get_where($table,$where);
    if($returnType == 'array')
    {
        return $query->result_array();
    }
    else{
        return  $query->result();
    }
}
public function _getrowQuote($columns,$table,$where,$returnType = 'array')
{
    $query = $this->db->select($columns, FALSE)
    ->from($table)
    ->where($where,NULL,FALSE)->get();
    if($returnType == 'array')
    {
        return $query->result_array();
    }
    else{
        return  $query->result();
    }
}
public function _getoneOrmoreColumn($columns,$table,$where)
{
    return $this->db->select($columns)->get_where($table,$where)->row_array();
} 

public function _getAnalysisFirst($bg_code)
{
       $analysis1 = $this->db->select('an_code')->get_where('gl_analysis',['bg_code'=> $bg_code , 'an_type' => 0 , 'an_sequence' => 1])->row()->an_code;
       $analysis2 = $this->db->select('an_code')->get_where('gl_analysis',['bg_code'=> $bg_code , 'an_type' => 0 , 'an_sequence' => 2])->row()->an_code;
       $analysis3 = $this->db->select('an_code')->get_where('gl_analysis',['bg_code'=> $bg_code , 'an_type' => 0 , 'an_sequence' => 3])->row()->an_code;
       $analysis4 = $this->db->select('an_code')->get_where('gl_analysis',['bg_code'=> $bg_code , 'an_type' => 0 , 'an_sequence' => 4])->row()->an_code;
       $analysis5 = $this->db->select('an_code')->get_where('gl_analysis',['bg_code'=> $bg_code , 'an_type' => 0 , 'an_sequence' => 5])->row()->an_code;

       return ['analysis1' => $analysis1,'analysis2' => $analysis2,'analysis3' => $analysis3,'analysis4' => $analysis4,'analysis5' => $analysis5];
}

public function balance_Checking_against_analysis($bg_code = null)
{
    if($bg_code == null){
        $bg_code = $this->session->userdata("bg_code");
    }
    $balance_Checking_against_analysis = $this->db->select('par_data,par_desc')
                                                      ->where('bg_code', "$bg_code")
                                                      ->where_in('par_code', array('0000010071','0000010072','0000010073','0000010074','0000010075'))
                                                      ->get('si_parameter')
                                                      ->result_array();
    return array_combine(range(1, count(array_column($balance_Checking_against_analysis, 'par_data'))), array_column($balance_Checking_against_analysis, 'par_data'));
}

public function BankBalancesAgainstanalysis($post , $pkeys = '')
{
    $editCasebanckcash = '';
    if(!empty($pkeys))
    {
        $editCasebanckcash = " AND NOT ( ap_paymentdtl.paydtl_co_code = '".$pkeys['pay_cocode']."' AND ap_paymentdtl.paydtl_le_code = '".$pkeys['le_code']."' AND ap_paymentdtl.paydtl_site_code = '".$pkeys['pay_sitecode']."' AND ap_paymentdtl.paydtl_year = '".$pkeys['pay_year']."' AND ap_paymentdtl.paydtl_cd_period = '".$pkeys['pay_period']."' AND ap_paymentdtl.paydtl_no = '".$pkeys['pay_no']."' AND ap_paymentdtl.paydtl_pd_doccode = '".$pkeys['pay_doccodes']."' AND ap_paymentdtl.paydtl_pd_doctype = '".$pkeys['pay_doctype']."' AND ap_paymentdtl.paydtl_chqtype = '".$pkeys['pay_chqtype']."')";
    }

    $reconsile = $this->_get_column('par_data','si_parameter',['bg_code'=>$this->session->userdata('bg_code'),'par_code' => "0000010083"]) ?? FALSE;
            
    $pay_rec = '';
    $rec_rec = '';
    
    if( $reconsile  == 1 )
    {
    	$pay_rec = " AND (ap_paymentmst.pay_reconcile_status = 1 OR ap_paymentmst.pay_chqtype = 2)";
    	$rec_rec = " AND (si_receiptmaster.recm_reconcile_status= 2 OR si_receiptmaster.recm_mode_type = 1)";
    }
    else if($reconsile  == false)
    {
    	$rec_rec = " AND (si_receiptmaster.recm_reconcile_status= 0 OR si_receiptmaster.recm_mode_type = 1 OR si_receiptmaster.recm_mode_type = 3)";	
    }
        

    $balance_Checking_against_analysis = $this->balance_Checking_against_analysis();
    // Payment

    $paymentanalysis1 = ""; $paymentanalysis2 = ""; $paymentanalysis3 = ""; $paymentanalysis4 = ""; $paymentanalysis5 = "";
    $payment_group_by =[];
    // Receipt
    
    $receiptanalysis1 = ""; $receiptanalysis2 = ""; $receiptanalysis3 = ""; $receiptanalysis4 = ""; $receiptanalysis5 = "";
    $receipt_group_by =[];
     // FTR Dest
    
    $ftrdestanalysis1 = ""; $ftrdestanalysis2 = ""; $ftrdestanalysis3 = ""; $ftrdestanalysis4 = ""; $ftrdestanalysis5 = "";
    $ftrdest_group_by =[];
    // FTR src
    
    $ftrsrcanalysis1 = ""; $ftrsrcanalysis2 = ""; $ftrsrcanalysis3 = ""; $ftrsrcanalysis4 = ""; $ftrsrcanalysis5 = "";
    $ftrsrc_group_by =[];
    // purpose allocation
    
    $purposeanalysis1 = ""; $purposeanalysis2 = ""; $purposeanalysis3 = ""; $purposeanalysis4 = ""; $purposeanalysis5 = "";
    $purpose_group_by =[];



    if($balance_Checking_against_analysis[1] == 1)
    {
        if(isset($post['location']) && !empty($post['location']))
        {
            $paymentanalysis1 = " AND ap_paymentdtl.paydtl_whtanlys1 =".$post['location'];
        }
        $payment_group_by[] = "ap_paymentdtl.paydtl_whtanlys1";

        // Receipt 
        
        if(isset($post['location']) && !empty($post['location']))
        {
            $receiptanalysis1 = " AND si_trans_receipt_detail.recd_whtanlys1 =".$post['location'];
        }
        $receipt_group_by[] = "si_trans_receipt_detail.recd_whtanlys1";

         // FTR Dest 
        
        if(isset($post['location']) && !empty($post['location']))
        {
            $ftrdestanalysis1 = " AND gl_fundstransfer.des_analysis_1 =".$post['location'];
        }
        $ftrdest_group_by[] = "gl_fundstransfer.des_analysis_1";

        // FTR src 
        
        if(isset($post['location']) && !empty($post['location']))
        {
            $ftrsrcanalysis1 = " AND gl_fundstransfer.analysis_1 =".$post['location'];
        }
        $ftrsrc_group_by[] = "gl_fundstransfer.analysis_1";

        // purpose allocation
        
        if(isset($post['location']) && !empty($post['location']))
        {
            $purposeanalysis1 = " AND gl_pr_allocation_detail.analysis_1 =".$post['location'];
        }
        $purpose_group_by[] = "gl_pr_allocation_detail.analysis_1";
    }
    if($balance_Checking_against_analysis[2] == 1)
    {
        if(isset($post['majlis']) && !empty($post['majlis']))
        {
            $paymentanalysis2 = " AND ap_paymentdtl.paydtl_whtanlys2 =".$post['majlis'];
        }
        $payment_group_by[] = "ap_paymentdtl.paydtl_whtanlys2";

        // Receipt 
        
        if(isset($post['majlis']) && !empty($post['majlis']))
        {
            $receiptanalysis2 = "  AND si_trans_receipt_detail.recd_whtanlys2 =".$post['majlis'];
        }
        $receipt_group_by[] = "si_trans_receipt_detail.recd_whtanlys2";

        // FTR Dest 
        
        if(isset($post['majlis']) && !empty($post['majlis']))
        {
            $ftrdestanalysis2 = "  AND gl_fundstransfer.des_analysis_2 =".$post['majlis'];
        }
        $ftrdest_group_by[] = "gl_fundstransfer.des_analysis_2";

         // FTR src 
        
        if(isset($post['majlis']) && !empty($post['majlis']))
        {
            $ftrsrcanalysis2 = " AND gl_fundstransfer.analysis_2 =".$post['majlis'];
        }
        $ftrsrc_group_by[] = "gl_fundstransfer.analysis_2";

        // purpose allocation
        
        if(isset($post['majlis']) && !empty($post['majlis']))
        {
            $purposeanalysis2 = " AND gl_pr_allocation_detail.analysis_2 =".$post['majlis'];
        }
        $purpose_group_by[] = "gl_pr_allocation_detail.analysis_2";
    }
    if($balance_Checking_against_analysis[3] == 1)
    {
        if(isset($post['purpose']) && !empty($post['purpose']))
        {
            $paymentanalysis3 = "  AND ap_paymentdtl.paydtl_whtanlys3 =".$post['purpose'];
        }
        $payment_group_by[] = "ap_paymentdtl.paydtl_whtanlys3";

        // Receipt 
        
        if(isset($post['purpose']) && !empty($post['purpose']))
        {
            $receiptanalysis3 = " AND si_trans_receipt_detail.recd_whtanlys3 =".$post['purpose'];
        }
        $receipt_group_by[] = "si_trans_receipt_detail.recd_whtanlys3";

        // FTR Dest 
        
        if(isset($post['purpose']) && !empty($post['purpose']))
        {
            $ftrdestanalysis3 = " AND gl_fundstransfer.analysis_3 =".$post['purpose'];
        }
        $ftrdest_group_by[] = "gl_fundstransfer.analysis_3";

        // FTR src 
        
        if(isset($post['purpose']) && !empty($post['purpose']))
        {
            $ftrsrcanalysis3 = " AND gl_fundstransfer.analysis_3 =".$post['purpose'];
        }
        $ftrsrc_group_by[] = "gl_fundstransfer.analysis_3";

         // purpose allocation
        
        if(isset($post['purpose']) && !empty($post['purpose']))
        {
            $purposeanalysis3 = " AND gl_pr_allocation_detail.analysis_3 =".$post['purpose'];
        }
        $purpose_group_by[] = "gl_pr_allocation_detail.analysis_3";
    }
    if($balance_Checking_against_analysis[4] == 1)
    {
        if(isset($post['branches']) && !empty($post['branches']))
        {
            $paymentanalysis4 = " AND ap_paymentdtl.paydtl_whtanlys4 =".$post['branches'];
        }
        $payment_group_by[] = "ap_paymentdtl.paydtl_whtanlys4";

        // Receipt 
        
        if(isset($post['branches']) && !empty($post['branches']))
        {
            $receiptanalysis4 = " AND si_trans_receipt_detail.recd_whtanlys4 =".$post['branches'];
        }
        $receipt_group_by[] = "si_trans_receipt_detail.recd_whtanlys4";

        // FTR Dest 
        
        if(isset($post['branches']) && !empty($post['branches']))
        {
            $ftrdestanalysis4 = " AND gl_fundstransfer.des_analysis_4 =".$post['branches'];
        }
        $ftrdest_group_by[] = "gl_fundstransfer.des_analysis_4";

         // FTR src 
        
        if(isset($post['branches']) && !empty($post['branches']))
        {
            $ftrsrcanalysis4 = " AND gl_fundstransfer.analysis_4 =".$post['branches'];
        }
        $ftrsrc_group_by[] = "gl_fundstransfer.analysis_4";

         // purpose allocation
        
        if(isset($post['branches']) && !empty($post['branches']))
        {
            $purposeanalysis4 = " AND gl_pr_allocation_detail.analysis_4 =".$post['branches'];
        }
        $purpose_group_by[] = "gl_pr_allocation_detail.analysis_4";
    }
    if($balance_Checking_against_analysis[5] == 1)
    {
        if(isset($post['faqeer']) && !empty($post['faqeer']))
        {
            $paymentanalysis5 = " AND ap_paymentdtl.paydtl_whtanlys5 =".$post['faqeer'];
        }
        $payment_group_by[] = "ap_paymentdtl.paydtl_whtanlys5";

        // Receipt 
        
        if(isset($post['faqeer']) && !empty($post['faqeer']))
        {
            $receiptanalysis5 = " AND si_trans_receipt_detail.recd_whtanlys5 =".$post['faqeer'];
        }
        $receipt_group_by[] = "si_trans_receipt_detail.recd_whtanlys5";

        // FTR Dest 
        
        if(isset($post['faqeer']) && !empty($post['faqeer']))
        {
            $ftrdestanalysis5 = " AND gl_fundstransfer.des_analysis_5 =".$post['faqeer'];
        }
        $ftrdest_group_by[] = "gl_fundstransfer.des_analysis_5";

         // FTR src 
        
        if(isset($post['faqeer']) && !empty($post['faqeer']))
        {
            $ftrsrcanalysis5 = " AND gl_fundstransfer.analysis_5 =".$post['faqeer'];
        }
        $ftrsrc_group_by[] = "gl_fundstransfer.analysis_5";

         // purpose allocation
        
        if(isset($post['faqeer']) && !empty($post['faqeer']))
        {
            $purposeanalysis5 = " AND gl_pr_allocation_detail.analysis_5 =".$post['faqeer'];
        }
        $purpose_group_by[] = "gl_pr_allocation_detail.analysis_5";
    }

    $allowedposting_pay = '';
    $allowedposting_rec = '';
    $allowedposting_ftr = '';
    if($this->session->userdata("allowedposting") == 1)
    {
        
        // 		        $allowedposting_pay = "AND ap_paymentmst.pay_vchrcreated = 2";
        $allowedposting_rec = "AND (si_receiptmaster.recm_vchrcreated = 2 OR dm_receipt_number is not null)";
        // 		        $allowedposting_ftr = "AND gl_fundstransfer.ftr_vchrcreated = 2";
        
        
        /* 
         * old  condition commit till decide next scenario
         * 
         */
//         $allowedposting_pay = "AND ap_paymentmst.pay_vchrcreated = 2";
//         $allowedposting_rec = "AND si_receiptmaster.recm_vchrcreated = 2";
//         $allowedposting_ftr = "AND gl_fundstransfer.ftr_vchrcreated = 2";
    }

        $payment_case_allocated_not_count= "";
        if((isset($post['mode']) && $post['mode']=='000003') || $this->uri->segment(1)!='Payments' )
        {
        $payment_case_allocated_not_count = ' UNION ALL SELECT (SUM(gl_pr_allocation_detail.allocated_amount - gl_pr_allocation_detail.paid_amount) *-1) AS amount FROM gl_pr_allocation_master,gl_pr_allocation_detail,si_bank
                                            WHERE   
                                            gl_pr_allocation_master.bg_code = gl_pr_allocation_detail.bg_code
                                            AND gl_pr_allocation_master.le_code = gl_pr_allocation_detail.le_code
                                            AND gl_pr_allocation_master.ou_code = gl_pr_allocation_detail.ou_code
                                            AND gl_pr_allocation_master.year = gl_pr_allocation_detail.year
                                            AND gl_pr_allocation_master.period = gl_pr_allocation_detail.period
                                            AND gl_pr_allocation_master.docs_doccode = gl_pr_allocation_detail.docs_doccode
                                            AND gl_pr_allocation_master.doc_doctype = gl_pr_allocation_detail.doc_doctype
                                            AND gl_pr_allocation_master.doc_no = gl_pr_allocation_detail.doc_no

                                            AND `gl_pr_allocation_detail`.`bg_code` = '.$this->session->userdata ( 'bg_code' ).'
                                            AND `gl_pr_allocation_detail`.`le_code` = '.$this->session->userdata ( 'le_code' ).'
                                            AND `gl_pr_allocation_detail`.`ou_code` = '.$this->session->userdata ( 'ou_code' ).'
                                            AND `gl_pr_allocation_detail`.`dvn_code` = '.$this->session->userdata ( 'dvn_code' ).'
                                            AND `gl_pr_allocation_detail`.`bank` = '.$post['bank'].'
                                            AND gl_pr_allocation_master.is_active = 1
                                            AND si_bank.bg_code = gl_pr_allocation_detail.bg_code 
                                            AND si_bank.le_code = gl_pr_allocation_detail.le_code 
                                            AND si_bank.bnk_code = gl_pr_allocation_detail.bank
                                            AND si_bank.is_active=1
                                            AND  si_bank.cur_code = '.$post['curr'].'
                                            '.$purposeanalysis1
                                            .$purposeanalysis2
                                            .$purposeanalysis3
                                            .$purposeanalysis4
                                            .$purposeanalysis5;
        
                                            if($purpose_group_by){
                                                $payment_case_allocated_not_count.='
                                            GROUP BY
                                            '.implode(',', $purpose_group_by);
                                            }
        }
        $paygroup='';
        if($payment_group_by){
          $paygroup =  '  GROUP BY  '.implode(',', $payment_group_by);
        }
        $recgroup='';
        if($receipt_group_by){
            $recgroup =  ' GROUP BY  '.implode(',', $receipt_group_by);
        }
        $ftrdroup='';
        if($ftrdest_group_by){
            $ftrdroup =  ' GROUP BY  '.implode(',', $ftrdest_group_by);
        }
        $ftrsroup='';
        if($ftrsrc_group_by){
            $ftrsroup = ' GROUP BY
        '.implode(',', $ftrsrc_group_by);
        }
        
    $getBalances = $this->db->query('
        SELECT 
        (SUM(ap_paymentdtl.paydtl_fc_amount) *-1) AS amount 
        FROM
        ap_paymentmst,ap_paymentdtl,si_bank
        WHERE
        ap_paymentmst.pay_cocode = '.$this->session->userdata ( 'bg_code' ).' 
        AND ap_paymentmst.le_code = '.$this->session->userdata ( 'le_code' ).' 
        AND ap_paymentmst.pay_sitecode = '.$this->session->userdata ( 'ou_code' ).' 
        AND ap_paymentmst.dvn_code = '.$this->session->userdata ( 'dvn_code' ).' 
        AND ap_paymentmst.pay_bnkcode = '.$post['bank'].' 
        AND ap_paymentmst.pay_cocode = ap_paymentdtl.paydtl_co_code 
        AND ap_paymentmst.le_code = ap_paymentdtl.paydtl_le_code 
        AND ap_paymentmst.pay_sitecode = ap_paymentdtl.paydtl_site_code 
        AND ap_paymentmst.pay_year = ap_paymentdtl.paydtl_year 
        AND ap_paymentmst.pay_period = ap_paymentdtl.paydtl_cd_period 
        AND ap_paymentmst.pay_no = ap_paymentdtl.paydtl_no 
        AND ap_paymentmst.pay_doctype = ap_paymentdtl.paydtl_pd_doctype 
        AND ap_paymentmst.pay_doccodes = ap_paymentdtl.paydtl_pd_doccode 
        AND ap_paymentmst.is_active = 1
        AND si_bank.bg_code = ap_paymentmst.pay_cocode 
        AND si_bank.le_code = ap_paymentmst.le_code 
        AND si_bank.bnk_code = ap_paymentmst.pay_bnkcode
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].' 
        '.$allowedposting_pay
         .$paymentanalysis1
         .$paymentanalysis2
         .$paymentanalysis3
         .$paymentanalysis4
         .$paymentanalysis5
         .$editCasebanckcash
         .$pay_rec
         .$paygroup
        .'

        UNION ALL    

        SELECT  IFNULL ( SUM(si_trans_receipt_detail.recd_fc_amount) , 0 ) AS amount
        FROM
        si_receiptmaster,
        si_trans_receipt_detail,si_bank
        WHERE 
        si_receiptmaster.recm_cocode = si_trans_receipt_detail.recm_co_code 
        AND si_receiptmaster.recm_brcode = si_trans_receipt_detail.recm_le_code 
        AND si_receiptmaster.recm_sitecode = si_trans_receipt_detail.recm_site_code 
        AND si_receiptmaster.recm_year = si_trans_receipt_detail.recm_year 
        AND si_receiptmaster.recm_period = si_trans_receipt_detail.recm_cd_period 
        AND si_receiptmaster.docs_doccode = si_trans_receipt_detail.docs_doccode 
        AND si_receiptmaster.doctyp_code = si_trans_receipt_detail.doctyp_code 
        AND si_receiptmaster.recm_docno = si_trans_receipt_detail.recm_no 
        
        AND si_trans_receipt_detail.recm_co_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND si_trans_receipt_detail.recm_le_code = '.$this->session->userdata ( 'le_code' ).'
        AND si_trans_receipt_detail.recm_site_code = '.$this->session->userdata ( 'ou_code' ).'
        AND si_trans_receipt_detail.dvn_code = '.$this->session->userdata ( 'dvn_code' ).'
        AND si_receiptmaster.bnk_code = '.$post['bank'].'
        AND si_receiptmaster.is_active = 1
        AND si_bank.bg_code = si_receiptmaster.recm_cocode 
        AND si_bank.le_code = si_receiptmaster.recm_brcode 
        AND (si_bank.ou_code = si_receiptmaster.recm_sitecode OR si_bank.ou_code is null OR si_bank.ou_code ="") 
        AND (si_bank.dvn_code = "'.$this->session->userdata ( 'dvn_code' ).'" OR si_bank.dvn_code is null OR si_bank.dvn_code ="") 
        AND si_bank.bnk_code = si_receiptmaster.bnk_code 
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        '.$allowedposting_rec
         .$receiptanalysis1
         .$receiptanalysis2
         .$receiptanalysis3
         .$receiptanalysis4
         .$receiptanalysis5
         .$rec_rec
         .$recgroup
        .'
        

        UNION ALL 

        SELECT 
        SUM(gl_fundstransfer.ftr_amount/ftr_dest_fc_rate) AS amount 
        FROM
        gl_fundstransfer,si_bank
        WHERE 

        gl_fundstransfer.bg_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND gl_fundstransfer.le_code = '.$this->session->userdata ( 'le_code' ).'
        AND gl_fundstransfer.ftr_dest_oucode = '.$this->session->userdata ( 'ou_code' ).'
   	    AND gl_fundstransfer.ftr_dest_dvncode = '.$this->session->userdata ( 'dvn_code' ).' 
        AND gl_fundstransfer.ftr_dest_bank = '.$post['bank'].'
        AND gl_fundstransfer.is_active = 1
        AND si_bank.bg_code = gl_fundstransfer.bg_code 
        AND si_bank.le_code = gl_fundstransfer.le_code 
        AND si_bank.bnk_code = gl_fundstransfer.ftr_dest_bank
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        '.$allowedposting_ftr
         .$ftrdestanalysis1
         .$ftrdestanalysis2
         .$ftrdestanalysis3
         .$ftrdestanalysis4
         .$ftrdestanalysis5
        .$ftrdroup
        .'

        UNION ALL 

        SELECT 
        (SUM(gl_fundstransfer.ftr_fc_amount) *-1) AS amount 
        FROM
        gl_fundstransfer,si_bank
        WHERE 

        gl_fundstransfer.bg_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND gl_fundstransfer.le_code = '.$this->session->userdata ( 'le_code' ).'
        AND gl_fundstransfer.ou_code = '.$this->session->userdata ( 'ou_code' ).'
        AND gl_fundstransfer.dvn_code = '.$this->session->userdata ( 'dvn_code' ).'
        AND gl_fundstransfer.ftr_sourcebank = '.$post['bank'].'
        AND gl_fundstransfer.is_active = 1
        AND si_bank.bg_code = gl_fundstransfer.bg_code 
       AND si_bank.le_code = gl_fundstransfer.le_code 
       AND si_bank.bnk_code = gl_fundstransfer.ftr_sourcebank
       AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        '.$allowedposting_ftr
         .$ftrsrcanalysis1
         .$ftrsrcanalysis2
         .$ftrsrcanalysis3
         .$ftrsrcanalysis4
         .$ftrsrcanalysis5
         .$ftrsroup
         .$payment_case_allocated_not_count        
    )->result_array();
return $getBalances;

}


public function BankBalancesAgainstanalysisforPR($post)
{
    $reconsile = $this->_get_column('par_data','si_parameter',['bg_code'=>$this->session->userdata('bg_code'),'par_code' => "0000010083"]) ?? FALSE;
            
    $pay_rec = '';
    $rec_rec = '';
    
    if( $reconsile  == 1 )
    {
    	$pay_rec = " AND (ap_paymentmst.pay_reconcile_status = 1 OR ap_paymentmst.pay_chqtype = 2)";
    	$rec_rec = " AND (si_receiptmaster.recm_reconcile_status= 2 OR si_receiptmaster.recm_mode_type = 1)";
    }
    else if($reconsile  == false)
    {
    	$rec_rec = " AND (si_receiptmaster.recm_reconcile_status= 0 OR si_receiptmaster.recm_mode_type = 1 OR si_receiptmaster.recm_mode_type = 3)";	
    }

    $allowedposting_pay = '';
    $allowedposting_rec = '';
    $allowedposting_ftr = '';
    if($this->session->userdata("allowedposting") == 1)
    {
        
        // 		        $allowedposting_pay = "AND ap_paymentmst.pay_vchrcreated = 2";
        $allowedposting_rec = "AND (si_receiptmaster.recm_vchrcreated = 2 OR dm_receipt_number is not null)";
        // 		        $allowedposting_ftr = "AND gl_fundstransfer.ftr_vchrcreated = 2";
        
        /*
         * old  condition commit till decide next scenario
         *
         */
//         $allowedposting_pay = "AND ap_paymentmst.pay_vchrcreated = 2";
//         $allowedposting_rec = "AND si_receiptmaster.recm_vchrcreated = 2";
//         $allowedposting_ftr = "AND gl_fundstransfer.ftr_vchrcreated = 2";
    }

    $getBalances = $this->db->query('
        SELECT 
        (SUM(ap_paymentdtl.paydtl_fc_amount) *-1) AS amount 
        FROM
        ap_paymentmst,ap_paymentdtl,si_bank
        WHERE
        ap_paymentmst.pay_cocode = '.$this->session->userdata ( 'bg_code' ).' 
        AND ap_paymentmst.le_code = '.$this->session->userdata ( 'le_code' ).' 
        AND ap_paymentmst.pay_sitecode = '.$this->session->userdata ( 'ou_code' ).' 
        AND ap_paymentmst.dvn_code = '.$this->session->userdata ( 'dvn_code' ).'
        AND ap_paymentmst.pay_bnkcode = '.$post['bank'].' 
        AND ap_paymentmst.pay_cocode = ap_paymentdtl.paydtl_co_code 
        AND ap_paymentmst.le_code = ap_paymentdtl.paydtl_le_code 
        AND ap_paymentmst.pay_sitecode = ap_paymentdtl.paydtl_site_code 
        AND ap_paymentmst.pay_year = ap_paymentdtl.paydtl_year 
        AND ap_paymentmst.pay_period = ap_paymentdtl.paydtl_cd_period 
        AND ap_paymentmst.pay_no = ap_paymentdtl.paydtl_no 
        AND ap_paymentmst.pay_doctype = ap_paymentdtl.paydtl_pd_doctype 
        AND ap_paymentmst.pay_doccodes = ap_paymentdtl.paydtl_pd_doccode 
        AND ap_paymentmst.is_active = 1
        '.$allowedposting_pay.$pay_rec.'
        AND ap_paymentdtl.paydtl_whtanlys3 = '.$post['purpose'].'
        AND ap_paymentdtl.paydtl_whtanlys5 = '.$post['faqeer'].'

        AND si_bank.bg_code = ap_paymentmst.pay_cocode 
        AND si_bank.le_code = ap_paymentmst.le_code 
        AND si_bank.bnk_code = ap_paymentmst.pay_bnkcode
        AND si_bank.is_active=1

        AND si_bank.cur_code = '.$post['curr'].'
        GROUP BY
          ap_paymentdtl.paydtl_whtanlys3,
          ap_paymentdtl.paydtl_whtanlys5 

        UNION ALL    

        SELECT  IFNULL ( SUM(si_trans_receipt_detail.recd_fc_amount) , 0 ) AS amount
        FROM
        si_receiptmaster,
        si_trans_receipt_detail,si_bank
        WHERE 
        si_receiptmaster.recm_cocode = si_trans_receipt_detail.recm_co_code 
        AND si_receiptmaster.recm_brcode = si_trans_receipt_detail.recm_le_code
        AND si_receiptmaster.recm_sitecode = si_trans_receipt_detail.recm_site_code
        AND si_receiptmaster.recm_year = si_trans_receipt_detail.recm_year
        AND si_receiptmaster.recm_period = si_trans_receipt_detail.recm_cd_period
        AND si_receiptmaster.docs_doccode = si_trans_receipt_detail.docs_doccode 
        AND si_receiptmaster.doctyp_code = si_trans_receipt_detail.doctyp_code
        AND si_receiptmaster.recm_docno = si_trans_receipt_detail.recm_no
        AND si_trans_receipt_detail.recm_co_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND si_trans_receipt_detail.recm_le_code = '.$this->session->userdata ( 'le_code' ).'
        AND si_trans_receipt_detail.recm_site_code = '.$this->session->userdata ( 'ou_code' ).'
        AND si_trans_receipt_detail.dvn_code = '.$this->session->userdata ( 'dvn_code' ).'
        AND si_receiptmaster.bnk_code = '.$post['bank'].'
        AND si_receiptmaster.is_active = 1
        '.$allowedposting_rec.$rec_rec.'
        AND si_trans_receipt_detail.recd_whtanlys3 = '.$post['purpose'].'
        AND si_trans_receipt_detail.recd_whtanlys5 = '.$post['faqeer'].'
        AND si_bank.bg_code = si_receiptmaster.recm_cocode 
        AND si_bank.le_code = si_receiptmaster.recm_brcode 
        AND (si_bank.ou_code = si_receiptmaster.recm_sitecode OR si_bank.ou_code is null OR si_bank.ou_code ="") 
        AND (si_bank.dvn_code = "'.$this->session->userdata ( 'dvn_code' ).'" OR si_bank.dvn_code is null OR si_bank.dvn_code ="") 
        AND si_bank.bnk_code = si_receiptmaster.bnk_code 
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        GROUP BY si_trans_receipt_detail.recd_whtanlys3,si_trans_receipt_detail.recd_whtanlys5 

        UNION ALL 

        SELECT 
        SUM(gl_fundstransfer.ftr_fc_amount) AS amount 
        FROM
        gl_fundstransfer,si_bank
        WHERE 

        gl_fundstransfer.bg_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND gl_fundstransfer.le_code = '.$this->session->userdata ( 'le_code' ).'
       AND gl_fundstransfer.ftr_dest_oucode = '.$this->session->userdata ( 'ou_code' ).'
   	    AND gl_fundstransfer.ftr_dest_dvncode = '.$this->session->userdata ( 'dvn_code' ).'
        AND gl_fundstransfer.ftr_dest_bank = '.$post['bank'].'
        AND gl_fundstransfer.is_active = 1
        '.$allowedposting_ftr.'
        AND gl_fundstransfer.analysis_3 = '.$post['purpose'].'
        AND gl_fundstransfer.des_analysis_5 = '.$post['faqeer'].'
        AND si_bank.bg_code = gl_fundstransfer.bg_code 
        AND si_bank.le_code = gl_fundstransfer.le_code 
        AND si_bank.bnk_code = gl_fundstransfer.ftr_dest_bank
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        GROUP BY 
        gl_fundstransfer.analysis_3,
        gl_fundstransfer.des_analysis_5 

        UNION ALL 

        SELECT 
        (SUM(gl_fundstransfer.ftr_fc_amount) *-1) AS amount 
        FROM
        gl_fundstransfer,si_bank
        WHERE 

        gl_fundstransfer.bg_code = '.$this->session->userdata ( 'bg_code' ).' 
        AND gl_fundstransfer.le_code = '.$this->session->userdata ( 'le_code' ).'
        AND gl_fundstransfer.ou_code = '.$this->session->userdata ( 'ou_code' ).'
        AND gl_fundstransfer.dvn_code = '.$this->session->userdata ( 'dvn_code' ).'
        AND gl_fundstransfer.ftr_sourcebank = '.$post['bank'].'
        AND gl_fundstransfer.is_active = 1
        '.$allowedposting_ftr.'
        AND gl_fundstransfer.analysis_3 = '.$post['purpose'].'
        AND gl_fundstransfer.analysis_5 = '.$post['faqeer'].'
        AND si_bank.bg_code = gl_fundstransfer.bg_code 
       AND si_bank.le_code = gl_fundstransfer.le_code 
       AND si_bank.bnk_code = gl_fundstransfer.ftr_sourcebank
       AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        GROUP BY 
        gl_fundstransfer.analysis_3,
        gl_fundstransfer.analysis_5 

        UNION ALL

        SELECT 
        (SUM(gl_pr_allocation_detail.allocated_amount - gl_pr_allocation_detail.paid_amount) *-1) AS amount 
        FROM
        gl_pr_allocation_master,gl_pr_allocation_detail,si_bank

        WHERE   
        gl_pr_allocation_master.bg_code = gl_pr_allocation_detail.bg_code
        AND gl_pr_allocation_master.le_code = gl_pr_allocation_detail.le_code
        AND gl_pr_allocation_master.ou_code = gl_pr_allocation_detail.ou_code
        AND gl_pr_allocation_master.year = gl_pr_allocation_detail.year
        AND gl_pr_allocation_master.period = gl_pr_allocation_detail.period
        AND gl_pr_allocation_master.docs_doccode = gl_pr_allocation_detail.docs_doccode
        AND gl_pr_allocation_master.doc_doctype = gl_pr_allocation_detail.doc_doctype
        AND gl_pr_allocation_master.doc_no = gl_pr_allocation_detail.doc_no

        AND `gl_pr_allocation_detail`.`bg_code` = '.$this->session->userdata ( 'bg_code' ).'
        AND `gl_pr_allocation_detail`.`le_code` = '.$this->session->userdata ( 'le_code' ).'
        AND `gl_pr_allocation_detail`.`ou_code` = '.$this->session->userdata ( 'ou_code' ).'
        AND `gl_pr_allocation_detail`.`dvn_code` = '.$this->session->userdata ( 'dvn_code' ).'
        AND `gl_pr_allocation_detail`.`bank` = '.$post['bank'].'
        AND gl_pr_allocation_master.is_active = 1
        AND gl_pr_allocation_detail.analysis_3 = '.$post['purpose'].'
        AND gl_pr_allocation_detail.analysis_5 = '.$post['faqeer'].'
        AND si_bank.bg_code = gl_pr_allocation_detail.bg_code 
        AND si_bank.le_code = gl_pr_allocation_detail.le_code 
        AND si_bank.bnk_code = gl_pr_allocation_detail.bank
        AND si_bank.is_active=1
        AND si_bank.cur_code = '.$post['curr'].'
        GROUP BY 
        gl_pr_allocation_detail.analysis_3,
        gl_pr_allocation_detail.analysis_5

        '
    )->result_array();
return $getBalances;

}
    public function customer_balance($data,$plus='+',$check=true){
        if($check){
            $cst_balance = $this->_getrowQuote('cst_creditlimit - abs(cst_balance) cst_balance','si_customer',[
                'bg_code'            => $this->session->userdata('bg_code'),
                'le_code'            => $this->session->userdata('le_code'),
                'cst_code'           => $data['cst_code'],
                'cst_creditlimit > ' => 0,
                "ABS(cst_balance)+".exploadcomma($data['cst_balance'])."> cst_creditlimit" => NULL]
            );
            if(isset($cst_balance[0]['cst_balance'])){
                $_return = ['text' => 'Credit Limit Exceed. Available limit is '.$cst_balance[0]['cst_balance'],'errorCode' => 2];
                return $_return;
            }
        }
        $this->db->set("cst_balance ", "cst_balance $plus".exploadcomma($data['cst_balance']), FALSE)->where([
            'bg_code'   => $this->session->userdata('bg_code'),
            'le_code'   => $this->session->userdata('le_code'),
            'cst_code'  => $data['cst_code']])->update('si_customer');
            return true;
    }
    public function get_doctyp_parent_ref($data){
        $poh_doctyp = $this->db_select_single_row('doctyp_code','si_doctype',array('docs_doccode' => $data['docs_doccode'], 'doctyp_reftype' => $data['doctyp_code']))['doctyp_code'] ?? FALSE;
        if($poh_doctyp == FALSE){
            $this->session->set_flashdata('error', 'Reference Document Type is Required for '.$data['docs_doccode']);
            return false;
        }else{
            return $poh_doctyp;
        }
    }
    public function get_mis_user_by_dept(){
        $userList = $this->db_single_join(
            'login.id,login.name',
            'login', 'assign_dept',
            'login.bg_code = assign_dept.bg_code AND
            login.dept_code = assign_dept.dept_code',
            'inner', ['assign_dept.bg_code' => $this->session->userdata('bg_code'),
                'login.le_code' => $this->session->userdata('le_code'),
            'assign_dept.screen' => $this->uri->segment(1)]);
          
        return array_column($userList,'name','id');
    }
    public function get_user_ou(){
        $operating_unit = array_column($this->db_select('code ou_code','operating_unit',['bg_code' =>$this->session->userdata('bg_code'),'is_active' => 1]),'ou_code','ou_code');
        $disallowed_operatingunit = array_column($this->db_select('ou_code','disallowed_operatingunit',['bg_code' =>$this->session->userdata('bg_code'),'ou_code <>' =>$this->session->userdata('ou_code'),'usr_id' => $this->session->userdata('id')]),'ou_code','ou_code');
        $result = array_diff_assoc($operating_unit, $disallowed_operatingunit);
        return array_values($result);
    }
    public function ou_where_in(){
        $allowedOu = $this->get_user_ou();
        return "'".str_replace(",", "','", implode(",",$allowedOu)."'");
    }
	// --------------------------------------------------------------------

	/**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key){
        // Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}
    public function regionFilter($code,$array=false){
      if(isset($code) && !empty($code) && $array==false){
        return 'AND region.code = "'.$code.'" ';
      }else if(isset($code) && !empty($code) && $array==true){
         return array('region.code' =>$code);
      }
    }    
    public function OU_Filter($tbl,$feild,$code,$array=false){
      if(isset($code) && !empty($code) && $array==false ){
        return 'AND '.$tbl.'.'.$feild.' = "'.$code.'" ';
      }else if(isset($code) && !empty($code) && $array==true){
         return array($tbl.'.'.$feild =>$code);
      }
    }

    public function inv_trigger($key,$action='add',$trans_type='I' ){
        
        if ($trans_type == 'O') {  
           $containers = "(si_inventorydetail.invd_returnqty)*-1  AS containers,";
           $quatity = "(si_inventorydetail.invd_returnqty * si_inventorydetail.invd_conversion)*-1 AS quantity,";
        }else{
           $containers = "si_inventorydetail.invd_returnqty  AS containers,";
           $quatity = "(si_inventorydetail.invd_returnqty * si_inventorydetail.invd_conversion) AS quantity,";
        }

        $result = $this->db_select_array("si_inventorymaster.bg_code AS bg_code,
            si_inventorymaster.le_code AS le_code,
            si_inventorymaster.ou_code AS ou_code,
            si_inventorymaster.invm_year AS c_year,
            si_inventorymaster.invm_period AS c_period,
            si_inventorymaster.docs_doccode AS doc_code,
            si_inventorymaster.doctyp_code AS doc_type_code,
            si_inventorymaster.invm_no AS doc_no,
            si_inventorydetail.invd_seqno AS seq_no,
            si_inventorymaster.invm_date AS trans_date,
            si_inventorymaster.cst_code AS cst_code,
            si_inventorymaster.dvn_code AS dvn_code,
            si_inventorymaster.sup_code AS sup_code,
            si_inventorymaster.invm_locationfrom AS loc_from,
            si_inventorymaster.invm_locationto AS loc_to,
            si_inventorydetail.itm_itemcode AS item_code,
            si_inventorymaster.invm_createdwhen AS created_when,
            si_inventorydetail.loc_code AS location_code,
             ".$containers." 
            si_inventorydetail.invd_conversion AS c_size,
            si_inventorydetail.invd_rate AS rate,
            si_inventorydetail.invd_cost AS cost,
             ".$quatity." 
            si_inventorydetail.invd_buysellunitcode AS containerunit,
            si_inventorydetail.invd_storingunitcode AS storingunit,
            si_inventorydetail.invd_rack_no AS rackno,
            si_inventorydetail.invd_expiry_date AS expirydate,
            si_inventorydetail.invd_qa_no AS qa_no,
            si_inventorydetail.invd_batchno AS batchno,
            si_inventorydetail.dept_code_d AS dept_code,
            si_inventorydetail.invd_qty1 AS qty_1,
            si_inventorydetail.invd_qty2 AS qty_2,
            si_inventorydetail.invd_qty1_unitcode AS qty_1_unit,
            si_inventorydetail.invd_qty2_unitcode AS qty_2_unit,
            (
              si_inventorydetail.invd_qty_received * si_inventorydetail.invd_holdtag
            ) AS c_qty_hold,
            si_inventorydetail.invd_qa_no AS c_qa_no,
            si_inventorymaster.invm_parentrefdoccode AS parent_doccode_master,
            si_inventorymaster.invm_loan_type AS loan_type,
            si_inventorydetail.invd_mfg_date AS mfg_date,
            si_inventorydetail.invd_cc_id_1 AS cost_center_id_1,
            si_inventorydetail.invd_cc_id_2 AS cost_center_id_2,
            si_inventorydetail.invd_cc_id_3 AS cost_center_id_3,
            si_inventorydetail.invd_cc_id_4 AS cost_center_id_4,
            si_inventorymaster.invm_vchrcreated AS vchrcreated,
            si_inventorymaster.lnd_code AS lnd_code,
            si_inventorydetail.equip_code_d AS equip_code,
            si_inventorydetail.invd_acc_type AS acc_type,
            si_inventorydetail.invd_qty_short AS qty_short,
            si_inventorydetail.invd_narration AS narration,
            si_inventorydetail.invd_conversion2 AS conversion2,
            si_inventorydetail.invd_qc_revalid_date AS revalid_date,
            si_inventorydetail.invd_bonuspacks AS bonuspacks,
            si_inventorydetail.invd_bonusqty AS bonusqty,
            si_inventorydetail.invd_extra_qty AS extra_qty,
            si_inventorydetail.skt_code AS skt_code,
            si_inventorymaster.invm_stk_holdrel_tag AS stk_holdrel_tag,
            si_inventorydetail.invd_cc_id_1,
            si_inventorydetail.invd_cc_id_2,
            si_inventorydetail.invd_cc_id_3,
            si_inventorydetail.invd_cc_id_4,
            si_inventorydetail.invd_cc_id_5",'si_inventorymaster,si_inventorydetail',[
                'si_inventorymaster.bg_code = si_inventorydetail.bg_code' => NULL,
                'si_inventorymaster.le_code = si_inventorydetail.le_code' => NULL,
                'si_inventorymaster.ou_code = si_inventorydetail.ou_code' => NULL,
                'si_inventorymaster.invm_year = si_inventorydetail.invm_year' => NULL,
                'si_inventorymaster.invm_period = si_inventorydetail.invm_period' => NULL,
                'si_inventorymaster.docs_doccode = si_inventorydetail.docs_doccode' => NULL,
                'si_inventorymaster.doctyp_code = si_inventorydetail.doctyp_code' => NULL,
                'si_inventorymaster.invm_no = si_inventorydetail.invm_no' => NULL,
                'si_inventorydetail.bg_code' =>  $key['bg_code'],
                'si_inventorydetail.le_code' =>  $key['le_code'], 
                'si_inventorydetail.ou_code' =>  $key['ou_code'], 
                'si_inventorydetail.invm_year' =>  $key['invm_year'], 
                'si_inventorydetail.invm_period' =>  $key['invm_period'], 
                'si_inventorydetail.docs_doccode' =>  $key['docs_doccode'], 
                'si_inventorydetail.doctyp_code' =>  $key['doctyp_code'], 
                'si_inventorydetail.invm_no' =>  $key['invm_no'],
                'si_inventorydetail.invd_returnqty >' => 0
            ]
            ); 
        $this->db->delete('view_trans_in_out',[
                'bg_code' => $key['bg_code'],
                'le_code' => $key['le_code'],
                'ou_code' => $key['ou_code'],
                'c_year' => $key['invm_year'],
                'c_period'  => $key['invm_period'],
                'doc_code' => $key['docs_doccode'],
                'doc_type_code' => $key['doctyp_code'],
                'doc_no' => $key['invm_no']]);
        $this->db->delete('view_trans_in',[
                'bg_code' => $key['bg_code'],
                'le_code' => $key['le_code'],
                'ou_code' => $key['ou_code'],
                'c_year' => $key['invm_year'],
                'c_period'  => $key['invm_period'],
                'doc_code' => $key['docs_doccode'],
                'doc_type_code' => $key['doctyp_code'],
                'doc_no' => $key['invm_no']]
        );
        if ($trans_type == 'O') {
            $this->db->delete('view_trans_out',[
                    'bg_code' => $key['bg_code'],
                    'le_code' => $key['le_code'],
                    'ou_code' => $key['ou_code'],
                    'c_year' => $key['invm_year'],
                    'c_period'  => $key['invm_period'],
                    'doc_code' => $key['docs_doccode'],
                    'doc_type_code' => $key['doctyp_code'],
                    'doc_no' => $key['invm_no']]
            );
        }
        switch ($action) {
            case 'add':
            case 'update':
            if(count($result)){
                
                if ($trans_type == 'O') {
                
                    $this->db->insert_batch('view_trans_out',$result);
                    $view_trans_in_out = array_map(function($arr){
                    return $arr + ['trans_type' => 'O'];
                    }, $result);
                }
                else{
                    $this->db->insert_batch('view_trans_out',$result);
                    $view_trans_in_out = array_map(function($arr){
                    return $arr + ['trans_type' => 'I'];
                    }, $result);
                    $this->db->insert_batch('view_trans_in',$result);
                }
                $this->db->insert_batch('view_trans_in_out',$view_trans_in_out);
            }
            break;
        }
    }
/*Left Join analysis*/
public function setJoinAnalysis($getData,$table_ref='',$analysis='invd_cc_id_',$bg_code = 'bg_code' , $setJoin = 'INNER')
{
  $join = '';
  for ($i=1; $i <= 5 ; $i++) 
   {
      $SetAn = is_array($analysis) ?  $analysis[$i] : $analysis.$i;
      if(isset($getData['an_level_'.$i]) && !empty($getData['an_level_'.$i]))
        {
          if(MX_Controller::SuperAdmin != $this->session->userdata('role_id'))
            {
               
               $join.=" 
                       ".$setJoin." JOIN view_allowed_analysis as anal".$i."
                       ON ( 
                            anal".$i.".bg_code      = ".$table_ref.".".$bg_code." AND 
                            anal".$i.".an_code      = ".$table_ref.".".$SetAn." AND
                            anal".$i.".usr_id       = ".$this->session->userdata('id')." AND
                            anal".$i.".an_sequence  = ".$i."
                          )
                     ";
            }
            else if ((MX_Controller::SuperAdmin == $this->session->userdata('role_id')) && ($this->uri->segment(2) == 'analysisLedgerPrint' || $this->uri->segment(1) == 'PurposeBalanceIncome'))
            {
                    $join.=" 
                        ".$setJoin." JOIN view_allowed_analysis as anal".$i."
                        ON ( 
                            anal".$i.".bg_code      = ".$table_ref.".".$bg_code." AND 
                            anal".$i.".an_code      = ".$table_ref.".".$SetAn." AND
                            anal".$i.".an_sequence  = ".$i."
                        )
                    ";
            }
        }
    }
    return $join;
}
/*Left Join analysis*/
public function get_analysisDrop($getData,$table_ref='',$analysis='invd_cc_id_')
{
   $condition = FALSE;
   $where='';
   for ($i=1; $i <= 5 ; $i++) 
   {
     $SetAn = is_array($analysis) ?  $analysis[$i] : $analysis.$i;
     if(isset($getData['analysis_from'.$i]) && !empty($getData['analysis_from'.$i]))       
     {
        $range = strlen($getData['analysis_from'.$i][0]);

        if(isset($getData['an_level_'.$i]) && !empty($getData['an_level_'.$i]) && $getData['an_level_'.$i] !=5)
        {
            if($condition === TRUE)
            { 
               $implode = implode(',',$getData['analysis_from'.$i]);
               $wherein = [];
               foreach ($getData['analysis_from'.$i] as $value) 
               {
                   $wherein['analysis_from'.$i][] = str_pad($value, 14, '0', STR_PAD_RIGHT);
               }
               $where.=" AND ".$table_ref." ".$SetAn." IN (".$implode = implode(',',$wherein['analysis_from'.$i]).")";
            }
            else
            {
               $where.=" AND SUBSTRING(".$table_ref." ".$SetAn.",1,".$range.") IN (".implode(',',$getData['analysis_from'.$i]).") ";
            }
        }
        else
        {
            $where.=" AND ".$table_ref." ".$SetAn." IN (".implode(',',$getData['analysis_from'.$i]).") ";
        }
     }

     if(isset($getData['analysis_from_min'.$i]) && !empty($getData['analysis_from_min'.$i]))
     {
        $range = strlen($getData['analysis_from_min'.$i]);
        if($getData['analysis_from_min'.$i] > $getData['analysis_to'.$i])
        {
            $minCode = $getData['analysis_to'.$i];
            $maxCode = $getData['analysis_from_min'.$i];
        }
        else
        {
           $minCode = $getData['analysis_from_min'.$i];
           $maxCode = $getData['analysis_to'.$i]; 
        }
        if(isset($getData['an_level_'.$i]) && !empty($getData['an_level_'.$i]) && $getData['an_level_'.$i] !=5)
        {
            // (SUBSTRING(vd_analysis1,1,$count) BETWEEN '01' AND '01')
            if($condition === TRUE)
            {   
                $where.=" AND ".$table_ref." ".$SetAn." BETWEEN '".str_pad($minCode, 14, '0', STR_PAD_RIGHT). "' AND '".str_pad($maxCode, 14, '9', STR_PAD_RIGHT)."'";
            }
            else
            {
                $where.= " AND (SUBSTRING(".$table_ref." ".$SetAn." ,1,".$range.") BETWEEN '".$minCode."' AND '".$maxCode."')";
            }
        }
        else
        {
            $where.=" AND ".$table_ref." ".$SetAn." BETWEEN '".$minCode. "' AND '".$maxCode."'";   
        }
     }
   }
   return $where;
 }
 
 /*SMS Operation*/
private function getSmsAPI() 
    {
        $smsApi = $this->db->select('*')->get('sms_api')->result();
        if(!empty($smsApi)) {
            $smsApi = $smsApi[0];
        }
        return $smsApi;
    }

private function actionSmsAPI($number, $message , $time) 
    {
        $dialingCode = '92';

        $smsApi = $this->getSmsAPI();
        if(empty($smsApi)) {
            echo "API secret key not found.";
            return;
        }

        $url = $smsApi->url . "/api/broadcast/message";
        $postData = "secret_token=" . $smsApi->api_secret_key . "&country_code=" . $dialingCode . "&mobile=" . $number . "&message=" . $message;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $time);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        //  It is to remove verification for https so add following parameters to skip verification from only localhost development machine.
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

public function send_SMS($number, $message , $time = 2000)
    {
        $is_mode = $this->db->select('development_mode')->get_where('sms_config', array('is_enable' => 1), 1)->row();
        if( $is_mode && (bool) $is_mode->development_mode == TRUE)
        {
            return true;
        }

        return $this->actionSmsAPI($number, $message , $time);
    }
/*SMS Operation*/

public function DateGetFilter($get, $table,$prameter){

	    $GetFromDate = $get['from_date'];
	    $GetToDate = $get['to_date'];
	    $AddWhere = "";
	    if(!empty($GetFromDate)){
	        
	        $GetFromDate =  SQLDate($GetFromDate);
	        $AddWhere .=  " AND DATE($table.$prameter) >= '$GetFromDate'";
	    }
	    if(!empty($GetToDate)){
	        $GetToDate = SQLDate($GetToDate);
	        $AddWhere .=  " AND DATE($table.$prameter) <= '$GetToDate'";
	    }
	    return $AddWhere;
	}
	
	public function MisDateFilter($get, $table,$prameter,$all_date=false){
	    
	    $GetFromDate = $get['from_date'];
	    $GetToDate = $get['to_date'];
	    $AddWhere = "";
	    if(!empty($GetFromDate)){	        
	        $GetFromDate =  SQLDate($GetFromDate);
	    }
	    if(!empty($GetToDate)){
	        $GetToDate = SQLDate($GetToDate);	    
	    }
	    
	    
	    if($GetFromDate && $GetToDate){
	    $AddWhere = " AND  DATE($table.$prameter) BETWEEN '".$GetFromDate."' AND '".$GetToDate."' ";
	    if($all_date){
	        $AddWhere =" AND ( DATE($table.$prameter) BETWEEN '".$GetFromDate."' AND '".$GetToDate."'
                          OR DATE(fpc.fpc_on_air_date) BETWEEN '".$GetFromDate."' AND '".$GetToDate."'
                          OR DATE(fpc.fpc_off_air_date) BETWEEN '".$GetFromDate."' AND '".$GetToDate."' )";
	    }
	    }
	    
	    return $AddWhere;
	}

    public function MisDateFilterForFpcComments($get, $table,$prameter,$all_date=false){

        $GetFromDate = $get['from_date'];
        $GetToDate = $get['to_date'];
        $AddWhere = "";
        if(!empty($GetFromDate)){
            $GetFromDate =  SQLDate($GetFromDate);
        }
        if(!empty($GetToDate)){
            $GetToDate = SQLDate($GetToDate);
        }


        if($GetFromDate && $GetToDate){
            $AddWhere = " AND  DATE($table.$prameter) BETWEEN '".$GetFromDate."' AND '".$GetToDate."' ";
            if($all_date){
                $AddWhere =" AND  DATE(fpc.fpc_on_air_date) BETWEEN '".$GetFromDate."' AND '".$GetToDate."'";
            }
        }

        return $AddWhere;
    }

    public function disAllowOuToAllUsersById($id,$table)
    {
        if(is_array($id)){
            $ouFIlter = "ou.id IN(".implode(',',$id).")";
        }else{
            $ouFIlter = "ou.id = '".$id."'";
        }

        if($table=='operating_unit'){
            $codeSelector = 'ou.code as ou_code,';
            $insertTable = 'disallowed_operatingunit';
        }else{
            $codeSelector = 'ou.dvn_code,';
            $insertTable = 'disallowed_division';
        }

        $codeSelector = ($table=='operating_unit')?'ou.code as ou_code,':'ou.dvn_code,';
        $data = $this->db->query("SELECT
               ou.bg_code,
               ".$codeSelector."
               (SELECT
               login.id
               FROM
               login
               WHERE id = usr.id) AS usr_id,
               '0' AS created_by
               FROM
               ".$table." ou
               INNER JOIN login AS usr
               ON 1 = 1  AND role_id!=1
               WHERE ".$ouFIlter)->result_array();

        if($data)
        {
            $this->db->insert_batch($insertTable, $data);
        }
    }
    public function gaFpcDetail($fpc_code){
        
        $where = array(
            'mis_graphics_animation.bg_code' => $this->session->userdata('bg_code'),
            'mis_fpc_ga.ga_code = mis_graphics_animation.ga_code' => null,
            'mis_fpc_ga.fpc_code' => $fpc_code
        );
        
        $this->db->select('mis_fpc_ga.*,mis_graphics_animation.title');
        $this->db->from('mis_fpc_ga,mis_graphics_animation');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function commandAct($data,$table,$where=[]){
          if(!empty($where)){
            $data['modified_by']  = $this->session->userdata('id');
            $data['modified_date']= date('Y-m-d H:i:s');
          }else{
            $data['created_by']  = $this->session->userdata('id');
            $data['created_date']= date('Y-m-d H:i:s');
          }
         if(!empty($where)){
            $this->db->where($where);
            $this->db->update($table,$data);
         }else{
            $this->db->insert($table,$data);
         }

    }

}
