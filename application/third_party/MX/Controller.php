<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require_once __DIR__.'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller {

	public $autoload = array();

	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));

		log_message('debug', $class.' MX_Controller Initialized');

		Modules::$registry[strtolower($class)] = $this;

		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	

		/* autoload module items */
		$this->load->_autoloader($this->autoload);
	}

	public function __get($class)
	{
		return CI::$APP->$class;
	}
	// public function changeStatus(){
 //     $post=$this->input->post();
 //       if(!empty($post)){ 
 //            $id=decrypt($post['id']);
 //            $this->db->where('id',$id);
 //            $this->db->update($post['table'],['is_active'=>$post['active']]);
 //            $active = ($post['active']==1) ? ' Active Sucessfully' : 'In-active Sucessfully';
 //        echo json_encode($active);
 //        exit();
 //       }
 //  }
  	public function changeStatus($getData=[]){
  	
     $post= count($getData) ? $getData : $this->input->post() ;
     $this->load->model("Mdl_Login");
       if(!empty($post)){ 
            $id=decrypt($post['id']);
            $this->Mdl_Login->commandAct([$post['column']=>$post['active']],$post['table'],[$post['table_key']=>$id]);
            $active = 'Active Sucessfully';
            if($post['active']==0){
				$active = 'In-active Sucessfully';
            }else if($post['active']==2){
 				$active = 'Deleted Successfully';
            }  
        echo json_encode($active);
        exit();
       }
  }
}
