<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/



$route['search'] = 'Home/search';
$route['testing'] = 'Home/testing';
$route['Cart/Order'] = 'Home/Cart/Order';
// $route['Detail/:num'] = 'Home/Detail';
$route['contact'] = 'Home/contact';
$route['checkout'] = 'Home/checkout_form';
$route['wishlist'] = 'Home/wishlist';
$route['frequently-asked-questions'] = 'Home/faq';
$route['about-us'] = 'Home/aboutus';
$route['shop-cart'] = 'Home/shop_cart';
$route['my-order-list'] = 'Home/my_order_list';
$route['my-dashboard'] = 'Home/my_dashboard';
$route['my-invoice-list'] = 'Home/my_invoice_list';

$route['my-invoice-detail'] = 'Home/my_invoice_detail';

$route['my-payment-list'] = 'Home/my_payment_list';
$route['my-profile'] = 'Home/my_profile';
$route['my-order-tracking'] = 'Home/my_order_tracking';

$route['my-order-detail'] = 'Home/my_order_detail';
$route['my-order-detail/(:any)'] = 'Home/my_order_detail/$1';

$route['login'] = 'Login/index';
$route['login/(:any)'] = 'Login/index';
$route['customer-login'] = 'Login/index';
$route['admin-login'] = 'Login/index';

$route['Dashboard/Our-Customer'] = 'Dashboard/Our_Customer';

$route['delete_to_cart/(:any)'] = 'Home/delete_to_cart/$1';

$route['bundle-offer'] = 'Home/bundle_offer';
$route['new-arrival'] = 'Home/new_arrival';
$route['search-product'] = 'Home/search_product';
$route['policy'] = 'Home/policy';
$route['policy/(:any)'] = 'Home/policy/$1';
$route['collections'] = 'Home/collections';
$route['collections/(:any)'] = 'Home/collections/$1';

$route['product-details'] = 'Home/product_details';
$route['product-details/(:any)'] = 'Home/product_details/$1';

$route['brand'] = 'Home/brand';
$route['brand/(:any)'] = 'Home/brand/$1';
$route['VendorSubmit'] = 'Home/VendorSubmit';
// $route['Search'] = 'Home/Product';
$route['privacy-policy'] = 'Home/policy';
$route['about-us'] = 'Home/aboutus';
$route['term-condition'] = 'Home/termCondition';
$route['shipping'] = 'Home/shipping';


$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
