<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
?>

<script type="text/javascript">
	   datatablelist("subCategory");

$(document).ready(function() {
  
  $('#page_url').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z-]/g,'') ); }
    );
});
function submitPR(){
     
	  $('.submitIRM').submit(function (e) {
             if($('.submitIRM')[0].checkValidity(e)){
                $('button[type="submit"]').prop('disabled',true);
                var url = "<?php echo base_url($controller.'/command'); ?>";
                 $.ajax({
                     url:url,
                     type:"post",
                     data:new FormData(this),
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                      success: function(data){
                        var data = JSON.parse(data);
                        if(data.status=='Error'){
                            toastr.error(data.msg, 'Alert !',{timeOut: 8000});
                           $('button[type="submit"]').prop('disabled',false);
                        }else if(data.status=='success'){
                            toastr.success(data.msg, 'Success !');
                              if($('#hidden_id').val()){
                                setTimeout(function () {
                                    window.location.href = '<?= base_url($controller) ?>';
                                },3000);
                                }else{
                                   $('#category').val('');
                                   $('#description').val('');
                                   $('button[type="submit"]').prop('disabled',false);
                                }
                        }
                   }
                });
             }
            e.preventDefault(); 
    });
    }
    submitPR();

        $(".submitIRM").validate({
            rules: {
                category: {
                    required:true,          
                },
                store_id : {
                    required:true,
                }
            },
            messages:{
         
            }
        });


</script>