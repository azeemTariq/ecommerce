<?php
$store = modules::run('Category/stores');
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];
    
}
$display =($view=='view') ? 'disabled' : '';

$showincollection= $row['showincollection'] ?? 1 ;

?>
<div class="container col-10 mt-4">
           <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Category"><button class="btn btn-outline-success mb-4 justify-content-center">show Category</button></a>
            </div>

            <div class="card card-success_">
              <div class="card-header">
                <h3 class="card-title text-success"><b>Add category</b></h3>
              </div>
              <div class="card-body">
                     <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitIRM" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                        <input type="hidden" id="hidden_id" name="id" value="<?= $id ?>">
           
                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Category title </label>
                     <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                       <input <?= $display ?> type="" class="required-field form-control" required id="category" placeholder="Add category" name="category" value="<?= $row['category_name'] ?? '' ?>">
                     </div>
                   </div> 
                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Page Url </label>
                     <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                       <input <?= $display ?> type="" class="required-field form-control" required id="page_url" placeholder="Page Url" name="page_url" value="<?= $row['page_url'] ?? '' ?>">
                     </div>
                   </div> 
                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Show in </label>
                     <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                         <label class="radio-inline">
                            <input <?= $display ?> <?php if($showincollection==1) echo "checked";  ?> value="1"  type="radio" name="showincollection" > Collection 1
                          </label>
                          <label class="radio-inline">
                            <input <?= $display ?>  <?php if($showincollection==2) echo "checked";  ?> value="2"  type="radio" name="showincollection"> Collection 2
                          </label>
                          <label class="radio-inline">
                            <input <?= $display ?> <?php if($showincollection==3) echo "checked";  ?> value="1"  type="radio" name="showincollection"> Collection 3
                          </label>
                     </div>
                   </div>
                     <div class="form-group row" style="display: none_;">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
                       <div class="col-sm-6">
                         <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
                          <div class="image col-md-12 col-lg-9 col-sm-12">
                            <?php $img= (isset($row['banner']) && !empty($row['banner']) ) ? base_url().'uploads/banners/'.$row['banner'] :  base_url().'assets/clip.png'  ?>
                            <img id="imgView" width="400" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
                          </div>
                       </div>
                       </div>

                       <div class="form-group row ">
                         <label for="profile_image" class="col-md-3 col-sm-12 col-lg-3">Product Banner / Cover</label>
                          <div>
                          
                           <div class="input-group " style="padding-left: 17px">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                          </div>
                          <div class="custom-file">
                          <input <?= $display ?> type="file" class="custom-file-input profile_image" name="profile_image" id="profile_image">
                          <label class="custom-file-label col-md-12" for="inputGroupFile01">Choose file</label></div></div>
                        </div>                        
                      </div>

           

                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Description </label>
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                      <textarea <?= $display ?> class="form-control"  placeholder="Add description" name="description"  id="description" height="200"><?= $row['description'] ?? '' ?></textarea>
                     </div>
                   </div>
            




                  <!--     <div class="form-group row " style="display: none_;">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3">file or image                     </label>
                        <div>
                        
                         <div class="input-group ">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                          <input <?= $display ?> type="file" class="custom-file-input" name="profile_image" id="profile_image">
                          <label class="custom-file-label col-md-8" for="inputGroupFile01">Choose file</label></div></div>
                        </div>
                      </div> -->
                  <?php if($view!='view') {  ?>     
                    <div class="card-footer">
                      <button type="submit" class="btn btn-outline-success">Submit</button>
                    </div>
                  <?php } ?>
                <!-- /.card-body -->
              </form>   
            </div>
          </div>
            <!-- /.card -->



            
 
</div>