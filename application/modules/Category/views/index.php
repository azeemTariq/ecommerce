
<div class="container-fluid"> 

  <?php if(permission('add')){ ?>
    <div class="text-right mb-3"> 
            <a href="<?php echo base_url() ?>Category/add"><button class="btn btn-outline-success mx-3 ">Add Category</button></a> 
     </div> 
  <?php } ?>
</div>
<div class="container-fluid">

          <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="subCategory" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col" class="no-search">S.No</th>
              <th scope="col">Category</th>
              <th scope="col" >Description</th>
              <th scope="col" class="no-search">Page Url</th>
              <th scope="col" class="no-search">Collection</th>
              <th scope="col" class="no-search">Actions</th>

            </tr>
          </thead>
          <tbody>
    
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>