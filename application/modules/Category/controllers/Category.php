<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {

  public function __construct() {

    modules::run('Login/login');
		parent::__construct();
    modules::run('login/login');
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_Category');
	}
    public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= "Category List";
      $data['content'] = 'Category/index.php';
      $this->load->view('Template/template',$data);
    }

    public function add(){
      
      $data['pageTitle']= "Category";
      $data['content'] = 'Category/action';
      $this->load->view('Template/template',$data);
    }
      

    public function category(){
      $this->db->select("*");
      $this->db->from('category');   
      $this->db->where('is_active',1); 
      return $this->db->get()->result_array();
    }  
     public function getsubcategory($id=''){
      $this->db->select("*");
      $this->db->from('subcategory');
      // $this->db->where('status',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
    } 
    public function getsubcategorybyref(){
      $post = $this->input->post();
      $this->db->select("*");
      $this->db->from('sub_category');
      $this->db->where('is_active ',1);

      if(isset($post['cat_id'])){
        $this->db->where('cat_id',$post['cat_id']);
      }
      $data=$this->db->get()->result();
      echo json_encode($data);
   
    } 
    public function getsubcatByCat($id='true'){
      $data=array();
      
      $this->db->select("*");
      $this->db->from('category');
      $this->db->where('is_active ',1);
      if($id=='true'){
         $data=$this->db->get()->result();
        echo json_encode($data);
        exit();
      }else{
        $this->db->where('id',$id);
         return  $data=$this->db->get()->result_array();
      }

      
    }    
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'category.id' ,
            'category_name' ,
            'description'
        );

       
   

        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('category');
        $this->db->where('category.is_active<>',2);

  

        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/action/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","category","Mdl_Category") href="#"><i class="far fa-trash-alt"></i></a>';
          }

            $row['action'].='</div>';
            


            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['category_name'];
            $nestedData[] = $row['description']; 
            $nestedData[] = $row['page_url'];
            $nestedData[] = "Collection ".$row['showincollection']; 
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function command(){
      $this->load->library('form_validation');
      // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $post=$this->input->post();
       $id = (!empty($post['id'])) ? decrypt($post['id']) : '';
       if(!empty($post)){ 
          $this->form_validation->set_rules('category', ' Category', 'required|max_length[25]|is_unique_update[category.category_name@id@' .$id. ']');
          $this->form_validation->set_rules('description', 'Description', 'max_length[100]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }

          // $image=do_upload($_FILES['profile_image'] ?? '','uploads/category/');
           $banner =do_upload($_FILES['profile_image'] ?? '','uploads/banners/',null,'profile_image');
           $Data=array(
            'category_name'    =>$post['category'],
            'showincollection'    =>$post['showincollection'],
            'page_url'    =>$post['page_url'],
            'description' =>$post['description']
           );
            if(!empty($banner)){
            $Data['banner'] =$banner;
           }
            if(!empty($post['id'])){
            $id=decrypt($post['id']);
            $this->Mdl_Category->commandAct($Data,'category',['id'=>$id]);
            $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
            $this->Mdl_Category->commandAct($Data,'category');
          }

           echo json_encode($_return);
           exit();
        }
        redirect(base_url($this->controller.'/'));
}

   public function action(){
     $data['pageTitle']= "Edit Category";
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getsubcatByCat($id);
      $data['content'] = 'Category/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }
}
