<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    modules::run('Login/login');
    $this->load->model('Mdl_Pages');
  }
  public $controller = __CLASS__;
    public function index(){
        $data['pageTitle']= "About Us";
        $data['content'] = 'Pages/about_us';
        $this->load->view('Template/template',$data);
    }
    public function add_policy($id="")
    {
        $content=$this->getContent('policies',['id'=>decrypt($id)]);
            
        if(!empty($content))
        {
            $data_1=(array)$content[0];
            $data['pageTitle']= $data_1['page_title'];
            $data['row']= $data_1;
            $data['content'] = 'Pages/add_policy';
            $this->load->view('Template/template',$data);
        }
        else
        {
            $data['pageTitle']= "Add Policy";
            $data['page']= "";
            $data['content'] = 'Pages/add_policy';
            $this->load->view('Template/template',$data);          
        }
    }
    public function policies()
    {
        $data_1=$this->getContent('policies',[]);
        $data['pageTitle']= "Policies";
        $data['page']= "policy";
        $data['data'] = $data_1;
        $data['content'] = 'Pages/policies';
        $this->load->view('Template/template',$data);
    }
     
    public function FAQs(){
        $data['pageTitle']= "Frequent Ques & Answers";
        $data['content'] = 'Pages/faqs';
        $this->load->view('Template/template',$data);
    }

      
     
public function faq_category_list(){
   return $this->Mdl_Pages->db_select('*','faq_category',['is_active'=>1]);
}
public function getContent($table,$where=[]){
    if($table=='tbl_faq'||$table=='policies'){
       return $this->Mdl_Pages->db_select('*',$table,$where);
    }
    return $this->Mdl_Pages->db_select_single_row('*',$table,$where);
}
    public function command(){
       $post=$this->input->post();
       $this->load->library('form_validation');

       if(isset($post['policy']))
       {
           $this->form_validation->set_rules('page_title', 'Page Title', 'required');
           $this->form_validation->set_rules('page_url', 'Page Url', 'required|is_unique_update[policies.page_url@id@' .($post['id'] ?? ''). ']');
           if ($this->form_validation->run() == FALSE){
                $_return['msg'] = validation_errors();
                $_return['status'] = "Error";
                $_return['error'] = 1;
                echo json_encode($_return);
                exit();
              }
        }
       if(isset($post['aboutus']) && $post['aboutus']==1){
          $this->loadAboutUsContent($post);
       }
       else if(isset($post['policy'])){
            if($post['id']==0)
            {
                $this->CreatePolicy($post);
            }
            else
            {
                $this->UpdatePolicy($post);
            }
       }else if(isset($post['faqs']) && $post['faqs']==1){
          $this->loadFAQS($post);
       }

    } 

   public function loadAboutUsContent($post){
          $data=array(
            'our_mission'     => $post['our_mission'],
            'our_vision'      => $post['our_vision'],
            'our_goal'        => $post['our_goal'],
            'our_journy'      => $post['our_journy'],
           );
          $_return = [];
          if(!empty($post['id'])){
           $_return=['status'=>'success','msg'=>'successfully Updated !'];
           $this->Mdl_Pages->commandAct($data,'about_us',['id'=> $post['id']]);
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
             $this->Mdl_Pages->commandAct($data,'about_us');
          } 
      echo json_encode($_return);
   }
    public function CreatePolicy($post){
        $array = [];
         foreach ($post['content'] as $key => $value) {
             $array[] = $value;
         }
          $data=array(
            'page_title'      => $post['page_title'],
            'content'         => json_encode($array),
            'page_url'        => $post['page_url'],
            'is_active'        => $post['is_active'],
           );
            $this->Mdl_Pages->commandAct($data,'policies');
            $_return=['status'=>'success','msg'=>'successfully Done !'];
          
      echo json_encode($_return);
   }
   public function loadFAQS($post){
          $this->db->truncate('tbl_faq'); 
         foreach ($post['ques'] as $key => $value) {
          $data=array(
            'ques'        => $value,
            'answer'      => $post['answer'][$key],
            'category'    => $post['category'][$key],
           );
            $_return=['status'=>'success','msg'=>'successfully Done !'];
             $this->Mdl_Pages->commandAct($data,'tbl_faq');
          } 
      echo json_encode($_return);
   }
   
  public function UpdatePolicy($post){
        $array = [];
        $this->Mdl_Pages->commandAct(['content'=>''],'policies',['id'=>$post['id']]);
         foreach ($post['content'] as $key => $value) {
             $array[] = $value;
         }
         $is_active=1;
         if(isset($post['is_active']))
            $is_active=$post['is_active'];
          $data=array(
            'page_title'      => $post['page_title'],
            'content'         => json_encode($array),
            'page_url'        => $post['page_url'],
            'is_active'        => $is_active,
           );
            $this->Mdl_Pages->commandAct($data,'policies',['id'=>$post['id']]);
            $_return=['status'=>'success','msg'=>'successfully Done !'];
          
      echo json_encode($_return);
   }    

}
