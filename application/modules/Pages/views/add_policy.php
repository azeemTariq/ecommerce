<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$display =($view=='view') ? 'disabled' : '';
?>
<style type="text/css">
  textarea{
    height:140px !important;
  }
</style>

<div class="container-fluid col-11 mt-4">

        
            <div class="card card-black">
              <div class="card-header">
                <h3 class="card-title text-success"><b><?= $pageTitle ?> Content</b></h3>
                <a href="<?php echo base_url() ?>Pages/policies"><button class="btn btn-outline-success mx-3 ">Back to List</button></a> 

              </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitForm" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                <input type="hidden" name="policy" value="">
                <input type="hidden" name="id" value="<?= $row['id'] ?? 0 ?>">
                <div class="card-body">
                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-2 col-sm-2 col-lg-2 col-xs-12"> Page Title </label>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                       <input <?= $display ?> type="" class="required-field form-control" required id="page_title" placeholder="Add Page title" name="page_title" value="<?= $row['page_title'] ?? '' ?>">
                     </div>
                   </div> 

                     <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-2 col-sm-2 col-lg-2 col-xs-12"> Page Url </label>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php if((isset($row['id']) && $row['id']>4)|| empty($row)){ ?>
                       <input type="" class="required-field form-control" required="" id="page_url" placeholder="Page Url" name="page_url" value="<?= $row['page_url'] ?? '' ?>">
                      <?php } else { ?>
                       <input type="" class="form-control" disabled="" value="<?= $row['page_url'] ?? '' ?>">
                       <input type="hidden"   id="page_url" placeholder="Page Url" name="page_url" value="<?= $row['page_url'] ?? '' ?>">
                      <?php } ?>
                     </div>
                   </div> 
                   <?php if((isset($row['id']) && $row['id']>4) || empty($row)) { ?>
                    <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-2 col-sm-2 col-lg-2 col-xs-12"> Active </label>
                     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                      <select class="form-control" name="is_active">
                        <option <?php if(isset($row['is_active']) && $row['is_active']==1) { echo "selected" ;} ?>  value="1">Active</option>
                        <option <?php if(isset($row['is_active']) && $row['is_active']==0) { echo "selected" ;} ?>   value="0">Inactive</option>
                      </select>
                     </div>
                   </div> 
                  <?php } ?>


                <div class="row">  
                    <div class="col-12 white-box">
                     <div class="table-responsive">
                        <table id="users" class="table table-bordered table-striped dataTable table_header">
                          <thead>
                            <tr>
                              <th scope="col">S.No</th>
                              <th scope="col"> Add <?= $pageTitle ?></th>
                              <th scope="col"><button  type="button" class="btn btn-primary" id="add_exercise"><i class="fa fa-plus"></i></button></th>
                            </tr>
                          </thead>
                          <tbody id="exercises">
                            <?php  if(isset($row)){  if(json_decode($row['content'])){ foreach (json_decode($row['content']) as $key => $value) {?>
                            <tr id="exercises-tr">
                            <td class="nameGroupCount"><?= ($key+1) ?></td>
                            <td><input type="text" class="form-control text-left" placeholder="<?= $pageTitle ?>"  name="content[]" value="<?= ($value) ?>"></td>
                            <td><button type="button" class="btn btn-default removeDetail"><i class="fa fa-trash"></i></button></td>
                          </tr>
                           <?php }} }else{ ?>
                          <tr id="exercises-tr">
                            <td class="nameGroupCount">1</td>
                            <td><input type="text" class="form-control text-left" placeholder="<?= $pageTitle ?>"  name="content[]" value=""></td>
                            <td><button type="button" class="btn btn-default removeDetail"><i class="fa fa-trash"></i></button></td>
                          </tr>
                            <?php }  ?>
                         
                          </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                     


                    <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                      <button type="submit" class="btn btn-primary" name="policies">Submit</button>
                    </div>

                 
                </div>
                 </form> 
                <!-- /.card-body -->

              
            
            </div>


            
</div>
         