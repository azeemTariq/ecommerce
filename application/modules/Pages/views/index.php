<div class="container-fluid"> 
    <div class="text-right mb-3"> 
      <?php if($this->session->userdata('role')==0){ ?>
            <a href="<?php echo base_url() ?>Users/add"><button class="btn btn-outline-success mx-3 ">Add User</button></a> 
      <?php } ?>

     </div> 
</div>
<div class="container-fluid">
  <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
  
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="users" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col" class="no-search">S.No</th>
              <th scope="col"> Name</th>
              <th scope="col"> Username</th>
              <th scope="col">Mobile</th>
              <th scope="col">Shop</th>
              <th scope="col">User Role</th>
              <th scope="col" class="no-search">User / Vendor Approval</th>
              <th scope="col" class="no-search">Active User</th>
               <th scope="col">Action</th>
              

            </tr>
          </thead>
          <tbody>
         
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>