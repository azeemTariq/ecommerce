<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$tbl_faq = modules::run('Pages/getContent','tbl_faq');
$faq_category = modules::run('Pages/faq_category_list');
$id = $this->uri->segment(4);
$row=[];

$display =($view=='view') ? 'disabled' : '';
?>
<style type="text/css">
  textarea{
    height:140px !important;
  }
</style>

<div class="container-fluid col-11 mt-4">

        
            <div class="card card-black">
              <div class="card-header">
                <h3 class="card-title text-success"><b>FAQ's Content</b></h3>
                

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitForm" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                <input type="hidden" name="id" value="<?= $tbl_faq['id'] ?? "" ?>">
                <input type="hidden" name="faqs" value="1">
                <div class="card-body">
                    <div class="row">
                      <div class="col-12 white-box">
                       <div class="table-responsive">
                          <table id="users" class="table table-bordered table-striped dataTable table_header">
                            <thead>
                              <tr>
                                <th scope="col">S.No</th>
                                <th scope="col">FAQ's Type</th>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                                <th scope="col"><button  type="button" class="btn btn-primary" id="add_exercise"><i class="fa fa-plus"></i></button></th>
                              </tr>
                            </thead>
                            <tbody id="exercises">
                              <?php $i=1 ; if(!empty($tbl_faq) && count($tbl_faq)){ foreach ($tbl_faq as  $value) { ?>
                              <tr id="exercises-tr">
                              <td class="nameGroupCount"><?= $i ?></td>
                              <td>
                                <select class="form-control select2 category" name="category[]" >
                                  <option value="">Select an Option</option>
                                  <?php foreach ($faq_category as  $val){  ?>
                                  <option <?= ($val->id==$value->category) ? 'selected' : '' ?> value="<?= $val->id ?>"><?= $val->title ?></option>
                                 <?php } ?>
                                  </select>
                              </td>
                             

                              <td>
                                <input type="" class="form-control text-left" placeholder="Add Question"  name="ques[]" value="<?= $value->ques ?>"></td>
                                <td><input type="" class="form-control text-left" placeholder="Add Answer"  name="answer[]" value="<?= $value->answer ?>">
                              </td>
                              <td><button type="button" class="btn btn-default removeDetail"><i class="fa fa-trash"></i></button></td>
                            </tr>
                             <?php $i++ ;}}else{ ?>
                            <tr id="exercises-tr">
                              <td class="nameGroupCount">1</td>
                               <td>
                                <select class="form-control select2 category" name="category[]" >
                                 <option value="">Select an Option</option>
                                  <?php foreach ($faq_category as  $val){  ?>
                                  <option value="<?= $val->id ?>"><?= $val->title ?></option>
                                 <?php } ?>
                                </select>
                              </td>
                              <td><input type="" class="form-control" placeholder="Add Question"  name="ques[]" value=""></td>
                              <td><input type="" class="form-control" placeholder="Add Answer"  name="answer[]" value=""></td>
                              <td><button type="button" class="btn btn-default removeDetail"><i class="fa fa-trash"></i></button></td>
                            </tr>
                              <?php } ?>
                           
                            </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                      <button type="submit" class="btn btn-primary" name="faqs">Submit</button>
                    </div>
                </div>
              </form> 
            <!-- /.card-body -->

              
            
            </div>


            
</div>
         