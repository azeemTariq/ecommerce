
<?php
$roles = modules::run('Role/getRole',true);
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];
    
}
$display =($view=='view') ? 'disabled' : '';
$pass = ($id) ? 'disabled' : '';



$param = '';
if($id){
  $param = $id;
}
?>

 
 <div class="container"> 
              <div class="text-right"> 
                <?php if($this->session->userdata('role')==0){ ?>
                  <a href="<?php echo base_url() ?>Users/index"><button class="btn btn-outline-success mx-1 justify-content-center">Users List</button></a> 
                <?php } ?>
                     <!--  <a href="<?php echo base_url() ?>Users/vendor"><button class="btn btn-outline-success mx-1 justify-content-center">Vendor List</button></a>  -->
             
        </div> 
      </div>
  <div class="container col-10 mt-4">


            <div class="card card-success_">
              <div class="card-header">
                <h3 class="card-title text-success"><b>Users Registration</b></h3>
                

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form"  action="<?php echo base_url($controller.'/command/'.$param)?>" method="POST" >
               
                <div class="card-body">

                   <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Name  </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                      <input <?= $display ?> type="" class="form-control"   placeholder="Enter Name" name="name" required value="<?= $row['name'] ?? '' ?>">
                      <div class="error"><?php echo form_error('name'); ?></div>
                    </div>
                     <input type="hidden" name="inputid" value="<?= $id ?>">
                  </div>

                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Username</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                       <input <?= $display ?> type="" class="form-control" id="exampleInputEmail1" placeholder="Enter Name" name="username" value="<?= $row['username'] ?? '' ?>">
                      <div class="error"><?php echo form_error('username'); ?></div>
                    </div>
                  </div>

                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">email address</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <input <?= $display ?> type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"
                    name="email" value="<?= $row['email'] ?? '' ?>">
                      <div class="error"><?php echo form_error('email'); ?></div>
                    </div>
                  </div>



                 
                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Role</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <select  <?= $display ?> class="select roleId  form-control select2" name="roleId" required>
                      <option>Select Role</option>
                      <?php foreach ($roles as $k => $value) { ?>
                            <option <?= (isset($row['roleId']) &&  $row['roleId']==$value['id'] ) ? ' selected' : ''; ?>     value="<?php echo $value['id']; ?>"><?php echo $value['role']; ?></option>
                      <?php } ?>
                    </select>
                      <div class="error"><?php echo form_error('roleId'); ?></div>
                    </div>
                  </div>   


                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Shop Name</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <input <?= $display ?> type="" class="form-control" id="shop" placeholder="Enter Shop Name"
                        name="shop" value="<?= $row['shop'] ?? '' ?>">
                      <div class="error"><?php echo form_error('shop'); ?></div>
                    </div>
                  </div>

                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Mobile #:</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <input <?= $display ?> type="" class="form-control" id="phone" placeholder="Enter Mobile # "
                        name="phone" value="<?= $row['phone'] ?? '' ?>">
                      <div class="error"><?php echo form_error('phone'); ?></div>
                    </div>
                  </div>

                  <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Address : </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <input <?= $display ?> type="" class="form-control" id="address" placeholder="Enter address Here"
                        name="address" value="<?= $row['address'] ?? '' ?>">
                      <div class="error"><?php echo form_error('address'); ?></div>
                    </div>
                  </div>

                 
                
                  <div class="form-group row">
                    <label for="colFormLabelSm" class="col-md-2 col-sm-3 col-lg-2 col-xs-12">Password here</label>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <input <?= $display ?> type="password" <?= $pass ?> size="8" class="form-control" id="password" placeholder="Password" name="password" required value="<?= $row['password'] ?? '' ?>">

                    <div class="error"><?php echo form_error('password'); ?></div>
                    </div>
                    <?php if($pass!=''){ ?>
                    <div class="col-lg-3 col-md-3">
                        <label class="switch">
                          <input type="checkbox" <?= $display ?> onchange="passwardActive(this)" >
                          <span class="slider round"></span>
                        </label>
                   </div>
                    <?php } ?>
                  </div>

 

                  <div class="form-check">
                     <input type="checkbox" <?= (isset($row['add']) &&  $row['add']==1 ) ? ' checked' : ''; ?>  class="form-check-input" <?= $display ?> id="exampleCheck1" name="add" value="1">
                    <label class="form-check-label" for="exampleCheck1">Add</label>

                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <input type="checkbox" <?= (isset($row['view']) &&  $row['view']==1 ) ? ' checked' : ''; ?> class="form-check-input"  <?= $display ?> id="exampleCheck1" name="view" value="1">
                    <label class="form-check-label" for="exampleCheck1">view</label>


                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <input type="checkbox" <?= (isset($row['edit']) &&  $row['edit']==1 ) ? ' checked' : ''; ?> class="form-check-input" <?= $display ?> id="exampleCheck1" name="edit" value="1">
                    <label class="form-check-label" for="exampleCheck1">edit</label>


                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <input type="checkbox" <?= (isset($row['delete']) &&  $row['delete']==1 ) ? ' checked' : ''; ?> class="form-check-input" <?= $display ?> id="exampleCheck1" name="delete" value="1">
                    <label class="form-check-label" for="exampleCheck1">delete</label>


                  </div>
                 
                 <div class="form-check mt-4">
                  
                   <div class="col-md-12  bt-switch">
                    <input type="checkbox" <?= $display ?>  name="is_active"class="changeStatus" <?= (isset($row['is_active']) &&  $row['is_active']==1 ) ? ' checked' : ''; ?> data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
                  </div>

                

                </div>

                
                <!-- /.card-body -->

                <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                  <button type="submit" class="btn btn-outline-success">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
 
</div>