<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$Pages = modules::run('Pages/getContent','about_us');
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];

}
$display =($view=='view') ? 'disabled' : '';
?>
<style type="text/css">
  textarea{
    height:140px !important;
  }
</style>

<div class="container-fluid col-11 mt-4">

        
            <div class="card card-black">
              <div class="card-header">
                <h3 class="card-title text-success"><b>About Us Content</b></h3>
                

              </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitForm" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                <input type="hidden" name="id" value="<?= $Pages['id'] ?? "" ?>">
                <input type="hidden" name="aboutus" value="1">
                <div class="card-body">



                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Our Journy</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <textarea class="form-control" placeholder="Our Journy" name="our_journy"><?=$Pages['our_journy'] ?? ""?></textarea> 
                       </div>
                     </div>  
       

                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Our Mission  </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <textarea  class="form-control" placeholder="Our Mission" name="our_mission"><?=$Pages['our_mission'] ?? ""?></textarea>
                       <label for="store" class="error" ></label>
                       </div>
                     </div>

                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Our Vision </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <textarea  class="form-control" placeholder="Our Vision"  name="our_vision"><?=$Pages['our_vision'] ?? ""?></textarea> 
                       </div>
                     </div>                     
             

                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Our Goal</label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <textarea class="form-control" placeholder="Our Goal" name="our_goal"><?=$Pages['our_goal'] ?? ""?></textarea> 
                       </div>
                     </div>  


                 

           



                     


                    <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                      <button type="submit" class="btn btn-primary" name="aboutus">Submit</button>
                    </div>

                 
                </div>
                 </form> 
                <!-- /.card-body -->

              
            
            </div>


            
</div>
         