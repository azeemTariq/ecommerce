<?php
$controller = $this->uri->segment(1);
$Customaction = $this->uri->segment(2);

?>
<script type="text/javascript">

$(document).ready(function() {
$('#page_url').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z-]/g,'') ); }
    );
});

function submit(){
    $('.submitForm').submit(function (e) {
       if($('.submitForm')[0].checkValidity(e)){
        count = 0;
        if($('input[name="faqs"]').length==1){
           $.each($('.category'),function(i,elem){
               if($(elem).val()<1){
                 toastr.info("Category Not be NULL At Line No. "+(i+1), 'Info !',{timeOut: 3000});
                 count = 1;
                 return false;
               }
           });
        }

        if(count==1){
          return false;
        }

      var url = "<?php echo base_url($controller.'/command'); ?>";   
       $('button[type="submit"]').prop('disabled',true);  
         $.ajax({
             url:url,
             type:"post",
             data:new FormData(this),
             processData:false,
             crossOrigin:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                     var data = JSON.parse(data);
                        if(data.status=='Error'){
                            toastr.error(data.msg, 'Alert !',{timeOut: 8000});
                           $('button[type="submit"]').prop('disabled',false);
                        }else if(data.status=='success'){
                      toastr.success(data.msg, 'Success !');
                      if($('#hidden_id').val()){
                          setTimeout(function () {
                              window.location.href = '<?= base_url($controller) ?>';
                          },3000);
                      }else{
                             $('#product_name').val('');
                             $('#price').val('');
                             $('#expire').val('');
                             $('#description').val('');
                              //clear image src
                             $('#removearray').val('');
                             $('#array').val('');
                             $('.iui-close').trigger('click');
                             $('button[type="submit"]').prop('disabled',false);
                    }
                }
           }
         });

  }e.preventDefault(); 
});
}
submit();



$(".submitForm").validate({
    rules: {
        store_id: {
            required:true,
        },
        cat_id: {
            required:true,
           
        }, 
        product_name: {
            required:true,
           
        },
         price: {
            required:true,
           
        }
       
    },
    messages:{
 
    }
});




var clsi = $('#exercises tr').length;
$('#add_exercise').on('click', function() {
    clsi++;
        var $tr    = $('#exercises-tr:last-child');
         countin = $('#exercises tr').length+1;
          $clone = $tr.clone();
          $clone.find('.chosen-container-single, .select2 span, .select2-container--default').remove();
          $clone.find('.category').removeClass().addClass('select2 category select2-hidden-accessible');
          $clone.find('.category').eq(0).select2();
          $clone.find('td:eq(0)').html(clsi);
        $tr.after($clone);
        $('tr').find('.category').select2();
        remove();
       singleTrNotRemove($(".removeDetail").length,"true");
       let tr = $('#exercises tr');
       changeNameGroup(tr);
});
singleTrNotRemove($(".removeDetail").length,"true");


function changeNameGroup(tr,srno=1,name='nameGroup_'){
tr.each(function() {
    $(this).find("td:first").html($(this).find("td:first").html().replace(/[0-9]+/, srno));
    $(this).find(".nameGroupCount").val(srno);
    srno++;
});
}

function singleTrNotRemove(trs,f="false"){
    if( ( trs=='2' && f=='false' ) || (trs=='1' && f=='true') ){
     $(".removeEdit").css('display','none');
     $(".removeDetail").css('display','none');
    }else{
        $(".removeEdit").css('display','block');
        $(".removeDetail").removeAttr('style');
    }
}


function remove(){
  $('.removeDetail').on('click',function(){
      $(this).closest('tr').remove();
      singleTrNotRemove($(".removeDetail").length,"true");
      let tr = $('#exercises tr');
      changeNameGroup(tr);
  });
}
remove();


</script>