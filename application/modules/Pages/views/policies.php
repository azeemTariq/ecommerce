
<div class="container-fluid"> 
  <?php  if(permission('add')){ ?>
    <div class="text-right mb-3"> 
            <a href="<?php echo base_url() ?>Pages/add_policy"><button class="btn btn-outline-success mx-3 ">Add policy</button></a> 
     </div> 
   <?php }  ?>
</div>
<div class="container-fluid">
   <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="policies" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col">Title</th>
              <th scope="col">Page Url</th>
              <th scope="col" class="no-search">Active Item</th>
              <th scope="col" class="no-search">Actions</th>

            </tr>
          </thead>
          <tbody>
            <?php   foreach($data AS $d){ ?>
              <tr>
                <td scope="col"><?= $d->page_title ?></td>
                <td scope="col"><?= $d->page_url ?></td>
                <td scope="col" class="no-search"><?= $d->is_active==1 ? "Active" : "Inactive" ?></td>
                <td scope="col" class="no-search"><a href="<?= base_url('Pages/add_policy/'.encrypt($d->id))?>" class="btn btn-info">Edit</a></td>
              </tr>
              <?php } ?>
          </tbody>
          
      </table>
      </div>
    </div>
  </div>
</div>


   