<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

  public function __construct() {
		parent::__construct();
    modules::run('Login/login');
		$this->load->model('Mdl_Dashboard');
	}
 public $controller = __CLASS__;
	  public function index(){
      
      
	  	$data['pageTitle'] = 'Dashboard';
	
	  		$data['admin']= $this->Mdl_Dashboard->getOrderStatus();	
            $data['list']=  $this->Mdl_Dashboard->getStoreProduct();      		
	  		$data['content'] = 'Dashboard/index2.php';
	  
        $this->load->view('Template/template',$data);
    }

     public function summary()
     {   
     	 echo json_encode($this->getBarGraph());
     }
     public function Our_Customer(){
        $data['pageTitle'] = 'Customer List';
        $data['content'] = 'Dashboard/customers';
        $this->load->view('Template/template',$data);
     }
     public function getBarGraph(){

        if($this->session->userdata('role')==2){
                $data['result'] = $this->Mdl_Dashboard->getStoreProduct($this->session->userdata('id'));
        }else{
            $data['result'] = $this->Mdl_Dashboard->getStoreProduct();
        }
        
        $labels = array();
        $count = 0;
        foreach($data['result']['Approved'] as $key => $val){
            $labels[] = $val['category_name'];
            $dataSet1[$key] = 0;
            $dataSet2[$key] = 0;
        }
        
        // $dataSet1 = [0,0,0,0,0,0,0,0];
        // $dataSet2 = [0,0,0,0,0,0,0,0];

        if(isset($data['result']['Approved'])){

            foreach ($data['result']['Approved'] as $k=>$v ){
                $labels1[$k] =  $v['category_name'];
                $dataSet1[$k] = $v['total'];
            }
        }
        if(isset($data['result']['Pending'])){

            foreach ($data['result']['Pending'] as $k=>$v ){
                $labels2[$k] = $v['category_name'];
                $dataSet2[$k] = $v['total'];
            }
        }

        return array(
            'labels'=>json_encode($labels),
            'dataSet1'=>json_encode($dataSet1),
            'dataSet2'=>json_encode($dataSet2)
        );
    }
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array( 
            '',
            'username',
            'name',
            'email',
            'phone',
            'address',
            'created_date',
          );
        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('login');
        $this->db->where('login.is_customer',1);

        $i = 0;
        $where_filters='';
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                  $this->db->like($value, $requestData['columns'][$i]['search']['value']); 
            }
            $i++;
        }
       
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);

        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        $data = array();
        foreach ($query as $key => $row) {
            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['username'];
            $nestedData[] = $row['name'];
            $nestedData[] = $row['email'];
            $nestedData[] = $row['phone'];
            $nestedData[] = $row['address'];
            $nestedData[] = $row['created_date'];
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

}