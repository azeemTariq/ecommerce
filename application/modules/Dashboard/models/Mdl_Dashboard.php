<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_Dashboard extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
    }
    public  function getOrderStatus()
    { 
      return $this->db_select_single_row('
    		sum(if(status=1,1,0)) New,
    		sum(if(status>1 AND status<4,1,0)) Pending,
    		sum(if(status=4,1,0)) Completed,
    		sum(if(status=5,1,0)) Cancel,
    		','orders');
    }    

    public  function getOrderStatusVendor()
    { 
      return $this->db_select_single_row('
        sum(if(order_detail.status=0, 1, 0)) New,
        sum(if(order_detail.status=1, 1, 0)) Pending, 
        sum(if(order_detail.status=2, 1, 0)) Completed, 
        sum(if(order_detail.status=3, 1, 0)) Cancel,
        ','orders,order_detail,product','orders.id = order_detail.OrderId
         AND orders.code = order_detail.code
         AND order_detail.ItemId = product.id
         AND product.created_by="'.$this->session->userdata('id').'" ');
    }

    public function getStoreProduct($id=null){

          $where = '';
          $result = array('Approved'=>array(),'Pending'=>array());
          if($id){
             $where = ' AND itm.created_by = '.$id;
          }

        
          $this->db->select('SUM(if(itm.is_active=1,1,0)) total,category.category_name');
          $this->db->from('category');
          $this->db->join('product itm','itm.cat_id =category.id AND itm.is_active <> 2'.$where,'left');

   

          $this->db->group_by('category.id');
          $this->db->order_by('itm.id desc');

          $result['Approved'] = $this->db->get()->result_array();

          $this->db->select('SUM(if(itm.is_active=0,1,0)) total,category.category_name');
         $this->db->from('category');
          
          $this->db->join('product itm','itm.cat_id =category.id AND  itm.is_active <> 2'.$where,'left');

          $this->db->group_by('category.id');
          $this->db->order_by('itm.id desc');

          $result['Pending'] = $this->db->get()->result_array();

          return $result;

      }
    
}