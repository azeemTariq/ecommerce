

<script>
    var dashboardAdmin= <?php echo isset($admin) ?  json_encode($admin): '0' ?>;
     $(document).ready(function() {

   var sparklineLogin = function() { 

        }
         sparklineLogin();

});


function PieGraph(dataSet,divId,type='doughnut'){
    var array=[];
    var valueTitle= (divId=='top7Sales') ? 'Qty ' : 'Value';
     $.each( dataSet, function( i, value ) {
          array[i] =  {name:value.label,y:parseInt(value.value)};
    });
 Highcharts.chart(divId, {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 65,
            beta: 0
        }
    },
    title: {
        text:''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 11,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: valueTitle,
        data:array,
    }]
 });
}

function BarGraph(dataSet1,dataSet2,labels,title){
var array1=[];
var array2=[];
for(var i = 0; i<dataSet1.length; i++) {
   array1[i]={y: parseInt(dataSet1[i]) ,color: 'orange'};
   array2[i]={y: parseInt(dataSet2[i]) ,color: 'purple'};
}
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'salesVsStock',   //title[0],
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    title: {
        text: ''
    },
     xAxis: {
            categories:labels,
           
        },
    subtitle: {
        text: ''
    },
    plotOptions: {
        column: {
            depth: 25
        }
    },series: [{
            name:title[0],
            data:array1,
            color:'orange'
        },{
            name:title[1],
            data:array2,
            color:'purpule'
        }]
   
});
}


if(dashboardAdmin){
var   arr=[
  {label : 'New Orders' , value :dashboardAdmin.New },
  {label : 'Pending Orders' , value :dashboardAdmin.Pending },
  {label : 'Complete Orders' , value :dashboardAdmin.Completed },
  {label : 'Cancel Orders' , value :dashboardAdmin.Cancel }

  ];

  PieGraph(arr,'top7Purchase','doughnut');
}



// BarGraph([12,13,23,34],[10,43,23,44],["azeem",'kkk','gtd','b'],['Approved','Pending']);











function GraphSumary(){
  $.ajax({
    url:'<?php echo base_url('Dashboard/summary'); ?>',
    type:'POST',
    data:{role_id:1},
    dataType: "json",
    success:function(res){
 
   // console.log(res);
                labels = JSON.parse(res.labels);
                dataSet1 = JSON.parse(res.dataSet1);
                dataSet2 = JSON.parse(res.dataSet2);
                BarGraph(dataSet1,dataSet2,labels,['Approved','Pending']);
        
         
        }
         
     }); 
}
GraphSumary();

datatablelist("customerList");


</script>