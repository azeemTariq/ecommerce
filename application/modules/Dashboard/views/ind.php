<div class="container">
	
	<div class="row">
	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">New Orders</h3>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['New'] ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Pending Orders</h3>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Pending'] ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Complete Orders</h3>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Completed'] ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                <div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Cancel Orders</h3>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Cancel'] ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>

	</div>
	









	<!-- <div class="col-lg-3 ">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Total Stock In</h3>
	                        <ul class="list-inline two-part">
	                            <li>
	                                <div id="stock_in"></div>
	                            </li>
	                            <li class="text-right"><i class="text-success errow1"></i> <span class="sparklinedash_counter text-success" style="font-size: 14px">0</span></li>
	                        </ul>
	                    </div>
	                </div>
	
	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Total Stock Out</h3>
                        <ul class="list-inline two-part">
                            <li>
                                <div id="stock_out"></div>
                            </li>
                            <li class="text-right"><i class="text-purple errow2"></i> <span class="sparklinedash2_counter text-purple" style="font-size: 14px">0</span></li>
                        </ul>
                    </div>
                </div>
    
	<div class="col-lg-3">
	                    <div class="white-box analytics-info" style="height:100px">
	                        <h3 class="box-title h3">Total POS Sale</h3>
                        <ul class="list-inline two-part">
                            <li>
                                <div id="pos_sale"></div>
                            </li>
                            <li class="text-right"><i class="text-warning errow3"></i> <span class="sparklinedash3_counter text-warning" style="font-size: 14px">0</span></li>
                        </ul>
                    </div>
                </div>
      <div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Active Users</h3>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers">20</span>)</span></li>
	                          </ul>
	                    </div>
	                </div>

	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Total Stock In</h3>
	                        <ul class="list-inline two-part">
	                            <li>
	                                <div id="stock_in"></div>
	                            </li>
	                            <li class="text-right"><i class="text-success errow1"></i> <span class="sparklinedash_counter text-success" style="font-size: 14px">0</span></li>
	                        </ul>
	                    </div>
	                </div>
	
	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h3 class="box-title">Total Stock Out</h3>
                        <ul class="list-inline two-part">
                            <li>
                                <div id="stock_out"></div>
                            </li>
                            <li class="text-right"><i class="text-purple errow2"></i> <span class="sparklinedash2_counter text-purple" style="font-size: 14px">0</span></li>
                        </ul>
                    </div>
                </div>
    
	<div class="col-lg-3">
	                    <div class="white-box analytics-info" style="height:100px">
	                        <h3 class="box-title h3">Total POS Sale</h3>
                        <ul class="list-inline two-part">
                            <li>
                                <div id="pos_sale"></div>
                            </li>
                            <li class="text-right"><i class="text-warning errow3"></i> <span class="sparklinedash3_counter text-warning" style="font-size: 14px">0</span></li>
                        </ul>
                    </div>
                </div> -->
      


<div class="row mt-0 mb-4" style="display: none_;">
	 <div class="col-xs-12 col-md-6 col-sm-6"  style="height:417px;margin-top:2%;    margin-bottom: 64px;float: left;position: relative;">
             <div class="white-box" >

                 <h3 class="box-title">Order Status Chart</h3>

                 <div>
                 	  <div id="top7Purchase"> </div> 
                     <!-- <canvas id="top7Purchase" height="150"></canvas> -->
                 </div>
             </div>
      
		 </div>

		 	<div class=" col-xs-12 col-md-6 col-sm-6" style="height:417px;margin-top:2%;    margin-bottom: 64px;float: left;position: relative;" >
             <div class="white-box" >
                 <h3 class="box-title">Store Wise Product Status</h3>
                 <div>
                       <div id="salesVsStock"> </div>
                       <!-- <canvas id="salesVsStock" height="150"></canvas> -->
                    
                 </div>
             </div>
			
         </div>
</div>







<div class="row">
    <div class="col-12 white-box">
    	<h3 class="box-title text-center">Store Wise Product List</h3>
     <div class="table-responsive">
        <table id="OrderList" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Stores</th>
              <th scope="col">Approved</th>
              <th scope="col">Pending</th>
            </tr>
          </thead>
          <tbody>
          	<?php foreach($list['Approved'] as $i => $value){ ?>
          		<tr>
          		 <td ><?= $i+1 ?></td>
          		 <td ><?= $list['Approved'][$i]['store_name'] ?></td>
          		 <td ><?= $list['Approved'][$i]['total'] ?></td>
          		 <td ><?= $list['Pending'][$i]['total'] ?></td>
          		</tr>
            <?php } ?>
          </tbody>
          
      </table>
      </div>
    </div>
  </div>


</div>