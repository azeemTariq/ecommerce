<div class="container">
	
	<div class="row">
	<div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h4 class="box-title">New Orders</h4>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['New']?? '' ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                <div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h4 class="box-title">Ready To Delivered</h4>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Pending']?? '' ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                <div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h4 class="box-title">Complete Orders</h4>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Completed'] ?? '' ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>
	                <div class="col-lg-3">
	                    <div class="white-box analytics-info">
	                        <h4 class="box-title">Cancel Orders</h4>
	                        <ul class="list-inline two-part">
	                            <li  ><i  class="icon-people text-danger"></i></li>
	                            <li class="text-right"><span class="text-info" style="font-size:21px;text-align:right">
	                            	(<span id="activeUsers"><?= $admin['Cancel']?? '' ?></span>)</span></li>
	                          </ul>
	                    </div>
	                </div>

	</div>
	

<div class="row mt-0 mb-4" style="display: ;">
	 <div class="col-xs-12 col-md-6 col-sm-6"  style="height:417px;margin-top:2%;    margin-bottom: 64px;float: left;position: relative;">
             <div class="white-box" >

                 <h3 class="box-title">Order Status Chart</h3>

                 <div>
                 	  <div id="top7Purchase"> </div> 
                     <!-- <canvas id="top7Purchase" height="150"></canvas> -->
                 </div>
             </div>
      
		 </div>

		 	<div class=" col-xs-12 col-md-6 col-sm-6" style="height:417px;margin-top:2%;    margin-bottom: 64px;float: left;position: relative;" >
             <div class="white-box" >
                 <h3 class="box-title">Category Wise Product Status</h3>
                 <div>
                       <div id="salesVsStock"> </div>
                       <!-- <canvas id="salesVsStock" height="150"></canvas> -->
                    
                 </div>
             </div>
			
         </div>
</div>


<div class="row" style="display: ;">
    <div class="col-12 white-box">
    	<h3 class="box-title text-center">Category Wise Product List</h3>
     <div class="table-responsive">
        <table id="OrderList" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Category</th>
              <th scope="col">Active</th>
              <th scope="col">Inactive</th>
            </tr>
          </thead>
          <tbody>
          	<?php foreach($list['Approved'] as $i => $value){ ?>
          		<tr>
          		 <td ><?= $i+1 ?></td>
          		 <td class="text-left" ><?= $list['Approved'][$i]['category_name'] ?></td>
          		 <td ><?= $list['Approved'][$i]['total'] ?></td>
          		 <td ><?= $list['Pending'][$i]['total'] ?></td>
          		</tr>
            <?php } ?>
          </tbody>
          
      </table>
      </div>
    </div>
  </div>



</div>