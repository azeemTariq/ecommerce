<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    modules::run('Login/login');
    $this->load->model('Mdl_Account');
  }
  public $controller = __CLASS__;
    public function index(){
      $data['pageTitle']= "Users List";
        $data['content'] = 'Account/index.php';
        $this->load->view('Template/template',$data);
    }
    public function userRights(){

      $data['pageTitle']= "User Rights";
      $data['result'] = $this->getOnlyUsers();
      $data['content'] = 'Account/Rights.php';
       $this->load->view('Template/template',$data);

    }    
    public function profile(){

      $data['pageTitle']= "Edit Profile";
      $data['content'] = 'Account/profile.php';
       $this->load->view('Template/template',$data);

    }
    public function command(){
       $post = $this->input->post();
       foreach ($post['users'] as $key => $value) {
          $data = array(
            'add'    =>$post['add'][$key],
            'edit'   =>$post['edit'][$key],
            'view'   =>$post['view'][$key],
            'delete' =>$post['delete'][$key],
          );
          $this->db->where('id',$value);
          $this->db->update('login',$data);
       }
        $this->session->set_flashdata('saved', 'successfully Updated');
        redirect(base_url() . 'Account/userRights', 'refresh');
    }

  public function assign() {
        $data['pageTitle'] = 'Assign Menus to Users';
        $data['content'] = 'Rights_Management_Setup_Users/Rights_Management_Setup_Assign_Menus';
        $data_tmp = array("id" => $this->uri->segment(3));
        if (MX_Controller::SuperAdmin != $this->session->userdata('role_id')) {
            foreach (json_decode($this->session->userdata('menu_permission')) as $key => $value) {
                $menu_permission_tmp[$key] = $value;
            } $menu_permission = $menu_permission_tmp;
            $permission = $menu_permission;
            $id = implode(", ", array_keys($permission));
            $data['admin_module_list'] = $this->db->from('admin_module_list')->where("is_enable=1 AND `id` IN ($id)")->get();
        } else {
            $data['admin_module_list'] = $this->Mdl_rights_Management_Setup_Users->get_menus_by_id();
        }
        $data['admin_module_list'] = $data['admin_module_list'];
        $data['admin_roles_list'] = $this->Mdl_rights_Management_Setup_Users->get_rights_by_id($data_tmp);
        $data['admin_roles_list'] = $data['admin_roles_list']->result();


        if (count($data['admin_roles_list']) >= 1) {
            $data['admin_roles_menu_list'] = $data['admin_roles_list'][0]->menus_on;
            $data['UserTitle'] = $data['admin_roles_list'][0]->name;
            $data['admin_roles_menu_list'] = json_decode($data['admin_roles_menu_list']);

            $data['admin_roles_button_list'] = $data['admin_roles_list'][0]->buttons_on;
            $data['admin_roles_button_list'] = json_decode($data['admin_roles_button_list']);
        }
        $data['admin_button_list'] = $this->Mdl_rights_Management_Setup_Users->get_all_buttons();
        $data['admin_button_list'] = $data['admin_button_list'];
        $this->load->view('Template/template', $data);
    }
      public function assignrights() {
        $data['pageTitle'] = 'Assign Menus to Users';

        $post = $this->input->post();
        $buttons_off = "";
        $menus_off = "";

        $all_menus = $this->Mdl_rights_Management_Setup_Users->get_all_menus();
        foreach ($all_menus->result() as $row)
            $all_menus_tmp[] = $row->id;
        $all_menus = $all_menus_tmp;
        $menus_off = array();
        if (isset($post['menus_on'])) {
            for ($a = 0; $a < count($all_menus); $a++) {

                $check = 0;
                foreach ($post['menus_on'] as $row) {
                    if ($all_menus[$a] == $row)
                        $check = 1;
                }
                if ($check == 0) {
                    $menus_off[] = $all_menus[$a];
                }
            }
            if (isset($menus_off))
                $menus_off = json_encode($menus_off);
        }
        else {
            $menus_off = "";
        }
        if (isset($post['menus_on']))
            $menus_on = json_encode($post['menus_on']);
        else
            $menus_on = "";




        $all_buttons = $this->Mdl_rights_Management_Setup_Users->get_all_buttons();
        foreach ($all_buttons->result() as $row)
            $all_buttons_tmp[] = $row->id;
        $all_buttons = $all_buttons_tmp ?? "";
        $buttons_off = array();
        if (isset($post['buttons_on'])) {
            for ($a = 0; $a < count($all_buttons); $a++) {

                $check = 0;
                foreach ($post['buttons_on'] as $row) {
                    if ($all_buttons[$a] == $row)
                        $check = 1;
                }
                if ($check == 0) {
                    $buttons_off[] = $all_buttons[$a];
                }
            }
            if (isset($buttons_off))
                $buttons_off = json_encode($buttons_off);
        }
        else {
            $buttons_off = "";
        }


        if (isset($post['buttons_on']))
            $buttons_on = json_encode($post['buttons_on']);
        else
            $buttons_on = "";




        $data1 = array(
            "id" => $post['id'],
            "menus_on" => $menus_on,
            "menus_off" => $menus_off,
            "buttons_on" => $buttons_on,
            "buttons_off" => $buttons_off
        );

        $db_command = $this->Mdl_rights_Management_Setup_Users->assign_menus_buttons($data1);


        $data['content'] = 'Rights_Management_Setup_Users/Rights_Management_Setup_Assign_Menus';
        $data_tmp = array(
            "id" => $post['id']
        );
        $data['admin_roles_list'] = $this->Mdl_rights_Management_Setup_Users->get_rights_by_id($data_tmp);
        $data['admin_roles_list'] = $data['admin_roles_list']->result();

        if (count($data['admin_roles_list']) >= 1) {
            $data['admin_roles_menu_list'] = $data['admin_roles_list'][0]->menus_on;
            $data['UserTitle'] = $data['admin_roles_list'][0]->name;
            $data['admin_roles_menu_list'] = json_decode($data['admin_roles_menu_list']);

            $data['admin_roles_button_list'] = $data['admin_roles_list'][0]->buttons_on;
            $data['admin_roles_button_list'] = json_decode($data['admin_roles_button_list']);
        }



        $data['admin_module_list'] = $this->Mdl_rights_Management_Setup_Users->get_menus_by_id();
        $data['admin_module_list'] = $data['admin_module_list'];

        $data['admin_button_list'] = $this->Mdl_rights_Management_Setup_Users->get_all_buttons();
        $data['suptyp_code'] = $data['admin_button_list'];





        if ($db_command == 1) {
            $this->session->set_flashdata('saved', 'your data successfully Saved');
            redirect(base_url() . 'Users', 'refresh');
        } else {
            $this->session->set_flashdata('Error', 'your data have not saved due to some error');
            $this->load->view('Template/template', $data);
        }
    }


        public function getOnlyUsers(){
          $this->db->select("*");
          $this->db->from('login');
          $this->db->where('is_active <> 2'); /*<> not equal to*/
          $this->db->where('roleId',1);
          return $this->db->get()->result_array();

        }



}
