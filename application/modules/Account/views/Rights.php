
<div class="container">
  <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
    <div class="col-12">
      <form action="<?php echo base_url() ?>Account/command" method="POST">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">
              <!-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="customCheckAll" checked>
                  <label class="custom-control-label" for="customCheckAll">S.No</label>
              </div> -->
              <label class="control-label" >S.No</label>
            </th>
            <th scope="col">Users Name</th>
            <th scope="col">Add</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
            <th scope="col">View</th>
          </tr>
        </thead>
        <tbody>
          <?php

          foreach ($result as $i => $value) { $count=1; 

            /*if (!empty($_GET['all_checked']))
              $all_checked = true;

          for ($i = 0; $i < 10; $i++)
              if ($all_checked)
                  echo '<input type="checkbox" checked="checked">';
              else
                  echo '<input type="checkbox">';*/
             
            ?> 
              


            <!-- for each loop  -->
                               
                        
          <tr>
            <td>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox"  class="custom-control-input subList" id="customCheck<?= $i ?>" checked>
                  <label class="custom-control-label" for="customCheck<?= $i ?>"></label>
                  <input type="hidden" name="users[]" value="<?= $value['id'] ?>">
              </div>
            </td>

            <td class="text-left"><?= $value['name'] ?></td>
            <?php  
            $set = $i."-".$count;?>
            <td><div class="custom-control custom-checkbox">
                  <input type="checkbox"   <?= ($value['add']==1) ? 'checked' : ''  ?>  onclick="unchecked(this)" class="custom-control-input usefor" id="customCheck<?= $set ?>">
                  <label class="custom-control-label" for="customCheck<?= $set ?>"></label>
                   <input type="hidden" class="setValue" name="add[]" value="<?= $value['add'] ?>">
              </div></td>
            <?php $count++; 
            $set = $i."-".$count;?>
            <td><div class="custom-control custom-checkbox">
                  <input type="checkbox" <?= ($value['edit']==1) ? 'checked' : ''  ?> onclick="unchecked(this)" class="custom-control-input usefor" id="customCheck<?= $set ?>" >
                  <label class="custom-control-label" for="customCheck<?= $set ?>"></label>
                   <input type="hidden" class="setValue" name="edit[]" value="<?= $value['edit'] ?>">
              </div></td>
             <?php $count++; 
             $set = $i."-".$count;?>
            <td><div class="custom-control custom-checkbox">
                  <input type="checkbox"  <?= ($value['delete']==1) ? 'checked' : ''  ?> onclick="unchecked(this)" class="custom-control-input usefor" id="customCheck<?= $set ?>" >
                  <label class="custom-control-label" for="customCheck<?= $set ?>"></label>
                  <input type="hidden" class="setValue"  name="delete[]" value="<?= $value['delete'] ?>">

              </div></td>
              <?php $count++; 
              $set = $i."-".$count;?>
              <td><div class="custom-control custom-checkbox">
                  <input type="checkbox" <?= ($value['view']==1) ? 'checked' : ''  ?>  onclick="unchecked(this)" class="custom-control-input usefor" id="customCheck<?= $set ?>" >
                  <label class="custom-control-label" for="customCheck<?= $set ?>"></label>
                  <input type="hidden"  class="setValue" name="view[]" value="<?= $value['view'] ?>">
              </div></td>
          </tr>
              <?php } ?>
                  </tbody>

      </table>

                  <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                  <button type="submit"  class="btn btn-primary">Submit</button>
                </div>
              </form>


    </div>
  </div>
</div>