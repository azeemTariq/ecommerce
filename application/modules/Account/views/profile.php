<div class="container-fluid rounded bg-white mt-5 mb-5">
    <div class="row">
        <div class="col-md-3 border-right">
            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                <img class="rounded-circle mt-5" src="<?= base_url().'assets/dist/img/admin.png' ?>">
                <span class="font-weight-bold">Amelly</span>
                <div class="d-flex justify-content-between align-items-center experience">
                    <span class="border add-experience">
                        <input type="file" name="" class="form-control">
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-5 border-right">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Profile Settings</h4>
                </div>
                <div class="row mt-2">
                    <div class="col-md-6"><label class="labels">Name</label><input type="" class="form-control" placeholder="first name" value=""></div>
                    <div class="col-md-6"><label class="labels">Surname</label><input type="" class="form-control" value="" placeholder="surname"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12"><label class="labels">PhoneNumber</label><input type="" class="form-control" placeholder="enter phone number" value=""></div>
                    <!-- <div class="col-md-12"><label class="labels">Address</label><input type="" class="form-control" placeholder="enter address" value=""></div>
                    <div class="col-md-12"><label class="labels">Email ID</label><input type="" class="form-control" placeholder="enter email id" value=""></div>
                    <div class="col-md-12"><label class="labels">Education</label><input type="" class="form-control" placeholder="education" value=""></div> -->
                </div>
                <!-- <div class="row mt-3">
                    <div class="col-md-6"><label class="labels">Country</label><input type="" class="form-control" placeholder="country" value=""></div>
                    <div class="col-md-6"><label class="labels">State/Region</label><input type="" class="form-control" value="" placeholder="state"></div>
                </div> -->
                <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Save Profile</button></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="p-3 py-5">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Change password</h4>
                </div><br>
                <div class="col-md-12"><label class="labels">Old pass</label><input type="" class="form-control" placeholder="experience" value=""></div> <br>
                <div class="col-md-12"><label class="labels">New pass</label><input type="" class="form-control" placeholder="additional details" value=""></div>
                <div class="col-md-12 mt-2"><label class="labels">Confirmation</label><input type="" class="form-control" placeholder="additional details" value=""></div>
                  <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Save Changes</button></div>
            </div>
        </div>
    </div>
</div>
