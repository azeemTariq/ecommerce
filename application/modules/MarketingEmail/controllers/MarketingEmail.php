<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MarketingEmail extends MX_Controller {

  public function __construct() {

    modules::run('login/login');
		parent::__construct();
    modules::run('Login/login');
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_MarketingEmail');
	}
    public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= "Contact Inquiry";
      $data['content'] = 'MarketingEmail/index.php';
      $this->load->view('Template/template',$data);
    }

    public function add(){
      
      $data['pageTitle']= "Add Brands";
      $data['content'] = 'MarketingEmail/action';
      $this->load->view('Template/template',$data);
    }
      

    public function getcategory(){
      $this->db->select("*");
      $this->db->from('category');   
      return $this->db->get()->result_array();
    }  

     public function getBrands($id=''){
      $this->db->select("*");
      $this->db->from('sub_category');
      // $this->db->where('status',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
    } 
    public function getsubcatByCat(){
      $this->db->select("*");
      $this->db->from('Brands');
      $this->db->where('category',$this->input->post('id'));
      $data=$this->db->get()->result();
      echo json_encode($data);
    }    
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'id' ,
            'name' ,
            'email' ,
            'subject',
            'message',
            'created_by',
        );

       
   

        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('contact');
        // $this->db->where('sub_category.is_active<>',2);
  

        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
          $row['action'] = '';
            
          

            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['name'];
            $nestedData[] = $row['email'];
            $nestedData[] = $row['subject'];
            $nestedData[] = $row['created_by'];
            $nestedData[] = $row['message'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function command(){
      $this->load->library('form_validation');
      // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $post=$this->input->post();
       $id = (!empty($post['id'])) ? decrypt($post['id']) : '';
       if(!empty($post)){ 
          $this->form_validation->set_rules('subCategoryName', ' Sub Category', 'required|max_length[25]|is_unique_update[sub_category.subCategoryName@id@' .$id. ']');
          $this->form_validation->set_rules('description', 'Description', 'max_length[100]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }

       $image=do_upload($_FILES['profile_image'] ?? '','uploads/');
           $Data=array(
            'subCategoryName' =>$post['subCategoryName'],
            'page_url'     =>$post['page_url'],
            'description'     =>$post['description']
           );
            if(!empty($image)){
            $Data['image'] =$image;
           }
            if(!empty($post['id'])){
            $id=decrypt($post['id']);
            $this->Mdl_Brands->commandAct($Data,'sub_category',['id'=>$id]);
            $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
            $this->Mdl_Brands->commandAct($Data,'sub_category');
          }

           echo json_encode($_return);
           exit();
        }
        redirect(base_url($this->controller.'/'));
}

   public function action(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getBrands($id);
      $data['content'] = 'MarketingEmail/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }
}
