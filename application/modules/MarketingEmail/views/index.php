
<div class="container-fluid"> 
  
</div>
<div class="container-fluid">

          <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="subCategory" class="table table-bordered table-striped dataTable ">
          <thead>
            <tr>
              <th scope="col" >Name</th>
              <th scope="col">Email</th>
              <th scope="col">Subject</th>
              <th scope="col" class="no-search">Message</th>
              <th scope="col" >Ip Address</th>

            </tr>
          </thead>
          <tbody>
    
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>