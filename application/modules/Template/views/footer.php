 <?php 

 $controller = $this->uri->segment(1); 
$this->load->library('user_agent');
 $view = $this->uri->segment(2); 
 $c = $this->uri->segment(3); 

 ?>

<!-- ./content-wrapper -->
</div>
<!-- ./content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; <?= date("Y") ?> <a href="#">Root Software Solution</a>.</strong>
    <!-- All rights reserved. -->
    <div class="float-right d-none d-sm-inline-block">
      
    </div>
  </footer>
<!-- ./wrapper -->
</div>
<!-- ./wrapper -->


<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url() ?>assets/plugins/sparklines/sparkline.js"></script>


<?php  if($controller=='' || ($controller=="MainController" && $view=="") || ($controller=="MainController" && $view=="index") ){ ?>
<!-- JQVMap -->
<script src="<?php echo base_url() ?>assets/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<?php } ?>

<script src="<?php echo base_url() ?>assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url() ?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url() ?>assets/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>


<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')?>"></script>
<!-- toastr js -->

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/toastr.min.js"></script>


<script src="<?php echo base_url('assets/datatables/jquery.dataTables.min.js')?>"></script>
<!-- scripting -->

<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/plugins/select2/js/select2.full.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>

<!-- Bootstrap Switch -->
<script src="<?php echo base_url('assets/plugins/bower_components/bootstrap-switch/bootstrap-switch.min.js')?>"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url() ?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

<!--Style Switcher -->
<script src="<?php echo base_url() ?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js "></script>

<script src="<?php  echo base_url('assets/plugins/bower_components/switchery/dist/switchery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/higherChart.js')?>"></script>
<script src="<?php echo base_url('assets/js/highcharts-3d.js')?>"></script>
<script src="<?php  echo base_url('links/js/validate.min.js')?>" ></script>
<!-- <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> -->
<script>
    $('.alert').delay(6000).fadeOut(300);

</script>


<?php echo  $this->load->view('Template/custom.php'); ?>



<?php
  foreach (Modules::$locations as $location => $offset) {
  
        if(file_exists($location.$controller.'/views/js.php') == true) {
            $this->load->view($controller.'/js');
        }
        $validateJson = '';
        if(file_exists($location.$controller.'/views/validate.json') == true){
           $validateJson = $this->load->view($controller.'/validate.json', '', true);
        }
    }
  ?>

<script type="text/javascript">

 fields = 'nojson';
    <?php if (isset($validateJson) && !empty($validateJson)){ ?>
    var  fields = <?php echo $validateJson; ?>;
    <?php  } ?>



    $(function () {
      //Initialize Select2 Elements
    $('.select2').select2();
   
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
   

  });



</script>
<script src="<?php echo base_url('assets/js/global-validation.js')?>"></script>
</body>
</html>