 <?php
  $role       = array_column(modules::run('Role/getRole'),'role','id');
  $controller = $this->uri->segment(1);
  $view = $this->uri->segment(2);

 ?>
 <!-- Main Sidebar Container -->
  <aside style="background-color:black" class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('Dashboard') ?>" align="center" class="brand-link">
      <img class="logo-main" style="height: 70px;" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo" />
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url() ?>assets/dist/img/admin.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $this->session->userdata('name') ?? ' not found ';?></a>
        </div>
      </div>

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview <?= ($controller=='Dashboard' && $view!='Our-Customer') ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>Dashboard" class="nav-link <?= ($controller=='Dashboard' && $view!='Our-Customer') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          </li>

            <li class="nav-item has-treeview <?= ($controller=='Dashboard' &&  $view=="Our-Customer") ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>Dashboard/Our-Customer" class="nav-link <?= ($controller=='Dashboard' &&  $view=="Our-Customer") ? 'active' : '' ?>">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Customer List
                
              </p>
            </a>
          </li>


        <li class="nav-item has-treeview <?= ($controller=='Account' && $view=='profile') ? 'menu-open' : '' ?>" style="display:none" >
            <a href="<?php echo base_url() ?>Account/profile" class="nav-link <?= ($controller=='Account' && $view=='profile') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-pencil-alt"></i>
              <p>
                Edit Profile
                
              </p>
            </a>
          </li>



          <?php if($this->session->userdata('role')==0){ ?>
            <li class="nav-item has-treeview <?= ($controller=='Account') ? 'menu-open' : '' ?>" style="display:none; ;">
            <a href="#" class="nav-link <?= ($controller=='Account') ? 'active' : '' ?>">
              <i class="far fa-user"></i>
              


              <p>
                Admin
                <i class="fas fa-angle-left right"></i>
                
              </p>
            </a>
            <ul class="nav nav-treeview">
            
              <li class="nav-item">
                <a href="<?php echo base_url() ?>Account/userRights                 
                  " class="nav-link <?= ($controller=='Account' && $view == 'userRights') ? 'active' : '' ?>">
                  <i class="fas fa-users-cog nav-icon"></i>
                  <p>Rights </p>
                </a>
              </li>

            </ul>
          </li>
           <li class="nav-item has-treeview <?= ($controller=='Role') ? 'menu-open' : '' ?>" style="display:none; ;">
            <a href="#" class="nav-link <?= ($controller=='Role') ? 'active' : '' ?>">
              <i class="far fa-user"></i>
              <p>
                role
                <i class="fas fa-angle-left right"></i>
                
              </p>
            </a>


         <ul class="nav nav-treeview" style="display:none; ;">
          <li class="nav-item">
                <a href="<?php echo base_url()?>Role/" class="nav-link  <?= ($controller=='Role' && $view == '') ? 'active' : '' ?>">
                  <i class="fas fa-users-cog nav-icon"></i>
                  <p>Manage </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url() ?>Role/add" class="nav-link <?= ($controller=='Role' && $view == 'add') ? 'active' : '' ?>">
                  <i class="fas fa-user-plus nav-icon"></i> 
                  <p>Add</p>
                </a>
              </li>
            </ul>
          </li>

          <?php  } ?>

          
          <li class="nav-item has-treeview <?= ($controller=='Users' && $view != 'vendor') ? 'menu-open' : '' ?>" style="display:none; ;">
            <a href="#" class="nav-link <?= ($controller=='Users' && $view != 'vendor') ? 'active' : '' ?>">
              <i class="far fa-user"></i>
              <p>
                User
                <i class="fas fa-angle-left right"></i>
                
              </p>
            </a>
            <ul class="nav nav-treeview">
            
              <li class="nav-item">
                <a href="<?php echo base_url() ?>Users/                 
                  " class="nav-link <?= ($controller=='Users' && $view == '') ? 'active' : '' ?>">
                  <i class="fas fa-users-cog nav-icon"></i>
                  <p>Manage </p>
                </a>
              </li>
               <?php if($this->session->userdata('role')==0){ ?>
                <li class="nav-item">
                <a href="<?php echo base_url() ?>Users/add" class="nav-link <?= ($controller=='Users' && $view == 'add') ? 'active' : '' ?>">
                  <i class="fas fa-user-plus nav-icon"></i>
                  <p>add </p>
                </a>
              </li>
            <?php } ?>
            </ul>
          </li>

<!--           <li class="nav-item has-treeview <?= ($controller=='Users'  && $view == 'vendor') ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>Users/vendor" class="nav-link <?= ($controller=='Users'  && $view == 'vendor') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Vendor
                
              </p>
            </a>
          </li>


          <li class="nav-item has-treeview <?= ($controller=='Users'  && $view == 'vendor') ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>Users/vendor" class="nav-link <?= ($controller=='Users'  && $view == 'vendor') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Courior
                
              </p>
            </a>
          </li> -->



          <li class="nav-header"> INVENTORY</li>
          

          
          <li class="nav-item has-treeview  <?= ($controller=='Category') ? 'menu-open' : '' ?>">
            <a href="" class="nav-link <?= ($controller=='Category') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Category
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">



              <li class="nav-item">
                <a href="<?php echo base_url("Category/") ?>" class="nav-link <?= ($controller=='Category' && $view=='') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Category/add") ?>" class="nav-link <?= ($controller=='Category' && $view=='add') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Add </p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?= ($controller=='Brands') ? 'menu-open' : '' ?>" >
            <a href="" class="nav-link <?= ($controller=='Brands') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Brands
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url("Brands/") ?>" class="nav-link <?= ($controller=='Brands' && $view=='') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Brands/add") ?>" class="nav-link <?= ($controller=='Brands' && $view=='add') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Add </p>
                </a>
              </li>
            </ul>
          </li>



          <li class="nav-item has-treeview <?= ($controller=='Product') ? 'menu-open' : '' ?>">
            <a href="" class="nav-link <?= ($controller=='Product') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview">
                <a href="<?php echo base_url("Product") ?>" class="nav-link <?= ($controller=='Product' && $view=='') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Manage
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Product/add") ?>" class="nav-link <?= ($controller=='Product' && $view=='add') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Add </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview <?= ($controller=='Orders') ? 'menu-open' : '' ?>" style="display: none ;" >
            <a href="<?php echo base_url() ?>Orders" class="nav-link <?= ($controller=='Orders') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Order Management
                
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview <?= ($controller=='Orders') ? 'menu-open' : '' ?>" >
            <a href="" class="nav-link <?= ($controller=='Orders') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
               Order Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url("Orders/") ?>" class="nav-link <?= ($controller=='Orders' && $view=='') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Received Order</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Orders/OrderConfirm") ?>" class="nav-link <?= ($controller=='Orders' && $view=='OrderConfirm') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Confirm Order </p>
                </a>
              </li>
                <li class="nav-item">
                <a href="<?php echo base_url("Orders/OrderDispatch") ?>" class="nav-link <?= ($controller=='Orders' && $view=='OrderDispatch') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dispatch Order </p>
                </a>
              </li>
                <li class="nav-item">
                <a href="<?php echo base_url("Orders/OrderDelivered") ?>" class="nav-link <?= ($controller=='Orders' && $view=='OrderDelivered') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Delivered Order </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header"> Other info</li>
          
          <li class="nav-item has-treeview <?= ($controller=='Partners' ) ? 'menu-open' : '' ?>" style="display:;">
            <a href="" class="nav-link <?= ($controller=='Partners' ) ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Partner Logo
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url("Partners/") ?>" class="nav-link <?= ($controller=='Partners' &&  $view=='' ) ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href= "<?php echo base_url("Partners/add") ?>"  class="nav-link <?= ($controller=='Partners' &&  $view=='add' ) ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Add </p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?= ($controller=='MarketingEmail') ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>MarketingEmail" class="nav-link <?= ($controller=='MarketingEmail') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Contact Inquiry
                
              </p>
            </a>
          </li>





          <li class="nav-item has-treeview <?= ($controller=='Pages') ? 'menu-open' : '' ?>">
            <a href="" class="nav-link <?= ($controller=='Pages') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Pages
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview">
                <a href="<?php echo base_url("Pages") ?>" class="nav-link <?= ($controller=='Pages' && $view=='') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    About Us
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Pages/policies") ?>" class="nav-link <?= ($controller=='Pages' && $view=='policies') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Policies</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Pages/FAQs") ?>" class="nav-link <?= ($controller=='Pages' && $view=='FAQs') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>FAQ's</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url("Pages/") ?>" class="nav-link <?= ($controller=='Pages' && $view=='contact_us') ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact Us</p>
                </a>
              </li>
              

            </ul>
          </li>




             <li class="nav-item has-treeview <?= ($controller=='SalesReport') ? 'menu-open' : '' ?>" >
            <a href="<?php echo base_url() ?>/SalesReport" class="nav-link <?= ($controller=='SalesReport') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                Sales Report
                
              </p>
            </a>
          </li>






        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>