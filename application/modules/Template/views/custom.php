<?php
$controller = $this->uri->segment(1);
$view       = $this->uri->segment(2);
?>
<script type="">

var rows_data = [];
toastr.options = {
  "debug": false,
  "positionClass": "toast-bottom-right",
  "onclick": null,
  "fadeIn": 300,
  "fadeOut": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000
}


<?php if($controller!='Users' &&  $view!='vendor'){ ?>
    jQuery.validator.addMethod("notenglish", function (value, element) {
        return this.optional(element) || /^[^a-zA-Za-z]+$/i.test(value);
    }, "English alphabetical characters Are not allowed");
       jQuery.validator.addMethod("onlyEng_", function (value, element) {
        return this.optional(element) || /^[a-zA-Za-z0-9-_ ]+$/i.test(value);
    }, "Only english alphabetical characters and _ - are  allowed");
    jQuery.validator.addMethod("numerics", function (value, element) {
        return this.optional(element) || /^[0-9]+$/i.test(value);
    }, "English alphabetical characters Are not allowed");

<?php  } ?>
    //example: use order_by as [id,'desc']
    function datatablelist(tableid,trclass=null,id=null,optionalurl='getList',route='<?=$controller?>',topScroll=true,order_by=null){
       
         var url = "<?php echo base_url(); ?>"+route+"/"+optionalurl;

         console.log(url);

        if(id!=null){
            url = url+"?id="+id;
        }
        $('#'+tableid).dataTable({
            "bDestroy": true
        }).fnDestroy();
        var dataTable = $('#'+tableid).DataTable( {
        <?php if($controller =='Inventory_Products'){?>
        "sPaginationType": "listbox",
        "dom": '<"top"flp<"clear">>rt<"bottom"flp<"clear">>',
        <?php } ?>
        "processing": true,
        "serverSide": true,

       

        <?php
          $page=array('Personnel', 'Inventory_Locators','Inventory_Products','COA_Manege','Inventory_Entity','GL_Cost_Center','GL_Analysis');
        if(!in_array($controller, $page)){
        ?>

        "order": order_by||[0,"desc"],
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
         }],
        <?php } ?>


      

        "ajax":{
            url :  url, // json datasource
            type: "post",
            "data": function ( d ) {
                d.key = $("#id").val() || "";
                d.delete = $("#delete").val() || "";
            },
            error: function(){
                //unblockDiv($('#'+tableid));
                $("."+tableid+"-grid-error").html("");
                $("#"+tableid+"-grid").append('<tbody class="'+tableid+'-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                $("#"+tableid+"-grid_processing").css("display","none");
            },complete: function () {
                //unblockDiv($('#'+tableid));
                <?php if($controller == 'Dashboard'){?>
               // dataTableTimeOut(tableid);
                <?php } ?>

                /* Top Scroll JS Start */

                if(topScroll){
                $('.table-responsive').addClass('wrapper2');
                $('.table').addClass('div2');

                if($('.wrapper1').length==0){
                $( "<div class=\"wrapper1\"><div class=\"div1\"></div></div>" ).insertBefore( ".wrapper2" );
                }
                $(".div1").css("width", $('.div2').width());

                $(".wrapper1").scroll(function(){
                    $(".wrapper2")
                        .scrollLeft($(".wrapper1").scrollLeft());
                });
                $(".wrapper2").scroll(function(){
                    $(".wrapper1")
                        .scrollLeft($(".wrapper2").scrollLeft());
                });
                }

               $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
               $("input[type='checkbox']:checked").parents('.bt-switch').find('.bootstrap-switch-container').css('margin-left','0px');

                /* Top Scroll JS End */

               
               try{
               datatbl_function();
               }
               catch(e){}
               dataTable
                .rows()
                .nodes()
                .to$()
                .attr( 'id','exercises-tr' );
            if(trclass!=null){
                dataTable
                .rows()
                .nodes()
                .to$()
                .attr( 'class',trclass );
                }
                $('#'+tableid+'_filter').hide();
               
            }
        }
    });

    
    $('#' + tableid + ' thead th').each(function(i) {
        var title = $(this).text();
        var maxWidth = (title.length)*10; 
        if(title!='' && title !='Status' && title !='Cheque Print' && title !='Print' && title !='Voucher Print' && title !='Action' && !$(this).hasClass( "no-search" ) && !$(this).hasClass( "search-off")){
            // $(this).attr('style','padding-left:0px !important; padding-right:0px !important;');
            $(this).html('<input style="max-width:'+maxWidth+'px !important;" type="text" placeholder="' + title + '" class="searchtbl form-control"/>');
        }
    });
var typingTimer;
    var doneTypingInterval = 500;
    var table = $('#' + tableid).DataTable();
    
    <?php
     $filterset ='';
        if($controller == 'HelpPanel' || $controller == 'Mis_FPC' || $controller == 'Gl_Voucher_Type'){
        $filterset ='change';
    } ?>
    
    $('#' + tableid + ' thead input,'+'#' + tableid + ' thead select').on('keyup <?php echo $filterset?>' ,function(){

        var thisinput = $(this);
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function () {
            table
                .column(thisinput.parent().index() + ':visible')
                .search(thisinput.val())
                .draw();
        }, doneTypingInterval);
      
    });
    $('.searchtbl').on('click', function(e) {
        e.stopPropagation();
    });
    checkbox_dt(tableid)
}
    function checkbox_dt(tableid){
        $('#' + tableid +' tbody').on('click', '.checkbox_dt', function(e){
            var $row = $(this).closest('tr');
            var row_val = $row.find('.checkbox_dt').val();
            var index = $.inArray(row_val, rows_data);
            if(this.checked && index === -1){
                rows_data.push(row_val);
            } else if (!this.checked && index !== -1){
                rows_data.splice(index, 1);
            }
            e.stopPropagation();
        });
    }


function readURL(input) {


    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
           if('<?= $controller?>'=='Products'){
              $(input).closest('.row').find('#imgView').attr('src', e.target.result);
           }else{
            $('#imgView').attr('src', e.target.result);
           }
            
            
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(".profile_image").change(function(){ 
    readURL(this);
});


function changeStatus(obj,Id,controller,table,action='update',column='is_active',table_key='id') { 

       var is_active= 1;
    if(action=='update'){
        if($(obj)[0].checked==false){
          is_active= 0;
        }
    }else{
      is_active =2;
    }

   $.ajax({
        url: '<?= base_url() ?>'+controller+'/changeStatus',
        type:'post',
       data:{id:Id,active:is_active,table:table,controller:controller,column:column,table_key:table_key},   
       beforeSend: function(){
            toastr.info('Processing !');
        },
      success:function(data) {
            $('.toast-info').hide();
            if(action!='deleted'){
                toastr.success(data, 'Success');
            }
            
      }
    });
}

function deleted(obj,Id,table,models) { 

    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {


// console.log(table);

         changeStatus('',Id,'Users',table,'deleted');
$(obj).closest('tr').remove();
         
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    });

}

$(document).ready(function(){



    function datePicker() {
        jQuery('.datepicker-autoclose').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
         orientation: "right bottom"
        });

        jQuery('.datepicker-pos-only').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
         orientation: "right bottom"
        });
    }
    datePicker();

    function datePickerNormal() {
        jQuery('.datepicker-autoclose_normal').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
         orientation: "right bottom"
        });
    }
    datePickerNormal();

});

function openPrintWindow(url, name, specs) {
    var printWindow = window.open(url, name, specs);
      var printAndClose = function() {
          if (printWindow.document.readyState == 'complete') {
              clearInterval(sched);
//              printWindow.print();
//              printWindow.close();
          }
      } 
      var sched = setInterval(printAndClose, 200);
  };
jQuery(document).ready(function ()
{
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function () {
        new Switchery($(this)[0], $(this).data());
    });
 $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();
 $("input[type='checkbox']:checked").parents('.bt-switch').find('.bootstrap-switch-container').css('margin-left','0px');
  });
    </script>