<html>
<head>
  <title>Grandeur | Perfume Shop</title>
  <!-- styling -->

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="shortcut icon" href="<?= base_url() ?>assets/website/img/favicon.ico" type="image/x-icon" />

  <!-- <title>Admin Panel | Dashboard</title> -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/font/ionic.min.css"> -->
   <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" />

   <link href="<?php echo base_url('assets/plugins/bower_components/bootstrap-switch/bootstrap-switch.min.css')?>" rel="stylesheet">

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->

    <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->

  <!-- Custom Css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/custom.css">

  <link rel="stylesheet" href="<?php echo base_url('assets/datatables/jquery.dataTables.min.css')?>"/>


    
<!-- style switcher -->
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/bower_components/switchery/dist/switchery.min.css')?>" />
   <!-- custom style -->
    
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/custom_style.css">
    
<!-- toastr css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/toastr.min.css">

  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

  <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="<?php echo base_url('Dashboard') ?>" class="nav-link">Home</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <!-- <a href="#" class="nav-link">Profile</a> -->
          </li>
        </ul>
     
        <!-- SEARCH FORM -->
        <!-- <form class="form-inline ml-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form> -->

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
          <!--       <div class="media">
                  <img src="<?php echo base_url() ?>assets/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Brad Diesel
                      <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">Call me whenever you can...</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div> -->
                <!-- Message End -->
              </a>
              <!-- <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <!--  -->
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link"  href="<?php echo base_url('Account/profile') ?>">
              <i class="fa fa-edit"></i> Edit Profile 
              
            </a>
            <li class="nav-item">
           <a href="<?php echo base_url('Login/logout') ?>"> 
            <img  src="<?php echo base_url() ?>assets/img/on_of.png" class="" alt="User Image"> 
          </a>
          </li>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
             
            </div>
          </li>
          
        </ul>
      </nav>
  <!-- /.navbar -->
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">

