<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {


   function __construct(){
        parent::__construct();
        $this->load->model('Mdl_template');
    }
	public function index()
	{
		$this->load->view('template');
	}
}
