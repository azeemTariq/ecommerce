<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    // $this->load->model('Login/Mdl_Login');
  }
 public $controller = __CLASS__;
    public function index(){
       if($this->session->userdata('logged_in')){
          redirect(base_url('Product/'));
       }
        $data['content'] = 'index';
        $this->load->view('Login/template',$data);
    }

    public function signup(){
        $data['content'] = 'action';
        $this->load->view('Login/template',$data);
    }

    public function add(){
      $data['content'] = 'Login/action';
      $this->load->view('Login/template',$data);
    }

    public function authentication(){
      $post=$this->input->post();
       if($post){
        $username = $post['username'];
        $password = $post['password'];
        $record= $this->getUserInfo($username,$password);
          if($record){
            $newdata = array( 
             'username'  => $record->username, 
              'id'       => $record->id,
             'logged_in' => TRUE
          );  
          $this->session->set_userdata($newdata);
          redirect(base_url('/'));
          }
          
       }else{
         redirect(base_url('/'));
       }



    }

    public function getUserInfo($username,$password){

       $this->db->select('*');
       $this->db->from('login');
       $this->db->where(['username'=>$username,'password'=>$password]);
       return $this->db->get()->row();
    }
public function login(){
  if(!$this->session->userdata('logged_in')){
          redirect(base_url('/'));
  }
}

function logout()
{
    $newdata = array(
                'username'  =>'',
                'id' => '',
                'logged_in' => FALSE,
               );

     $this->session->unset_userdata($newdata);
     $this->session->sess_destroy();

     redirect(base_url('/'));
}
        
       


  }
      