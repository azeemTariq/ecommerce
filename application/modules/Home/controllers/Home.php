<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

  public $perpage;
  public function __construct() {
    parent::__construct();

    $this->load->helper('url', 'form');
    $this->load->library('email');
    $this->load->model('Mdl_Home');
    $this->perpage = 1;
  }
  public $controller = __CLASS__;
  public function index(){
      $sale_where=" retail_price > price ";
      $data['sale_list'] = $this->Mdl_Home->get_product_list_new($sale_where);
      $data['new_arrival_list'] = $this->Mdl_Home->get_product_list_new();
      $popular_where=" retail_price > price ";
      $data['popular_sale_list'] = $this->Mdl_Home->most_sale();
      $data['content']    = 'index';
     return $this->load->view('WebView/template',$data);
  }
  public function collections($fragances_name=""){
    
    if(empty($fragances_name)||empty($this->Mdl_Home->category_list(['page_url'=>$fragances_name])))
    {
      redirect('/', 'refresh');
    }
    else{

      $fragances=(array)$this->Mdl_Home->category_list(['page_url'=>$fragances_name])[0];
      
      $data['fragances_name']    = $fragances['category_name'];
      $data['description']    = $fragances['description'];
      $data['category_id']    = $fragances['id'];
      $data['page_url']    = $fragances['page_url'];
      $data['content']    = 'frangraces_page';
     return $this->load->view('WebView/template',$data);
    }
  } 
  public function brand($brand=""){
    
    if(empty($brand)||empty($this->Mdl_Home->brand_list(['page_url'=>$brand])))
    {
      redirect('/', 'refresh');
    }
    else{

      $brands=(array)$this->Mdl_Home->brand_list(['page_url'=>$brand])[0];
      
      $data['brand']    = $brands['subCategoryName'];
      $data['description']    = $brands['description'];
      $data['category_id']    = $brands['id'];
      $data['page_url']    = $brands['page_url'];
      $data['content']    = 'brands_page';
     return $this->load->view('WebView/template',$data);
    }
  }
  public function customerAuth(){
     $post = $this->input->post();
     $array = array(
         'username'   =>$post['username'],
         'password'   =>$post['password'],
         'is_customer'    => 1
       );
       $this->db->select('*');
       $this->db->from('login');
       $this->db->where($array);
       $record = $this->db->get()->row();
       $_return= [];
       if(!empty($record)){
            $fname= explode(' ', $record->name)[0];
            $lname= explode(' ', $record->name)[1];
            $newdata = array( 
             'id'  =>         $record->id, 
             'first_name'     => $fname, 
             'username'       => $record->username,
             'last_name'      => $lname, 
             'email'          => $record->email,
             'cell'           => $record->phone,
             'is_customer'    => 1,
             'customer_logged_in' => TRUE
          );  
    
       $this->session->set_userdata($newdata);

        if(isset($post['is_customer'])){  
          redirect(base_url('Home'));
        }
        $_return['status'] = 'success';
        $_return['errorCode'] = '0';
        $_return['msg'] = 'Login Successfully !';
       }else{

        if(isset($post['is_customer'])){  
            $this->session->set_flashdata('error','Invalid Username or password !!');
            redirect(base_url('Login?s='.encrypt('is_customer')));
        }
          $_return['status'] = 'error';
          $_return['errorCode'] = '1';
          $_return['msg'] = 'Invalid Username or password !!';
       }
       echo json_encode($_return);
  }
  public function signout(){
    $newdata = array(
                'username'  =>'',
                'id' => '',
                'shop_name' => '',
                'customer_logged_in' => FALSE,
               );

     $this->session->unset_userdata($newdata);
     $this->session->sess_destroy();

     redirect(base_url('checkout'));
}
  public function bundle_offer(){
      $data['content']    = 'bundle_offer';
     return $this->load->view('WebView/template',$data);
    
  }
  public function product_details($id=null){
      $p=explode("-",$id);
      $id=end($p);
      $product=$this->Mdl_Home->get_product_list_new(['id'=>$id]);
      if(empty($product) || count($product)==0)
        redirect('/', 'refresh');
      else
      {
        $_product    = (array)$product[0];
        $data['product']    = $_product;
        $data['related_product']    = $this->Mdl_Home->get_product_list_new('( cat_id='.$_product['cat_id'].' OR subCatID='.$_product['subCatID'].' ) AND id!='.$_product['id'],10,0,"","",'id');
        
        $data['content']    = 'product_details';
        return $this->load->view('WebView/template',$data);
    
      }
  }
  public function my_order_list(){
      modules::run('Login/customer_login_check');

      $data['data']       = $this->Mdl_Home->_getrow('*','orders',['cust_id'=>$_SESSION['id']]);
      $data['content']    = 'my_order_list';
     return $this->load->view('WebView/template',$data);
    
  }
  public function my_order_detail($id=""){
      modules::run('Login/customer_login_check');
        $id=decrypt($id);
      $order_check = $this->Mdl_Home->_getrow('*','orders',['cust_id'=>$_SESSION['id'],'id'=>$id]);
    if(empty($order_check) || count($order_check)==0)
        redirect('/', 'refresh');
      else
      {
      $data['order'] =$order_check;
      $data['order_detail'] =$this->db->query(" SELECT * FROM `order_detail` INNER JOIN product ON product.id=order_detail.ItemId Where OrderId=".$id)->result_array();;

      $data['content']    = 'my_order_detail';
     return $this->load->view('WebView/template',$data);
    
      }
  }
  public function order_details()
  {
    modules::run('Login/customer_login_check');
    $customer_id=$_SESSION['id'] ?? 0;
    $order_no=$_POST['order_no'];
    $order=$this->Mdl_Home->get_row('orders',['cust_id'=>$customer_id,'code'=>$order_no]);
    if($order)
    echo json_encode($order->status);
    else
    echo json_encode([]);
  }
  public function my_invoice_list(){
      modules::run('Login/customer_login_check');
    
      $data['data']       = $this->Mdl_Home->_getrow('*','orders',['cust_id'=>$_SESSION['id']]);
    
      $data['content']    = 'my_invoice_list';
     return $this->load->view('WebView/template',$data);
    
  }
  public function my_invoice_detail(){
      modules::run('Login/customer_login_check');
    
      $order_check       = $this->Mdl_Home->_getrow('*','orders',['cust_id'=>$_SESSION['id'],'id'=>$_COOKIE['orderID']]);
    if(!empty($order_check)&&count($order_check)){
      $data['order']=$order_check[0];
      $data['order_detail'] =$this->db->query(" SELECT * FROM `order_detail` INNER JOIN product ON product.id=order_detail.ItemId Where OrderId=".$_COOKIE['orderID'])->result_array();;
     return $this->load->view('my_invoice_detail',$data);
    }
    else
      echo "No Invoice Found";
  }
  public function my_dashboard(){
      modules::run('Login/customer_login_check');
      $data['content']    = 'my_dashboard';
     return $this->load->view('WebView/template',$data);
    
  }
   public function my_payment_list(){
       modules::run('Login/customer_login_check');
       $data['data']       = $this->Mdl_Home->_getrow('*','orders',['cust_id'=>$_SESSION['id']]);
    
      $data['content']    = 'my_payment_list';
     return $this->load->view('WebView/template',$data);
    
  }
   public function my_profile(){
    modules::run('Login/customer_login_check');
      $data['data']       = $this->Mdl_Home->_getrow('*','login',['id'=>$_SESSION['id']]);
      $data['content']    = 'my_profile';
     return $this->load->view('WebView/template',$data);
    
  }
  public function update_my_profile(){
    modules::run('Login/customer_login_check');
     echo true;
  }
   public function my_order_tracking(){
      modules::run('Login/customer_login_check');
      $data['content']    = 'my_order_tracking';
     return $this->load->view('WebView/template',$data);
    
  }
  public function new_arrival(){
      $data['content']    = 'new_arrival';
     return $this->load->view('WebView/template',$data);
  }
  public function search_product(){
      $data['content']    = 'search_product';
     return $this->load->view('WebView/template',$data);
  }
  public function contact(){
      $data['content']    = 'contact';
     return $this->load->view('WebView/template',$data);
  }
  public function aboutus(){
    $data['about'] = $this->Mdl_Home->about_us_content();
    $data['content']    = 'aboutus';
    return $this->load->view('WebView/template',$data);
  }
  public function shop_cart(){
    $data['content']    = 'shop_cart';
    return $this->load->view('WebView/template',$data);
  }
  public function delete_to_cart($row_id){
    $data = array(
        'rowid' => $row_id,
        'qty'   => 0
      );
    $this->cart->update($data);
    return redirect("shop-cart");
  }
  public function wishlist(){
    $data['content']    = 'wishlist';
    return $this->load->view('WebView/template',$data);
  }
  public function checkout_form(){
    $data['content']    = 'checkout_form';
    return $this->load->view('WebView/template',$data);
  }
  public function policy($policy=""){
       $content = '';
      if(empty($policy))
    {
      redirect('/', 'refresh');
    }
    else{

         $content=(array)$this->Mdl_Home->privacyContent(['page_url'=>$policy]);
       // if(empty($content))   
        // redirect('/', 'refresh');
        
      $data['contents'] = $content['content'];
      $data['policy']    = $content['page_title'];
      $data['content']    = 'policy';
     return $this->load->view('WebView/template',$data);
    }
  }

  public function faq(){
      $data['faq_content'] = $this->Mdl_Home->faq_list();
      $data['content']    = 'faq';
      return $this->load->view('WebView/template',$data);
  }
  public function termCondition(){
      $data['content']    = 'term';
      return $this->load->view('WebView/template',$data);
  }

public function testing(){
  $data['content']    = 'index';
  $data['partners'] = $this->Mdl_Home->partnerLogos();
  $data['product_list'] = $this->Mdl_Home->get_product_list();
  return $this->load->view('WebView/template',$data);

}public function shipping(){
  $data['content']    = 'shipping';;
  return $this->load->view('WebView/template',$data);

}

  public function update_cart(){
    $post = $this->input->post();
    if(isset($post['update_cart'])){
            foreach ($post['rowid'] as $key => $value) {
              $data = array(
                'rowid'    => $post['rowid'][$key],
                'qty'   => $post['num-product'][$key]
            );
          $this->cart->update($data);
            }
      
    
    }
     return redirect(base_url("shop-cart"));
  }


  
public function categoryByStore($id='true'){
      $data=array();
      $this->db->select("*");
      $this->db->from('category');
      $this->db->where('is_active=1 AND store_id  ='.$id);
         return  $data=$this->db->get()->result();
    } 
    public function subCategoryByStore($id='true'){
      $data=array();
      $this->db->select("*");
      $this->db->from('sub_category');
      $this->db->where('is_active=1 AND store_id  ='.$id);
      $this->db->order_by('cat_id');
      $data=$this->db->get()->result();
      $array=[];
      foreach ($data as $key => $value) {
          $array[$value->cat_id][] = $value;
      }
      return $array;
    } 
public function Product(){

    if($this->uri->segment(3)){
      $id= decrypt($this->uri->segment(3));
      $banners = $this->Mdl_Home->db_select('*','banners','is_active=1 AND store_id = "'.$id.'"  ');
      if(count($banners)>1){
         redirect(base_url($this->controller));
      } 
      $data['banners'] = $banners;
      $data['store_id']  = $id;
      $data['content']= 'Product';
      return $this->load->view('WebView/template',$data); 
         
      }else{
        redirect(base_url($this->controller));
      }
 }

      
public function productDetails(){
  $data['content']= 'productdet.php';
  $this->load->view('WebView/template',$data);

}
public function viewCart(){
  if(count($this->cart->contents())){
     $data['content']= 'cart_';
     return $this->load->view('WebView/template',$data);
  }
   redirect(base_url($this->controller));
 
}
public function CheckOut(){
  if(count($this->cart->contents())){
    $data['content']= 'Check';
  return $this->load->view('WebView/template',$data);
  }
   redirect(base_url($this->controller));

}
public function thanks(){
  $data['content']= 'thank';
  $this->load->view('WebView/template',$data);
}public function emailChecking(){
  $data['content']= 'emailChecking';
  $this->load->view('WebView/template',$data);
}

public function bannersList($id='',$type=null){
  return $this->Mdl_Home->bannersList($id,$type);
}
public function vendor(){
     $data['content']= 'vendor';
      $this->load->view('Home/vendor',$data);
 }
   public function getAllPageLinks($count,$href) {

    // dd($href);
    $output = '';
    if(!isset($_GET["page"])) $_GET["page"] = 1;
    if($this->perpage != 0)
      $pages  = ceil($count/$this->perpage);
    if($pages>1) {
      if($_GET["page"] == 1) 
        $output = $output . '<span class="item-pagination flex-c-m trans-0-4 disabled">&#8810;</span><span class="item-pagination flex-c-m trans-0-4 disabled">&#60;</span>';
      else  
        $output = $output . '<a class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . (1) . '\')" >&#8810;</a><a class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . ($_GET["page"]-1) .'\')" >&#60;</a>';
      
      
      if(($_GET["page"]-3)>0) {
        if($_GET["page"] == 1)
          $output = $output . '<span id=1 class="item-pagination flex-c-m trans-0-4 active-pagination">1</span>';
        else        
          $output = $output . '<a class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . '1\')" >1</a>';
      }
      if(($_GET["page"]-3)>1) {
          $output = $output . '<span class="dot item-pagination flex-c-m trans-0-4">...</span>';
      }
      
      for($i=($_GET["page"]-2); $i<=($_GET["page"]+2); $i++)  {
        if($i<1) continue;
        if($i>$pages) break;
        if($_GET["page"] == $i)
          $output = $output . '<span id='.$i.' class="item-pagination flex-c-m trans-0-4 active-pagination">'.$i.'</span>';
        else        
          $output = $output . '<a class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . $i . '\')" >'.$i.'</a>';
      }
      
      if(($pages-($_GET["page"]+2))>1) {
        $output = $output . '<span class="dot item-pagination flex-c-m trans-0-4">...</span>';
      }
      if(($pages-($_GET["page"]+2))>0) {
        if($_GET["page"] == $pages)
          $output = $output . '<span id=' . ($pages) .' class="item-pagination flex-c-m trans-0-4 active-pagination">' . ($pages) .'</span>';
        else        
          $output = $output . '<a class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href .  ($pages) .'\')" >' . ($pages) .'</a>';
      }
      
      if($_GET["page"] < $pages)
        $output = $output . '<a  class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . ($_GET["page"]+1) . '\')" >></a><a  class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . ($pages) . '\')" >&#8811;</a>';
      else        
        $output = $output . '<span class="item-pagination flex-c-m trans-0-4 disabled">></span><span class="item-pagination flex-c-m trans-0-4 disabled">&#8811;</span>';
      
      
    }
    return $output;
  }
  public function getPrevNext($count,$href) {
    $output = '';
    if(!isset($_GET["page"])) $_GET["page"] = 1;
    if($this->perpage != 0)
      $pages  = ceil($count/$this->perpage);
    if($pages>1) {
      if($_GET["page"] == 1) 
        $output = $output . '<span class="item-pagination flex-c-m trans-0-4 disabled first">Prev</span>';
      else  
        $output = $output . '<a class="item-pagination flex-c-m trans-0-4 " onclick="getresult(\'' . $href . ($_GET["page"]-1) . '\')" >Prev</a>';     
      
      if($_GET["page"] < $pages)
        $output = $output . '<a  class="item-pagination flex-c-m trans-0-4" onclick="getresult(\'' . $href . ($_GET["page"]+1) . '\')" >Next</a>';
      else        
        $output = $output . '<span class="item-pagination flex-c-m trans-0-4 disabled">Next</span>';
      
      
    }
    return $output;
  }


public function getRecord(){
          
       
        $pagination_setting = $_GET["pagination_setting"];
        $catId = $_GET["catId"] ?? '';
        $SubCatId = $_GET["SubCatId"] ?? '';
        $item_search = $_GET["search"] ?? '';
        $from = $_GET["from"] ?? '';
        $to = $_GET["to"] ?? '';
        $manual = $_GET["manual"] ?? '';
               // pre($manual);  
        $page = 1;
        $page0= 12;
        if(isset($_GET["page"]) &&  !empty($_GET["page"])) {
        $page = $_GET["page"];
        }

        $start = ($page-1)*$page0;
        if($start < 0) $start = 0;

  
        $date = date('Y-m-d').' 000:00:00';
       $where = " WHERE 
             product.created_by = lg.id
            AND product.is_active=1 
            AND lg.is_active=1 
            AND product.is_approved=1 
            AND lg.is_approved=1 
            AND (expirey_date ='' OR expirey_date ='0000-00-00' OR expirey_date >= '".$date."' )
            ";
 $PageAdd='';
       if($manual==''){
        $where.= ' AND store_id= "'.$_GET['store_id'].'"';
       }
      

       if($item_search!=''){
           $PageAdd.= 'search='.$_GET["search"];
            $where.= ' AND product_name LIKE "%'.$item_search.'%" ';
       }else if($from!='' && $to!=''){
           $PageAdd.= 'from='.$from.'&to='.$to;
            $where.= ' AND price Between '.$from.' AND '.$to.' ';
       }else if($manual!=''){
        $where.= ' AND product_name LIKE "%'.$manual.'%"';
        $PageAdd.= 'manual='.$_GET["manual"];
       }
       else{
        if($catId!=''){
           $PageAdd.= 'catId='.$_GET["catId"];
           $where.= ' AND cat_id = "'.$catId.'" ';
       }
       if($SubCatId!=''){
            $where.= ' AND subCatID ="'.$SubCatId.'" ';
            $PageAdd.= '&subCatID='.$_GET["SubCatId"];
       }

       }
       



        $paginationlink = base_url()."Home/getRecord?".$PageAdd."&page=";  
       $Query ="SELECT product.* from product,login lg  ".$where."  order by product.id DESC" ;
        $sql = $this->db->query($Query." limit " . $start . "," . $page0);
   
        $faq = $sql->result_array();
        $all_record = 0;
         $_GET["rowcount"]= '';
        if(empty($_GET["rowcount"])) {
        $all_record=$this->db->query($Query)->num_rows();
          $_GET["rowcount"] = ceil($all_record/12);
        }
        // dd( $this->db->last_query());

    
        if($pagination_setting == "prev-next") {
          $perpageresult = $this->getPrevNext($_GET["rowcount"], $paginationlink,$pagination_setting); 
        } else {
          $perpageresult = $this->getAllPageLinks($_GET["rowcount"], $paginationlink,$pagination_setting); 
        }
      
       $output1 = $output2 = '';

 

        if(count($faq)==0){

             $output1='
        <div class="col-12">
          <!-- Card -->
            <div class="card promoting-card">
              <div class="card-body">
                 <h4 style="color:green">Product Not Found</h4>
              </div>
          </div>
        <!-- Card -->
      </div>';
      }else{
      foreach($faq as $k=>$v) {
       $output1.=$this->divReady($v);
       $output1.' <input type="hidden" id="rowcount" name="rowcount" value="' . $_GET["rowcount"] . '" />';
      }
    }


      if(!empty($perpageresult) ) {
      $output2 .= '<div id="pagination">' . $perpageresult . '</div>';
      }
      $data['result']= $output1;
      $data['pagination']= $output2;
      $data['count']= isset($_GET["rowcount"]) ? $_GET["rowcount"] : 0; 
      $last= ($start+$page0);
      $start =     ($start+1);
      if($all_record==0 ){
         $start =0;
         $last = 0;
      }else if($all_record<$last){
           $last = $all_record;
      }
      $data['total']=   $start. " - " .$last." Of ".$all_record;
      echo json_encode($data);
}

// <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
//                           <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
//                          <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
 // <button type="submit" class=" size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
 //                                                <i class="fa fa-eye" aria-hidden="true"></i>  
 //                                            </button>
//                       </a>
//  // 
public function divReady($value){
   $oldPrice = ($value['retail_price']>0) ? '<del>'.$value['retail_price'].'</del> ' : '';
  $out='
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 p-b-50 mobilStyle">
          <!-- Block2 -->
          <div class="block2">
              <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew image-reload">
                  <img src="'.base_url('uploads/product/'.$value['image1']).'" alt="IMG-PRODUCT" height="240">

                  <div class="block2-overlay trans-0-4">
                      

                                         
                                        <div class="btn-group block2-btn-addcart w-size2 trans-0-4">
                                            <!-- Buton -->
                                            <input type="hidden" name="inputId" value="'.$value['id'].'" >
                                           
                                            
                                              <button type="submit" class=" size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                               <a style="color:#fff" href="'.base_url('Home/Detail/'.encrypt($value['id'])).'" > <i class="fa fa-eye" aria-hidden="true"></i>  </a>
                                            </button>

                                            <button type="button" class="flex-c-m btn-addcart  size1 bg4 bo-rad-23 s-text1 trans-0-4 mx-2" data-id="'.$value["id"].'">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button> 

                                        </div>
                                       
                    </div>
                  </div>
              </div>

              <div class="block2-txt p-t-20" style="display:none">
                  <a href="'.base_url('Home/Detail/'.encrypt($value['id'])).'" class="block2-name dis-block s-text3 p-b-5">
                     '.$value['product_name'].'
                  </a>

                  <span class="block2-oldprice m-text7 p-r-5">
                      '.$oldPrice.'
                  </span>


                  <span class="block2-newprice m-text8 p-r-5">
                     Rs. '.$value['price'].'
                  </span>

              </div>


              <div class="block2-txt p-t-20">
                      
                    <a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
                       '.$value["product_name"].'  
                    </a>
                    <span class="block2-price m-text6 p-r-5 m-b-0">
                       Rs. '.$oldPrice.' '.$value['price'].'
                       <!-- <code class="text-success pull-right"><i class="fa fa-truck" aria-hidden="true"></i> |  <i class="fa fa-motorcycle" aria-hidden="true"></i></code> -->
                    </span>
                  </div>





          </div>
      </div>';



  return $out;

}


  public function Contact_Inquery(){
    $post=$this->input->post();
     if(!empty($post)){
      
        $Data=array(
        'name'      => $post['name'],
        'email'  => $post['email'],
        'subject'     => $post['subject'],
        'message'      => $post['message'],
        'created_by'      => $this->input->ip_address(),
       );
 
        $this->db->insert('contact',$Data); 
        echo json_encode(true);  
     }
     else{
      redirect(base_url());
     }
  }


  public function SubmitReview(){
    $post=$this->input->post();
     if(!empty($post)){
      
        $Data=array(
        'customer_id'      => $_SESSION['id'],
        'order_id'  => decrypt($post['order_id']),
        'order_details_id'     => decrypt($post['order_detail_id']),
        'product_id'     => decrypt($post['product_id']),
        'stars'      => $post['whatever1'],
        'comment'      => $post['comment'],
        'is_active'      => 1,
       );
 
        $this->db->insert('product_reviews',$Data); 
        echo json_encode(true);  
     }
     else{
      redirect(base_url());
     }
  }
  public function VendorSubmit(){

     $post=$this->input->post();
     if(!empty($post)){
       $this->load->library('form_validation');
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $this->form_validation->set_rules('name', 'Name', 'required|max_length[50]');
       $this->form_validation->set_rules('username', 'Username', 'required|usernameOnly|is_unique_update[login.username@id@]');
       $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique_update[login.email@id@]');
       $this->form_validation->set_rules('phone', 'Phone', 'required|max_length[13]|min_length[10]');
        $this->form_validation->set_rules('shop', 'Shop Name', 'required|max_length[50]');
        $this->form_validation->set_rules('address', 'Address', 'required|max_length[100]');

        if ($this->form_validation->run() == FALSE)
      {
           
          $data['result_set']=$post;
          $data['content'] = 'Home/vendor';

          $this->session->set_flashdata('error', 'Please Correction required In You Form!!');

          return  $this->load->view('vendor',$data);                    
      }


      $Data=array(
        'name'      => $post['name'],
        'username'  => $post['username'],
        'email'     => $post['email'],
        'shop'      => $post['shop'],
        'phone'     => $post['phone'],
        'address'   => $post['address'],
        'roleId'    => 2,
        'add'       => 1,
        'edit'      => 1,
        'delete'    => 1 ,
        'view'      => 1,
        'is_active' => 0,
       );
 
        $this->db->insert('login',$Data);
        $this->session->set_flashdata('saved', 'Registration Successfully !');
         redirect(base_url($this->controller.'/vendor'));
         }   
    redirect(base_url('Home'));
  }
  public function Detail(){

      $id = decrypt($this->uri->segment(3));
 
      if($id){
          $data['content'] = 'Home/detail';
         $data['detail'] = $this->Mdl_Home->get_productDetal($id);
         $data['store_list'] = $this->Mdl_Home->get_store_list();
         if(count($data['detail'])==0){
            redirect(base_url($this->controller.'/'));
         }
        return $this->load->view('WebView/template',$data);
      }
      redirect(base_url($this->controller.'/'));
  }
  public function relatedItem($category,$except){
      return $this->Mdl_Home->db_select('*','product','is_active =1 AND cat_id= '.$category.' AND id !='.$except);
  }
  public function search(){
    $data['content']    = 'searchItems';
    $data['store_list'] = $this->Mdl_Home->get_store_list();
    return $this->load->view('WebView/template',$data);
  }
 
public function send_mail($email=null,$subject=null,$message=null,$attachment=null)
{

 
        $config['protocol']         = 'smtp';                   // 'mail', 'sendmail', or 'smtp'
        $config['smtp_host']        = 'ssl://smtp.googlemail.com';
        $config['smtp_user']        = '_mainaccount@estorez.com.pk';
        $config['smtp_pass']        = 'n8271csoEM@$';
        $config['smtp_port']    = '465';                 // (in seconds)
        $config['smtp_timeout'] = 7;
        $config['newline']      = "\r\n";
        $config['crlf']         = "\r\n";
        $config['mailtype']     = 'html';
        $config['charset']      = 'iso-8859-1';                 // 'UTF-8', 'ISO-8859-15', ...; NULL (preferable) means config_item('charset'), i.e. the character set of the site.
   
/*
$config = array(
    'protocol' => 'sendmail', // 'mail', 'sendmail', or 'smtp'
    'smtp_host' => 'smtp.example.com', 
    'smtp_port' => 465,
    'smtp_user' => '_mainaccount@estorez.com.pk',
    'smtp_pass' => 'n8271csoEM@$',
    'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
    'mailtype' => 'html', //plaintext 'text' mails or 'html'
    'smtp_timeout' => '4', //in seconds
    'charset' => 'iso-8859-1',
   // 'wordwrap' => TRUE
);
*/
 
    $this->load->library('email', $config);

$this->email->set_newline("\r\n");

    $this->email->from('az.tariq03@gmail.com','E-Storez');
    $this->email->to("shanisoftwareengineer@gmail.com"); 
    $this->email->subject("azeem tsting");
    $this->email->message("azeem testing"); 
 
      if($attachment!=null){
        $this->email->attach($attachment, 'attachment', 'report.pdf');
     }
     $this->email->send();
     dd(1);
}


public function sending(){


$config['protocol']    = 'smtp';
$config['smtp_host']    = 'ssl://smtp.googlemail.com';
$config['smtp_port']    = '465';
$config['smtp_timeout'] = '7';
// $config['smtp_user']    = '_mainaccount@estorez.com.pk';
// $config['smtp_pass']    = 'n8271csoEM@$';


 $config['smtp_user']    = 'az.tariq03@gmail.com';
 $config['smtp_pass']    = '@zeem123';

$config['charset']    = 'utf-8';

// $config['newline']    = "\r\n";
$config['mailtype'] = 'html'; // or html
// $config['validation'] = TRUE; // bool whether to validate email or not      


$this->load->library('email', $config);

$this->email->from('az.tariq03@gmail.com', 'e store');
$this->email->to('shanisoftwareengineer@gmail.com'); 
$this->email->subject('Email Test');
$this->email->message('Testing the email class.');  

$this->email->send();

echo $this->email->print_debugger();
}

public function recovery(){
    $data['content']    = 'recover';
    return $this->load->view('WebView/template',$data);
}
}
 
