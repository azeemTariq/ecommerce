<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carts extends MX_Controller {

  public $perpage;
  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    $this->load->model('Mdl_Home');
    $this->perpage = 1;
  }
  public $controller = __CLASS__;
  public function index(){
    $data['content']    = 'index';
    // $data['store_list'] = $this->Mdl_Home->get_store_list();
    // $data['product_list'] = $this->Mdl_Home->get_product_list();
     return $this->load->view('html');
    // return $this->load->view('WebView/template',$data);
  }
 
     public function select_product_info_by_product_id($product_id){
    $data = $this->db->select('*')
        ->from("product")
        ->where("id",$product_id)
        ->get()
        ->row();
        return $data;
    }  
    public function select_product_info_by_product_id_ajax($product_id){
    $product = $this->db->select('product.*,category.category_name,subCategoryName AS brand')
        ->from("product")
        ->join("category","category.id=product.cat_id",'left')
        ->join("sub_category","sub_category.id=product.subCatID",'left')
        ->where("product.id",$product_id)
        ->get()
        ->row();
          if(file_exists('uploads/product/'.$product->image1))
        $product->image1=base_url('uploads/product/'.$product->image1);
        else
         $product->image1=base_url('assets/404.png');
        $product->product_code= !empty($product->product_code) ? " - [".$product->product_code."]" : "";
        $product->description=decrypt($product->description);
        if(strlen($product->description)>800)
        $product->description=substr_replace($product->description,'',0, 800)." <a href='#'> See More</a>";

      echo json_encode($product);
    }
    public function AddToCart(){
      $product_id = $this->input->post("p_id") ?? $this->input->get("p_id");
      $qty          = $this->input->post('qty') ?? 1;  
      $suggestion   = $this->input->post('suggestion') ??  '--';
      $product_info = $this->select_product_info_by_product_id($product_id);
      $data = array(
          'id'      => $product_info->id,
          'qty'     => $qty,
          'suggestion'=>$suggestion,
          // 'price'   => ($product_info->ProductDiscount>=1) ? ($product_info->ProductPrice-$product_info->ProductDiscount) : $product_info->ProductPrice,
          'price'   =>  $product_info->price,
          'name'    => $product_info->product_name,
          'options' => array('pro_image' => $product_info->image1)
        );


      // total_items


        if($this->input->post('cart_row')){
          $data = array(
              'rowid'       => $this->input->post('cart_row'),
              'qty'         => $qty,
              'suggestion'  =>$suggestion,
          );
         $this->cart->update($data);
        }
        else{ 
          $this->cart->insert($data);
        }


       
    
        if($this->input->post("isAjax")){


            $cart_content = $this->cart->contents();
                    $Subtotal = 0;
                    $output   = '';
                       foreach ($cart_content as $items){ 
                        $Subtotal += $items['qty']*$items['price'];

                         $output.='
                                <div class="product-cart-item">
                                   <div class="product-img">
                                    <a href="#"><img src='.base_url('uploads/product/'.$items['options']['pro_image']).' alt=""></a>
                                  </div>
                                  <div class="product-info">
                                    <h4 class="title"><a href="#">'.$items['name'].'</a></h4>
                                    <span class="info">'.$items['qty'].' x '.$items['price'].'</span>
                                  </div>
                                  <div class="product-delete"><a href="'.base_url('delete_to_cart/'.$items['rowid']).'">×</a></div>
                                </div>';
                             }

        $_return['cart']= $output;
        $_return['total']= '<h4 style="color:black">Total: <span class="money">'.$Subtotal.'</span></h4>';
        $cart_content = $this->cart->contents();
        $_return['count']= 0;
        foreach ($cart_content as $items)
        { 
          $_return['count']+=$items['qty'];;
        }
           echo json_encode($_return);
        }else{
           return redirect(base_url("Home"));
        }
    
    }
    
    public function AddToWish(){
      $product_id = $this->input->post("p_id") ?? $this->input->get("p_id");
      $product_info = $this->select_product_info_by_product_id($product_id);
      $data = array(
          'id'      => $product_info->id,
          'price'   =>  $product_info->price,
          'name'    => $product_info->product_name,
          'options' => array('pro_image' => $product_info->image1)
        );

        $_SESSION['wishlist'][$product_info->id]=$data;
         if(isset($_SESSION['wishlist']))
        $wishlist=$_SESSION['wishlist'];
      else
        $wishlist=[];
        echo json_encode(count($wishlist));
    
    }
    public function RemoveWishlist()
    {
        $product_id = $this->input->post("p_id") ?? $this->input->get("p_id");
        unset($_SESSION['wishlist'][$product_id]);
         if(isset($_SESSION['wishlist']))
        $wishlist=$_SESSION['wishlist'];
      else
        $wishlist=[];
        echo json_encode(count($wishlist));    
    }

  public function show_cart(){
    $data['main_content'] = $this->load->view('front/view_cart','',true);
    $this->load->view('front/index',$data);
  }
  public function delete_to_cart($row_id){
    $data = array(
        'rowid' => $row_id,
        'qty'   => 0
      );
    $this->cart->update($data);
    return redirect("shop-cart");
  }
  public function Order(){
       $post=$this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $MasterRules = $this->Mdl_Home->MasterRules;
        $this->form_validation->set_rules($MasterRules);
       if ($this->form_validation->run() == FALSE){
          $data['row'] = $post;
          $data['content'] = 'Home/checkout_form';
         return $this->load->view('WebView/template',$data);
       }
    
    $id = $post['cust_id'];
       if(empty($post['cust_id'])){
        $record = $this->db->select('email,username')->from('login')
        ->where('username',$post['first_name'])->or_where('email',$post['email'])->get()->row();
        if(count($record)>0){
          $data['row'] = $post;
            if($record->email==$post['email']){
              $this->session->set_flashdata('error', 'Email Address Already Exist !');
             }else{
              $this->session->set_flashdata('error', 'Username Already Exist !');
             }
          $data['content'] = 'Home/checkout_form';
         return $this->load->view('WebView/template',$data);
        }
          $data=array(
                'username'      =>$post['first_name'],
                'name'          =>$post['first_name']." ".$post['last_name'],
                'email'         =>$post['email'],
                'phone'         =>$post['cell'],
                'password'       =>'@#$#FGFFF447',
                'is_active'     =>1,
                'is_customer'   =>1,
              );
              $this->db->insert('login',$data);
          $id = $this->db->insert_id();
       }


      $cart_content = $this->cart->contents();
     if(count($cart_content) && count($post)){
      $this->db->trans_start();
            $datas = array(
              'date'                => date('Y-m-d'),
              'time'                => date('H:i:s'),
              'year'                => date('Y'),
              'cust_id'             => $id,
              'first_name'          => $post['first_name'],
              'last_name'           => $post['last_name'],
              'ship_address'             => $post['ship_address'],
              'ship_country'             => $post['ship_country'],
              'ship_postal'               => $post['ship_postal'],
              'bill_address'             => $post['bill_address'],
              'bill_country'             => $post['bill_country'],
              'bill_postal'             => $post['bill_postal'],
               'cell'                => $post['cell'],
               'email'               => $post['email'],
              'items'               =>  count($cart_content),
              'TotalAmount'         => $post['TotalAmount'],
              'created_date'        => date('Y-m-d H:i:s'),
              'Status'              => 1,
              'payment_method'      => isset($post['payment_method']) ? 1 : 0,
            ); 
            $this->db->insert('orders',$datas);
            $OrderId=$this->db->insert_id();
            $orderCode=$this->Mdl_Home->db_select_single_row('code','orders',['id'=>$OrderId]);
            if($OrderId){
              foreach ($cart_content as $key => $value) {
                $item = array(
                  'OrderId' =>$OrderId,
                  'year'    => date('Y'),
                  'code'    => $orderCode['code'],
                  'ItemId'  => $value['id'],
                  'ItemQty' => $value['qty'],
                  'suggestion' => "",
                  'Status'  => 1
                );
                $this->db->insert('order_detail',$item);
              }
              $this->send_mail($post['email']);
              $this->db->trans_complete();
              $data['order'] = $orderCode['code'];
              $this->clear();
              $_POST = [];
              $data['content'] = 'Home/thank';
              return $this->load->view('WebView/template',$data);
            }else{
               redirect(base_url('Home/viewCart'));
            }
    }else{
      redirect(base_url('/'));
    }
  }

function clear()
 {
  $this->load->library("cart");
  $this->cart->destroy();
 }
  function remove()
 {
  $this->load->library("cart");
  $row_id = decrypt($this->input->post('id'));
  $data = array(
   'rowid'  => $row_id,
   'qty'  => 0
  );
  $this->cart->update($data);
  echo json_encode(1);
}
  
public function send_mail($mail) {
    return false;
    $this->load->library('email');   
    $config = array();
    $config['protocol']     = "smtp"; // you can use 'mail' instead of 'sendmail or smtp'
    $config['smtp_host']    = "ssl://smtp.googlemail.com";// you can use 'smtp.googlemail.com' or 'smtp.gmail.com' instead of 'ssl://smtp.googlemail.com'
    $config['smtp_user']    = " account mail"; // client email gmail id
    $config['smtp_pass']    = "pass here"; // client password
    $config['smtp_port']    =  465;
    $config['smtp_crypto']  = 'ssl';
    $config['smtp_timeout'] = "";
    $config['mailtype']     = "html";
    $config['charset']      = "iso-8859-1";
    $config['newline']      = "\r\n";
    $config['wordwrap']     = TRUE;
    $config['validate']     = FALSE;
    $this->load->library('email', $config); // intializing email library, whitch is defiend in system

    $this->email->set_newline("\r\n"); // comuplsory line attechment because codeIgniter interacts with the SMTP server with regards to line break

  
    $this->email->from('from email');
    $this->email->to($mail);
    $this->email->subject(' Order Completed! '); 
    $this->email->message('Thank you for Order us');  // we can use html tag also beacause use $config['mailtype'] = 'HTML'
    //Send mail
    if($this->email->send()){
       // $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
       // echo "email_sent";
    }
    else{
        echo "email_not_sent";
        echo $this->email->print_debugger();  // If any error come, its run
    }
}

        

}