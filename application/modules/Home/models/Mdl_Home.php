<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_Home extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_store_list(){
    	return $this->db_select('*','store','is_active  <> 2');
    }
   public function get_product_list(){
        $date = date('Y-m-d').' 000:00:00';
        return $this->db_select('pd.*','product pd,login lg',
            "pd.created_by = lg.id
            AND pd.is_active=1 
            AND lg.is_active=1 
             ",'','','id DESC',null,null,'24');
    }
    public function get_product_list_new($where=[],$limit=8,$start=0,$orderby="",$orderbyvalue="",$group_by=""){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('is_active',1);
        if($where)
        $this->db->where($where);
        if(empty($group_by))
        $this->db->group_by($group_by);
        if(empty($orderby))
        $this->db->order_by('id',"DESC");
        else
        $this->db->order_by($orderby,$orderbyvalue);
        $this->db->limit($limit,$start);
        
        return $this->db->get()->result();
    }
    public function get_product_total($where=[]){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('is_active',1);
        if($where)
        $this->db->where($where);
        
        return $this->db->count_all_results();
    }
    public function check_product_review($order_id,$order_detail_id,$product_id){
        $this->db->select('*');
        $this->db->from('product_reviews');
        $this->db->where('is_active',1);
        $this->db->where('order_id',$order_id);
        $this->db->where('order_details_id',$order_detail_id);
        $this->db->where('product_id',$product_id);
        $this->db->where('customer_id',$_SESSION['id']);
        return $this->db->count_all_results();
    }
    public function most_sale($where=[],$limit=8){
        $this->db->select('product.*');
        $this->db->from('product');
        $this->db->join('order_detail','order_detail.ItemId = product.id','left');
      
        $this->db->where('product.is_active',1);
        
        if($where)
        $this->db->where($where);
      
        $this->db->group_by('product.id');
        $this->db->order_by('COUNT(order_detail.ItemId)',"DESC");
        $this->db->limit($limit);
        
        return $this->db->get()->result();
    }
    public function get_productDetal($id){
        return $this->db_select_single_row('product.*,store_name,category_name','product,store,category'," 
            product.store_id = store.id
            AND product.cat_id = category.id
            AND product.id = ".$id);
    }
    public function bannersList($id,$type=null){

        $this->db->select('banners.*,store_name');
        $this->db->from('banners');
        $this->db->join('store','store.id = banners.store_id','left');
        $this->db->where('banners.is_active',1);
        if($type!=null){
            $this->db->where('banners.store_id',0);
        }
        if(!empty($id)){
            $this->db->where('banners.id',$id);
        }
       return $this->db->get()->result();
    }    
    public function partnerLogos(){
        $this->db->select('*');
        $this->db->from('partners');
        $this->db->where('is_active',1);
       return $this->db->get()->result();
    }
    public function category_list($where=[])
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('is_active',1);
        if($where)
        $this->db->where($where);
        
        return $this->db->get()->result();
    }
    public function brand_list($where=[])
    {
        $this->db->select('*');
        $this->db->from('sub_category');
        $this->db->where('is_active',1);
        if($where)
        $this->db->where($where);
        
        return $this->db->get()->result();
    }   
    public function privacyContent($where=[])
    {
        $this->db->select('*');
        $this->db->from('policies');
        $this->db->where('is_active',1);
        if($where)
        $this->db->where($where);
        
        return $this->db->get()->row();
    }   
    public function about_us_content($where=[]){
        $this->db->select('*');
        $this->db->from('about_us');
        if($where)
        $this->db->where($where);
        
        return $this->db->get()->row();
    }   
    public function faq_list($where=[]){
        $this->db->select('tbl_faq.id,faq_category.id cat_id,faq_category.title,tbl_faq.ques,answer');
        $this->db->from('tbl_faq');
        $this->db->join('faq_category','faq_category.id = tbl_faq.category','inner');
        if($where)
        $this->db->where($where);
        
        $this->db->order_by('faq_category.id');
        return $this->db->get()->result();
    }
    public $MasterRules = array (
            'first_name' => array (
                    'field' => 'first_name',
                    'label' => 'First Name',
                    'rules' => 'required|regex_match[/^[a-zA-Z]+$/]|max_length[25]'
            ),
            'last_name' => array (
                    'field' => 'last_name',
                    'label' => 'Last Name',
                    'rules' => 'required|regex_match[/^[a-zA-Z]+$/]|max_length[25]'
            ),
            'cell' => array (
                    'field' => 'cell',
                    'label' => 'Cell No',
                    'rules' => 'required|regex_match[/^[0-9+]+$/]|max_length[15]'
            ),
            'email' => array (
                    'field' => 'email',
                    'label' => 'Email Address',
                    'rules' => 'required|trim|valid_email|max_length[25]'
            ),
            'ship_address' => array (
                    'field' => 'ship_address',
                    'label' => 'Address',
                    'rules' => 'max_length[100]'
            ),
             'bill_address' => array (
                    'field' => 'bill_address',
                    'label' => 'Address',
                    'rules' => 'max_length[100]'
            ),
            'bill_country' => array (
                    'field' => 'bill_country',
                    'label' => 'Country Code',
                    'rules' => 'integer|trim|regex_match[/^[0-9]+$/]|max_length[16]'
            ),   
            'ship_country' => array (
                    'field' => 'ship_country',
                    'label' => 'Country Code',
                    'rules' => 'integer|trim|regex_match[/^[0-9]+$/]|max_length[16]'
            ),    
            'documentno' => array (
                    'field' => 'documentno',
                    'label' => 'Document no',
                    'rules' => 'integer|trim|regex_match[/^[0-9]+$/]|max_length[16]'
            ),
            'massage' => array (
                    'field' => 'massage',
                    'label' => 'Description',
                    'rules' => 'max_length[255]'
            ),
        
        );

}