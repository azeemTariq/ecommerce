<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Grandeur | Invoice</title>

    <!--== Favicon ==-->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/website/img/favicon.ico" type="image/x-icon" />

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 2px solid;
    }
</style>
<style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   ;
   color: black;
   text-align: center;
}
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title" align="center">
                <h2 class="pull-left">Invoice</h2><img class="logo-main" style="height: 50px;width: 150px;margin-top: 10px;margin-left: 15px;" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo" /><h3 class="pull-right">Order # <?= $order['code'] ?></h3>
            </div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					<?= $order['first_name']." ".$order['last_name'] ?><br>
    					<?= $order['bill_address'] ?><br>
    					Pakistan<br>
    					<?= $order['bill_postal'] ?>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong>Shipped To:</strong><br>
    					<?= $order['first_name']." ".$order['last_name'] ?><br>
                        <?= $order['ship_address'] ?><br>
                        Pakistan<br>
                        <?= $order['ship_postal'] ?>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
                    <?php $order['payment_method']==1 ? $order['payment_method']="Cash in Hand": $order['payment_method']="Credit / Debit Card"; ?>
    				<address>
    					<strong>Payment Method:</strong><br>
    					<?= $order['payment_method'] ?><br>
    					<?= $order['email'] ?>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					<?= date("F d, Y",strtotime($order['date'])) ?><br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td>Product</td>
                                    <td><strong>SKU</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<?php $Subtotal=0; foreach($order_detail as $items) { ?>
    							<tr>
    								<td><?= $items['product_name'] ?></td>
                                    <td><?= $items['product_code'] ?></td>
    								<td class="text-center"><?= $items['price'] ?></td>
    								<td class="text-center"><?= $items['ItemQty'] ?></td>
    								<td class="text-right"><?= $Subtotal+=$items['ItemQty']*$items['price'] ?></td>
    							</tr>
                               
                                <?php } ?>
    							<tr>
    								<td class="thick-line"></td>
                                    <td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right"><?= $Subtotal ?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
                                    <td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Shipping</strong></td>
    								<td class="no-line text-right">0</td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
                                    <td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right"><?= $Subtotal ?></td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<div class="footer">
  <p> All rights reserved by <strong>Gradeur</strong></p>
</div>
</body>
</html>