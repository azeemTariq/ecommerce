<style type="text/css">
  .qty-btn{
    color: #fff;
  }
</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">Your Wish List</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Your Wish List</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

    <!--== Start Cart Area Wrapper ==-->
    <section style="background-color:black;" class="product-area cart-page-area ">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="cart-table-wrap">
              <form action="#" method="POST">
              <div class="cart-table table-responsive">
                <table>
                  <thead>
                    <tr>
                      <th class="pro-product">Product</th>
                      <th class="pro-price text-center">Price</th>
                      <th class="pro-remove text-center">Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $Subtotal=0;
                     if(isset($_SESSION['wishlist']))
                      $wishlist=$_SESSION['wishlist'];
                    else
                      $wishlist=[];
                     foreach ($wishlist as $key => $value) {
                    ?>
                    <tr>
                      <td class="pro-product">
                        <div class="product-info">
                          <div class="product-img">
                            <a href="#"><img src='<?= base_url('uploads/product/'.$value['options']['pro_image']) ?>' alt=""></a>
                          </div>
                          <div class="product-info"><?= $value['name'] ?></a></h4>
                          </div>
                        </div>
                      </td>
                       <input type="hidden"  name="rowid[]" value="<?= $value['id'] ?>" />
                      <td class="pro-price text-center"><span><?= $value['price'] ?></span></td>
                      <td data-id="<?= $value['id'] ?>" class="pro-remove remove-wishlist text-center"><a href="#/">×</a></td>
                    </tr>
                    <?php } 

                     
                    if(empty($wishlist)){
                      ?>
                      <tr>
                      <td colspan="3" align="center">
                        No Product in Wish List
                        </td>
                    </tr>
                    <?php }  ?>
                  </tbody>
                </table>
              </div>
             

              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Cart Area Wrapper ==-->