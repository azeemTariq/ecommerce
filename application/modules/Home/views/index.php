   <!--== Start Hero Area Wrapper ==-->
    <section class="home-slider-area slider-default">
      <div class="home-slider-content">
        <div class="swiper-container home-slider-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <!-- Start Slide Item -->
              <div class="home-slider-item">
                <div class="slider-content-area">
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-xl-6 col-md-8">
                        <div class="content">
                          <div class="inner-content">
                            <div class="tittle-wrp" style="background-color: #141417;">
                              <h2>The Best</h2>
                              <h1 >PERFUME</h1>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-6 col-md-4">
                        <div class="slider-thumb">
                          <img src="<?= base_url() ?>assets/website/img/slider/5A.png" alt="Grandeur-Image">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Slide Item -->
            </div>
            <div class="swiper-slide">
              <!-- Start Slide Item -->
              <div class="home-slider-item">
                <div class="slider-content-area">
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-xl-6 col-md-8">
                        <div class="content">
                          <div class="inner-content">
                            <div class="tittle-wrp" style="background-color:  #141417" >
                              <h2>The Best</h2>
                              <h1>PERFUME</h1>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-6 col-md-4">
                        <div class="slider-thumb">
                          <img src="<?= base_url() ?>assets/website/img/slider/4A.png" alt="Grandeur-Image">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Slide Item -->
            </div>
          </div>          
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
      </div>
    </section>
    <!--== End Hero Area Wrapper ==-->






    <!--== Start Featured Area Wrapper ==-->
    <section class="featured-area featured-default-area">
      <div class="container"  >
        <div class="row align-items-center">
          <div class="col-md-7" data-aos="fade-up" data-aos-duration="1000">
            <div class="row">
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content">
                    <div class="content">
                      <h4 class="title">Happiness Guarantee</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content">
                    <div class="content">
                      <h4 class="title">Free Shipping</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content mb-lg-0">
                    <div class="content">
                      <h4 class="title">24/7 Support Expert</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content mb-0">
                    <div class="content">
                      <h4 class="title">All Trusty Brand</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5" data-aos="fade-left" data-aos-duration="1000" >
            <div class="featured-thumb">
              <img src="<?= base_url() ?>assets/website/img/photos/discount1.jpg" alt="Grandeur-Image">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Featured Area Wrapper ==-->

    <!--== Start Featured Product Area Wrapper ==-->
    <section class="divider-area divider-default-area" style="background-color: #141417">
      <div class="container">
        <div class="row">
          <div class="col-md-7 order-2 order-md-1" data-aos="fade-up" data-aos-duration="1000">
            <div class="thumb mr-70 sm-mr-0">
              <a href="#"><img src="<?= base_url() ?>assets/website/img/photos/Img2.jpg" alt="Grandeur-Image"></a>
            </div>
          </div>
          <div class="col-md-5 order-1 order-md-2" data-aos="fade-left" data-aos-duration="1000">
            <div class="divider-content divider-content-style2">
              <h2 class="title"><a href="#">The Perfume Item <br> find Here</a></h2>
              <p>Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular</p>
              <a class="btn-link" href="#">Browse All Products</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Featured Product Area Wrapper ==-->

    <!--== Start Product Tab Area Wrapper ==-->
    <section class="product-area product-default-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div class="row">
                <div class="col-xl-12 col-md-12" >
                  <div class="nav nav-tabs"  id="nav-tab" role="tablist">
                      <button class="nav-link active" style="margin-left: 5px;"  id="new-arrivals-tab" data-bs-toggle="tab" data-bs-target="#new-arrivals" type="button" role="tab" aria-controls="new-arrivals" aria-selected="false">New Arrivals</button>
                      <button class="nav-link " style="margin-left: 5px;" id="new-product-tab" data-bs-toggle="tab" data-bs-target="#new-product" type="button" role="tab" aria-controls="new-product" aria-selected="true">Sale Products</button>
                      <button class="nav-link " style="margin-left: 5px;"  id="popular-product-tab" data-bs-toggle="tab" data-bs-target="#popular-product" type="button" role="tab" aria-controls="popular-product" aria-selected="false">Popular Products</button>
                      
                  </div>
                </div>
                <div class="col-xl-12 col-md-12">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade " id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">

                         <?php  foreach ($popular_sale_list as $key => $value) {
                           $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                           
                            $image1=base_url('assets/404.png');
                            if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                            $image1=base_url('uploads/product/'.$value->image1);

                          ?>
                        <div class="col-12 col-sm-3">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="<?= $link ?>">
                                <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <?php  if($value->price!=$value->retail_price) { ?>
                                <span class="ribbon ribbon-hot">Sale</span>
                                <?php  } ?>
                                <!-- <span class="ribbon ribbon-onsale align-right">-35%</span> -->
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="javascript:;">
                                   <button class="btn-addcart" data-id="<?= $value->id ?>">
                                  <i class="icofont-shopping-cart"></i>
                                </button>
                                </a>
                                <a class="action-quick-view" data-id="<?= $value->id ?>" href="javascript:void(0);" >
                                  <i class="icon-zoom"></i>
                                </a>
                                <!-- <a class="action-compare"  href="javascript:void(0);" title="Share">
                                  <i class="icon-compare"></i>
                                </a> -->
                                <a class="action-wishlist" data-id="<?= $value->id ?>" href="javascript:void(0);" title="Wishlist">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div style="display: none;" class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="tab-pane fade show  active" id="new-arrivals" role="tabpanel" aria-labelledby="new-arrivals-tab">
                      <div class="row">
                        <?php foreach ($new_arrival_list as $key => $value) {
                           $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                           
                            $image1=base_url('assets/404.png');
                            if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                            $image1=base_url('uploads/product/'.$value->image1);
                         ?>
                        <div class="col-12 col-sm-3">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="<?= $link ?>">
                                <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <?php  if($value->price!=$value->retail_price) { ?>
                                <span class="ribbon ribbon-hot">Sale</span>
                                <?php  } ?>
                                <!-- <span class="ribbon ribbon-onsale align-right">-35%</span> -->
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="javascript:;">
                                   <button class="btn-addcart" data-id="<?= $value->id ?>">
                                  <i class="icofont-shopping-cart"></i>
                                </button>
                                </a>
                                <a class="action-quick-view" data-id="<?= $value->id ?>" href="javascript:void(0);" >
                                  <i class="icon-zoom"></i>
                                </a>
                                <!-- <a class="action-compare"  href="javascript:void(0);" title="Share">
                                  <i class="icon-compare"></i>
                                </a> -->
                                <a class="action-wishlist" data-id="<?= $value->id ?>" href="javascript:void(0);" title="Wishlist">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div style="display: none;" class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="tab-pane fade " id="new-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">
                        <?php foreach ($sale_list as $key => $value) { 
                            $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                            $image1=base_url('assets/404.png');
                            if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                            $image1=base_url('uploads/product/'.$value->image1);
                          ?>
                        <div class="col-12 col-sm-3">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="<?= $link ?>">
                                <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <?php  if($value->price!=$value->retail_price) { ?>
                                <span class="ribbon ribbon-hot">Sale</span>
                                <?php  } ?>
                                <!-- <span class="ribbon ribbon-onsale align-right">-35%</span> -->
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="javascript:;">
                                   <button class="btn-addcart" data-id="<?= $value->id ?>">
                                  <i class="icofont-shopping-cart"></i>
                                </button>
                                </a>
                                <a class="action-quick-view" data-id="<?= $value->id ?>" href="javascript:void(0);" >
                                  <i class="icon-zoom"></i>
                                </a>
                                <!-- <a class="action-compare"  href="javascript:void(0);" title="Share">
                                  <i class="icon-compare"></i>
                                </a> -->
                                <a class="action-wishlist" data-id="<?= $value->id ?>" href="javascript:void(0);" title="Wishlist">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div style="display: none;" class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                      <?php } ?>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Product Tab Area Wrapper ==-->
    <script type="text/javascript">
      
setInterval(function() { 
  $('.swiper-button-next').click();
}, 6000);
    </script>