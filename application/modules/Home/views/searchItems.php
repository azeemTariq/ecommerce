<style type="text/css">
    
.item-pagination{
    float: left;
}


.storeBox{
        padding-top: 4px;
    padding: 18px;
    margin-top: 8px;
    border: 1px solid #ccc;
}
</style>


<!-- Title Page -->
 <div class="container" style="display: none;">
    <?php $image = isset($banners[0]->image) ? base_url('uploads/banners/'.$banners[0]->image) :  base_url('assets/img/logo/s3.jpg'); ?>
    <section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo $image ?>);">
        <h2 class="l-text2 t-center">
            <?= isset($banners[0]->bannes_name) ? $banners[0]->bannes_name : '' ?>
        </h2>
        <p class="m-text13 t-center">
             <?= isset($banners[0]->description) ? $banners[0]->description : '' ?>
        </p>
    </section>
</div>
<input type="hidden" id="store_id"  value="<?= $store_id ?? '' ?>">


    <!-- Content page -->
    <section class="bgwhite p-t-55 p-b-65">
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-md-4 col-lg-3 ">
                    <div class="leftbar p-r-20 p-r-0-sm">

                        <!--  -->
                        <h4 class="m-text14 p-b-7">
                            Our Stores
                        </h4>

                        <ul class="p-b-54">

                        <?php  foreach ($store_list as $key => $value) { ?>
                             
                            <li class=" storeBox">
                                <div >
                    <img  style="border-radius: 2px !important;width:80px;height:50px;float: left;margin-top: -13px;" src="<?= base_url('uploads/store/'.$value->image); ?>" alt="IMG">
                  </div> 
                  
                                <a href="<?= base_url('Home/Product/'.encrypt($value->id)) ?>" data-id='<?= $value->id; ?>' class="s-text13 pl-2">
                                   
                                   <?= $value->store_name; ?>

                                </a>
       
                            </li>
                        <?php } ?>

                            
                        </ul>
                    </div>


                </div>

                <div class="col-sm-6 col-md-8 col-lg-9 ">
                    <!--  -->
                    <div class="flex-sb-m flex-w p-b-35">
                       

                            <div class="col-md-8 col-sm-6 col-xs-12">




                                    <div class="sec-title p-b-22">
                                        <h3 class="m-text5 ">
                                          Search List of "<?= $_GET['item'] ?? '' ?>"
                                        </h3> 
                                    </div>
                              <!--   <select class="form-control rm-select2" name="sorting">
                                    <option>Price</option>
                                    <option>$0.00 - $50.00</option>
                                    <option>$50.00 - $100.00</option>
                                    <option>$100.00 - $150.00</option>
                                    <option>$150.00 - $200.00</option>
                                    <option>$200.00+</option>

                                </select> -->
                            </div>
                        

                        <span class="s-text8 p-t-5 p-b-5" style="color: green">
                            <b>Showing <span id="total_row"></span> results</b>
                        </span>
                    </div>
                    <div id="wait" style="display:none;margin-left: 45%;">
                            <img src='<?= base_url() ?>assets/demo_wait.gif' width="84" height="84" />
                            <br>
                        </div> 

                    <!-- Product -->
                    <div class="row" id="product_list">
                         

                    </div>

                    <!-- Pagination -->
                    <div  id="pagination-result" class="pagination flex-m flex-w p-t-26">
                        <!-- <a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
                        <a href="#" class="item-pagination flex-c-m trans-0-4">2</a> -->

                        <input type="hidden" name="rowcount" id="rowcount" />
                    </div>
                      <div style="border-bottom: #F0F0F0 1px solid;margin-bottom: 15px;display:none">
                            Pagination Setting:<br> 
                            <select name="pagination-setting" onChange="changePagination(this.value);"  class="pagination-setting" id="pagination-setting">
                            <option value="all-links" selected="">Display All Page Link</option>
                            <option value="prev-next">Display Prev Next Only</option>
                            </select>
                        </div>
                </div>
            </div>
        </div>
    </section>
