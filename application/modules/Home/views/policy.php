<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/4.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content ">
              <h2 class="title"><?= $policy ?></h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active"><?= $policy ?></span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

    <!--== Start About Area ==-->
    <section class="about-area about-default-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="about-content">
                    <?php  if(!empty($contents) && isset($contents)){
                    foreach(json_decode($contents) as $list){ ?>
                    <p class="mb-15"><?= $list ?></p>
                     <?php } }?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End About Area ==-->