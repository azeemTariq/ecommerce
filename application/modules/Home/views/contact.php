<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/1.png');background-repeat: no-repeat;background-size: 100%;"  class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content ">
              <h2 class="title">Contact</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Contact</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

    <!--== Start Contact Area ==-->
    <section class="contact-area">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class=" col-lg-9 col-md-8">
            <div class="contact-form mb-sm-30">
              <form class="contact-form-wrapper" id="contact-form"  method="post">
                <div class="row">
                  <div class="col-lg-10 m-auto">
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="section-title">
                          <h2 class="title">Get In Touch</h2>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input class="form-control" type="text" name="con_name" id="con_name" placeholder="Your Name...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input class="form-control" type="email" name="con_email" id="con_email" placeholder="Your Email...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <input class="form-control" type="text" name="con_subject" id="con_subject" placeholder="Your Subject...">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group mb-0">
                          <textarea class="form-control textarea" name="con_message"  id="con_message" placeholder="Your Message"></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group mb-0">
                          <button onclick="submitcontact()" class="btn btn-theme btn-black" type="button">Send</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- Message Notification -->
            <div class="form-message"></div>
          </div>
          <div class=" col-lg-3 col-md-4">
            <div class="row">
              <div class="col-lg-12">
                <div class="contact-info-content">
                  <div class="contact-info-item">
                    <div class="content">
                      <h4>Head Quater</h4>
                      <p>104-C, Khe-e-Ittehad, 11th commercial Lane, Phase 2 ext, <br>DHA, Karachi, Pakistan. PO BOX 75500.</p>
                    </div>
                  </div>
                  <div class="contact-info-item">
                    <div class="content">
                      <h4>Contact with</h4>
                      <p>info@grandeur.com</p>
                      <p>+966 56 361 9999</p>
                    </div>
                  </div>
                  <div class="contact-info-item mb-0 pb-0">
                    <div class="content">
                      <h4>Follow us</h4>
                      <div class="social-widget">
                        <a href="#/"><i class="icon-social-twitter"></i></a>
                        <a href="#/"><i class="icon-social-pinterest"></i></a>
                        <a href="#/"><i class="icon-social-instagram"></i></a>
                        <a href="#/"><i class="icon-social-facebook-square"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Contact Area ==-->

    <!--== Start Map Area ==-->
    <div class="contact-map-area">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.8402891185374!2d144.95373631590425!3d-37.81720974201477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2ssg!4v1607294780661!5m2!1sen!2ssg"></iframe>
    </div>
    <!--== End Map Area ==-->