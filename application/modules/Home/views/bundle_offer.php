 <!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/3.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content ">
              <h2 class="title">Bundle Offer</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Bundle Offer</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->


<?php
    $current_page=1;
    $per_page=10;
    $active_tab='nav-grid';
    $sort_by='title-ascending';
    $page_url="bundle-offer";
    if(isset($_COOKIE[$page_url."-current-page"])) 
    {
      $current_page=$_COOKIE[$page_url."-current-page"];
    }
    if(isset($_COOKIE[$page_url."-per-page"])) 
    {
      $per_page=$_COOKIE[$page_url."-per-page"];
    }
    if(isset($_COOKIE["active-tab"])) 
    {
      $active_tab=$_COOKIE["active-tab"];
    }
    if(isset($_COOKIE["sort-by"])) 
    {
      $sort_by=$_COOKIE["sort-by"];
    }
    $orderby="id";
    $orderbyvalue="DESC";
    

    if($current_page==1)
    $start=0;
    else
    $start=$per_page*($current_page-1);
    $limit=10;
    $where=" retail_price > price ";
    $product = $this->Mdl_Home->get_product_list_new($where,$limit,$start,$orderby,$orderbyvalue);
    $get_product_total = $this->Mdl_Home->get_product_total($where);
    $pages = ceil($get_product_total / $limit);

 ?>



    <!--== Start Shop Area Wrapper ==-->
    <div class="product-area product-grid-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-12">
                <div class="shop-topbar-wrapper">
                  <div class="collection-shorting">
                     <div class="shop-topbar-left">
                      <div class="view-mode">
                        <nav>
                          <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button data-cookie="active-tab" data-page="nav-grid"  class="nav-link <?php  if($active_tab=="nav-grid") { echo 'active';} ?>" id="nav-grid-tab" data-bs-toggle="tab" data-bs-target="#nav-grid" type="button" role="tab" aria-controls="nav-grid" aria-selected="true"><i class="fa fa-th"></i></button>
                            <button  data-cookie="active-tab" data-page="nav-list" class="nav-link <?php  if($active_tab=="nav-list") { echo 'active';} ?>" id="nav-list-tab" data-bs-toggle="tab" data-bs-target="#nav-list" type="button" role="tab" aria-controls="nav-list" aria-selected="false"><i class="fa fa-list"></i></button>
                          </div>
                        </nav>
                      </div>
                    </div>
                    <div class="product-show-content"><p>Showing <?= $start+1 ?> - <?= count($product) ?> of <?= $get_product_total ?> result</p></div>
                    
                  </div>
                </div>
              </div>
            </div>
             <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade <?php  if($active_tab=="nav-grid") { echo 'show active';} ?> " id="nav-grid" role="tabpanel" aria-labelledby="nav-grid-tab">
                <div class="row">
                  <?php foreach($product as  $key => $value) {
                        $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                            
                            $image1=base_url('assets/404.png');
                            if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                            $image1=base_url('uploads/product/'.$value->image1);
                          ?>
                    <div class="col-lg-3 col-md-6">
                      <!-- Start Product Item -->
                      <div class="product-item">
                        <div class="product-thumb">
                          <a href="<?= $link ?>">
                            <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                          </a>
                          <div class="ribbons">
                             <?php  if($value->price!=$value->retail_price) { ?>
                                <span class="ribbon ribbon-hot">Sale</span>
                                <?php  } ?>
                            <!-- <span class="ribbon ribbon-onsale align-right">-15%</span> -->
                            <!-- <span class="ribbon ribbon-new align-right-top">New</span> -->
                          </div>
                          <div class="product-action">
                            <a class="action-cart btn-addcart" data-id="<?= $value->id ?>" href="javascript:void(0);" >
                              <i class="icofont-shopping-cart"></i>
                            </a>
                            <a class="action-quick-view"  data-id="<?= $value->id ?>" href="javascript:void(0);" title="Quick View">
                              <i class="icon-zoom"></i>
                            </a>
                            <!-- <a class="action-compare" href="javascript:void(0);" title="Quick View">
                              <i class="icon-compare"></i>
                            </a> -->
                            <a class="action-wishlist"  data-id="<?= $value->id ?>" href="javascript:void(0);" title="Wish List">
                              <i class="icon-heart-empty"></i>
                            </a>
                          </div>
                        </div>
                        <div class="product-info">
                              <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                              </div>
                        </div>
                      </div>
                      <!-- End Product Item -->
                    </div>
                  <?php } ?>
                </div>
              </div>
              <div class="tab-pane fade <?php  if($active_tab=="nav-list") { echo 'show active';} ?> " id="nav-list" role="tabpanel" aria-labelledby="nav-list-tab">
                <div class="row">
                  <?php foreach($product as  $key => $value) { 
                      $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                           
                            $image1=base_url('assets/404.png');
                            if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                            $image1=base_url('uploads/product/'.$value->image1);
                          ?>
                  <div class="col-12">
                    <!-- Start Product Item -->
                    <div class="product-item product-item-list">
                      <div class="product-thumb">
                        <a href="<?= $link ?>"><img src="<?= $image1 ?>" alt="Grandeur-Shop"></a>
                        <div class="product-action-quick-view">
                          <a class="action-quick-view"  data-id="<?= $value->id ?>" href="javascript:void(0);" title="Quick View">
                              <i class="icon-zoom"></i>
                            </a>
                        </div>
                        <div class="ribbons">
                          <?php  if($value->price!=$value->retail_price) { ?>
                                <span class="ribbon ribbon-hot">Sale</span>
                                <?php  } ?>
                        </div>
                      </div>
                      <div class="product-info">
                        <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                        <div class="prices">
                          <span class="price">$<?= $value->price ?></span>
                          <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                        </div>
                        <p><?= decrypt($value->description) ?></p>
                        <div class="product-action-btn">
                          <!-- <a class="btn-compare" href="javascript:void(0);">
                            <i class="fa icon-compare"></i>
                          </a> -->
                          <a style="background-color: black;border: 1px solid white;margin-left: -5px;" class="btn-add-cart btn-addcart"  data-id="<?= $value->id ?>" href="javascript:void(0);" >Add to cart</a>
                          <a class="btn-wishlist" data-id="<?= $value->id ?>" href="javascript:void(0);">
                            <i class="icon-heart-empty"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                    <!-- End Product Item -->
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="pagination-area">
              <nav>
                <ul class="page-numbers">
                  <li>
                    <a <?php $i=1; if($current_page!=1) { ?> href="<?= base_url($page_url)?>" data-cookie="<?= $page_url.'-current-page' ?>" data-page="<?= $current_page-1 ?>"   <? } else { ?> href="javascript:void(0);" data-cookie="<?= $page_url.'-current-page' ?>" data-page="<?= $current_page ?>" <?php }  ?>    class="page-number disabled prev" href="<?= base_url($page_url)?>">
                      <i class="icofont-long-arrow-left"></i>
                    </a>
                  </li>
                  <?php for($i=$i;$i<=$pages;$i++) { ?>
                  <li>
                    <a  data-cookie="<?= $page_url.'-current-page' ?>" data-page="<?= $i ?>" class="page-number  <?php if($i==$current_page) { echo "active"; }  ?>  " href="<?= base_url($page_url)?>"><?= $i ?></a>
                  </li>
                  <?php }   ?>
                  
                  <li >
                    <a  <?php if($pages>$current_page) { ?> href="<?= base_url($page_url)?>" data-cookie="<?= $page_url.'-current-page' ?>" data-page="<?= $current_page+1 ?>"  <? } else { ?> href="javascript:void(0);" data-cookie="<?= $page_url.'-current-page' ?>" data-page="<?= $current_page ?>" <?php }  ?> class="page-number next" >
                      <i class="icofont-long-arrow-right"></i>
                    </a>
                  </li>
                </ul>
              </nav>
            </div> 
          </div>
        </div>
      </div>
    </div>
    <!--== End Shop Area Wrapper ==-->