

   <!--== Start Hero Area Wrapper ==-->
    <section class="home-slider-area slider-default">
      <div class="home-slider-content">
        <div class="swiper-container home-slider-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <!-- Start Slide Item -->
              <div class="home-slider-item">
                <div class="slider-content-area">
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-xl-6 col-md-8">
                        <div class="content">
                          <div class="inner-content">
                            <div class="tittle-wrp">
                              <h2>The Best</h2>
                              <h1>Bread OIL</h1>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-6 col-md-4">
                        <div class="slider-thumb">
                          <img src="assets/website/img/slider/home1-slider1.webp" alt="Grandeur-Image">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Slide Item -->
            </div>
            <div class="swiper-slide">
              <!-- Start Slide Item -->
              <div class="home-slider-item">
                <div class="slider-content-area">
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-xl-6 col-md-8">
                        <div class="content">
                          <div class="inner-content">
                            <div class="tittle-wrp">
                              <h2>The Best</h2>
                              <h1>Bread OIL</h1>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-6 col-md-4">
                        <div class="slider-thumb">
                          <img src="assets/website/img/slider/home1-slider2.webp" alt="Grandeur-Image">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Slide Item -->
            </div>
          </div>          
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
      </div>
    </section>
    <!--== End Hero Area Wrapper ==-->





    
    <!--== Start Featured Area Wrapper ==-->
    <section class="featured-area featured-default-area">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-7" data-aos="fade-up" data-aos-duration="1000">
            <div class="row">
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content">
                    <div class="content">
                      <h4 class="title">Happiness Guarantee</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content">
                    <div class="content">
                      <h4 class="title">Free Shipping</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content mb-lg-0">
                    <div class="content">
                      <h4 class="title">24/7 Support Expert</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="featured-item">
                  <div class="inner-content mb-0">
                    <div class="content">
                      <h4 class="title">All Trusty Brand</h4>
                      <p>Free Shipping for any product and it's world wide</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5" data-aos="fade-left" data-aos-duration="1000">
            <div class="featured-thumb">
              <img src="assets/website/img/photos/discount1.jpg" alt="Grandeur-Image">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Featured Area Wrapper ==-->

    <!--== Start Featured Product Area Wrapper ==-->
    <section class="divider-area divider-default-area">
      <div class="container">
        <div class="row">
          <div class="col-md-7 order-2 order-md-1" data-aos="fade-up" data-aos-duration="1000">
            <div class="thumb mr-70 sm-mr-0">
              <a href="shop.html"><img src="assets/website/img/photos/discount2.jpg" alt="Grandeur-Image"></a>
            </div>
          </div>
          <div class="col-md-5 order-1 order-md-2" data-aos="fade-left" data-aos-duration="1000">
            <div class="divider-content divider-content-style2">
              <h2 class="title"><a href="shop.html">The Bread Oil Item <br> find Here</a></h2>
              <p>Conveniently repurpose web-enabled supply chains after technically sound action items. Progressively implement granular</p>
              <a class="btn-link" href="shop.html">Browse All Products</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Featured Product Area Wrapper ==-->

    <!--== Start Product Tab Area Wrapper ==-->
    <section class="product-area product-default-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div class="row">
                <div class="col-xl-3 col-md-3">
                  <ul class="nav nav-tabs nav-tabs-style" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <button class="nav-link active" id="popular-product-tab" data-bs-toggle="tab" data-bs-target="#popular-product" type="button" role="tab" aria-controls="popular-product" aria-selected="false">Popular Products</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="new-arrivals-tab" data-bs-toggle="tab" data-bs-target="#new-arrivals" type="button" role="tab" aria-controls="new-arrivals" aria-selected="false">New Arrivals</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="new-product-tab" data-bs-toggle="tab" data-bs-target="#new-product" type="button" role="tab" aria-controls="new-product" aria-selected="true">New Products</button>
                    </li>
                  </ul>
                </div>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">

                        <?php foreach ($product_list as $key => $value) {?>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/8.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                      </div>
                    <?php } ?>

                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/9.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-35%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Product title here</a></h4>
                              <div class="prices">
                                <span class="price">$39.00</span>
                                <span class="price-old">$60.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/9.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Demo product title</a></h4>
                              <div class="prices">
                                <span class="price">$29.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/10.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-27%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Product dummy title</a></h4>
                              <div class="prices">
                                <span class="price">$55.00</span>
                                <span class="price-old">$75.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/3.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Product title here</a></h4>
                              <div class="prices">
                                <span class="price">$50.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/3.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Demo product title</a></h4>
                              <div class="prices">
                                <span class="price">$50.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="new-arrivals" role="tabpanel" aria-labelledby="new-arrivals-tab">
                      <div class="row">


                      <?php foreach ($product_list as $key => $value) {?>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
<<<<<<< HEAD
                                <img src="<?= base_url('uploads/product/'.$value->image1) ?>" alt="Olivine-Shop">
=======
                                <img src="assets/website/img/shop/7.jpg" alt="Grandeur-Shop">
>>>>>>> a15ef1518f36d3ceeb5b5c5e6d404b273d00096a
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-10%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Demo product title</a></h4>
                              <div class="prices">
                                <span class="price">$19.00</span>
                                <span class="price-old">$21.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/1.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">11. Product with video</a></h4>
                              <div class="prices">
                                <span class="price">$39.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/8.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">9. Without shortcode product</a></h4>
                              <div class="prices">
                                <span class="price">$79.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/10.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-27%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">8. Countdown product</a></h4>
                              <div class="prices">
                                <span class="price">$39.00</span>
                                <span class="price-old">$60.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/11.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">7. Sample affiliate product</a></h4>
                              <div class="prices">
                                <span class="price">$29.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/12.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-27%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">6. Variable with soldout product</a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old">$<?= $value->retail_price ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>

                      <?php } ?>



                       
                      </div>
                    </div>
                    <div class="tab-pane fade" id="new-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">


                      <?php foreach ($product_list as $key => $value) {?>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                                <img src="assets/website/img/shop/1.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">$<?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <?php } ?>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/13.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-35%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                              <div class="countdown-content">
                                <ul class="countdown-timer">
                                  <li><span class="days">00</span><p class="days_text">Day</p></li>
                                  <li><span class="hours">00</span><p class="hours_text">Hour</p></li>
                                  <li><span class="minutes">00</span><p class="minutes_text">Minute</p></li>
                                  <li><span class="seconds">00</span><p class="seconds_text">Second</p></li>
                                </ul>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Dummy text for title</a></h4>
                              <div class="prices">
                                <span class="price">$39.00</span>
                                <span class="price-old">$60.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>

                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/9.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Demo product title</a></h4>
                              <div class="prices">
                                <span class="price">$29.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/2.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Dummy product name</a></h4>
                              <div class="prices">
                                <span class="price">$29.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/14.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="ribbons">
                                <span class="ribbon ribbon-hot">Sale</span>
                                <span class="ribbon ribbon-onsale align-right">-27%</span>
                              </div>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Dummy text for title</a></h4>
                              <div class="prices">
                                <span class="price">$55.00</span>
                                <span class="price-old">$75.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                        <div class="col-12 col-sm-6">
                          <!-- Start Product Item -->
                          <div class="product-item">
                            <div class="product-thumb">
                              <a href="shop-single-product.html">
                                <img src="assets/website/img/shop/3.jpg" alt="Grandeur-Shop">
                              </a>
                              <div class="product-action">
                                <a class="action-cart" href="#/">
                                  <i class="icofont-shopping-cart"></i>
                                </a>
                                <a class="action-quick-view" href="#/" title="Wishlist">
                                  <i class="icon-zoom"></i>
                                </a>
                                <a class="action-compare" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-compare"></i>
                                </a>
                                <a class="action-wishlist" href="javascript:void(0);" title="Quick View">
                                  <i class="icon-heart-empty"></i>
                                </a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h4 class="title"><a href="shop-single-product.html">Demo product title</a></h4>
                              <div class="prices">
                                <span class="price">$50.00</span>
                              </div>
                            </div>
                          </div>
                          <!-- End Product Item -->
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Product Tab Area Wrapper ==-->