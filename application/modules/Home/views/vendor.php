<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Estorez | Smart Shopping</title>

    <!-- Icons font CSS-->
          <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>links/logo.png">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>links/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url() ?>links/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>links/vendor/daterangepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url() ?>links/fonts/main.css" rel="stylesheet" media="all">
    <style type="text/css">
        .alert-danger{
                color: #fff;
                /* border: 1px solid #ccc; */
                height: 33px;
                line-height: 0;
                padding: 31px;
                background-color: #d20c43;
        }
        .alert-success{
            color:#fff;
            /*border:1px solid #ccc;*/
            height: 33px;
            line-height: 0;
            padding:31px;
            background-color: green;
        }
        .error{
            color:red;
        }
    </style>
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">

        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                
                <div class="card-heading">
                  <!-- <img src="http://localhost/multi_vendor/assets/img/logo/log.png" width="200" style="margin-left: 30%;margin-bottom:20px;margin-top: -20px "> -->
                </div>
                <div class="card-body">
                    <img src="<?= base_url('assets/img/logo/log.png') ?>" width="200" style="margin-left: 25%;margin-bottom:20px;margin-top: -20px ">
                    <!-- <hr/> -->

                      <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
                    <h2 class="title">Vendor Registration</h2>
                    
                    <form method="POST" action="<?php echo base_url('VendorSubmit') ?>">
                        
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="Name" name="name" value="<?= $result_set['name'] ?? '' ?>">
                        </div>
                        <?php echo form_error('name'); ?>

                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="Username" name="username" value="<?= $result_set['username'] ?? '' ?>">
                        </div>
                        <?php echo form_error('username'); ?>

                        <div class="input-group">
                            <input class="input--style-1" type="email" placeholder="Example@mail.com" name="email" value="<?= $result_set['email'] ?? '' ?>">
                        </div>
                        <?php echo form_error('email'); ?>
                        <!-- <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-1 js-datepicker" type="text" placeholder="BIRTHDATE" name="birthday">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-1" type="number" placeholder="Mobile no." name="phone">
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">GENDER</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                            <option>Other</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="input-group">
                                    <input class="input--style-1" type="number" placeholder="Mobile no." name="phone" value="<?= $result_set['phone'] ?? '' ?>">
                        </div>
                        <?php echo form_error('phone'); ?>
                        <div class="input-group">
                              <input class="input--style-1" type="text" placeholder="Shop Name" name="shop" value="<?= $result_set['shop'] ?? '' ?>">
                        </div>
                        <?php echo form_error('shop'); ?>
                        <div class="input-group">
                              <input class="input--style-1" type="text" placeholder="Address" name="address"  value="<?= $result_set['address'] ?? '' ?>">
                        </div>
                        <?php echo form_error('address'); ?>
                  
                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
        <script type="text/javascript" src="<?php echo base_url() ?>links/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!-- Vendor JS-->
    <script src="<?php echo base_url() ?>links/vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>links/vendor/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url() ?>links/vendor/daterangepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url() ?>links/js/global.js"></script>
    <script> $('.alert').delay(6000).fadeOut(300);</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
