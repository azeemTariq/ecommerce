<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Grandeur | Perfume Shop</title>

    <!--== Favicon ==-->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/website/img/favicon.ico" type="image/x-icon" />

    <!--== Google Fonts ==-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,700,900,900i" rel="stylesheet">

    <!--== Bootstrap CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/bootstrap.min.css" rel="stylesheet"/>
    <!--== Font-awesome Icons CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/font-awesome.min.css" rel="stylesheet"/>
    <!--== Icofont Min Icons CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/icofont.min.css" rel="stylesheet"/>
    <!--== Fontello CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/fontello.css" rel="stylesheet"/>
    <!--== Vandella CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/font-vandella.css" rel="stylesheet"/>
    <!--== Animate CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/animate.css" rel="stylesheet"/>
    <!--== Aos CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/aos.css" rel="stylesheet"/>
    <!--== FancyBox CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/jquery.fancybox.min.css" rel="stylesheet"/>
    <!--== Slicknav CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/slicknav.css" rel="stylesheet"/>
    <!--== Swiper CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/swiper.min.css" rel="stylesheet"/>
    <!--== Main Style CSS ==-->
    <link href="<?= base_url() ?>assets/website/css/style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/toastr.min.css">

</head>


<body >

<!--wrapper start-->
<div class="wrapper home-default-wrapper">

  <!--== Start Preloader Content ==-->
  <div class="preloader-wrap">
    <div class="preloader">

      <span class="dot"><img  src="<?= base_url() ?>assets/website/img/favicon.ico" style="background-color: black; width: 50px;height: 50px;"></span>
      <!-- <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div> -->
    </div>
  </div>
  <!--== End Preloader Content ==-->