</main>
<!--main wrapper end-->

 <!--== Start Footer Area Wrapper ==-->
  <footer class="footer-area">
    <div class="footer-main">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 m-auto">
            <div class="widget-item text-center">
              <div class="widget-logo-area">
                <a href="index.html">
                  <img class="logo-main" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo" />
                </a>
              </div>
              <p>Distinctively synergize orthogonal architectures without equity invested bandwidth.</p>
              <div class="social-widget">
                <a href="#/"><i class="icon-social-twitter"></i></a>
                <a href="#/"><i class="icon-social-pinterest"></i></a>
                <a href="#/"><i class="icon-social-instagram"></i></a>
                <a href="#/"><i class="icon-social-facebook-square"></i></a>
              </div>
              <div class="widget-menu-wrap">
                <ul class="nav-menu">
                  <li><a href="<?= base_url('about-us') ?>">About Us</a></li>
                  <li><a href="<?= base_url('frequently-asked-questions') ?>">FAQ</a></li>
                  <li><a href="<?= base_url('contact') ?>">Contact</a></li>
                  <li><a href="<?= base_url('policy/shipping-and-return') ?>">Shipping & Return</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="footer-bottom-content">
          <div class="row align-items-center">
            <div class="col-md-6">
              <div class="widget-copyright text-center">
                <p>Copyright © 2021  Powered BY <a style="color:black;font-weight: bold;" target="_blank" href="#">Root Software Solution</a></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="widget-bottom-menu-wrap">
                <ul class="nav-menu">
                  <li><a href="<?= base_url('policy/terms-and-condition') ?>">Terms</a></li>
                  <li><a href="<?= base_url('policy/privacy-policy') ?>">Privacy</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--== End Footer Area Wrapper ==-->
  
  <!--== Scroll Top Button ==-->
  <div class="scroll-to-top"><span class="icofont-arrow-up"></span></div>

  <!--== Start Quick View Menu ==-->
  <aside class="product-quick-view-modal">
    <div class="product-quick-view-inner">
      <div class="product-quick-view-content">
        <button type="button" class="btn-close">
          <span class="close-icon"><i class="fa fa-times-circle"></i></span>
        </button>
        <div class="row">
          <div class="col-lg-5 col-md-5 col-12">
            <div class="thumb">
              <img  style="height: 350px;"  id="modal_image" src="<?= base_url() ?>assets/404.png" alt="Grandeur-Shop">
            </div>
          </div>
          <div class="col-lg-7 col-md-7 col-12">
            <div class="content">
              <h3 style="color:black;" id="modal_title" >&nbsp;</h4>
              <h6 style="color:black;" id="modal_brand" ></h6>
              <div style="color:black;font-size: 20px;font-weight: bold;" class="prices">
                Price: <del style="color:black;" id="modal_old_price" class="price-old"></del>
                <span style="color:black;" id="modal_new_price" class="price"></span>
              </div>
              <p style="color:black;" id="modal_description" >&nbsp;</p>
              <!-- <div class="quick-view-select">
                <div class="quick-view-select-item">
                  <label for="forSize" class="form-label">Size:</label>
                  <select class="form-select" id="forSize" required>
                    <option selected value="">s</option>
                    <option>m</option>
                    <option>l</option>
                    <option>xl</option>
                  </select>
                </div>
                <div class="quick-view-select-item">
                  <label for="forColor" class="form-label">Color:</label>
                  <select class="form-select" id="forColor" required>
                    <option selected value="">red</option>
                    <option>green</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>white</option>
                  </select>
                </div>
              </div> -->
              <div class="action-top">
                <div class="pro-qty2">
                  <input type="text" id="quantity" class="quantity2"  style="color: black;width: 40px;" title="Quantity" value="1" />
                         
                </div>
                <button style="margin-left: 10px;" id="modal_add_to_cart_button" type="button" class=" btn btn-black">Add to cart</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="canvas-overlay"></div>
  </aside>
  <!--== End Quick View Menu ==-->
  <!--== Start Quick View Menu ==-->
  <aside class="login-view-modal">
    <div class="product-quick-view-inner">
      <div class="product-quick-view-content">
        <button type="button" class="btn-close">
          <span class="close-icon"><i class="fa fa-times-circle"></i></span>
        </button>
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-12">
            <form class="loginSubmit" method="POST" role="form">
            <div class="content">
              <h3 style="color:black;" id="modal_title" >Login to your Account</h3>
              <div class="quick-view-select">
                <div class="quick-view-select-item">
                  <label for="forSize" class="form-label">Username:</label>
                  <input type="text" class="form-control" name="username" id="username">
                </div>
                <div class="quick-view-select-item">
                  <label for="forSize" class="form-label">Password:</label>
                  <input type="password" class="form-control" name="password" id="password">
                </div>
                
              </div>
              <div class="action-top">
                <button style="margin-left: 10px;width: 100%;"  type="submit" class=" btn btn-black">Login</button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <div class="canvas-overlay"></div>
  </aside>
  <!--== End Quick View Menu ==-->
  
  <!--== Start Sidebar Cart Menu ==-->
  <aside class="sidebar-cart-modal">
    <div class="sidebar-cart-inner">
      <div class="sidebar-cart-content">
        <a class="cart-close" href="javascript:void(0);"><i class="icofont-close-line"></i></a>
        <div class="sidebar-cart">
          <h4 class="sidebar-cart-title">Shopping Cart</h4>
          <div class="product-cart">
            <?php
                $cart_content = $this->cart->contents();
                $Subtotal = 0;
                $output   = '';
                $cart_quantity   = 0;
                foreach ($cart_content as $items){ 
                        $Subtotal += $items['qty']*$items['price'];
                        $cart_quantity+=$items['qty'];
                    ?>
                     
                                <div class="product-cart-item">
                                   <div class="product-img">
                                    <a href="#"><img src='<?= base_url('uploads/product/'.$items['options']['pro_image']) ?>' alt=""></a>
                                  </div>
                                  <div class="product-info">
                                    <h4 class="title"><a href="#"><?= $items['name'] ?></a></h4>
                                    <span class="info"><?= $items['qty'].' x '.$items['price'] ?></span>
                                  </div>
                                  <div class="product-delete"><a href="<?= base_url('delete_to_cart/'.$items['rowid']) ?>">×</a></div>
                                </div>';
                             
               <?php } ?>
          </div>
          <div class="cart-total">
            <h4 style="color: black;">Total: <span class="money"><?= $Subtotal ?></span></h4>
          </div>
          <div class="cart-checkout-btn">
            <a class="btn-theme" href="<?= base_url('shop-cart') ?>">View Cart</a>
            <a class="btn-theme" href="<?= base_url('checkout') ?>">Checkout</a>
          </div>
        </div>
      </div>
    </div>
  </aside>
  <!--== End Sidebar Cart Menu ==-->

  <!--== Start Side Menu ==-->
  <aside style="background-color:black;" class="off-canvas-wrapper">
    <div class="off-canvas-inner">
      <div class="off-canvas-overlay d-none"></div>
      <!-- Start Off Canvas Content Wrapper -->
      <div class="off-canvas-content">
        <!-- Off Canvas Header -->
        <div class="off-canvas-header">
          <div class="close-action">
            <button class="btn-close"><i class="icofont-close-line"></i></button>
          </div>
        </div>

        <div class="off-canvas-item">
          <!-- Start Mobile Menu Wrapper -->
          <div class="res-mobile-menu">
            <!-- Note Content Auto Generate By Jquery From Main Menu -->
          </div>
          <!-- End Mobile Menu Wrapper -->
        </div>
        <!-- Off Canvas Footer -->
        <div class="off-canvas-footer"></div>
      </div>
      <!-- End Off Canvas Content Wrapper -->
    </div>
  </aside>
  <!--== End Side Menu ==-->
</div>
<!--wrapper start-->

<!--=======================Javascript============================-->

<!--=== Modernizr Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/modernizr.js"></script>
<!--=== jQuery Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery-3.6.0.min.js"></script>
<!--=== jQuery Migration Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery-migrate.js"></script>
<!--=== Popper Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/popper.min.js"></script>
<!--=== Bootstrap Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/bootstrap.min.js"></script>
<!--=== jquery Appear Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery.appear.js"></script>
<!--=== jquery Swiper Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/swiper.min.js"></script>
<!--=== jQuery Slick Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/slick.min.js"></script>
<!--=== jquery Fancybox Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/fancybox.min.js"></script>
<!--=== jquery Aos Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/aos.min.js"></script>
<!--=== jquery Slicknav Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery.slicknav.js"></script>
<!--=== jquery Countdown Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery.countdown.min.js"></script>
<!--=== jquery Wow Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/wow.min.js"></script>
<!--=== jQuery Zoom Min Js ===-->
<script src="<?= base_url() ?>assets/website/js/jquery-zoom.min.js"></script>

<!--=== Custom Js ===-->
<script src="<?= base_url() ?>assets/website/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/toastr.min.js"></script>

<?php $this->load->view('Home/js'); ?>
<script type="text/javascript">
  $('.countOFCart').html('<?= $cart_quantity ?>');


  function loginSubmit(){
    $('.loginSubmit').on('submit',function(e){
      $.ajax({
        url:"<?= base_url('Home/customerAuth') ?>",
        type:'post',
        data:$('.loginSubmit').serialize(),
        success:function(res){
          var res = JSON.parse(res); 
          if(res.errorCode==0){
            toastr.success(res.msg, res.status+'!',{timeOut: 5000});
           window.location.href = '<?= base_url('checkout') ?>';
          }else{
            toastr.error(res.msg, res.status+'!',{timeOut: 5000});
          }
          return false;
        }
      });
      e.preventDefault();
    });
  }
loginSubmit();
</script>
</body>



</html>