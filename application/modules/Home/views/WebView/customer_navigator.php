<!--== Start Header Wrapper ==-->
  <header class="header-wrapper">
 <!--    <div class="header-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6">
            <div class="header-info">+96 656 361 9999</div>
          </div>
          <div class="col-6">
            <div class="header-top-nav">
              <ul>
                <li><a href="#">Login</a></li>
                <li><a href="#">Wishlist</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <?php
      $where['showincollection']=1;    
      $collection_one=$this->Mdl_Home->category_list($where);
      $where['showincollection']=2;    
      $collection_two=$this->Mdl_Home->category_list($where);;
      $where['showincollection']=3;    
      $collection_three=$this->Mdl_Home->category_list($where);;
      
      $brands=$this->Mdl_Home->brand_list();;
      if(isset($_SESSION['wishlist']))
        $wishlist=$_SESSION['wishlist'];
      else
        $wishlist=[];
     ?>
    <div class="header-area header-default sticky-header">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="col-5 col-sm-3 col-md-3 col-lg-2">
            <div class="header-logo-area">
              <a href="<?= base_url() ?>">
                <img class="logo-main" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo" />
                <img class="logo-light" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo" />
              </a>
            </div>
          </div>
          <div class="col-7 col-sm-9 col-md-9 col-lg-10">
            <div class="header-align">
              <div class="header-navigation-area">
                <ul class="main-menu nav justify-content-center">
                  <li class="active"><a href="<?= base_url() ?>">Home</a>
                    
                  </li>
                  <li class="has-submenu full-width"><a href="#">Fragrances</a>
                    <ul class="submenu-nav submenu-nav-mega">
                      <li class="mega-menu-item">
                        <a class="srmenu-title" href="#">Collection 01</a>
                        <ul>
                          <?php foreach($collection_one As $col) {
                            $col=(array)$col;
                           ?>
                          <li><a href="<?= base_url() ?>collections/<?= $col['page_url'] ?>"><?= $col['category_name'] ?></a></li>
                          <?php } ?>
                        </ul>
                      </li>
                      <li class="mega-menu-item">
                        <a class="srmenu-title" href="#">Collection 02</a>
                        <ul>
                          <?php foreach($collection_two As $col) {
                            $col=(array)$col;
                           ?>
                          <li><a href="<?= base_url() ?>collections/<?= $col['page_url'] ?>"><?= $col['category_name'] ?></a></li>
                          <?php } ?>
                          
                        </ul>
                      </li>
                      <li class="mega-menu-item">
                        <a class="srmenu-title" href="#">Collection 03</a>
                        <ul>
                          <?php foreach($collection_three As $col) {
                            $col=(array)$col;
                           ?>
                          <li><a href="<?= base_url() ?>collections/<?= $col['page_url'] ?>"><?= $col['category_name'] ?></a></li>
                          <?php } ?>
                         
                        </ul>
                      </li>
                      <li class="mega-menu-item banner-menu-content-wrap">
                        <ul>
                          <li>
                            <a href="#">
                              <img src="<?= base_url() ?>assets/website/img/photos/menu1.jpg" alt="Grandeur-Shop">
                            </a>
                            <div class="banner-menu-content">
                              <h2>New <br>Collections</h2>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                   <li class="has-submenu "><a href="#">Brands</a>
                    <ul class="submenu-nav">
                      <?php foreach($brands As $col) {
                            $col=(array)$col;
                           ?>
                          <li><a href="<?= base_url() ?>brand/<?= $col['page_url'] ?>"><?= $col['subCategoryName'] ?></a></li>
                          <?php } ?>
                    </ul>
                  </li>
                  <li class=""><a href="<?= base_url('new-arrival') ?>">New Arrival</a>
                  <li class=""><a href="<?= base_url('bundle-offer') ?>">Bundle Offer</a>
                  

                </ul>
              </div>
              <div class="header-action-area">
                <div class="header-action-search header-search-rs">
                  <div class="btn-search-content">
                    <form action="<?= base_url('search-product') ?>" method="post">
                      <div class="form-input-item">
                        <label for="search" class="sr-only">Search our store</label>
                        <input type="text" id="search" placeholder="Search our store">
                        <button type="type"  class="btn-src btn-search">
                          <i class="icofont-search-1"></i>
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="header-action-cart">
                  <a title="Login" class="login-icon" href="<?= base_url('Login') ?>">
                    <i class="icofont-login"></i>
                  </a>
                </div>
                <div class="header-action-cart">
                  <a title="Wish List" class="wishlist-icon" href="<?= base_url('wishlist') ?>">
                    <i class="icofont-heart-alt"></i>
                  </a>
                  <sup><span class="badge badge-success countOFWish"><?= count($wishlist) ?></span></sup>
                </div>
                <div class="header-action-cart">
                  <a title="Shopping Cart" class="cart-icon" href="javascript:void(0);">
                    <i class="icofont-shopping-cart"></i>
                  </a>
                  <sup><span class="badge badge-success countOFCart">1</span></sup>
                </div>
                <button class="btn-menu btn-menu-rs d-xl-none">
                  <span></span>
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--== End Header Wrapper ==-->
      <!--== main-content ==-->
  <main class="main-content">