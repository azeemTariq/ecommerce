

    <section class="ftco-section ftco-no-pb ftco-no-pt ">

      <div class="sec-title p-b-22 mt-5" id="stores">
     

                <h2 class="m-text5 t-center mt-4">
          Terms & Condition
        </h2>
          <h3 class="m-text11 t-center">
          Welcome To Estorez
        </h3>

      </div>


      <div class="container">
        <div class="col-md-12" style="text-align: center;">
          
          <div class="flex-w ">
           <!--  <div class="flex-col-c   p-t-16 p-b-15 respon1">
              <div class="ml-md-0">
         
              </div>
            </div> -->


            <div class="pb-md-5">
              <p>Estorez is the online Super Mart and Grocery Store in Pakistan (Currently operational in Karachi). Being a multi-vendor one window buying & selling option for a number of buyers & sellers in the market. Estorez is based on Business Process Automation where each process is preset from placing the order to delivery of product(s) at customer’s doorstep. </p>

              <p>   At Estorez we began our operations with the idea to explore Online Stores having vision to become an organization that develops partnerships with its customers, believes in an aspiring, accountable, and action-oriented approach to provide services excellence and innovation in our Online Shopping Services striving to offer you the best price in the market.  </p>
              <p>  
                     We have a team of market analysts, designers, and developers well versed with different technologies, tools, and best practices as we are young, passionate, and enthusiastic. We are looking forward to meeting our client’s expectations and have planned to exceed them consistently.
              </p>

              <p>  
                  Our team is passionate and competitive who is geared up to devote what it takes, thanking to our Vendors & Super Suppliers who makes us able to build up trust of our valued customers by rapid supplies of products available to us and so on for our customers. We will make sure the best services by working smart as per our client’s feasibility & need-based. We promote a huge variety of products online, by  vivid people for quality check of products & as well as Vendors from all over the country. Before we render our team members at our customers, we ensure that they are trained, have first-hand exposure to different tools and practices.    

              </p>
              <p> 
                  Estorez offers an incredible shopping experience that is exceptional in Pakistan. Our Designers & Developers have been providing software solutions & services for multiple clients, starting from small companies, and continuing with huge clients, such as well-known companies.
              </p>
              
            </div>
          </div>
         
        </div>
      </div>
    </section>

    
    
    