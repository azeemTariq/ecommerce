<?php 

  $product_name=$product['product_name'];
    $image1=base_url('assets/404.png');
    if(file_exists('uploads/product/'.$product['image1']) && !empty($product['image1']))
    $image1=base_url('uploads/product/'.$product['image1']);

    $banner=base_url('assets/website/img/blog/5.png');
    if(file_exists('uploads/product/banner/'.$product['banner']) && !empty($product['banner']))
    $banner=base_url('uploads/product/banner/'.$product['banner']);
?>
<style type="text/css">
  .single-product-info h4,.single-product-info div ,.single-product-info span ,.single-product-info .btn,.active,ul li{
    color: white!important;
  }  
</style>
 <!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= $banner ?>');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title"><?= $product_name ?></h2>
              <div class="bread-crumbs"><a href="#">Home<span class="breadcrumb-sep">></span></a><span class="active"><?= $product_name ?></span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

    <!--== Start Shop Area ==-->
    <section class="product-area shop-single-product">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="single-product-thumb simple-product-thumb">
              <div class="zoom zoom-hover">
                <div class="thumb-item">
                  <a class="lightbox-image" data-fancybox="gallery" href="<?= $image1 ?>">
                    <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="single-product-info">
              <h4 class="title"><?= $product_name ?></h4>
              <div class="product-ratting">
                <div class="product-sku">
                  SKU: <span><?=$product['product_code'] ?></span>
                </div>
              </div>
              <p class="product-desc"><?= decrypt($product['description']) ?></p>
              <div class="prices">
                <span class="price"><?= $product['price']  ?></span>
              </div>
              <div class="quick-product-action">
                <div class="action-top">
                  <div class="pro-qty-area">
                    <span class="qty-title">Quantity:</span>
                    <div class="pro-qty">
                      <input type="text" name="num-product" id="quantity" title="Quantity" value="1" />
                    </div>
                  </div>
                </div>
                <div class="action-bottom">
                  <button  type="button"   data-id="<?= $product['id'] ?>" class="btn-addcart btn btn-black">Add to cart</button>
                  <a class="btn btn-wishlist"  data-id="<?= $product['id'] ?>"  href="#"><i class="icon-heart-empty"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Shop Area ==-->

    <!--== Start Shop Tab Area ==-->
    <section class="product-area product-description-review-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="product-description-review">
              <ul class="nav nav-tabs product-description-tab-menu" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="product-desc-tab" data-bs-toggle="tab" data-bs-target="#productDesc" type="button" role="tab" aria-controls="productDesc" aria-selected="true">Description</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="product-review-tab" data-bs-toggle="tab" data-bs-target="#productReview" type="button" role="tab" aria-controls="productReview" aria-selected="false">Reviews</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="product-custom-tab" data-bs-toggle="tab" data-bs-target="#productCustom" type="button" role="tab" aria-controls="productCustom" aria-selected="false">Shipping Policy</button>
                </li>
              </ul>
              <div class="tab-content product-description-tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="productDesc" role="tabpanel" aria-labelledby="product-desc-tab">
                  <div class="product-desc">
                    <p><?= $product['long_description'] ?></p>
                  </div>
                </div>
                <div class="tab-pane fade" id="productReview" role="tabpanel" aria-labelledby="product-review-tab">
                  <?php $reviews=$this->db->query("SELECT product_reviews.*,login.name FROM `product_reviews` INNER JOIN login ON login.id=product_reviews.customer_id WHERE product_reviews.product_id=".$product['id'])->result_array();  ?>
                  <div class="product-review">
                    <div class="review-header">
                      <h4 class="title">Customer Reviews</h4>
                      <div class="review-info">
                        <ul class="review-rating">
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star"></i></li>
                          <li><i class="fa fa-star-o"></i></li>
                        </ul>
                        <span class="review-caption">Based on 1 review</span>
                      </div>
                    </div>
                    <?php foreach($reviews AS $review) { ?>
                    <div class="review-content">
                      <div class="review-item">
                        <ul class="review-rating">
                          <?php for ($i=0; $i < $review['stars']; $i++) { 
                            ?>
                          <li><i class="fa fa-star"></i></li>
                          <?php }for (; $i < 5; $i++) { 
                            ?>
                          <li><i class="fa fa-star-o"></i></li>
                          <?php } ?>
                        </ul>
                        <h4 class="title"><?= $review['name'] ?></h4>
                        <h5 class="review-date"><span><?= $review['name'] ?></span> on <span><?= date("F d, Y",strtotime($review['created_at'])) ?></span></h5>
                        <p><?= $review['comment'] ?></p>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="tab-pane fade" id="productCustom" role="tabpanel" aria-labelledby="product-custom-tab">
                  <div class="product-shipping-policy">
                    <div class="section-title">
                      <h2 class="title">Shipping policy for our store</h2>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate</p>
                    </div>
                    <ul class="shipping-policy-list">
                      <li>1-2 business days (Typically by end of day)</li>
                      <li><a href="#">30 days money back guaranty</a></li>
                      <li>24/7 live support</li>
                      <li>odio dignissim qui blandit praesent</li>
                      <li>luptatum zzril delenit augue duis dolore</li>
                      <li>te feugait nulla facilisi.</li>
                    </ul>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum</p>
                    <p>claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per</p>
                    <p>seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Shop Tab Area ==-->

    <!--== Start Popular Products Area Wrapper ==-->
    <section class="product-area similar-product-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 m-auto">
            <div class="section-title">
              <h2 class="title title-style2">Related Product</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="swiper-container product4-slider-container">
              <div class="swiper-wrapper">

                        
                      
                <?php  foreach ($related_product as $key => $value) {
                       $link=base_url('product-details/').clean($value->product_name).'-'.$value->id;
                       
                        $image1=base_url('assets/404.png');
                        if(file_exists('uploads/product/'.$value->image1)  && !empty($value->image1))
                        $image1=base_url('uploads/product/'.$value->image1);

                      ?>    
                      <div class="swiper-slide">
                        <!-- Start Product Item -->
                        <div class="product-item">
                          <div class="product-thumb">
                             <a href="<?= $link ?>">
                              <img src="<?= $image1 ?>" alt="Grandeur-Shop">
                            </a>
                              <div class="ribbons">
                                  <?php  if($value->price!=$value->retail_price) { ?>
                                  <span class="ribbon ribbon-hot">Sale</span>
                                  <?php  } ?>
                                  <!-- <span class="ribbon ribbon-onsale align-right">-35%</span> -->
                                </div>
                                <div class="product-action">
                                  <a class="action-cart" href="javascript:;">
                                     <button class="btn-addcart" data-id="<?= $value->id ?>">
                                    <i class="icofont-shopping-cart"></i>
                                  </button>
                                  </a>
                                  <a class="action-quick-view" data-id="<?= $value->id ?>" href="javascript:void(0);" >
                                    <i class="icon-zoom"></i>
                                  </a>
                                  <!-- <a class="action-compare"  href="javascript:void(0);" title="Share">
                                    <i class="icon-compare"></i>
                                  </a> -->
                                  <a class="action-wishlist" data-id="<?= $value->id ?>" href="javascript:void(0);" title="Wishlist">
                                    <i class="icon-heart-empty"></i>
                                  </a>
                                </div>
                          </div>
                          <div class="product-info">
                            <h4 class="title"><a href="<?= $link ?>"><?= $value->product_name ?></a></h4>
                              <div class="prices">
                                <span class="price">$<?= $value->price ?></span>
                                <span class="price-old"><?= $value->price!=$value->retail_price ? '$'.$value->retail_price :"" ?></span>
                              </div>
                          </div>
                        </div>
                        <!-- End Product Item -->
                      </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Popular Products Area Wrapper ==-->

 