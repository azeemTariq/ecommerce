<style type="text/css">
  .product-area  h1,.product-area   h4, .product-area  p, .product-area  span, .product-area  a, .product-area  label{
    color: white!important;
  }

.product-area .btn-theme{
  border: 1px solid white;
}.error{
  color: red;
  font-size: 11px;
}.alert-block p{
  color:red !important;
}
</style>
<!--== Start Product Area Wrapper ==-->
    <section class="product-area product-information-area">




      <div class="container">
                  <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block alert-success">
                    
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
        <form action="<?= base_url('Home/Carts/Order') ?>" method="POST">
        <div class="product-information">
          <div class="row">
            <div class="col-lg-7">
              <div style="display: none;" class="edit-checkout-head">
                <div class="header-logo-area">
                  <a href="<?= base_url() ?>">
                    <img class="logo-main" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo">
                    <img class="logo-light" src="<?= base_url() ?>assets/website/img/logo.png" alt="Logo">
                  </a>
                </div>
                <div class="breadcrumb-area">
                  <ul>
                    <li><a href="shop-cart.html">Cart</a><i class="fa fa-angle-right"></i></li>
                    <li class="active">Information<i class="fa fa-angle-right"></i></li>
                    <li>Shipping<i class="fa fa-angle-right"></i></li>
                    <li>Payment</li>
                  </ul>
                </div>
              </div>
              <div class="edit-checkout-information">
                <?php  if(isset($_SESSION['customer_logged_in'])) { ?>
                <h4 class="title">Customer information</h4>
                <div class="logged-in-information">
                  <div class="thumb" data-bg-img="<?= base_url() ?>assets/website/img/photos/gravatar2.png"></div>
                  <p>
                    <span class="name"><?= $_SESSION['first_name']." ".$_SESSION['last_name'] ?></span>
                    <span>(<?= $_SESSION['username'] ?>)</span>
                    <a href="<?= base_url('Home/signout') ?>">Log out</a>
                  </p>
                </div>
              <?php  } else { ?>
                <h4 class="title">Already have account?</h4>
                <div class="logged-in-information">
                 <button type="button" class="btn-theme login-action" data-toggle="modal" data-target="#loginModal">Log in to your account</button>
                </div>
               <?php } ?>
                <div style="display:none;" class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                  <label class="form-check-label" for="inlineCheckbox1">Keep me up to date on news and exclusive offers</label>
                </div>
                <div class="edit-checkout-form">
                  <h4 class="title">Personal Information</h4>
                  <div class="row row-gutter-12">
                    <div class="col-lg-6">
                      <div class="form-floating">
                        <input type="text" class="form-control" name="first_name" id="floatingInputGrid" placeholder="First name" value="<?= $_SESSION['first_name'] ?? $row['first_name'] ?? ''  ?>" required="">
                        <label for="floatingInputGrid">First name </label>
                        <input type="hidden" name="cust_id" value="<?= $_SESSION['id'] ?? '' ?>">
                      </div>
                      <?= form_error('first_name'); ?>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-floating">
                        <input type="text" name="last_name" class="form-control" id="floatingInput2Grid" placeholder="Last name" value="<?= $_SESSION['last_name'] ?? $row['last_name'] ?? ''  ?>" required="">
                        <label for="floatingInput2Grid">Last name</label>
                      </div>
                       <?= form_error('last_name'); ?>
                    </div>
                    <div class="col-lg-6">
                       <div class="form-floating">
                        <input type="text" class="form-control" name="email" id="floatingInput7Grid" placeholder="example@mail.com" value="<?= $_SESSION['email'] ?? $row['email'] ?? ''  ?>" required="">
                        <label for="floatingInput7Grid">Email</label>
                      </div>
                      <?= form_error('email'); ?>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-floating">
                        <input type="number" name="cell" class="form-control" id="floatingInput6Grid" placeholder="Mobile #" value="<?= $_SESSION['cell'] ?? $row['cell'] ?? ''  ?>" required="">
                        <label for="floatingInput6Grid">Mobile #</label>
                      </div>
                      <?= form_error('cell'); ?>
                    </div>
                    <h4 class="title">Shipping address</h4>
                    <div class="col-lg-12">
                      <div class="form-floating">
                        <input type="text" class="form-control" name="ship_address" id="floatingInput3Grid" placeholder="address" value="<?= $row['ship_address'] ?? ''  ?>" required="">
                        <label for="floatingInput3Grid">Address</label>
                      </div>
                      <?= form_error('ship_address'); ?>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-floating">
                        <select class="form-select form-control" name="ship_country" id="floatingInput6rid" aria-label="Floating label select example">
                          <option value="1" <?= (isset($row['ship_address']) && $row['ship_address']==1) ? 'selected' : ''  ?>>Pakistan</option>
                        </select>
                        <div class="field-caret"></div>
                        <label for="floatingInput6rid">Country/Region</label>
                      </div>
                      <?= form_error('ship_country'); ?>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-floating">
                        <input type="text" class="form-control" id="floatingInput7Grid" name="ship_postal" placeholder="postal Code" value="<?= $row['ship_postal'] ?? ''  ?>">
                        <label for="floatingInput7Grid">Postal code</label>
                      </div>
                    </div>
                    <h4 class="title">Billing address</h4>
                    <div class="col-lg-12">
                      <div class="form-floating">
                        <input type="text" class="form-control" id="floatingInput4Grid"name="bill_address"  placeholder="billing Address" value="<?= $row['bill_address'] ?? ''  ?>">
                        <label for="floatingInput4Grid">Billing Address</label>
                      </div>
                    </div>
                     <div class="col-lg-6">
                      <div class="form-floating">
                        <select class="form-select form-control" name="bill_country" id="floatingInput6rid" aria-label="Floating label select example">
                          <option value="1" <?= (isset($row['bill_address']) && $row['bill_address']==1) ? 'selected' : ''  ?>>Pakistan</option>
                        </select>
                        <div class="field-caret"></div>
                        <label for="floatingInput6rid">Country/Region</label>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-floating">
                        <input type="text" class="form-control"  name="bill_postal" id="floatingInput7Grid" placeholder="postal Code" value="<?= $row['bill_postal'] ?? ''  ?>">
                        <label for="floatingInput7Grid">Postal code</label>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-floating">
                        <input type="text" class="form-control" name="massage" id="floatingInput5Grid" placeholder="massage" value="<?= $row['massage'] ?? ''  ?>">
                        <label for="floatingInput5Grid">Comment (optional)</label>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="btn-box" >
                        <a class="btn-return btn-theme" href="<?= base_url('shop-cart') ?>"><svg focusable="false" aria-hidden="true" class="icon-svg icon-svg--color-accent icon-svg--size-10 previous-link__icon" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10"><path d="M8 1L7 0 3 4 2 5l1 1 4 4 1-1-4-4"></path></svg>Return to cart</a>
                        <a class="btn-shipping" href="javascript:;"><button type="submit" >Confirm order </button></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="shipping-cart-subtotal-wrapper">
                <div class="shipping-cart-subtotal">
                  <h4 class="title">Order Information</h4>
                <?php $Subtotal=0; foreach ($this->cart->contents() as $key => $items) {
                     $Subtotal += $items['qty']*$items['price']; ?>
                  <div class="shipping-cart-item">
                    <div class="thumb">
                      <img src="<?= base_url('uploads/product/'.$items['options']['pro_image']) ?>" alt="">
                      <span class="quantity"><?= $items['qty'] ?></span>
                    </div>
                    <div class="content">
                      <h4 class="title"><?= $items['name'] ?></h4>
                      <span class="info">m / gold</span>
                      <span class="price"><?= $items['price'] ?></span>
                    </div>
                  </div>
                  <?php }  ?>
                     <input type="hidden" name="TotalAmount" value="<?= $Subtotal ?>">
                  <div class="shipping-subtotal">
                    <p><span>Subtotal</span><span><strong>$<?= $Subtotal ?></strong></span></p>
                    <p><span>Shipping</span><span>Calculated at next step</span></p>
                  </div>
                  <div class="shipping-total">
                    <p class="total">Total</p>
                    <p class="price"><span class="usd">USD</span>$<?=  $Subtotal ?></p>
                  </div>
                </div>
              </div>

              <div class="shipping-cart-subtotal-wrapper">
                <div class="shipping-cart-subtotal">
                 
                 <div class="shipping-subtotal">
                 
                 <h4 class="title">Payment Method</h4>
                 </div>
                 <div class="shipping-subtotal">
                    <input checked type="radio" name="payment_method"> Cash on Delivery
                 </div>
                 <div class="shipping-subtotal">
                   <input  type="radio" name="payment_method"> Pay through Card
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      </div>
    </section>
    <!--== End Product Area Wrapper ==-->
    <div class="edit-checkout-footer">
      <p>All rights reserved Grandeur</p>
    </div>