
<?php


  $row_list = [];
      foreach ($this->cart->contents() as $key => $value) {
        if($detail['id']==$value['id']){
               $row_id['rowid']         =  $value['rowid'];
               $row_list['qty']         =  $value['qty'];
               $row_list['suggestion']  =  $value['suggestion'];
         }
      }



?>
<style type="text/css">
    .description{
       max-width: 400px
    }
</style>
<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
    <a href="index.html" class="s-text16">
                Home
    <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
    </a>

    <a href="<?php echo base_url('Home/Product/'.encrypt($detail['store_id'])) ?>" class="s-text16">
        <?= $detail['store_name'] ?>
           
    <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
    </a>

    <a href="#" class="s-text16">
                <?= $detail['category_name'] ?>
    <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
    </a>

    <span class="s-text17">
                <?=  $detail['product_name'] ?>
    </span>
</div>

<!-- Product Detail -->
<div class="container bgwhite p-t-35 p-b-80">
<div class="flex-w flex-sb">
<div class="w-size13 p-t-30 respon5">
<div class="wrap-slick3 flex-sb flex-w">
<div class="wrap-slick3-dots"></div>

<div class="slick3">

<?php for($i=1; $i<=10; $i++){    
    $image =$detail['image'.$i];
    if($image){ ?>
   
    <div class="item-slick3" data-thumb="<?php echo base_url('uploads/product/'.$image) ?>">
    <div class="wrap-pic-w">
    <img  class="img_01" src="<?php echo base_url('uploads/product/'.$image) ?>" alt="IMG-PRODUCT"  data-zoom-image="<?php echo base_url('uploads/product/'.$image) ?>"  >
    </div>
    </div>

<?php } } ?>

  <!--   <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/img/logo/l5.jpg">
    <div class="wrap-pic-w">
    <img src="<?php echo base_url() ?>assets/img/logo/l5.jpg" alt="IMG-PRODUCT">
    </div>
    </div>

    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/img/logo/l5.jpg">
    <div class="wrap-pic-w">
    <img src="<?php echo base_url() ?>assets/img/logo/l5.jpg" alt="IMG-PRODUCT">
    </div>
    </div>

    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/img/logo/l5.jpg">
    <div class="wrap-pic-w">
    <img src="<?php echo base_url() ?>assets/img/logo/l5.jpg" alt="IMG-PRODUCT">
    </div>
    </div>

    <div class="item-slick3" data-thumb="<?php echo base_url() ?>assets/img/logo/l4.jpg">
    <div class="wrap-pic-w">
    <img src="<?php echo base_url() ?>assets/img/logo/l4.jpg" alt="IMG-PRODUCT">
    </div>
    </div> -->
</div>
</div>
</div>

<div class="w-size14 p-t-30 respon5">
<h4 class="product-detail-name m-text16 p-b-13">
                  <?=   $detail['product_name'] ?>
</h4>

<span class="m-text17">
                Rs.  <?=   $detail['price'] ?>
</span>

<p class="s-text8 p-t-10 description">
                    <?=   ($detail['des_status']==1) ? decrypt($detail['description']) : $detail['description'] ?>
</p>

<!--  -->
<div class="p-t-33 p-b-60">
<div class="flex-m flex-w p-b-10">
<!-- <div class="s-text15 w-size15 t-center">
                            Size
</div> -->

<!-- <div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
  <input type="" name="">
</div>
</div> -->

<div class="flex-m flex-w">
<div class="s-text15 t-center">
                            Color/Size
</div>

<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
   <input type="text" name="suggestion" placeholder="Optional" class="form-control" id="suggestion" value="<?= isset($row_list['suggestion']) ? $row_list['suggestion'] : '' ?>">
   <input type="hidden" name="cart_row" id="cart_row" value="<?= isset($row_id['rowid']) ? $row_id['rowid'] : ''?>">
</div>
</div>

<div class="flex-r-m flex-w p-t-10">
<div class=" flex-m flex-w">
<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
</button>

<input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="<?= isset($row_list['qty']) ? $row_list['qty'] : 1 ?>">


<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
</button>
</div>

<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
<!-- Button -->
<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 btn-addcart" data-id="<?= $detail['id']  ?>">
                                    Add to Cart
</button>
</div>
</div>
</div>
</div>

<div class="p-b-45">
<span class="s-text8 m-r-35">Store : <?= $detail['store_name'] ?></span>
<span class="s-text8">Categories : <?= $detail['category_name'] ?></span>
</div>

<!--  -->
<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        Our Store Brands
<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
</h5>

<div class="dropdown-content dis-none p-t-15 p-b-23">

                            <?php foreach($store_list as $k => $value){ 
                                 if( $k!=0){?>,<?php } ?>

                               
                                 <a style="color:#cc1818" href="<?php echo base_url('Home/Product/'.encrypt($value->id)) ?>"><?= $value->store_name ?></a>      
                            
                

<?php } ?>



</div>
</div>






<!-- <div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        Additional information
<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
</h5>

<div class="dropdown-content dis-none p-t-15 p-b-23">
<p class="s-text8">
                            Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat
</p>
</div>
</div> -->

<!-- <div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        Reviews (0)
<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
</h5>

<div class="dropdown-content dis-none p-t-15 p-b-23">
<p class="s-text8">
                            Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat
</p>
</div>
</div> -->



</div>
</div>
</div>
</div>



<?php 
$related = modules::run('Home/relatedItem',$detail['cat_id'],$detail['id']);






?>
<!-- Relate Product -->
<section class="relateproduct bgwhite p-t-45 p-b-138">
<div class="container">
<div class="sec-title p-b-60">

    <?php if(count($related)){?>
<h3 class="m-text5 t-center">
                    Related Products
</h3>
 <?php } ?>
</div>


<!-- Slide2 -->
<div class="wrap-slick2">
<div class="slick2">
  <?php foreach($related As $value){ 
    $oldPrice = ($value->retail_price>0) ? '<del>'.$value->retail_price.'</del> ' : ''; 
    ?>  
    <div class="item-slick2 p-l-15 p-r-15">
        <!-- Block2 -->
        <div class="block2">
          <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew  image-reload">
             <img src="<?php echo base_url('uploads/product/'.$value->image1) ?>" alt="IMG-PRODUCT"  width="200" height="200">
                <a href="<?= base_url('Home/Detail/'.encrypt($value->id)) ?>">
                    <div class="block2-overlay trans-0-4">
                        <input type="hidden" name="inputId" value="<?= $value->id ?>" >
                                            <button type="submit" class="s-text1 trans-0-4" style="width: 100%;height: 100%;display: block;">
                                                 View Details
                                            </button>
                    </div>  
                </a>
            </div>
        <div class="block2-txt p-t-20">
        <a href="<?php echo base_url('Home/Detail/'.encrypt($value->id)) ?>" class="block2-name dis-block s-text3 p-b-5"><?= $value->product_name ?></a>
        <span class="block2-price m-text6 p-r-5">
            Rs. <?=  $oldPrice."".$value->price ?>
            <!-- <code class="text-success pull-right"><b>Available Stock</b></code> -->
        </span>
        </div>
        </div>
    </div>
<?php } ?>

    </div>
</div>

</div>
</section>
