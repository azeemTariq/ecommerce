<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  .nav-link i{
    font-size: 25px;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 10px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody th {
    border: 1px solid white!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }

</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">My Dashboard</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">My Dashboard </span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

   <!--== Start Product Tab Area Wrapper ==-->
    <section style="border-top: 1px solid white;" class="product-area product-default-area">
      <div class="container">
        <div  class="row">
          <div  class="col-12">
            <div  class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div  class="row">
                <?php $this->load->view('WebView/customer_sidebar',['page'=>"dashboard"]); ?>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row" style="margin-left:20px;">

                        <h1><?= $_SESSION['first_name']." ".$_SESSION['last_name'] ?>, <br> Welcome to Grandeur!</h1>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>