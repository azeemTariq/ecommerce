<style type="text/css">
    
.item-pagination{
    float: left;
}

</style>

<?php
$Category = modules::run('Home/categoryByStore',$store_id);
$list = modules::run('Home/subCategoryByStore',$store_id);

?>
<!-- Title Page -->
 <div class="container">
    <?php $image = isset($banners[0]->image) ? base_url('uploads/banners/'.$banners[0]->image) :  base_url('assets/img/logo/s3.jpg'); ?>
    <section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo $image ?>);">
        <h2 class="l-text2 t-center">
            <?= isset($banners[0]->bannes_name) ? $banners[0]->bannes_name : '' ?>
        </h2>
        <p class="m-text13 t-center">
             <?= isset($banners[0]->description) ? $banners[0]->description : '' ?>
        </p>
    </section>
</div>
<input type="hidden" id="store_id"  value="<?= $store_id ?? '' ?>">


    <!-- Content page -->
    <section class="bgwhite p-t-55 p-b-65">
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-md-4 col-lg-3 ">
                    <div class="leftbar p-r-20 p-r-0-sm">

                         <div class="search-product pos-relative bo4 of-hidden_ mb-4">
                            <input class="s-text7 size6 p-l-23 p-r-50" type="text" name="search-product" id="search-product" placeholder="Search Products...">

                            <button class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4" onclick="search_product()">
                                <i class="fs-12 fa fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                        <!--  -->
                        <h4 class="m-text14 p-b-7 cate_header">
                            Categories
                        </h4>


    <nav>
        <!-- Menu Toggle btn-->
        <div class="menu-toggle">
            <h3>Menu</h3>
            <button type="button" id="menu-btn">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Responsive Menu Structure-->
        <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
        <ul id="respMenu" class="ace-responsive-menu p-b-40" data-menu-style="vertical">
            <li>
                <a href="javascript:;" data-id='' class="active menus">
                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    <span class="title">All Category</span>
                </a>
            </li>

          <?php  foreach ($Category as $key => $value) { ?>
            <li>
                <a href="javascript:;" data-id='<?= $value->id; ?>' class="menus">
                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    <span class="title"><?= $value->category_name; ?></span>
            <!-- <span class="arrow"></span>  -->
                </a>
                 <?php if(isset($list[$value->id])){ ?> 
                <ul style="display: none;">
                <?php foreach($list[$value->id] as $key => $item){ ?>
                    <li>
                        <a href="javascript:;" data-id='<?= $value->id; ?>' data-sub='<?= $item->id; ?>' class="sub-menus">
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                <?= $item->subCategoryName; ?>                  
                        </a>
                    </li>
                 <?php } ?> 
                </ul>
                 <?php } ?> 
            </li>
           <?php } ?>  
               </ul>
        </nav>

   



                        <!-- <p> -->
<!--    <label for="amount">Price range:</label>
   <input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;">
  </p>
  <div id="slider-range"></div>
 -->


                        <!--  -->
                        <h4 class="m-text14 p-b-32" style="padding-top: 75px">
                            Filters
                        </h4>

                    <div class="filter-price p-t-22 bo3">
                            <div class="m-text15 p-b-17">
                                Price
                            </div>

                            <div class="wra-filter-bar">
                                <div id="filter-bar"></div>
                            </div>

                            <div class="flex-sb-m flex-w p-t-16">
                                <div class="w-size11">
                                    
                                    <button class="flex-c-m size4 bg7 bo-rad-15 hov1 s-text14 trans-0-4" id="Filter">
                                        Filter
                                    </button>
                                </div>

                                <div class="s-text3 p-t-10 p-b-10">
                                    Range: Rs. <span id="value-lower">610</span> - Rs. <span id="value-upper">980</span>
                                </div>
                            </div>
                        </div>

                       <!--  <div class="filter-color p-t-22 p-b-50 bo3">
                            <div class="m-text15 p-b-12">
                                Color
                            </div>

                            <ul class="flex-w">
                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter1" type="checkbox" name="color-filter1">
                                    <label class="color-filter color-filter1" for="color-filter1"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter2" type="checkbox" name="color-filter2">
                                    <label class="color-filter color-filter2" for="color-filter2"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter3" type="checkbox" name="color-filter3">
                                    <label class="color-filter color-filter3" for="color-filter3"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter4" type="checkbox" name="color-filter4">
                                    <label class="color-filter color-filter4" for="color-filter4"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter5" type="checkbox" name="color-filter5">
                                    <label class="color-filter color-filter5" for="color-filter5"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter6" type="checkbox" name="color-filter6">
                                    <label class="color-filter color-filter6" for="color-filter6"></label>
                                </li>

                                <li class="m-r-10">
                                    <input class="checkbox-color-filter" id="color-filter7" type="checkbox" name="color-filter7">
                                    <label class="color-filter color-filter7" for="color-filter7"></label>
                                </li>
                            </ul>
                        </div>

                        -->
                    </div>
                </div>

                <div class="col-sm-6 col-md-8 col-lg-9 ">
                    <!--  -->
                    <div class="flex-sb-m flex-w p-b-35">
                       

                            <div class="col-md-8 col-sm-6 col-xs-12">




                                    <div class="sec-title p-b-22 ">
                                        <h3 class="m-text5 ">
                                          Product List
                                        </h3>
                                    </div>
                              <!--   <select class="form-control rm-select2" name="sorting">
                                    <option>Price</option>
                                    <option>$0.00 - $50.00</option>
                                    <option>$50.00 - $100.00</option>
                                    <option>$100.00 - $150.00</option>
                                    <option>$150.00 - $200.00</option>
                                    <option>$200.00+</option>

                                </select> -->
                            </div>
                        

                        <span class="s-text8 p-t-5 p-b-5" style="color: green">
                            <b>Showing <span id="total_row"></span> results</b>
                        </span>
                    </div>
                    <div id="wait" style="display:none;margin-left: 45%;">
                            <img src='<?= base_url() ?>assets/demo_wait.gif' width="84" height="84" />
                            <br>
                        </div> 

                    <!-- Product -->
                    <div class="row" id="product_list">
                         

                    </div>

                    <!-- Pagination -->
                    <div  id="pagination-result" class="pagination flex-m flex-w p-t-26">
                        <!-- <a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
                        <a href="#" class="item-pagination flex-c-m trans-0-4">2</a> -->

                        <input type="hidden" name="rowcount" id="rowcount" />
                    </div>
                      <div style="border-bottom: #F0F0F0 1px solid;margin-bottom: 15px;display:none">
                            Pagination Setting:<br> 
                            <select name="pagination-setting" onChange="changePagination(this.value);"  class="pagination-setting" id="pagination-setting">
                            <option value="all-links" selected="">Display All Page Link</option>
                            <option value="prev-next">Display Prev Next Only</option>
                            </select>
                        </div>
                </div>
            </div>
        </div>
    </section>
