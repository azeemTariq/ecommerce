<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 5px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody td {
    border: 1px solid white!important;
    padding-top: 10px!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }
.product-area .btn-theme{
  border: 1px solid white;
}
</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">Your Shopping Cart</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Your Shopping Cart</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

    <!--== Start Cart Area Wrapper ==-->
    <section style="background-color:black;" class="product-area cart-page-area ">
      <div class="container">
        <div class="row">
          <div class="col-12">
           
            <div style="border:1px solid white;" class="cart-table-wrap">
              <form action="<?= base_url('Home/update_cart') ?>" method="POST">
              <div class="cart-table table-responsive">
                <table>
                  <thead>
                    <tr>
                      <th class="pro-product">Product</th>
                      <th class="pro-price text-center">Price</th>
                      <th class="pro-quantity text-center">Quantity</th>
                      <th class="pro-subtotal text-center">Total</th>
                      <th class="pro-remove text-center">Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $Subtotal=0; foreach ($this->cart->contents() as $key => $value) {
                     $Subtotal += $value['qty']*$value['price']; ?>
                    <tr>
                      <td class="pro-product">
                        <div class="product-info">
                          <div align="" class="product-img">
                            <a href="#"><img style="margin-left: 15px;" src='<?= base_url('uploads/product/'.$value['options']['pro_image']) ?>' alt=""></a>
                          </div>
                          <div class="product-info"><?= $value['name'] ?></a></h4>
                          </div>
                        </div>
                      </td>
                       <input type="hidden"  name="rowid[]" value="<?= $value['rowid'] ?>" />
                      <td class="pro-price text-center"><span>$<?= $value['price'] ?></span></td>
                      <td class="pro-quantity text-center">
                        <div class="quick-product-action">
                          <div class="pro-qty2">
                            <input type="text" id="quantity" name="num-product[]" style="color: white" title="Quantity" value="<?= $value['qty'] ?>" />
                          </div>
                        </div>
                      </td>
                      <td class="pro-subtotal text-center"><span>$<?= $value['qty']*$value['price'] ?></span></td>
                      <td class="pro-remove text-center"><a href="<?= base_url('delete_to_cart/'.$value['rowid']) ?>">×</a></td>
                    </tr>
                    <?php }  if(empty($this->cart->contents())){
                      ?>
                      <tr>
                      <td colspan="5" align="center">
                        Your shopping cart is empty
                        </td>
                    </tr>
                    <?php }  ?>
                  </tbody>
                </table>
              </div>
              

            </form>
            </div>
          </div>
          <div class="col-12">
            <div class="cart-payment">
              <div class="row">
                <div class="col-lg-6">
                  <div style="" class="culculate-shipping">
                    <h3 class="title">Special instructions for seller</h3>
                    <form action="#">
                      <div class="form-group">
                        <textarea class="form-control"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="cart-subtotal">
                    <h3 class="title">Cart Totals</h3>
                    <table>
                      <tbody>
                        <tr class="cart-subtotal">
                          <th>Subtotal</th>
                          <td>
                            <span class="amount">$<?= $Subtotal ?></span>
                          </td>
                        </tr>
                        <tr class="order-total">
                          <th>Total</th>
                          <td>
                            <span class="price">$<?= $Subtotal ?></span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
                <div class="col-12">
                  <div class="cart-buttons">
                      <button type="submit" class="btn btn-theme" name="update_cart" value="1">Update Cart</button>
                    <a class="btn-theme continue-shopping" href="<?= base_url() ?>">Continue Shopping</a>       
                    <a class="btn-theme" href="<?= base_url('checkout') ?>">Proceed to Checkout</a>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </section>
    <!--== End Cart Area Wrapper ==-->