 <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/website/faq/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/website/faq/font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?= base_url() ?>assets/website/faq/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/website/faq/bootstrap/js/bootstrap.min.js"></script>
<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
        color: #fff;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
</style>
<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/6.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content ">
              <h2 class="title">Frequently Asked Questions</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Frequently Asked Questions</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

<!--== Start About Area ==-->
    <section style="background-color:black;" class="about-area about-default-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="panel-group" id="accordion">


		        

		        <?php $array=[]; foreach ($faq_content as $key => $value) { ?>
                    <?php if(!in_array($value->cat_id,$array)){ ?>
                    <div class="faqHeader"><?= $value->title ?></div>
                    <?php  $array[]=$value->cat_id; } ?>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle <?= ( $key!=0) ? 'collapsed' : '' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=  $value->id ?>"><?= $value->ques ?>?</a>
		                </h4>
		            </div>
		            <div id="collapse<?=  $value->id ?>" class="panel-collapse collapse <?= ( $key==0) ? 'in' : '' ?>">
		                <div class="panel-body">
		                    <?= $value->answer ?> 
		                </div>
		            </div>
		        </div>
		        
		      <?php  } ?>
		      
		    

		<!--         <div class="faqHeader">Sellers</div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Who cen sell items?</a>
		                </h4>
		            </div>
		            <div id="collapseTwo" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Any registed user, who presents a work, which is genuine and appealing, can post it on <strong>PrepBootstrap</strong>.
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">I want to sell my items - what are the steps?</a>
		                </h4>
		            </div>
		            <div id="collapseThree" class="panel-collapse collapse">
		                <div class="panel-body">
		                    The steps involved in this process are really simple. All you need to do is:
		                    <ul>
		                        <li>Register an account</li>
		                        <li>Activate your account</li>
		                        <li>Go to the <strong>Themes</strong> section and upload your theme</li>
		                        <li>The next step is the approval step, which usually takes about 72 hours.</li>
		                    </ul>
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How much do I get from each sale?</a>
		                </h4>
		            </div>
		            <div id="collapseFive" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Here, at <strong>PrepBootstrap</strong>, we offer a great, 70% rate for each seller, regardless of any restrictions, such as volume, date of entry, etc.
		                    <br />
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Why sell my items here?</a>
		                </h4>
		            </div>
		            <div id="collapseSix" class="panel-collapse collapse">
		                <div class="panel-body">
		                    There are a number of reasons why you should join us:
		                    <ul>
		                        <li>A great 70% flat rate for your items.</li>
		                        <li>Fast response/approval times. Many sites take weeks to process a theme or template. And if it gets rejected, there is another iteration. We have aliminated this, and made the process very fast. It only takes up to 72 hours for a template/theme to get reviewed.</li>
		                        <li>We are not an exclusive marketplace. This means that you can sell your items on <strong>PrepBootstrap</strong>, as well as on any other marketplate, and thus increase your earning potential.</li>
		                    </ul>
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">What are the payment options?</a>
		                </h4>
		            </div>
		            <div id="collapseEight" class="panel-collapse collapse">
		                <div class="panel-body">
		                    The best way to transfer funds is via Paypal. This secure platform ensures timely payments and a secure environment. 
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">When do I get paid?</a>
		                </h4>
		            </div>
		            <div id="collapseNine" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Our standard payment plan provides for monthly payments. At the end of each month, all accumulated funds are transfered to your account. 
		                    The minimum amount of your balance should be at least 70 USD. 
		                </div>
		            </div>
		        </div>

		        <div class="faqHeader">Buyers</div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">I want to buy a theme - what are the steps?</a>
		                </h4>
		            </div>
		            <div id="collapseFour" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Buying a theme on <strong>PrepBootstrap</strong> is really simple. Each theme has a live preview. 
		                    Once you have selected a theme or template, which is to your liking, you can quickly and securely pay via Paypal.
		                    <br />
		                    Once the transaction is complete, you gain full access to the purchased product. 
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Is this the latest version of an item</a>
		                </h4>
		            </div>
		            <div id="collapseSeven" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Each item in <strong>PrepBootstrap</strong> is maintained to its latest version. This ensures its smooth operation.
		                </div>
		            </div>
		        </div> -->
		</div>	
          </div>
        </div>
      </div>
    </section>
    <!--== End About Area ==-->
