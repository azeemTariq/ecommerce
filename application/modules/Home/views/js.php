<script type="text/javascript">


    $('.view-invoice').on('click', function(){
        setCookie('orderID',$(this).data('id'));
        window.open("<?= base_url('my-invoice-detail') ?>", "Grandeur Invoice", "width=900,height=700");

    });
    $('.add-review').on('click', function(){
            if($('#whatever1').val()<1){
                toastr.error("Please add at least one star", 'Error !',{timeOut: 5000});
            }
            else if($('#comment').val()==""){
                toastr.error("Comment is required", 'Error !',{timeOut: 5000});
            }
            else{
                $.ajax({
                   url: '<?= base_url('Home/SubmitReview') ?>',
                   type: 'post',
                   data: $("#review_form").serialize(),
                   success:function(data){
                     toastr.success("Thank you for review our product", 'Success !',{timeOut: 5000});
                      setTimeout(function () {
                              location.reload();
                          },1500);
                  }
                });
            }
            
        });
     




    function setCookie(cName, cValue, expDays="8400") {
        let date = new Date();
        date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
        const expires = "expires=" + date.toUTCString();
        document.cookie = cName + "=" + cValue + "; " + expires + "; path=/";
    }

    $('.page-number , .nav-link').on('click', function(){
        var cookie_name=$(this).attr('data-cookie');
        var page=$(this).attr('data-page');
        if(page<1)
            page=1;
        setCookie(cookie_name,page);
    }); 
     $('#SortBy').on('change', function(){
        
        setCookie('sort-by',$(this).val());
    });
     $('#search').on('change', function(){
        
        setCookie('search_product',$('#search').val());
        window.location.href='<?= base_url('search-product')?>';
    });
     $('.btn-search').on('click', function(){
        
        setCookie('search_product',$('#search').val());
        window.location.href='<?= base_url('search-product')?>';
    });

     $('#change_password').on('click', function(){
        
        if ($(this).prop("checked"))
        {
            $("#password").attr('disabled',false);
            $("#cpassword").attr('disabled',false);
        }
        else
        {
            $("#password").attr('disabled',true);
            $("#cpassword").attr('disabled',true);
        }
    });
    function addSubmit(){
          var nameProduct = '';
            $('.btn-addcart').on('click', function(){
            var json={};
            json['p_id']   = $(this).attr('data-id');
            json['isAjax'] = 1;

            if($('input[name="num-product"]').val()){
             json['qtybutton']  = $('input[name="num-product"]').val();
             json['cart_row']  = $('#cart_row').val();
            }
            if($('#suggestion').val()){
              json['suggestion']  = $('#suggestion').val();
            }
            $.ajax({
               url: '<?= base_url('Home/Carts/AddToCart') ?>',
               type: 'post',
               data: json,
               success:function(data){
                 data = JSON.parse(data);
                 $('.product-cart').html('');
                 $('.product-cart').html(data.cart); 
                 $('.countOFCart').html(data.count); 
                 $('.cart-total').html(data.total); 
                  toastr.success("Shopping Cart Updated", 'Success !',{timeOut: 5000});

                  // swal(nameProduct, "is added to cart !", "success");
               }
            });
            });

            
        }

        $('#order_list_search').on('keyup', function(){
              var input, filter, table, tr, td, i, txtValue;
              input = document.getElementById("order_list_search");
              filter = input.value.toUpperCase();
              table = document.getElementById("order_list_table");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
              
                th = tr[i].getElementsByTagName("th");
              for (j = 0; j < th.length; j++) {
               
                 td = tr[i].getElementsByTagName("th")[j];

                    if (td) {
                      txtValue = td.textContent || td.innerText;
                      if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                      } else {
                        tr[i].style.display = "none";
                      }
                    }       
                }       
              }
        });

        $('#invoice_list_search').on('keyup', function(){
              var input, filter, table, tr, td, i, txtValue;
              input = document.getElementById("invoice_list_search");
              filter = input.value.toUpperCase();
              table = document.getElementById("invoice_list_table");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
              
                th = tr[i].getElementsByTagName("th");
              for (j = 0; j < th.length; j++) {
               
                 td = tr[i].getElementsByTagName("th")[j];

                    if (td) {
                      txtValue = td.textContent || td.innerText;
                      if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                      } else {
                        tr[i].style.display = "none";
                      }
                    }       
                }       
              }
        });
        $('#payment_list_search').on('keyup', function(){
              var input, filter, table, tr, td, i, txtValue;
              input = document.getElementById("payment_list_search");
              filter = input.value.toUpperCase();
              table = document.getElementById("payment_list_table");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
              
                th = tr[i].getElementsByTagName("th");
              for (j = 0; j < th.length; j++) {
               
                 td = tr[i].getElementsByTagName("th")[j];

                    if (td) {
                      txtValue = td.textContent || td.innerText;
                      if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break;
                      } else {
                        tr[i].style.display = "none";
                      }
                    }       
                }       
              }
        });
         $('#modal_add_to_cart_button').on('click', function(){
            var json={};
            json['p_id']   = $(this).attr('data-id');
            json['isAjax'] = 1;
            json['qtybutton']  = $('.quantity2').val();
                
            $.ajax({
               url: '<?= base_url('Home/Carts/AddToCart') ?>',
               type: 'post',
               data: json,
               success:function(data){
                 data = JSON.parse(data);
                 $('.product-cart').html('');
                 $('.product-cart').html(data.cart); 
                 $('.countOFCart').html(data.count); 
                 $('.cart-total').html(data.total); 
                  toastr.success("Shopping Cart Updated", 'Success !',{timeOut: 5000});
                  $(".btn-close").click();
                  // swal(nameProduct, "is added to cart !", "success");
               }
            });
            });

            $('#update_profile').on('click', function(){
                var json={};
                json['name']   = $("#loginname").val();
                json['email']   = $("#loginemail").val();
                json['phone']   = $("#loginphone").val();
                json['address']   = $("#loginaddress").val();
                 
                if ($(this).prop("checked"))
                {
                    json['change_password'] = 1;
                    json['password']   = $("#password").val();
                    if($("#password").val().length<8||$("#password").val().length>32)
                    {
                        toastr.error("Password must be between 8 and 32 character", 'Error !',{timeOut: 5000});
                        return false;
                    }
                    else if($("#password").val()!=$("#cpassword").val())
                    {
                        toastr.error("Confirm Password not match", 'Error !',{timeOut: 5000});
                        return false;

                    }

                }
                else
                {
                    json['change_password'] = 0;
                    json['password']   = "";
                }
                if(json['name']=="")
                {
                     toastr.error("Name is required", 'Error !',{timeOut: 5000});
                        return false;

                }
                else if(json['email']=="")
                {
                     toastr.error("Email is required", 'Error !',{timeOut: 5000});
                        return false;

                }
                else if(json['phone']=="")
                {
                     toastr.error("Phone is required", 'Error !',{timeOut: 5000});
                        return false;

                }
                else if(json['address']=="")
                {
                     toastr.error("Address is required", 'Error !',{timeOut: 5000});
                    return false;

                }
                else
                    {
                    $.ajax({
                       url: '<?= base_url('Home/update_my_profile') ?>',
                       type: 'post',
                       data: json,
                       success:function(data){
                          toastr.success("Successfully updated", 'Success !',{timeOut: 5000});
                       }
                    });
                }
            });

            $('#order_track_button').on('click', function(){
                var json={};
                json['order_no']   = $("#order_no").val();
                $.ajax({
                   url: '<?= base_url('Home/order_details') ?>',
                   type: 'post',
                   data: json,
                   success:function(data){
                     data = JSON.parse(data);
                     if(data){
                            $("#order_status_div").css("display",'block');
                            if(data==1)
                            {
                               $("#order_status_title").html("Pending");
                               $("#order_status_pending").css('color','greenyellow'); 
                               $("#order_status_confirm").css('color','white'); 
                               $("#order_status_dispatch").css('color','white'); 
                               $("#order_status_delivered").css('color','white'); 
                            } 
                            else if(data==2)
                            {
                               $("#order_status_title").html("Confirm");
                               $("#order_status_pending").css('color','greenyellow'); 
                               $("#order_status_confirm").css('color','greenyellow'); 
                               $("#order_status_dispatch").css('color','white'); 
                               $("#order_status_delivered").css('color','white'); 
                            }
                            else if(data==3)
                            {
                               $("#order_status_title").html("Dispatch");
                               $("#order_status_pending").css('color','greenyellow'); 
                               $("#order_status_confirm").css('color','greenyellow'); 
                               $("#order_status_dispatch").css('color','greenyellow'); 
                               $("#order_status_delivered").css('color','white'); 
                            }
                            else if(data==4)
                            {
                               $("#order_status_title").html("Delivered");
                               $("#order_status_pending").css('color','greenyellow'); 
                               $("#order_status_confirm").css('color','greenyellow'); 
                               $("#order_status_dispatch").css('color','greenyellow'); 
                               $("#order_status_delivered").css('color','greenyellow'); 
                            }
                        }
                        else{
                            $("#order_status_div").css("display",'none');
                             toastr.error("No Order Found", 'Error !',{timeOut: 5000});
                        }
                   }
                });
            });
        $('.action-wishlist').on('click', function(){
        var json={};
        json['p_id']   = $(this).attr('data-id');
        json['isAjax'] = 1;
        $.ajax({
           url: '<?= base_url('Home/Carts/AddToWish') ?>',
           type: 'post',
           data: json,
           success:function(data){
                $('.countOFWish').html(data); 
              toastr.success("Wish List Updated", 'Success !',{timeOut: 5000});

              // swal(nameProduct, "is added to cart !", "success");
           }
        });
        });
        
          $('.remove-wishlist').on('click', function(){
            var json={};
            json['p_id']   = $(this).attr('data-id');
            json['isAjax'] = 1;
            $.ajax({
               url: '<?= base_url('Home/Carts/RemoveWishlist') ?>',
               type: 'post',
               data: json,
               success:function(data){
                
                  toastr.success("Wish List Updated", 'Success !',{timeOut: 5000});
                   setTimeout(function () {
                             location.reload();
                          },3000);
                  // swal(nameProduct, "is added to cart !", "success");
               }
            });
            });

            $('.remove-cart').on('click', function(){
            var json={};
            json['p_id']   = $(this).attr('data-id');
            json['isAjax'] = 1;
            $.ajax({
               url: '<?= base_url('Home/Carts/RemoveWishlist') ?>',
               type: 'post',
               data: json,
               success:function(data){
                
                  toastr.success("Wish List Updated", 'Success !',{timeOut: 5000});
                   setTimeout(function () {
                             location.reload();
                          },3000);
                  // swal(nameProduct, "is added to cart !", "success");
               }
            });
            });

            $('.action-quick-view').on('click', function(){
            var json={};
            json['p_id']   = $(this).attr('data-id');
            json['isAjax'] = 1;
            $.ajax({
               url: '<?= base_url('Home/Carts/select_product_info_by_product_id_ajax') ?>/'+$(this).attr('data-id'),
               type: 'post',
               data: json,
               success:function(data1){
                var data=JSON.parse(data1);
                    $("#modal_title").html(data.product_name +" "+data.product_code);
                    $("#modal_old_price").html(data.retail_price);
                    $("#modal_new_price").html(data.price);
                    $("#modal_description").html(data.description);
                    $("#modal_brand").html("Brand : "+data.brand+" <br><br> Category : "+data.category_name);
                    $("#modal_image").attr("src", data.image1);
                    $("#modal_add_to_cart_button").attr('data-id',data.id);
               }
            });
            });
        function submitcontact(){
          var nameProduct = '';
            var json={};
            json['name']   = $("#con_name").val();
            json['email']   = $("#con_email").val();
            json['subject']   = $("#con_subject").val();
            json['message']   = $("#con_message").val();
            json['isAjax'] = 1;

            if($('#con_name').val()==""){
                toastr.error("Name is required", 'Error !',{timeOut: 5000});
            }
            else if($('#con_email').val()==""){
                toastr.error("Email is required", 'Error !',{timeOut: 5000});
            }
            else if($('#con_subject').val()==""){
                toastr.error("Subject is required", 'Error !',{timeOut: 5000});
            }
            else if($('#con_message').val()==""){
                toastr.error("Message is required", 'Error !',{timeOut: 5000});
            }
            else{
                $.ajax({
                   url: '<?= base_url('Home/Contact_Inquery') ?>',
                   type: 'post',
                   data: json,
                   success:function(data){
                     toastr.success("Thank you for contact us", 'Success !',{timeOut: 5000});
                      setTimeout(function () {
                              window.location.href = '<?= base_url('contact') ?>';
                          },3000);
                  }
                });
            }
            
        }
        addSubmit();
 

 $('.deleteLine').on('click',function(){
      var $this= $(this).closest('tr');
      var amount = $this.find('.amount').text(); 
      var sub_total = $('#sub_total').text(); 
      console.log(amount);
       $('.sub_total').text(''); 
        $('.sub_total').text(sub_total-amount); 
      $this.find('.loading').css('background',"url('<?=base_url() ?>links/images/loader.gif') no-repeat 50% 50%");
      var id = $(this).attr('data-id');
      $.ajax({
               url: '<?= base_url('Home/Carts/remove') ?>',
               type: 'post',
               data: {id:id},
         success:function(data){
              setTimeout(function() {
                 $this.closest('tr').remove();
            }, 1000);
           
         }
      });


});

function getresult(url,c='',s='',from='',to='',manual='') {
    $("#product_list").html('');
    

    var datas={};
    datas['rowcount'] =$("#rowcount").val();
    datas['pagination_setting'] =$("#pagination-setting").val();
    datas['store_id'] =$("#store_id").val();
    if(manual!=''){
        datas['manual'] =manual;
    }else if(from!='' && to!='') {
        datas['from'] = from;
        datas['to'] = to;
    }else if($('#search-product').val()){
        datas['search'] = $('#search-product').val();
    }else{
        datas['catId'] =c;
        datas['SubCatId'] =s;
    }
  

    $.ajax({
        url: url,
        type: "GET",
        data:  datas,
        beforeSend: function(){
            $("#wait").css("display", "block");
        },
        success: function(data){
        
        var res= JSON.parse(data);

        setTimeout(function () {
            $("#wait").css("display", "none");
            $("#pagination-result").html(res.pagination); 
            $("#product_list").html(res.result);
            $("#total_row").html(res.total);
             addSubmit();
        },1000);

        
        // setInterval(function() {$("#overlay").hide(); },500);
        },
        error: function() 
        {}          
   });
}
function getresult1(url,c='',s='') {
    var datas={};
    datas['rowcount'] =$("#rowcount").val();
    datas['pagination_setting'] =$("#pagination-setting").val();
    datas['catId'] =c;
    datas['SubCatId'] =s;

   $("#product_list").html('');
    $.ajax({
        url: url,
        type: "GET",
        data: datas,
        beforeSend: function(){ $("#wait").css("display", "block");},
        success: function(data){
        var res= JSON.parse(data);
         $("#wait").css("display", "none");
        $("#pagination-result").html(res.pagination); 
        $("#product_list").html(res.result);

        $('#subCateWise').focus();
        // setInterval(function() {$("#overlay").hide(); },500);
        },
        error: function() 
        {}          
   });
}


$('.menus').on('click',function() {
    var cate_id= $(this).attr('data-id');
    $('#search-product').val("");
    getresult('<?php echo base_url('Home/getRecord') ?>',cate_id);
});
$('.sub-menus').on('click',function() {
    var cate_id= $(this).attr('data-id');
    var subcat_id= $(this).attr('data-sub');

    $('#search-product').val("");
    getresult('<?php echo base_url('Home/getRecord') ?>',cate_id,subcat_id);
});
$('#Filter').on('click',function() {

    first= $('#value-lower').html();
    second=  $('#value-upper').html();
    $('#search-product').val("");
    getresult('<?php echo base_url('Home/getRecord') ?>','','',first,second);
});
function search_product(){
   getresult('<?php echo base_url('Home/getRecord') ?>');
}

<?php if($this->uri->segment(2)=='Product'){ ?>
getresult("<?php echo base_url('Home/getRecord') ?>");
<?php } ?>

<?php  if($this->uri->segment(2)=='search'){ ?>
getresult("<?php echo base_url('Home/getRecord') ?>",'','','','','<?= $_GET['item'] ?>');
<?php } ?>







</script>