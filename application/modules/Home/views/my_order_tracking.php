<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  .nav-link i{
    font-size: 25px;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 10px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody th {
    border: 1px solid white!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }

</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">Order Tracking</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Order Tracking</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

   <!--== Start Product Tab Area Wrapper ==-->
    <section style="border-top: 1px solid white;" class="product-area product-default-area">
      <div class="container">
        <div  class="row">
          <div  class="col-12">
            <div  class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div  class="row">
                <?php $this->load->view('WebView/customer_sidebar',['page'=>"order_tracking"]); ?>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">

                         <form style="margin-left:20px;" id="order_form">
                            <div class="form-group">
                              <label for="order_no">Enter your order number #</label>
                              <input type="text" class="form-control" id="order_no"  name="order_no"  placeholder="Enter your order number #">
                            </div>
                            <br>
                           <div style="width: 100%;" align="center">
                            <button type="button" id="order_track_button" style="background-color: black;border: 1px solid white;color: white;" class="btn btn-lg  ">Track your order</button>
                            </div>
                          </form>

                          <div style="margin-left:20px;display: none;margin-top: 20px;" class="row" id="order_status_div" >
                              <h1>Your order is <span id="order_status_title"  style="color: greenyellow;">"Pending"</span></h1><br><br>
                              <div class="col-lg-12" style="display:inline-flex;">
                                  <div class="col-sm-3" id="order_status_pending"  style="text-align:center;color: greenyellow;"><i style="font-size:90px;" class="icofont-sand-clock"></i><br> Pending</div>
                                  <div class="col-sm-3" id="order_status_confirm" style="text-align:center;"><i style="font-size:90px;" class="icofont-check-circled"></i><br>Confirm</div>
                                  <div class="col-sm-3" id="order_status_dispatch" style="text-align:center;"><i style="font-size:90px;" class="icofont-delivery-time"></i><br> Dispatch</div>
                                  <div class="col-sm-3" id="order_status_delivered" style="text-align:center;"><i style="font-size:90px;" class="icofont-fast-delivery"></i><br> Delivered</div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>