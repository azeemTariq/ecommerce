<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  .nav-link i{
    font-size: 25px;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 10px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody th {
    border: 1px solid white!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }

</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">Order Detail</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">Order Detail</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

   <!--== Start Product Tab Area Wrapper ==-->
    <section style="border-top: 1px solid white;" class="product-area product-default-area">
      <div class="container">
        <div  class="row">
          <div  class="col-12">
            <div  class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div  class="row">
                <?php $this->load->view('WebView/customer_sidebar',['page'=>"order_list"]); ?>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">

                         <div class="cart-table-wrap">
                             <div style="float:right;padding-bottom: 20px;" class="pull-right"><a href="<?= base_url('my-order-list') ?>"  class="btn btn-success">Back to orders</a href="<?= base_url('my-order-list') ?>" ></div>  
                            <form style="float:right;width: 100%;" action="#" method="POST">
                            
                            <div class="cart-table table-responsive">
                              <h3>Order Info</h3>
                              <table >
                                <thead>
                                  <tr>
                                    <th  class="pro-product text-center">Order No</th>
                                    <th class=" text-center">Date</th>
                                    <th class=" text-center">No of Item</th>
                                    <th class=" text-center">Amount</th>
                                    <th class=" text-center">Status</th>
                                  </tr>
                                </thead>
                                <tbody >
                                  <?php $order_status=0;  $status=['Pending',"Confirm","Dispatch","Delivered"] ; foreach($order AS $d) {
                                    $order_status=$d['status'];
                                   ?>
                                    <tr>
                                      <th  class="pro-product text-center"><?= $d['code'] ?></th>
                                      <th class=" text-center"><?= date("d-m-Y",strtotime($d['date'])) ?></th>
                                      <th class=" text-center"><?= $d['items'] ?></th>
                                      <th class=" text-center"><?= $d['totalAmount'] ?></th>
                                      <th class=" text-center" style="color:greenyellow;"><?= $status[$d['status']-1]; ?></th>
                                    </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                              <br>
                              <h3>Order Product Detail</h3>
                              <table >
                                <thead>
                                  <tr>
                                    <th  class="pro-product text-center">Product</th>
                                    <th class=" text-center">SKU</th>
                                    <th class=" text-center">No of Item</th>
                                    <th class=" text-center">Amount</th>
                                    <th class=" text-center">Add review</th>
                                  </tr>
                                </thead>
                                <tbody >
                                  <?php $status=['Pending',"Confirm","Dispatch","Delivered"] ; foreach($order_detail AS $d) {

                                   ?>
                                    <tr>
                                      <th  class="pro-product text-center">
                                         <div class="product-info">
                                          <div align="" class="product-img">
                                            <a href="#"><img style="margin-left: 15px;width: 50px;height: 50px;" src='<?= base_url('uploads/product/'.$d['image1']) ?>' alt=""></a>
                                          </div>
                                          <div class="product-info"><?= $d['product_name'] ?></a></h4>
                                          </div>
                                        </div>
                                      </th>
                                      <th class=" text-center"><?= $d['product_code'] ?></th>
                                      <th class=" text-center"><?= $d['ItemQty'] ?></th>
                                      <th class=" text-center"><?= $d['price'] ?></th>

                                      <th class=" text-center" >
                                        <?php if($this->Mdl_Home->check_product_review($d['OrderId'],$d['OrderDetailId'],$d['id'])==0 && $order_status==4){  ?>
                                        <button type="button" data-product-id="<?= encrypt($d['id']) ?>" data-order-id="<?= encrypt($d['OrderId']) ?>" data-order-detail-id="<?= encrypt($d['OrderDetailId']) ?>"  class="review-action btn btn-info">Add Review</button>
                                        <?php }else if($order_status<4){ echo "When product is delivered";}else { echo "Thanks for review"; } ?>

                                      </th>
                                    </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                            </div>
                           

                            </form>
                          </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<style type="text/css">
  .star-rating {
  line-height:32px;
  font-size:30px;
  text-align: center;
}

.star-rating .fa-star{color: yellow;}
</style>

 <!--== Start Quick View Menu ==-->
  <aside  class="product-detail-modal">
    <div class="product-quick-view-inner">
      <div style="background-color:black;border: 1px solid white;" class="product-quick-view-content">
        <button type="button" class="btn-close">
          <span  class="close-icon" ><i style="color: white;"  class="fa fa-times-circle"></i></span>
        </button>
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-12">
            <form class="loginSubmit" id="review_form" method="POST" role="form">
            <div   class="content" align="center">
              <h3 style="" id="modal_title" >Add your review</h3>
              <div class="quick-view-select">
                 <input type="hidden" value="0" name="order_id" id="order_id">
                 <input type="hidden" value="0" name="order_detail_id" id="order_detail_id">
                 <input type="hidden" value="0" name="product_id" id="product_id">
                <div class="quick-view-select-item">
                  <div class="star-rating">
                    <span class="fa fa-star-o" data-rating="1"></span>
                    <span class="fa fa-star-o" data-rating="2"></span>
                    <span class="fa fa-star-o" data-rating="3"></span>
                    <span class="fa fa-star-o" data-rating="4"></span>
                    <span class="fa fa-star-o" data-rating="5"></span>
                    <input type="hidden" name="whatever1" id="whatever1" class="rating-value" value="0">
                  </div>
                </div>
                <div class="quick-view-select-item">
                  <label for="forSize" class="form-label">Comment:</label>
                  <textarea class="form-control" rows="7" id="comment" name="comment"></textarea>
                </div>
                
              </div>
              <div class="action-top">
                <button style="margin-left: 10px;width: 100%;"  type="button" class=" btn add-review btn-black">Add Review</button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <div class="canvas-overlay"></div>
  </aside>
  <!--== End Quick View Menu ==-->
  