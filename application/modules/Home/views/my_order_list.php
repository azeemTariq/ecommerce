<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  .nav-link i{
    font-size: 25px;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 10px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody th {
    border: 1px solid white!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }

</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">My Order List</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">My Order List</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

   <!--== Start Product Tab Area Wrapper ==-->
    <section style="border-top: 1px solid white;" class="product-area product-default-area">
      <div class="container">
        <div  class="row">
          <div  class="col-12">
            <div  class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div  class="row">
                <?php $this->load->view('WebView/customer_sidebar',['page'=>"order_list"]); ?>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">

                         <div class="cart-table-wrap">
                            <div style="float:right;padding-bottom: 20px;" class="pull-right">Search <input id="order_list_search" type="text" placeholder="Search"></div>  
                            <form style="float:right;width: 100%;" action="#" method="POST">
                            
                            <div class="cart-table table-responsive">
                              <table >
                                <thead>
                                  <tr>
                                    <th  class="pro-product text-center">Order No</th>
                                    <th class=" text-center">Date</th>
                                    <th class=" text-center">No of Item</th>
                                    <th class=" text-center">Amount</th>
                                    <th class=" text-center">Status</th>
                                    <th class=" text-center">Action</th>
                                  </tr>
                                </thead>
                                <tbody id="order_list_table">
                                  <?php $status=['Pending',"Confirm","Dispatch","Delivered"] ; foreach($data AS $d) {

                                   ?>
                                    <tr>
                                      <th  class="pro-product text-center"><?= $d['code'] ?></th>
                                      <th class=" text-center"><?= date("d-m-Y",strtotime($d['date'])) ?></th>
                                      <th class=" text-center"><?= $d['items'] ?></th>
                                      <th class=" text-center"><?= $d['totalAmount'] ?></th>
                                      <th class=" text-center" style="color:greenyellow;"><?= $status[$d['status']-1]; ?></th>
                                      <th class=" text-center" ><a href="<?= base_url('my-order-detail/'.encrypt($d['id']))  ?>" data-id="<?= $d['id'] ?>"  class="btn btn-info">View</a></th>
                                    </tr>
                                 <?php } ?>
                                </tbody>
                              </table>
                            </div>
                           

                            </form>
                          </div>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

