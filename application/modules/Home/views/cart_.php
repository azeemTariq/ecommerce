<!-- Header -->
	
	<!-- Title Page -->
	

	<!-- Cart -->
	<section class="cart bgwhite p-t-10 p-b-100">
		<div class="container">
			<form action="<?= base_url('Home/update_cart') ?>" method="POST">
			<!-- Cart item -->
	   <div class="sec-title p-b-22 mt-5" style="border:1px solid #ccc;padding-top:15px;">
        <h3 class="m-text5 t-center">
         View Cart
        </h3>
      </div>
			<div class="container-table-cart pos-relative">
				
				<div class="wrap-table-shopping-cart bgwhite smooth-scroll">
					<table class="table-shopping-cart">
						<tr class="table-head bg-light">
							<th class="column-1">Code</th>
							<th class="column-2">Product</th>
							<th class="column-2">Color / Size</th>
							<th class="column-3">Price</th>
							<th class="column-4 p-l-70">Quantity</th>
							<th class="column-5">Total</th>
							<th class="column-5">Action</th>
						</tr>


                       <?php  $subTotal = 0;
						foreach($this->cart->contents() as $k=>$value){ 
                             $subTotal += $value['qty'] *  $value['price'];
							?>
						<tr class="table-row">
							<input value="<?= $value['rowid'] ?>" name="rowid[]" type="hidden">
							<td class="column-1">
								<div class="cart-img-product b-rad-4 o-f-hidden">
									<img src="<?php echo base_url('uploads/product/').$value['options']['pro_image'] ?>" alt="IMG-PRODUCT">
								</div>
							</td>
								<td class="column-2"><?= $value['name'] ?></td>
							<td class="column-3"><?= $value['suggestion'] ?></td>
							<td class="column-3"><?= $value['price'] ?>.00</td>
							<td class="column-5">
								<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>
  
									<input class="size8 m-text18 t-center num-product" type="number" name="num-product[]" value="<?= $value['qty'] ?>">
 									
									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</td>
							<td class="column-3">RS.  <span class="amount"><?= $value['qty'] * $value['price'] ?></span></td>
							<td class="column-5 loading"><a href="javascript:;" class="deleteLine" data-id="<?= encrypt($value['rowid']) ?>"><i class="fa fa-trash"></i></a></span></td>
						</tr>

					   <?php }
?>					</table>
				</div>

			<div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
				<div class="flex-w flex-m w-full-sm">
				</div>

				
			</div>
		
   
				 <div class="card mb-4">
              <div class="card-body">

                <h5 class="mb-3"><b>The total amount of</b></h5>

     
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                    Temporary amount
                    <span >RS. <b  id="sub_total" class="sub_total"><?= $subTotal ?></b> (Only for karachi)</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                    Delivery Charges 
                    <span class="others">(Other City Delivery Charges are Subect to Standard Corior Service Charges)</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                   <!--  Shipping
                    <span>Gratis</span> -->
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                    <div>
                      <strong>The total amount of</strong>
                      <strong>
                        <p class="mb-0">(including Charges)</p>
                      </strong>
                    </div>
                    <span><strong >Rs. : <b class="sub_total"><?= $subTotal ?></b></strong></span>
                  </li>
                </ul>
                </ul>
               <div class="pull-right">
					<!-- Button -->
					<button class="btn btn-primary" name="update_cart" type="submit">
						Update Cart
					</button>
					<a href="<?= base_url('Home/CheckOut') ?>"><button class="btn btn-success" type="button">
						Processed to checkout
					</button></a>
				</div>


               <!--  <a href="<?= base_url('Home/CheckOut') ?>"><button type="button" class="btn btn-primary btn-block waves-effect waves-light">go to
                  checkout</button></a> -->

              </div>
            </div>
			</div>


			</form>
		</div>
	</section>



	


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>



<style type="text/css">
	.table-shopping-cart th{
		background-color: #f0f0f0;
		/*color:#fff !important;*/
	}
</style>	