<!-- <section class="ftco-section ftco-no-pt ftco-no-pb py-5 ">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
             

             <h2 style="text-align:center;">  </h2>
            <h5  style="text-align:center;" class="mt-5">  Standard Shipping will be charged on all orders </h5>
          </div>
          
          <hr class="mt-5">  
         </div> 
          <div class="row d-flex justify-content-center mb-5">
          
          <ul> 
       <li> . Karachi Rs. 200</li>
       <li>.  All over Pakistan Rs. 250 to 300 (Subject to Charged by Courier Company) </li>
        <li>  • Orders placed by 12:00 pm (PST) will be shipped the same day. </li>
        <li>• Orders received on Sundays and on Pakistan's National Holidays  </li>
        <li> will be processed and shipped on the next working day.</li>
        <li>• Delivery time will be within 24 hours (delivery on Sundays available for grocery only).</li>
        <li> • SUNDAY BAZAAR delivery will be processed same day (Or Maximum by Monday) </li>
          </ul> 
         </div> 
       </div>




          
           
     </section> -->


       <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/6.png');background-repeat: no-repeat;background-size: 100%;" class="container mt-4">
        <h4 class="m-text12 text-left">
          Shipping details
        </h4>

    <div class="flex-w p-l-15 p-r-15">
      <div class="flex-col-c  p-l-15 p-r-15 p-t-16 p-b-15 respon1">
        
        <a href="#" class="s-text11 text-left">
           <ul > 
       <li > . Karachi Rs. 200</li>
       <li>.  All over Pakistan Rs. 250 to 300 (Subject to Charged by Courier Company) </li>
        <li>  • Orders placed by 12:00 pm (PST) will be shipped the same day. </li>
        <li>• Orders received on Sundays and on Pakistan's National Holidays  </li>
        <li> will be processed and shipped on the next working day.</li>
        <li>• Delivery time will be within 24 hours (delivery on Sundays available for grocery only).</li>
        <li> • SUNDAY BAZAAR delivery will be processed same day (Or Maximum by Monday) </li>
          </ul> 
        </a>
      </div>


    </div>

  </section>

  <!-- Shipping -->
  <section class="shipping bgwhite p-t-62 p-b-46">
    <div class="flex-w p-l-15 p-r-15">
      <div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon1">
        <h4 class="m-text12 t-center">
          Delivery Timeline & Details
        </h4>

        <a href="#" class="s-text11 t-center">
          Click here for more info
        </a>
      </div>

      <div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 bo2 respon2">
        <h4 class="m-text12 t-center">
          10 Days Return & Exchange Policy
        </h4>

        <span class="s-text11 t-center">
          Simply return it within 10 days for an exchange.
        </span>
      </div>

      <div class="flex-col-c w-size5 p-l-15 p-r-15 p-t-16 p-b-15 respon1">
        <h4 class="m-text12 t-center">
          Store Timing
        </h4>

        <span class="s-text11 t-center">
          Shop open 24 Hours 
        </span>
      </div>
    </div>
  </section>
