<div class="container chekout">
      <div class="py-5 text-center">
     <!--    <img class="d-block mx-auto mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> -->
        <h2>Checkout form</h2>
        <p class="lead">Order Placement Form.</p>
      </div>

      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill"><?= count($this->cart->contents()) ?></span>
          </h4>
          <ul class="list-group mb-3">



               <?php 
                $TotalAmount =0;
                $TotalQty =count($this->cart->contents());
               foreach ($this->cart->contents() as $key => $items) { 
                  $TotalAmount+=$items['qty']*$items['price'];
                ?>
           
              <li class="list-group-item d-flex  lh-condensed mb-1">
                  <div class="header-cart-item-img">
                    <img id="cart-img" src="<?php echo base_url('uploads/product/'.$items['options']['pro_image']) ?>" alt="IMG">
                  </div>
              <div>
                <h6 class="my-0"><?= $items['name']  ?></h6>
                <small class="text-success"><?= $items['qty'].' x '.$items['price'] ?> </small>
                
              </div>
              <span class="text-muted">(<?= $items['qty']*$items['price'] ?>) </span>
            </li>

            <?php } ?>
          
            <li class="list-group-item d-flex justify-content-between bg-light">
              <div class="text-success">
                <h6 class="my-0">Delivery Charges</h6>
                <small></small>
              </div>
              <span class="text-success">Rs. 200</span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (PAK)</span>
              <strong>PKR :<?= ($TotalAmount) + 200 ?></strong>
            </li>
          </ul>



        <!--   <form class="card p-2" action="<?= base_url('Home/') ?>" method="post">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Promo code">
              <div class="input-group-append">
                <button type="submit" class="btn btn-secondary">Redeem</button>
              </div>
            </div>
          </form> -->
        </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Billing address</h4>
          <form class="needs-validation was-validated"  action="<?= base_url('Home/Carts/Order') ?>" method="post">
            

              <div class="mb-3">
              <label for="name">Name <span class="text-muted">(Full Name)</span></label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Name" required="">
              
            </div>

          <!--   <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="firstName" placeholder="Enter First Name" value="" required="">
               
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="lastName" placeholder="Enter Last Name" value="" required="">
                
              </div>
            </div> -->

           <!--  <div class="mb-3">
              <label for="username">Username</label>
              <div class="input-group">
                
                <input type="text" class="form-control" id="username" placeholder="Username" required="">
                
              </div>
            </div> -->


               <div class="row">
              <div class="col-md-6 mb-3">
                <label for="email">Email </label>
              <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" required="">
               
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Cell # <span class="text-muted"></span></label>
                <input type="text" class="form-control" id="cell" placeholder="e.g 03xxxxxxxxx" name="cell"  required="">
              </div>
            </div>

          <div class="row">
              <div class="col-md-6 mb-3">
                <label for="email">Date <span class="text-muted">(Optional)</span> </label>
              <input type="date" class="form-control date"  name="date" id="date" placeholder="you@example.com">
               
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Time <span class="text-muted">(Optional)</span></label>
                <input type="time" class="form-control" id="lastName" name="time" placeholder="Enter Last Name" value="">
              </div>
            </div>

            <input type="hidden" name="TotalAmount" value="<?= $TotalAmount ?>">
            <input type="hidden" name="items" value="<?= $TotalQty ?>">



          

           <!--  <div class="mb-3">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" placeholder="1234 Main St" required="">
              
            </div> -->

            <div class="mb-3">
              <label for="address2">Address  <span class="text-muted"></span></label>
              <input type="text" class="form-control" id="address2" name="address" placeholder="Address">
            </div>

            <div class="mb-3">
              <label for="address2">Message  <span class="text-muted">(Optional)</span></label>
              <textarea  class="form-control"  name="msg" placeholder="Add Message"></textarea>
            </div>


            <!-- <hr class="mb-4"> -->
          <!--   <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="same-address">
              <label class="custom-control-label" for="same-address">Submit yoru Order With Coninue Option</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="save-info">
              <label class="custom-control-label" for="save-info">Save this information for next time</label>
            </div> -->
            <hr class="mb-4">


            <button class="btn btn-primary btn-lg btn-block mb-4" type="submit">Continue to checkout</button>
          </form>
        </div>
      </div>
    </div>


    <style type="text/css">
      .chekout input{
        border: 1px solid #ccc !important;

      }
      .custom-select{

       border-color:#ccc !important;
      }
    </style>