<style type="text/css">
  .qty-btn{
    color: #fff;
  }
  .nav-link i{
    font-size: 25px;
  }
  table thead th {
    border: 1px solid black!important;
    padding-bottom: 10px!important;
    font-weight: bold!important;
    background-color: white!important;
    color: black!important;
  }
  table tbody th {
    border: 1px solid white!important;
    padding-bottom: 10px!important;
    font-weight: normal;
  }

</style>

<!--== Start Page Title Area ==-->
    <section style="background-image:url('<?= base_url() ?>assets/website/img/blog/5.png');background-repeat: no-repeat;background-size: 100%;" class="page-title-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-title-content">
              <h2 class="title">My Profile</h2>
              <div class="bread-crumbs"><a href="<?= base_url() ?>">Home<span class="breadcrumb-sep">></span></a><span class="active">My Profile </span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--== End Page Title Area ==-->

   <!--== Start Product Tab Area Wrapper ==-->
    <section style="border-top: 1px solid white;" class="product-area product-default-area">
      <div class="container">
        <div  class="row">
          <div  class="col-12">
            <div  class="product-tab-content" data-aos="fade-up" data-aos-duration="1000">
              <div  class="row">
                <?php $this->load->view('WebView/customer_sidebar',['page'=>"update_profile"]); ?>
                <div class="col-xl-9 col-md-9">
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="popular-product" role="tabpanel" aria-labelledby="new-product-tab">
                      <div class="row">
                        <?php foreach($data AS $d) { ?> 
                         <div class="cart-table-wrap">
                            <form>
                              <div class="form-group">
                                <label for="loginname">Name</label>
                                <input type="text" class="form-control" value="<?= $d['name'] ?>" id="loginname" placeholder="Enter Full Name">
                              </div>

                              <div class="form-group">
                                <label for="loginemail">Email address</label>
                                <input type="email" class="form-control"  value="<?= $d['email'] ?>" id="loginemail" placeholder="Enter email">
                              </div>
                              
                              <div class="form-group">
                                  <label for="username">Username</label>
                                  <input type="text" disabled class="form-control"  value="<?= $d['username'] ?>" id="username" placeholder="Username">
                              </div>

                               <div class="form-group">
                                  <label for="loginphone">Phone No.</label>
                                  <input type="text"  class="form-control"  value="<?= $d['phone'] ?>" id="loginphone" placeholder="Enter Phone No">
                                </div>
                               <div class="form-group">
                                  <label for="loginaddress">loginAddress.</label>
                                  <textarea type="text"  class="form-control"  id="loginaddress" placeholder="Enter Your Address"><?= $d['address'] ?></textarea>
                                </div>
                              <br>
                              <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="change_password">
                                <label class="form-check-label" for="change_password">Change Password</label>
                              </div>

                              <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" disabled class="form-control" id="password" placeholder="Password">
                              </div>
                              <div class="form-group">
                                <label for="cpassword">Confirm Password</label>
                                <input type="password" disabled class="form-control" id="cpassword" placeholder="Confirm Password">
                              </div>
                              <br>
                              <button type="button" id="update_profile" class="btn btn-primary">Update Information</button>
                            </form>
                          </div>
                          <?php } ?>
                      </div>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>