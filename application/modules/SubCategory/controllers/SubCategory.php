<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCategory extends MX_Controller {

  public function __construct() {

    modules::run('login/login');
		parent::__construct();
    modules::run('Login/login');
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_SubCategory');
	}
    public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= "Sub-Category List";
      $data['content'] = 'SubCategory/index.php';
      $this->load->view('Template/template',$data);
    }

    public function add(){
      
      $data['pageTitle']= "Add Sub-Category";
      $data['content'] = 'SubCategory/action';
      $this->load->view('Template/template',$data);
    }
      

    public function getcategory(){
      $this->db->select("*");
      $this->db->from('category');   
      return $this->db->get()->result_array();
    }  

     public function getsubcategory($id=''){
      $this->db->select("*");
      $this->db->from('sub_category');
      // $this->db->where('status',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
    } 
    public function getsubcatByCat(){
      $this->db->select("*");
      $this->db->from('subcategory');
      $this->db->where('category',$this->input->post('id'));
      $data=$this->db->get()->result();
      echo json_encode($data);
    }    
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'id' ,
            'subCategoryName' ,
            'description',
            'category_name',
            'store_name'
        );

       
   

        $this->db->start_cache();
        $this->db->select("sub_category.*,category_name");
        $this->db->from('sub_category');
         $this->db->join('category','sub_category.cat_id=category.id AND category.is_active =1','inner');
        $this->db->where('sub_category.is_active<>',2);

  

        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/action/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","sub_category","Mdl_SubCategory") href="#"><i class="far fa-trash-alt"></i></a>';
          }

            $row['action'].='</div>';
        

            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['subCategoryName'];
            $nestedData[] = $row['description'];
            $nestedData[] = '<i class="text-success">'.$row['category_name'].'<i>';
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function command(){
      $this->load->library('form_validation');
      // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $post=$this->input->post();
       $id = (!empty($post['id'])) ? decrypt($post['id']) : '';
       if(!empty($post)){ 
          $this->form_validation->set_rules('cat_id', 'Category', 'required|max_length[16]');
          $this->form_validation->set_rules('subCategoryName', ' Sub Category', 'required|max_length[25]|is_unique_update[sub_category.subCategoryName@id@' .$id. ']');
          $this->form_validation->set_rules('description', 'Description', 'max_length[100]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }

       $image=do_upload($_FILES['profile_image'] ?? '','uploads/');
           $Data=array(
            'cat_id'          =>$post['cat_id'],
            'subCategoryName' =>$post['subCategoryName'],
            'description'     =>$post['description']
           );
            if(!empty($image)){
            $Data['image'] =$image;
           }
            if(!empty($post['id'])){
            $id=decrypt($post['id']);
            $this->Mdl_SubCategory->commandAct($Data,'sub_category',['id'=>$id]);
            $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
            $this->Mdl_SubCategory->commandAct($Data,'sub_category');
          }

           echo json_encode($_return);
           exit();
        }
        redirect(base_url($this->controller.'/'));
}

   public function action(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getsubcategory($id);
      $data['content'] = 'SubCategory/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }
}
