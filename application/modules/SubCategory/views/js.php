<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
  if(isset($result_set[0])){
    $row=$result_set[0];
  }
?>

<script type="text/javascript">
	   datatablelist("subCategory");
function submitPR(){
	  $('.submitIRM').submit(function (e) {
       if($('.submitIRM')[0].checkValidity(e)){
                $('button[type="submit"]').prop('disabled',true);
      var url = "<?php echo base_url($controller.'/command'); ?>"; 
         $.ajax({
             url:url,
             type:"post",
             data:new FormData(this),
             processData:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                 var data = JSON.parse(data);
                        if(data.status=='Error'){
                            toastr.error(data.msg, 'Alert !',{timeOut: 8000});
                           $('button[type="submit"]').prop('disabled',false);
                        }else if(data.status=='success'){
                      toastr.success(data.msg, 'Success !');
                      if($('#hidden_id').val()!=''){
                          setTimeout(function () {
                              window.location.href = '<?= base_url($controller) ?>';
                          },3000);
                      }else{
                             $('#subCategoryName').val('');
                             $('#description').val('');
                             $('button[type="submit"]').prop('disabled',false);
                    }
                }
           }
         });
       } e.preventDefault(); 
    });
  }
  submitPR();

  function getSubCategory() {
    var cat = $('#store').val();
    $.ajax({
        url: '<?= base_url("Category/getsubcatByCat") ?>',
        type:'post',
        data:{id:cat},  
      success:function(data) {

          var data = JSON.parse(data);
          $('#category').empty();
          $('#category').append('<option value="">Select category</option>');
          // loop 
          $.each(data, function( i, val ) {
             $('#category').append('<option value="'+val.id+'">'+val.category_name+'</option>');
          });
          // loop 
          $('#category').val($('#hideCatgory').val());
      }
    });
}


$(".submitIRM").validate({
    rules: {
        cat_id: {
            required:true,
           
        },
        subCategoryName : {
            required:true,
        }
    },
    messages:{
 
    }
});


<?php if(!empty($row)){ ?>
  getSubCategory();
<?php } ?>


</script>