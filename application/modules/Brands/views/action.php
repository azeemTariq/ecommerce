<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];
    
}
$display =($view=='view') ? 'disabled' : '';
?>
<div class="container col-10 mt-4">
     <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Brands"><button class="btn btn-outline-success mb-1 justify-content-center">show Brands</button></a></div>

            <div class="card card-outline-success">
              <div class="card-header">
                <h3 class="card-title text-success"><b>Add Brands</b></h3>
              </div>
              <div class="card-body">
                     <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitIRM" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                        <input type="hidden" id="hidden_id" name="id" value="<?= $id ?>">
            


                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Brand Name </label>
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                       <input <?= $display ?> type="" class="form-control" id="subCategoryName" placeholder="Brand Name" name="subCategoryName" value="<?= $row['subCategoryName'] ?? '' ?>" required>
                       <input type="hidden" name="hideCatgory" id="hideCatgory" value="<?= $row['cat_id'] ?? ''?>">
                     </div>
                   </div>
                   <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Page Url </label>
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                       <input <?= $display ?> type="" class="required-field form-control" required id="page_url" placeholder="Page Url" name="page_url" value="<?= $row['page_url'] ?? '' ?>">
                     </div>
                   </div> 
                  <div class="form-group row">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Description </label>
                     <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                      <textarea <?= $display ?> class="form-control"  placeholder="Add description" id="description" name="description" height="200"><?= $row['description'] ?? '' ?></textarea>

                     </div>
                   </div>
                     <div class="form-group row" style="display: ;">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
                       <div class="col-sm-6">
                         <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
                          <div class="image col-md-12 col-lg-9 col-sm-12">
                            <?php  $img= (isset($row['banner']) && !empty($row['banner']) ) ? base_url().'uploads/banners/'.$row['banner'] :  base_url().'assets/clip.png'  ?>
                            <img id="imgView" accept="image/x-png,image/gif,image/jpeg" width="380" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
                          </div>
                       </div>
                       </div>

                      <div class="form-group row " style="margin-left: 10px ;">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3">file or image                     </label>
                        <div>
                        
                         <div class="input-group ">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                          <input <?= $display ?> type="file" class="custom-file-input ml-2 profile_image" name="profile_image" id="profile_image">
                          <label class="custom-file-label col-md-12" for="inputGroupFile01">Choose file</label></div></div>
                        </div>
                      </div>

                <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                  <button type="submit" class="btn btn-outline-success">Submit</button>
                </div>
                <!-- /.card-body -->
              </form>   
            </div>
          </div>
            <!-- /.card -->



            
 
</div>