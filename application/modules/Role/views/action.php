<?php

$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];
}
$display =($view=='view') ? 'disabled' : '';
?>
  <div class="container"> 
        <div class="text-right col-md-11 col-md-offset-1"> 
            <a href="<?php echo base_url() ?>Role/"><button class="btn btn-success justify-content-center">List View</button></a> 
              </div> 
        </div>
  <div class="container col-9 mt-4">


            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Role</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form"  action="<?php echo base_url($controller.'/command')?>" method="POST" >
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="card-body">
                  <div class="form-group col-md-6 col-sm-12 col-lg-6">
                    <label for="exampleInputEmail1">Role</label>
                    <input <?= $display ?> type="text" class="form-control" id="exampleInputEmail1" placeholder="Add Role" name="role" required="" value="<?= $row['role'] ?? '' ?>">
             <?= form_error('role') ?>
                  </div>
                  <div class="form-group col-md-6 col-sm-12 col-lg-6">
                    <label for="exampleInputEmail1"> Description</label>
                    <textarea <?= $display ?> class="form-control" name="description" ><?= $row['description'] ?? '' ?></textarea>
                    <?= form_error('description') ?>
                

                    </select>
                  </div>
                  
                </div>

                <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
 
</div>