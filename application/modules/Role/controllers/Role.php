

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MX_Controller {

   function __construct() {
    modules::run('Login/login');
		parent::__construct();
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_Role');
	}

    public $controller = __CLASS__;
 
    public function index(){
        $data['pageTitle']= "Role List";
        $data['result']=$this->getRole();
        $data['content'] = 'Role/index.php';

        $this->load->view('Template/template',$data);
    }

    public function add(){

	  	 $data['pageTitle']= "Role";
        $data['content'] = 'Role/action';
        $this->load->view('Template/template',$data);
    }

    
      public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'id',
            'role',
            'description'
           
        );
        
        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('role');
  

        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
     
        $query = $this->db->get()->result_array();
        $data = array();
        foreach ($query as $key => $row) {
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
           $row['action'].='<a class="green" target="_blank" href="' . base_url($controller . '/action/view/' . encrypt($row['id'])) . '"><i class="far fa-eye"></i></a>';

          
           // $row['action'].='<a class="green" target="_blank" href="' . base_url($controller . '/action/edit/' . encrypt($row['id'])) . '"><i class="far fa-edit"></i></a>';
           //  $row['action'].='<a class="green" target="_blank" href="' . base_url($controller . '/action/copied/' . $row['id']) . '"><i class="far fa-trash-alt"></i></a>';

            $row['action'].='</div>';
        

            $nestedData = array();
            $nestedData[] = $row['id'];
            $nestedData[] = $row['role'];
            $nestedData[] = $row['description'];
            $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }



    public function getRole($type='',$id=''){
     $orderby=' WHERE is_active=1';
      if($id!=''){
       $orderby .= ' AND id = '.$id;
     }
     if($type==''){
         $orderby.=' order by id DESC ';
     }
      return   $this->db->query(" SELECT * FROM role $orderby ")->result_array();
    }

   public function command(){

       $post=$this->input->post();
        $this->load->library('form_validation');
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

      if($post){


        $this->form_validation->set_rules('role', 'role', 'required|max_length[50]|min_length[6]');
        $this->form_validation->set_rules('description', 'description', 'required|max_length[10]');
        
        if ($this->form_validation->run() == FALSE)
      {
          $data['result_set'][] = $post;
          $data['content'] = 'Role/action';
          return  $this->load->view('Template/template',$data);                    
      } 

          
           $Data=array(
            'role'    =>$post['role'],
            'description' =>$post['description'],
           );


           if(!empty($post['id'])){
            $id=decrypt($post['id']);
            $this->db->where('id',$id);
            $this->db->update('role',$Data);
           $this->session->set_flashdata('update', 'your data successfully Updated');
          }else{
              $this->session->set_flashdata('saved', 'your data successfully Saved');
              $this->db->insert('role',$Data);
          }
           redirect(base_url($this->controller.'/'));
       }
       else{
        redirect(base_url($this->controller.'/'));
       }
     }

    public function action(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getRole('',$id);
      $data['content'] = 'Role/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }

    
}

