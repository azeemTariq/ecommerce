<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$store= modules::run('Category/stores');
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];

}
$display =($view=='view') ? 'disabled' : '';
?>


<div class="container col-10 mt-4">
   <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Storez/Banners"><button class="btn btn-outline-success mb-1 justify-content-center">Banner List</button></a></div>
  <div class="card card-success">
    <div class="card-header">
        <h3 class="card-title">Add Banner </h3>
    </div>
              <!-- /.card-header -->
              <!-- form start -->
    <form role="form"  action="<?php echo base_url($controller.'/inserted')?>" method="POST" enctype="multipart/form-data" >
      <input type="hidden" name="id" value="<?= $id ?>">
      <div class="card-body">  

         <div class="form-group row">
           <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3"  > Store </label>
           <div class="col-sm-5">
            <select <?= $display ?> class="form-control select2" name="store_id" >
               <option value="" >Main</option>
                        <?php foreach ($store as $key => $value) { ?>
                               <option <?= (isset($row->store_id) && $row->store_id==$value['id']) ? 'selected' : '' ?>  value="<?= $value['id'] ?>"><?= $value['store_name'] ?></option>
                <?php } ?>
               </select>
           </div>
         </div>
        <div class="form-group row">
           <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3"  > Banner name </label>
           <div class="col-sm-5 col-xs-12">
             <input <?= $display ?> type="" class="form-control" id="colFormLabelSm" placeholder="Add name" name="bannes_name" required="" value="<?= $row->bannes_name ?? '' ?>">
             <?= form_error('bannes_name') ?>

           </div>
         </div>




          <div class="imagination">
             <div class="form-group row">
             <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
             <div class="col-sm-6">
               <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
                <div class="image col-md-12 col-lg-9 col-sm-12">
                  <?php $img= (isset($row->image) && !empty($row->image) ) ? base_url().'uploads/banners/'.$row->image :  base_url().'assets/clip.png'  ?>
                  <img id="imgView" width="100%" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
                </div>
             </div>
             </div>

                      <div class="form-group row ">
                     <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3">Banner Image </label>
                      <div>
                      
                       <div class="input-group " style="padding-left: 20px">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Upload</span>
                      </div>

                      <div class="custom-file m-l-10" >
                        <input <?= $display ?>  type="file" class="custom-file-input profile_image" name="profile_image" id="profile_image">
                        <label class="custom-file-label col-md-8" for="inputGroupFile01">Choose file</label></div></div>
                      </div>
                    </div>
                  </div>


        <div class="form-group row">
           <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" >Target Url </label>
           <div class="col-sm-5 col-xs-12">
            <select <?= $display ?> class="form-control select2" required name="target_url" >
               <option value="" >Select an option</option>
                        <?php foreach ($store as $key => $value) { ?>
                               <option <?= (isset($row->target_url) && $row->target_url==$value['id']) ? 'selected' : '' ?>  value="<?= $value['id'] ?>"><?= $value['store_name'] ?></option>
                <?php } ?>
               </select>
           </div>
        </div>        
        <div class="form-group row">
           <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" >description </label>
           <div class="col-sm-5 col-xs-12">
             <textarea <?= $display ?> class="form-control"  placeholder="Add description" name="description" height="200"><?= $row->description ?? '' ?></textarea>
             <?= form_error('description') ?>
           </div>
           </div>
                <div class="card-footer" <?= ($view=='view') ? 'style=display:none'  : ''; ?>>
                  <button type="submit" class="btn btn-outline-success" >Submit</button>
                </div>
                <!-- /.card-body -->

                
            </div>
              </form>
          </div>
            <!-- /.card -->




</div>



