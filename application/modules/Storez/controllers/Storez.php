<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storez extends MX_Controller {

  public function __construct() {
		parent::__construct();
    modules::run('Login/login');
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_Store');
	}
 public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= " Storez List";
      
	  	 $data['result']=$this->getStorez();
        $data['content'] = 'Storez/index.php';
        $this->load->view('Template/template',$data);
    }

    public function add(){
       $data['pageTitle']= " Add Storez";
	  	 $data['result']=$this->getStorez();
        $data['content'] = 'Storez/action';
        $this->load->view('Template/template',$data);
    }
    
    public function getStorez($id=''){

      $this->db->select("*");
      $this->db->from('store');
      $this->db->where('is_active',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
     
     

    }    
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'id',
            'store_name',
            'description'
           
        );

        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('store');
        $this->db->where('is_active<>',2);
  
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);


        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
           $id= encrypt($row['id']);

          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/action/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","store","Mdl_Store") href="#"><i class="far fa-trash-alt"></i></a>';
          }
            $row['action'].='</div>';  

           $row['status']='<div class="col-md-12  bt-switch">
               <input type="checkbox" data-size="mini"  class="changeStatus" value="'.encrypt($row['id']).'" data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
            </div>';



            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['store_name'];
            $nestedData[] = $row['description'];
            // $nestedData[] = $row['status'];
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
     public function command(){
      $post = $this->input->post();
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $_return=[];
       if(!empty($post)){ 
          $this->form_validation->set_rules('store_name', 'Store Name', 'required|max_length[25]');
          $this->form_validation->set_rules('description', 'Description', 'max_length[50]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }
          // dd($_FILES['profile_image']);
          $image=do_upload($_FILES['profile_image']?? '','uploads/store/','6000000000');
          $Data=array(
            'store_name'    =>$post['store_name'],
            'description' =>$post['description'],
          );
           if(!empty($image)){
            $Data['image'] =$image;
           }

          if(!empty($post['id'])){
            $id=decrypt($post['id']);
           $this->Mdl_Store->commandAct($Data,'store',['id'=>$id]);
           $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
            $this->Mdl_Store->commandAct($Data,'store');
          }
          echo json_encode($_return);
          exit();
        }
        redirect(base_url($this->controller.'/'));
    }
  public function action(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $data['pageTitle']= " Storez ".$this->uri->segment(3);
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getStorez($id);
      $data['content'] = 'Storez/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }

  public function upload(){

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']=true;
                // $config['max_size']             = 100;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;

                $this->load->library('upload', $config);
                $data=[];
                if ($this->upload->do_upload('profile_image'))
                {
                      
                     $data = $this->upload->data('file_name');

              }
              return $data;
     
  }
  public function Banners(){
        $data['pageTitle']= " banners List";
        $data['content'] = 'Storez/banner_list';
        $this->load->view('Template/template',$data);
  }
  public function addBanners(){
        $data['pageTitle']= " Add banners";
        $data['store']=$this->getStorez();
        $data['content'] = 'Storez/banner_view';
        $this->load->view('Template/template',$data);
  }
  public function banners_list(){
      $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'banners.id',
            'bannes_name',
            'description',
            'store.store_name',
            'st.store_name target'
           
        );
        $this->db->start_cache();
        $this->db->select("banners.*,store.store_name,st.store_name target");
        $this->db->from('banners');
        $this->db->join('store','banners.store_id =store.id','left');
        $this->db->join('store st','banners.target_url =st.id','left');
        $this->db->where('banners.is_active <>',2);
      
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // dd($this->db->last_query());
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
     
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/editBanner/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/editBanner/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","banners") href="#"><i class="far fa-trash-alt"></i></a>';
          }

            $row['action'].='</div>';  

            $is_active =($row['is_active']==1) ? 'checked' : '';

          $row['status']='<div class="col-md-12  bt-switch">
               <input type="checkbox" data-size="mini"   '.$is_active.' onchange=changeStatus(this,"'.$id.'","Storez","banners")  data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
            </div>';



            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['bannes_name'];
            $nestedData[] = $row['description'];
            $nestedData[] = '<i class=text-success>'.$row['target'].'</i>';
            $nestedData[] = ($row['store_name']) ?? '<span style="color:green;font-weight: bold">Main Slider</span>';
            $nestedData[] = $row['status'];
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
  }
  public function inserted(){

    $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $post=$this->input->post();
       if(!empty($post)){ 
          $this->form_validation->set_rules('description', 'Description', 'max_length[50]');
          if ($this->form_validation->run() == FALSE){
            $_return['result_set'][] = $post;
            $_return['content'] = 'category/action';
            return $this->load->view('Template/template',$_return);
          }
          // dd($_FILES['profile_image']);
          $image=do_upload($_FILES['profile_image']?? '3000000000000','uploads/banners/');
          $Data=array(
            'store_id'    =>$post['store_id'],
            'bannes_name' =>$post['bannes_name'],
            'target_url' =>$post['target_url'],
            'description' =>$post['description'],
          );
           if(!empty($image)){
            $Data['image'] =$image;
           }
          if(!empty($post['id'])){
            $id=decrypt($post['id']);
            $this->db->where('id',$id);
            $this->db->update('banners',$Data);
           $this->session->set_flashdata('update', 'your data successfully Updated');
          }else{
              $this->session->set_flashdata('saved', 'your data successfully Saved');
              $this->db->insert('banners',$Data);
          }

           redirect(base_url($this->controller.'/Banners'));
       }else{
        redirect(base_url($this->controller.'/Banners'));
       }

  }
    public function editBanner(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $data['pageTitle']= "Edit Banners ".$this->uri->segment(3);
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['store']= $this->getStorez();
      $data['result_set'] = $this->Mdl_Store->db_select('*','banners','id='.$id);
      $data['content'] = 'Storez/banner_view';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }
}
