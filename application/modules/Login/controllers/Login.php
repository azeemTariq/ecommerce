<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    // $this->load->model('Login/Mdl_Login');
  }
 public $controller = __CLASS__;
    public function index(){
       if($this->session->userdata('logged_in')){
          redirect(base_url('Dashboard/'));
       }
       else if($this->session->userdata('customer_logged_in')){
          redirect(base_url('my-dashboard'));
       }
        $data['content'] = 'index';
        $this->load->view('Login/template',$data);
    }

    public function signup(){
        $data['content'] = 'action';
        $this->load->view('Login/template',$data);
    }

    public function add(){
      $data['content'] = 'Login/action';
      $this->load->view('Login/template',$data);
    }

    public function authentication(){
      $post=$this->input->post();

       if($post){
        $username = $post['username'];
        $password = $post['password'];
        $record= $this->getUserInfo($username,$password);
           
          if(!empty($record)){
            if($record->is_customer==0)
            {
                $newdata = array( 
                 'username'  => $record->username, 
                 'name'      => $record->name, 
                 'id'        => $record->id,
                 'shop_name'      => $record->shop,
                 'role'      => 0,
                 'logged_in' => TRUE
              );
            }
            else
            {
            
              $fname= explode(' ', $record->name)[0] ?? '';
              $lname= explode(' ', $record->name)[1] ?? '';
              $newdata = array( 
               'id'  =>         $record->id, 
               'first_name'     => $fname, 
               'username'       => $record->username,
               'last_name'      => $lname, 
               'email'          => $record->email,
               'cell'           => $record->phone,
               'is_customer'    => 1,
               'customer_logged_in' => TRUE
            );  
            }  
            if($record->is_approved=='0'){
              if($record->roleId=='1'){
                $this->session->set_flashdata('error', 'Please Contact Your System Administration / Technical Department!');
              }
              else
              {  
              $this->session->set_flashdata('error', 'Please Verify Your Email !!!');
              }
              redirect(base_url('Login'));
            }
            else
            {
              $this->session->set_userdata($newdata);
              redirect(base_url('Login'));
            }
          }

          $this->session->set_flashdata('error', 'Invalid Username or Passowrd !');
        }

         redirect(base_url('Login/'));
     
    }

    public function getUserInfo($username,$password){
       $this->db->select('*');
       $this->db->from('login');
       $this->db->where(['username'=>$username,'password'=>$password]);
       return $this->db->get()->row();
    }
public function login(){
  if(!$this->session->userdata('logged_in')){
          redirect(base_url('Login'));
    }
}
public function customer_login_check(){
  if(!$this->session->userdata('customer_logged_in')){
          redirect(base_url('Login'));
  }
}

function logout()
{
    $newdata = array(
                'username'  =>'',
                'id' => '',
                'shop_name' => '',
                'logged_in' => FALSE,
               );

     $this->session->unset_userdata($newdata);
     $this->session->sess_destroy();

     redirect(base_url('Login/'));
}
        
       


  }
      