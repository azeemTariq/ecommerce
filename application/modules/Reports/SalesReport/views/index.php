<?php
   $controller = $this->uri->segment ( 1 );
   $view = $this->uri->segment ( 2 );
   
 
  $StartandEndSelected= rangeMonth(date("Y-m-d"));
?>
<div class="container">
  <div class="col-xs-12"><?php
  if ($this->session->flashdata ()) {
    foreach ( $this->session->flashdata () as $key => $value ) :
        if ($key == 'update' || $key == 'saved') {
           $alert_class = 'alert-success';
        } else {
           $alert_class = 'alert-danger';
        }
        ?>
 <div
    class="alert alert-block <?php echo $alert_class; ?>">
    <button type="button" class="close" data-dismiss="alert">
    <i class="icon-remove"></i>
    </button>
    <p>
       <strong> <i class="icon-ok"></i>
       <?php echo ucwords(strtolower($key)); ?> !
       </strong>
       <?php echo $value; ?>
    </p>
 </div>
 <?php endforeach;
    }?>

  </div>
</div>
         <div class="container white-box">
            <div class="content-wrap text-center">
               <div class="row">
                  <div class="col-md-8 offset-2">
                     <div class="white-box">
                        <h2 class="box-title  text-center"><?=$pageTitle;?></h2>
                        <!-- PAGE CONTENT BEGINS -->
                        <?php
                           $attributes = array (
                                 'class' => 'form-horizontal form-validate',
                                 'id' => 'register-form',
                                 'role' => 'form',
                                 'data-toggle' => 'validator', 
                                 'target'=>'_blank',
                                 'method'=>'get'
                           );
                           echo form_open ( base_url ( $parameter ), $attributes ); ?>



                         <div class="row mt-4">
                          <input type="hidden" id="doc_type_exist" value="0">
                           <div class="col-md-6">
                              <div class="form-group">
                                    <label
                                    class="control-label col-xs-12 col-sm-3 no-padding-right"
                                    for="date">From Date</label>
                                    <!-- <div class="col-xs-12 col-sm-9"> -->
                                          <div class="input-group">
                                                <input type="text" id="date" value="<?php echo $StartandEndSelected['start']; ?>" name="DateFrom"
                                                old-username=""
                                                placeholder="Date"
                                                class="form-control datepicker-autoclose"
                                                tabindex="5" data-error="Date is Required" required/> <span
                                                class="input-group-addon"><i class="icon-calender"></i></span>
                                          </div>
                                          <div class="help-block with-errors"></div>
                                        <!-- </div> -->
                                  </div>
                            </div>
                            <div class="col-md-6">
                                  <div class="form-group">
                                        <label class="control-label text-center" for="date">To Date</label>
                                        <!-- <div class="col-xs-12 col-sm-9"> -->
                                            <div class="input-group">
                                                <input type="text" id="date" value="<?php echo $StartandEndSelected['end']; ?>" name="DateTo"
                                                    old-username=""
                                                    placeholder="Date"
                                                    class="form-control datepicker-autoclose"
                                                    tabindex="6" data-error="Date is Required" required /> <span
                                                    class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                          <div class="help-block with-errors"></div>
                                        <!-- </div> -->
                                  </div>
                            </div>
                         </div>

                          <div class="row">
                            <div class="col-md-6">
                                  <div class="form-group">
                                        <label class="control-label text-center" for="date">Report Type</label>
                                        <!-- <div class="col-xs-12 col-sm-9"> -->
                                            <div class="input-group">
                                              <select class="status select2" name="status">
                                                    <option value="all">All</option>
                                                    <option value="1">New Orders</option>
                                                    <option value="2">Confirmed Orders</option>
                                                    <option value="3">Dispatched Orders</option>
                                                    <option value="4">Deliverd Orders</option>
                                                    <option value="5">Cancel Orders</option>
                                              </select>

                                              <span class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                          <div class="help-block with-errors"></div>
                                        <!-- </div> -->
                                  </div>
                            </div>
                             <div class="col-md-6">
                                  <div class="form-group">
                                        <label class="control-label text-center" for="date">Orders #</label>
                                        <!-- <div class="col-xs-12 col-sm-9"> -->
                                            <div class="input-group">
                                                <input type="text"  name="order_no" id="order_no"  
                                                    old-username=""
                                                    placeholder="e.g ES-00000000"
                                                    class="form-control"
                                                    tabindex="6" /> <span
                                                    class="input-group-addon"><i class="icon-calender"></i></span>
                                            </div>
                                          <div class="help-block with-errors"></div>
                                        <!-- </div> -->
                                  </div>
                            </div>
                         </div>

                    


     
                        <!-- PAGE CONTENT ENDS -->
                     <div class="clearfix form-actions">
                       
                           <button class="btn btn-info btn-validate mt-4 mr-4 float-right" type="submit">
                           <i class="icon-ok bigger-110"></i>Process
                           </button>
                      
                     </div>
                     <?php  echo form_close();  ?>
                     </div>
                     <!-- /white-box -->
                  </div>
               </div>
            </div>
         </div>
      </div>

   </div>
</div>
<style type="text/css">
    .single_Account button.actions-btn, .AccountRow button.actions-btn {
        display: none;
    }
</style>