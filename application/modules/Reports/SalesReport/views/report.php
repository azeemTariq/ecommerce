<!DOCTYPE html>
<html>
<head>
	<title>Report</title>


<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>links/logo.png">

  <!-- <title>Admin Panel | Dashboard</title> -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/font/ionic.min.css"> -->
   <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" />

</head>
<body>


	<table class="table table-bordered table-striped dataTable table_header">
		<thead>
			<tr>
				<th>Order #</th>
				<th>Customer Name</th>
				<th>Email</th>
				<th>Cell</th>
				<th>Net Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($result_set as $key => $value) { ?>
			<tr>
				<td><?= $value->code ?></td>
				<td><?= $value->first_name." ".$value->last_name ?></td>
				<td><?= $value->email ?></td>
				<td><?= $value->cell ?></td>
				<td><?= $value->totalAmount ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
</body>
</html>