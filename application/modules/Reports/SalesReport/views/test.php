<!DOCTYPE html>
<html>
<head>
  <title></title>

  <style type="text/css">
    h2 {
  text-align: center;
  padding: 20px 0;
}

.table-bordered {
  border: 1px solid #ddd !important;
}

table caption {
  padding: .5em 0;
}

@media screen and (max-width: 767px) {
  table caption {
    display: none;
  }
}

.p {
  text-align: center;
  padding-top: 140px;
  font-size: 14px;
}
#ExportExcel p{
  font-weight: normal;
      font-size: 21px;
    text-decoration: none;
}
.tr{
  background: #e4e4e4
}
</style>
<?php

$arrayList=array('1'=>'New Orders','Confirmed Orders','Dispatched Orders','Delivered Orders','Cancelled Orders');


?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Bootstrap 4 Responsive Table</title>
 <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>links/logo.png">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">

<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>

<script src="<?php echo base_url('public_html/assets/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<style>
    .bs-example{
      margin: 20px;
    }
</style>
</head>
<body>


<h2>Sales Report</h2>
 <div class="center col-md-12" id="ExportExcel">
           
             <p style="padding-left: %; text-align: center;">
                <span>Date From: <?= $getRow['DateFrom'] ?></span> <span> To: <?= $getRow['DateTo'] ?></span><br>
                <span>Report Type : <?= ($getRow['status']!='all') ? $arrayList[$getRow['status']]: ' All Orders ' ?> </span> </span>
                
           </p>
           <br>
        </div>



<div class="bs-example">
    <div class="table-responsive"> 
        <table class="table table-bordered">
            <thead>
               <tr class="tr">
        <th class="text-center">Order #</th>
        <th class="text-center">Customer Name</th>
        <th class="text-center">Email</th>
        <th class="text-center">Cell</th>
        <th class="text-center">Net Amount</th>
      </tr>
            </thead>
            <tbody>
               <?php foreach ($result_set as $key => $value) { ?>
                  <tr>
                    <td class="text-center"><?= $value->code ?></td>
                    <td class="text-center"><?= $value->first_name.' '.$value->last_name ?></td>
                    <td class="text-center"><?= $value->email ?></td>
                    <td class="text-center"><?= $value->cell ?></td>
                    <td class="text-right"><?= $value->totalAmount ?></td>
                  </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>    
</body>
</html>