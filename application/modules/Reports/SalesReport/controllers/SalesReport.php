<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalesReport extends MX_Controller {

  public function __construct() {
		parent::__construct();
     modules::run('Login/login');
		$this->load->helper('url', 'form');
		 $this->load->model('Mdl_SalesReport');
	}
 public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= "Sales Report";
        $data['content'] = 'index';
          $data['parameter'] = $this->controller.'/Report';
        $this->load->view('Template/template',$data);
    }
    public function Report(){
      $post=$this->input->get();
      if(!empty($post)){
      $data['result_set'] = $this->Mdl_SalesReport->getResult($post);
      $data['getRow']     = $post;
      if(!empty($data['result_set'])){
        $data ['content'] = $this->controller.'/report';
        $this->load->view ( 'test', $data );
      }else{
      $this->session->set_flashdata ( 'Alert', 'No Record Found!' );
      redirect(base_url().$this->controller.'/');
    }
  }
}

function sendMail()
{
    $config = Array(
  'protocol' => 'smtp',
  'smtp_host' => 'ssl://smtp.gmail.com',
  'smtp_port' => 465,
  'smtp_user' => 'email here', // change it to yours
  'smtp_pass' => 'password here', // change it to yours
  'mailtype' => 'html',
  'charset' => 'iso-8859-1',
  'wordwrap' => TRUE
);

        $message = '';
        $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->from('email here'); // change it to yours
      $this->email->to('email here');// change it to yours
      $this->email->subject('Resume from JobsBuddy for your Job posting');
      $this->email->attach( $this->load->view ( 'email.php' ));
      if($this->email->send())
     {
      echo 'Email sent.';
     }
     else
    {
     show_error($this->email->print_debugger());
    }

}

}
