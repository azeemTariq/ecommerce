<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_SalesReport extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
    }
    public  function getResult($val){
    	$this->db->select('*');
    	$this->db->from('orders');
    	$this->db->where('date between "'.$val['DateFrom'].'" AND  "'.$val['DateTo'].'"');
    	if(!empty($val['order_no'])){
    		  $this->db->where('orders.code',$val['order_no']);
    	}
    	if($val['status']!='all'){
				$this->db->where('orders.status',$val['status']);
    	}
    	return $this->db->get()->result();
    }
    
}