<script type="text/javascript">

	<?php if($this->uri->segment(2)=='vendor'){ ?>
	   datatablelist("users",null,1);
    <?php }else{ ?>
	   datatablelist("users");
	 <?php } ?>

	 function passwardActive(thiss){

	 	$('#password').prop('disabled',false);
	       if($(thiss).is(':checked')==false){
	          $('#password').prop('disabled',true);
	        }
	 }
  
function is_approved(obj,Id) { 
   if($(obj).bootstrapSwitch('state')==false){
    return false;
  }
    Swal.fire({
      title: 'Are you sure?',
      text: "You won a Approve This Item!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Approve it!'
    }).then((result) => {
      if (result.isConfirmed) {

        $.ajax({
             url:"<?= base_url('Users/Emailapproved') ?>",
             type:"post",
             data:{id:Id},
              success: function(data){
               
                Swal.fire(
                          'Approved !',
                          'Your file has been Approved.',
                          'success'
                        )
                $(obj).closest('td').html('<i class="text-success">Approved<i>');
              }

            });
           
      }else{
        $(obj).bootstrapSwitch('state',false,true);
      }
    });

}


$('.roleId').on('change',function(){
   if($(this).val()==1){
     $('#shop').prop('disabled',true);
   }else{
     $('#shop').prop('disabled',false);
   }
});
<?php if($this->uri->segment(3)!='view') { ?>
$('.roleId').trigger('change');
<?php } ?>
</script>