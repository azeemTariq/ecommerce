<div class="container-fluid"> 
    <div class="text-right mb-3"> 
            <a  target="_blank" href="<?php echo base_url() ?>Home/vendor"><button class="btn btn-outline-success mx-3 ">Add Vendor</button></a> 

     </div> 
</div>
<div class="container-fluid">
  <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
  
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="users" class="table table-bordered table-striped dataTable ">
          <thead>
            <tr>
              <th scope="col" class="no-search">S.No</th>
              <th scope="col">Vendor</th>
              <th scope="col">Username</th>
              <th scope="col">Email</th>
              <th scope="col">Phone</th>
              <th scope="col">Shop Name</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
         
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>