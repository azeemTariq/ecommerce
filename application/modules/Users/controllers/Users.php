<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->helper('url', 'form');
    modules::run('Login/login');
    $this->load->model('Mdl_Users');
  }
  public $controller = __CLASS__;
    public function index(){
      $data['pageTitle']= "Users List";
       // $data['result']=$this->getUsers();
        $data['content'] = 'Users/index';
        $this->load->view('Template/template',$data);
    }
    public function vendor(){
      $data['pageTitle']= "Vendor List";
       // $data['result']=$this->getUsers();
        $data['content'] = 'Users/vendor';
        $this->load->view('Template/template',$data);
    }


    public function add(){
      if($this->session->userdata('role')!=0){
         redirect(base_url($this->controller.'/'));
      }
       $data['pageTitle']= "users";
        $data['content'] = 'Users/action';
        $this->load->view('Template/template',$data);
    }


      
      public function getList() {


        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = isset($_GET['id']) ? array('','Name','username','email','phone','shop') :array('id','Name','username','phone','shop','role','is_approved','') ;
         

        $this->db->start_cache();
        $this->db->select("login.*,role.role");
        $this->db->from('login');
        $this->db->join('role','role.id=login.roleId','inner');
        if($this->session->userdata('role')==1){
            $this->db->where('login.roleId != 1 AND login.roleId != 0');
        }
        // if(isset($_GET['id'])){
        //   $this->db->where('login.roleId',2);
        // }else{
        //     $this->db->where('login.roleId<>',2);
        // }
        $this->db->where('login.is_active <>',2);
  
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();

        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $this->db->limit($requestData['length'], $requestData['start']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
             $id= encrypt($row['id']);
              $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
            $row['action'].=' <a class="green" target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
            if($this->session->userdata('role')==0 ||$row['roleId']!=1 ){
            $row['action'].=' <a class="green" target="_blank" href="' . base_url($controller . '/action/edit/' .$id) . '"><i class="far fa-edit"></i></a>';
            $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","login","Mdl_Users") href="#"><i class="far fa-trash-alt"></i></a>';
            }
            $row['action'].=' </div>'; 
            $is_active =($row['is_active']==1) ? 'checked="true" ' : '';

            $row['status']='<div class="col-md-12  bt-switch">
               <input type="checkbox" '.$is_active.' data-size="mini" onchange=changeStatus(this,"'.$id.'","Users","login")  data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
            </div>';


           $row['approve']= ($row['is_approved']==0) ? '
                  <div class="col-md-12  bt-switch">
                    <input type="checkbox" onchange=is_approved(this,"'.$id.'") data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
                  </div>' : '<i class="text-success">Approved<i>';    

            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['name'];
            $nestedData[] = $row['username'];
            $nestedData[] = $row['phone'];
            $nestedData[] = $row['shop'];
            if(!isset($_GET['id'])){
              $nestedData[] = '<i class="text-success">'.$row['role'].'<i>';
              $nestedData[] = $row['approve'];
              $nestedData[] = $row['status'];
            }

            
            $nestedData[] = $row['action'];
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }





    public function getUsers($id=''){
      if($id){

        $id= ' Where login.id = '.$id;
      }
      return   $this->db->query("
        SELECT
          *
        FROM
          login
        JOIN
          role ON login.roleId = role.id
          ".$id."
          ORDER BY login.id DESC" )->result_array();

    }   
    public function command(){
       $post=$this->input->post();
       $id =decrypt($post['inputid']);
       $this->load->library('form_validation');
       $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
      if($post){
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[50]');
        $this->form_validation->set_rules('username', 'Username', 'required|usernameOnly|is_unique_update[login.username@id@' .$id. ']|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[50]|min_length[12]');
        if(isset($post['password'])){
          $this->form_validation->set_rules('password', 'password','required|max_length[20]|min_length[8]');
        }
        
        if ($this->form_validation->run() == FALSE)
      {
           
          $data['result_set'][]=$post;
          $data['content'] = 'Users/action';
          return  $this->load->view('Template/template',$data);                    
      }

       $active =0;
       if(isset($post['is_active'])){ 
        $active = 1;
       }

      $Data=array(
        'name'      => $post['name'],
        'username'  => $post['username'],
        'email'     => $post['email'],
        'roleId'    => $post['roleId'],
        'phone'     => $post['phone'],
        'address'   => $post['address'],
        'add'       => $post['add'] ?? 0,
        'edit'      => $post['edit'] ?? 0,
        'delete'    => $post['delete'] ?? 0 ,
        'view'      => $post['view'] ?? 0,
        'is_active' => $active,
       );

       if(isset($post['shop'])){
        $Data['shop']= $post['shop'];
      }else{
         $Data['shop']= 'Head Office Admin';
      }
       if(isset($post['password'])){
         $Data['password'] = $post['password'];
       }
          if(!empty($post['inputid'])){
              $id=decrypt($post['inputid']);
                 $this->db->where('id',$id);
                  $this->db->update('login',$Data);
                 $this->session->set_flashdata('update', 'your data successfully Updated');
            }else{
              $this->db->insert('login',$Data);
              $this->session->set_flashdata('saved', 'your data successfully Saved');
            }
         redirect(base_url($this->controller.'/'));
         }   
         redirect(base_url($this->controller.'/'));
       } 
  public function action(){
     if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $id= decrypt($this->uri->segment(4));
      if(!empty($id)){
        $data['result_set'] = $this->getUsers($id);


          $data['content'] = 'Users/action';
        return $this->load->view('Template/template',$data);
      }
    }
    redirect(base_url($this->controller.'/'));
  } 
  public function Emailapproved(){
     $id=decrypt($this->input->post('id'));
     $data=array('is_approved'=>1);
     $this->Mdl_Users->commandAct($data,'login',['id'=>$id]);
  }
}
