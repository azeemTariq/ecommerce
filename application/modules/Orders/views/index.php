
<div class="container-fluid"> 
  <?php if($this->session->userdata('role')==0 || $this->session->userdata('role')==1){ ?>
    <div class="mb-3"> 
            <a href="<?php echo base_url() ?>Orders/"><button class="btn btn-info  ">New Order</button></a> 
            <a href="<?php echo base_url() ?>Orders/OrderConfirm"><button class="btn btn-warning  first">Confirmed</button></a> 
            <a href="<?php echo base_url() ?>Orders/OrderDispatch"><button class="btn btn-primary  ">Dispatch</button></a> 
            <a href="<?php echo base_url() ?>Orders/OrderDelivered"><button class="btn btn-success  ">Delivered</button></a> 
     </div> 
   <?php }else if($this->session->userdata('role')==2){?>
        <a href="<?php echo base_url() ?>Orders/OrderConfirm"><button class="btn btn-primary  first">New Order</button></a> 
        <a href="<?php echo base_url() ?>Orders/OrderDispatch"><button class="btn btn-success  ">Complete</button></a>
    <?php }else if($this->session->userdata('role')==3){?>
        <a href="<?php echo base_url() ?>Orders/OrderConfirm"><button class="btn btn-primary first ">New Order</button></a> 
        <a href="<?php echo base_url() ?>Orders/OrderDispatch"><button class="btn btn-info  ">Ready to Delivered</button></a> 
        <a href="<?php echo base_url() ?>Orders/OrderDelivered"><button class="btn btn-success  ">Complete</button></a>
    <?php }?>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="OrderList" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col">Order #</th>
              <th scope="col">Order Date</th>
              <!-- <th scope="col">Order Time</th> -->
              <th scope="col">Description</th>
              <th scope="col" class="no-search">status</th>
              <?php  if($this->session->userdata('role')!='2'){ ?>
                  <th scope="col">no of items</th>
              <th scope="col">Amount</th>
               <th scope="col" class="no-search">Change Status</th>
              <th scope="col" class="no-search">Print</th>
               <?php  }  ?>
              <th scope="col" class="no-search">Payment</th>
              <th scope="col" class="no-search">Pay Date</th>
              <th scope="col" class="no-search">Actions</th>

            </tr>
          </thead>
          
      </table>
      </div>
    </div>
  </div>
</div>





<div id="classModal" class="modal fade bs-example-modal-lg" tabindex="-1"
    role="dialog" aria-labelledby="classInfo" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> X </button>
          </div>
          <div class="modal-body" style="overflow-x: scroll;">
            <h4>Order Receipt</h4>
            <table id="classTable" class="table table-bordered">
              <thead>
                <tr>
                    <th>Item Code</th>
                    <th>Item Name</th>
                    <th>Color / Size</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Total </th>
                    <th>Shop Name </th>
                    <th>Contact #</th>
                    <th> vendor status</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody id="detail">
                
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">
              Close
            </button> -->
          </div>
        </div>
      </div>
    </div>



    <div id="classModal2" class="modal fade bs-example-modal-sm" tabindex="-1"
    role="dialog" aria-labelledby="classInfo" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> X </button>
          </div>
          <input type="hidden" id="tr-row" value="">
          <input type="hidden" id="code" value="">
          <div class="modal-body">
            <h4>Payment</h4>
            <select id="Payment_status" class="select2">
               <option value="1">Yes</option>
               <option value="2">No</option>
            </select>
          
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="payButton">
              Save
            </button>
          </div>
        </div>
      </div>
    </div>


   