<script type="text/javascript">


	   datatablelist("OrderList",null,'<?= $this->uri->segment(2) ?>','<?= ($this->session->userdata('role')==2) ? 'vendorOrders' : 'getList' ?>');


function view_detail(id) {

	 $.ajax({
        url: '<?= base_url("Orders/view_detail") ?>',
        type:'post',
        data:{id:id},  
      success:function(data) {

          var data = JSON.parse(data);
          $('#detail').empty();
          $('#detail').append(data);
          $('#classModal').modal('show');

      		}
  		});

   
}



function changeOrder(obj,id,status){

  if($(obj).bootstrapSwitch('state')==false){
    return false;
  }
    Swal.fire({
      title: 'Are you sure?',
      text: "You won a Update Status Of this Order!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Update it!'
    }).then((result) => {
      if (result.isConfirmed) {

        $.ajax({
             url:"<?= base_url('Orders/changeOrder') ?>",
             type:"post",
             data:{id:id,status:status},
              success: function(data){
               
                Swal.fire(
                          'Update !',
                          'Order has been Updated.',
                          'success'
                        )
                $(obj).closest('tr').remove();
              }

            });
           
      }else{
        // $(obj).prop('checked',false);
        $(obj).bootstrapSwitch('state',false,true);
      }
    });

}



$(document).ajaxStop(function() {
   $('.payNow').on('click',function(){
      $('#tr-row').val($(this).attr('data-id'));
      $('#code').val($(this).closest('tr').find('td:eq(0)').html());
      $('#classModal2').modal('show');
  });
   // status update
$('#payButton').on('click',function(){
  var tr = $('#tr-row').val();
  if($('#Payment_status').val()!=1){
    $('#classModal2').modal('hide');
    return false;
  }
  $.ajax({
       url:'<?= base_url('Orders/updatePaymentType') ?>',
       type:'post',
       data:{'payment_status':1,'code':$('#code').val()},
       success:function(res){
        res = JSON.parse(res);
        $('tr:eq('+tr+')').find('td').eq(8).html(res.payment_status);
        $('tr:eq('+tr+')').find('td').eq(9).html(res.payment_date);
        $('#classModal2').modal('hide');
        toastr.success("Payment Done !", 'Saved !',{timeOut: 3000});
       }
  });
});

});


</script>