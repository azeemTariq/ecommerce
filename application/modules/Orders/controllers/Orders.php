<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MX_Controller {

  public function __construct() {
		parent::__construct();
    modules::run('Login/login');
	$this->load->model('Mdl_Orders');
	}
    public $controller = __CLASS__;
	 public function index(){
	  	$data['pageTitle'] = 'Order Received';
        $data['content'] = ( $this->session->userdata('role')==2) ?  'Orders/vendor' : 'Orders/index' ;
        $this->load->view('Template/template',$data);
    }
	 public function OrderConfirm(){
	  	$data['pageTitle'] = 'Order Confirmed';
        $data['content'] = ( $this->session->userdata('role')==2) ?  'Orders/vendor' : 'Orders/index' ;
        $this->load->view('Template/template',$data);
    }
    public function OrderDispatch(){
	  	$data['pageTitle'] = 'Order Dispatch';
        $data['content'] = ( $this->session->userdata('role')==2) ?  'Orders/vendor' : 'Orders/index' ;
        $this->load->view('Template/template',$data);
    }
    public function OrderDelivered(){
	  	$data['pageTitle'] = 'Order Delivered';
        $data['content'] = 'Orders/index.php';
        $this->load->view('Template/template',$data);
    }
    public function getList() {
        $statusTitle = array(1=>'New Order',2=>'Confirm',3=>'Dispatched',4=>'Delivered');
        $status = 1;
        if($this->session->userdata('role')==2 || $this->session->userdata('role')==3){
           $status = 2;
        }
        


        if($_GET['id']=='OrderConfirm'){
        $status = 2;
        }else if($_GET['id']=='OrderDispatch'){
        $status = 3;
        }else if($_GET['id']=='OrderDelivered'){
        $status = 4;
        }
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array( 
            'orders.code',
            'date',
            // 'time',
            'massage',
            '',
            'items',
            'totalAmount',
            'status'
          );
        $this->db->start_cache();

        if($this->session->userdata('role')==2){
            $this->db->select("orders.*,GROUP_CONCAT(product.created_by)");
            $this->db->from('orders,order_detail,product');
             $this->db->where('orders.id = order_detail.OrderId
             AND order_detail.ItemId = product.id
             AND product.created_by = '.$this->session->userdata('id'));
             $this->db->group_by('orders.id');
        }else{
                $this->db->select("*");
                $this->db->from('orders');
        }
        
        $this->db->where('orders.status',$status);
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);

        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);

          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs"> ';
        

          if(($this->session->userdata('role')==1 && permission('delete')) || $this->session->userdata('role')==0){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","orders") href="#"><i class="far fa-trash-alt"></i></a>';
            }

      

            
            $row['action'].='</div>';  

            $row['print'] = '<a class="green" href="javascript:openPrintWindow(\'' . base_url($this->controller . '/detail/' .$id). '\',\'Inventory_RequisitionMaster\',\'width=820,height=600\')"><i class="fa fa-print mt-1"></i></a>';

          

            $statusList='
             <div class="col-md-12  bt-switch">
                    <input type="checkbox" onchange=changeOrder(this,"'.$id.'",'.$status.') data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
                  </div>';

            $row['payment'] = '<button type="button" class="btn btn-info btn-​lg payNow" data-id="'.($key+1).'">Pay Now</button>';
            if($row['payment_status']==1){
                $row['payment'] = '<i class="text-success fa fa-check"><b> Paid</b><i>';
            }
            
            $nestedData = array();
            $nestedData[] = $row['code'];
            $nestedData[] = $row['date'];
            // $nestedData[] = $row['time'];
            $nestedData[] = $row['massage'];
         
            $title= $statusTitle[$row['status']];
            
            if(($this->session->userdata('role')==2 || $this->session->userdata('role')==3) && $status==2 ) {
                   $title = 'New Order';
            }
        
            $nestedData[] = '<i class="text-success"><b>'.$title.'</b><i>';

            if($this->session->userdata('role')!='2'){
                   $nestedData[] = $row['items'];
            $nestedData[] = $row['totalAmount'];
                  $nestedData[] = ($row['status']!=4) ? $statusList : '<i class="text-success"><b>Process Complete</b><i>';
                 $nestedData[] = $row['print'];
            }
           $nestedData[] = $row['payment'];
           $nestedData[] = ($row['payment_date']) ? $row['payment_date'] : '-';
           $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
    public function detail(){
        $id = decrypt($this->uri->segment(3));
        $data['master'] = $this->Mdl_Orders->db_select_single_row('*','orders',['id'=>$id]);
        if(count($data['master'])){
            $data['detail'] = $this->Mdl_Orders->db_select('order_detail.*,product.product_name,product.code,price','order_detail,product',' 
                product.id=order_detail.ItemId AND 
                OrderId = "'.$id.'" ');
            $this->load->view('print',$data);
        }
        
    }    
    public function updatePaymentType(){
        $post = $this->input->post();
        $set = array('payment_status'=>1,'payment_date'=>date('Y-m-d H:i:s'));
        $this->Mdl_Orders->commandAct($set,'orders',['code'=>$post['code']]);
        $set['payment_status'] = '<i class="text-success fa fa-check"><b> Paid</b><i>';
        echo json_encode($set);
    }
    public function view_detail(){
        $id = decrypt($this->input->post('id'));
        $user_id = $this->session->userdata('id');
        $where = '';

        if($this->session->userdata('role')==2){
            $where=' AND login.id = '.$user_id;
        }

            $json_data = $this->Mdl_Orders->db_select('order_detail.*,orders.status or_status,product.product_name,product.code p_code,price,login.shop,phone,login.id uersId','order_detail,product,login,orders',' 
                login.id = product.created_by AND 
                product.id=order_detail.ItemId AND 
                orders.id=order_detail.OrderId AND 
                OrderId = "'.$id.'" '.$where);
        
        $array ='';
        $statusTitle = array(0=>'Pending',1=>'Reay to Delivered',2=>'Cancel');
        foreach ($json_data as $key => $value) {
           $select = '<select onchange=changeStatus(this,"'.encrypt($value->OrderDetailId).'","Orders","order_detail","update","Status","OrderDetailId","_select")  ><option '.($value->Status==0 ? 'selected' : '' ).' value="0">Pending</option><option '.($value->Status==1 ? 'selected' : '' ).' value="1">Ready to Deliverd</option><option '.($value->Status==2 ? 'selected' : '' ).' value="2">Cancel</option><select>';

           $td ='<td class="text-danger"> vendor confirmation Required</td> ';
           if($value->Status>0 AND $value->or_status<4){
            $td = '<td>'.(($this->session->userdata('role')!=2) ? '<label class="switch">
                            <input type="checkbox" '.(($value->is_done==1) ? 'checked' : '').'  onchange=changeStatus(this,"'.encrypt($value->OrderDetailId).'","Orders","order_detail","update","is_done","OrderDetailId") >
                            <span class="slider round"></span>
                            </label>' : ' Vendor' ).'
                            </td>';
           }else if($value->or_status==4){
            $td ='<td class="text-primary"><b> Completed </b></td> ';
           }
           $array .='<tr>
                        <td>'.($value->p_code).'</td>
                        <td>'.($value->product_name).'</td>
                        <td>'.($value->suggestion).'</td>
                        <td>'.($value->ItemQty).'</td>
                        <td>'.($value->price).'</td>
                        <td>'.($value->ItemQty*$value->price).'</td>

                        <td><a target="_blank" href="'.base_url("Users/action/view/".encrypt($value->uersId)).' ">'.($value->shop).'</a></td>
                        <td>'.($value->phone).'</td>
                        <td>'.(($this->session->userdata('roleId')==2 && $value->Status!=0) ? $select : '<i class="text-success"><b>'.$statusTitle[$value->Status].'</b></i>').'</td>
                        '.$td.'
                    </tr>';
        }
        echo json_encode($array);    
    }
    function changeOrder(){
        $post=$this->input->post();
        $data=array('status'=>$post['status']+1);
        $this->Mdl_Orders->commandAct($data,'orders',['id'=>decrypt($post['id'])]);
    }


     public function vendorOrders() {
  
       
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array( 
            'orders.code',
            'product.code pcode',
            'product.product_name',
            'date',
            'time',
            '',
            'order_detail.ItemQty',
            'product.price',
            '(product.price*ItemQty)'
          );
        $this->db->start_cache();

            $this->db->select("orders.status or_status,orders.code,orders.id,orders.date,time,ItemQty,product_name,product_name,product.code pcode,(product.price*ItemQty) amount,product.price,order_detail.Status,OrderDetailId,is_done");
            $this->db->from('orders,order_detail,product');
             $this->db->where('orders.id = order_detail.OrderId
             AND order_detail.ItemId = product.id
             AND product.created_by = '.$this->session->userdata('id'));
            $this->db->where('orders.status <> ',1);
             if($_GET['id']=='OrderConfirm'){
                $this->db->where('order_detail.is_done = ',0);
            }else{
                $this->db->where('order_detail.is_done = ',1);
            }


        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);

        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // dd($this->db->last_query);
        $data = array();
        $statusTitle = array(0=>'Pending',1=>'Reay to Delivered',2=>'Cancel');
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
          $select= '<i class="text-success"><b>'.$statusTitle[$row['Status']].'</b></i>';
          if($row['is_done']==0 && $row['or_status']==2){
            $select = '<select class="select2 optional" onchange=changeStatus(this,"'.encrypt($row['OrderDetailId']).'","Orders","order_detail","update","Status","OrderDetailId","_select")  ><option '.($row['Status']==0 ? 'selected' : '' ).' value="0">Pending</option><option '.($row['Status']==1 ? 'selected' : '' ).' value="1">Ready to Deliverd</option><option '.($row['Status']==2 ? 'selected' : '' ).' value="2">Cancel</option><select>';
          }
           
         

            $nestedData = array();
            $nestedData[] = $row['code'];
            $nestedData[] = $row['pcode'];
            $nestedData[] = $row['product_name'];
            $nestedData[] = $row['date'];
            $nestedData[] = $row['time'];
            $nestedData[] = ($_GET['id']=='OrderConfirm') ? $select : ' <i class="text-success"> Completed </i>' ;
            $nestedData[] = $row['ItemQty'];
            $nestedData[] = $row['price'];
            $nestedData[] = $row['amount'];
         
           
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


}