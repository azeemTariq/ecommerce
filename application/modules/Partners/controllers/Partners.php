<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends MX_Controller {

  public function __construct() {
		parent::__construct();
    modules::run('Login/login');
		$this->load->helper('url', 'form');
		$this->load->model('Mdl_Partners');
	}
 public $controller = __CLASS__;
	  public function index(){
        $data['pageTitle']= " Partner List";
        $data['content'] = 'index';
        $this->load->view('Template/template',$data);
    }

    public function add(){
       $data['pageTitle']= " Add Partner";
	  	 $data['result']=$this->getBrands();
        $data['content'] = 'action';
        $this->load->view('Template/template',$data);
    }
    
 
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'id',
            'partner_name'
        );

        $this->db->start_cache();
        $this->db->select("*");
        $this->db->from('partners');
        $this->db->where('is_active<>',2);
  
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);


        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // getLastQuery(false,'test');
        $data = array();
        foreach ($query as $key => $row) {
           $id= encrypt($row['id']);

          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
            
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/action/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","partners","Mdl_Partners") href="#"><i class="far fa-trash-alt"></i></a>';
          }
            $row['action'].='</div>';  
            $is_active =($row['is_active']==1) ? 'checked' : '';
           $row['status']='<div class="col-md-12  bt-switch">
               <input type="checkbox" data-size="mini" '.$is_active.' onchange=changeStatus(this,"'.$id.'","Partners","partners") data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
            </div>';



            $nestedData = array();
            $nestedData[] =  '<img src="'.base_url('uploads/brands/'.$row['image']).'"  width="86px"/>';
            $nestedData[] = $row['partner_name'];
            $nestedData[] = $row['status'];
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
     public function command(){
      $post = $this->input->post();
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $_return=[];
       if(!empty($post)){ 
          $this->form_validation->set_rules('partner_name', 'Partner Name', 'required|max_length[25]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }
          // dd($_FILES['profile_image']);
          $image=do_upload($_FILES['profile_image']?? '','uploads/brands/','6000000000');
          $Data=array(
            'partner_name'    =>$post['partner_name'],
          );
           if(!empty($image)){
            $Data['image'] =$image;
           }

          if(!empty($post['id'])){
            $id=decrypt($post['id']);
           $this->Mdl_Partners->commandAct($Data,'partners',['id'=>$id]);
           $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
            $this->Mdl_Partners->commandAct($Data,'partners');
          }
          echo json_encode($_return);
          exit();
        }
        redirect(base_url($this->controller.'/'));
    }
  public function action(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $data['pageTitle']= " Partners ".$this->uri->segment(3);
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['result_set'] = $this->getBrands($id);
      $data['content'] = 'Partners/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }

  public function upload(){

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['overwrite']=true;
                // $config['max_size']             = 100;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;

                $this->load->library('upload', $config);
                $data=[];
                if ($this->upload->do_upload('profile_image'))
                {
                      
                     $data = $this->upload->data('file_name');

              }
              return $data;
     
  }
  public function Banners(){
        $data['pageTitle']= " banners List";
        $data['content'] = 'Storez/banner_list';
        $this->load->view('Template/template',$data);
  }
  public function addBanners(){
        $data['pageTitle']= " Add banners";
        $data['store']=$this->getBrands();
        $data['content'] = 'Storez/banner_view';
        $this->load->view('Template/template',$data);
  }
  public function banners_list(){
      $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array(
            'banners.id',
            'bannes_name',
            'description',
            'store_name'
           
        );
        $this->db->start_cache();
        $this->db->select("banners.*,store_name");
        $this->db->from('banners');
        $this->db->join('store','banners.store_id =store.id','left');
        $this->db->where('banners.is_active <>',2);
      
        $i = 0;
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                $this->db->like($value, $requestData['columns'][$i]['search']['value']);
            }
            $i++;
        }
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);
        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        // dd($this->db->last_query());
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);
     
          $row['action'] = '<div class="visible-md visible-lg visible-sm visible-xs">';
          if(permission('view')){ 
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/editBanner/view/' . $id) . '"><i class="far fa-eye"></i></a>';
          }
          if(permission('edit')){ 
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/editBanner/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
           }
          if(permission('delete')){ 
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","banners") href="#"><i class="far fa-trash-alt"></i></a>';
          }

            $row['action'].='</div>';  

            $is_active =($row['is_active']==1) ? 'checked' : '';

          $row['status']='<div class="col-md-12  bt-switch">
               <input type="checkbox" data-size="mini"   '.$is_active.' onchange=changeStatus(this,"'.$id.'","Storez","banners")  data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
            </div>';



            $nestedData = array();
            $nestedData[] = ($key+1);
            $nestedData[] = $row['bannes_name'];
            $nestedData[] = $row['description'];
            $nestedData[] = ($row['store_name']) ?? '<span style="color:green;font-weight: bold">Main Slider</span>';
            $nestedData[] = $row['status'];
             $nestedData[] = $row['action'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
  }

    public function editBanner(){
    if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $data['pageTitle']= "Edit Banners ".$this->uri->segment(3);
    $id= decrypt($this->uri->segment(4));
    if(!empty($id)){
      $data['store']= $this->getBrands();
      $data['result_set'] = $this->Mdl_Partners->db_select('*','banners','id='.$id);
      $data['content'] = 'Partners/action';
      return $this->load->view('Template/template',$data);
    }
  }
    redirect(base_url($this->controller.'/'));
  }

    public function getBrands($id=''){

      $this->db->select("*");
      $this->db->from('partners');
      $this->db->where('is_active',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
     
     

    }    
}
