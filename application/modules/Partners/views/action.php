<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];

}
$display =($view=='view') ? 'disabled' : '';
?>


<div class="container col-10 mt-4">
   <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Partners/"><button class="btn btn-outline-success mb-1 justify-content-center">Partner  List</button></a>
    </div>
  <div class="card card-success_ card-outline-primary">
    <div class="card-header">
        <h4 class="card-title text-success"><b>Create Partner</b></h4>

    </div>
              <!-- /.card-header -->
              <!-- form start -->
    <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitIRM" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
      <input type="hidden" name="id" id="hidden" value="<?= $id ?>">
      <div class="card-body">  
        






           <div class="form-group row">
           <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
           <div class="col-sm-6">
             <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
              <div class="image col-md-12 col-lg-9 col-sm-12">
                <?php $img= (isset($row['image']) && !empty($row['image']) ) ? base_url().'uploads/Partners/'.$row['image'] :  base_url().'assets/clip.png'  ?>
                <img id="imgView" width="200" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
              </div>
           </div>
           </div>

                      <div class="form-group row ">
                     <label for="profile_image" class="col-md-3 col-sm-12 col-lg-3">file or image                     </label>
                      <div>
                      
                       <div class="input-group " style="padding-left: 17px">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Upload</span>
                      </div>
                      <div class="custom-file">
                        <input <?= $display ?> type="file" class="custom-file-input profile_image" name="profile_image" id="profile_image">
                        <label class="custom-file-label col-md-8" for="inputGroupFile01">Choose file</label></div></div>
                      </div>
                    </div>


 
          <div class="form-group row">
                       <label for="partner_name" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Partner Name  </label>
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                      <input <?= $display ?> type="" class="form-control" id="partner_name"  placeholder="Enter Partner Name" name="partner_name" required value="<?= $row['partner_name'] ?? '' ?>">
                      <div class="error"><?php echo form_error('partner_name'); ?></div>
                    </div>

                  </div>
                <div class="card-footer" <?= ($view=='view') ? 'style=display:none'  : ''; ?>>
                  <button type="submit" class="btn btn-outline-success" >Submit</button>
                </div>
                <!-- /.card-body -->

                
            </div>
              </form>
          </div>
            <!-- /.card -->




</div>



