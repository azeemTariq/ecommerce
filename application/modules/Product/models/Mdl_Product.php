<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_Product extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
    }
      public function getStoreProduct($id=null){

          $where = '';
          $result = array('Approved'=>array(),'Pending'=>array());
          if($id){
             $where = ' AND itm.created_by = '.$id;
          }

        
          $this->db->select('SUM(if(itm.is_active=1,1,0)) total,category.category_name');
          $this->db->from('category');
          $this->db->join('product itm','itm.cat_id =category.id AND itm.is_active <> 2'.$where,'left');

   

          $this->db->group_by('category.id');
          $this->db->order_by('itm.id desc');

          $result['Approved'] = $this->db->get()->result_array();

          $this->db->select('SUM(if(itm.is_active=0,1,0)) total,category.category_name');
         $this->db->from('category');
          
          $this->db->join('product itm','itm.cat_id =category.id AND  itm.is_active <> 2'.$where,'left');

          $this->db->group_by('category.id');
          $this->db->order_by('itm.id desc');

          $result['Pending'] = $this->db->get()->result_array();

          return $result;

      }
      public function OrderStatusByCustomer(){
        $arr = array(
          'orders.id = order_detail.OrderId' =>Null,
          'orders.cust_id = login.id' =>Null
        );
          return $this->db_select('orders.code,orders.status,login.name,count(*) record','orders,order_detail,login',$arr,null,null,'orders.cust_id,orders.status');
      }
        public function ReviewsByCustomer(){
        $arr = array(
          'product_reviews.customer_id = login.id' =>Null,
          'product_reviews.is_active' =>1
        );
          return $this->db_select('product_reviews.*,login.name','product_reviews,login',$arr,null,null,'product_reviews.customer_id,product_reviews.product_id');
      }
}