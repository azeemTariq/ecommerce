<?php
	$controller = $this->uri->segment(1);
	$id = $this->uri->segment(4);
  $view = $this->uri->segment(3);
	$row=[];
	if(isset($result_set[0])){
	  $row=$result_set[0];
	}
?>


<script src="<?= base_url('links/js/tini.min.js') ?>" ></script>
<script type="text/javascript" src="<?= base_url('links/js/image-uploader.min.js') ?>"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script> -->
<script type="text/javascript">
var Screen= <?php echo isset($result_set[0]) ?  json_encode($result_set[0]): '0' ?>;

function CallTable(){
   datatablelist("product");

}
  CallTable();




function submitPR(){
    $('.submitIRM').submit(function (e) {
       if($('.submitIRM')[0].checkValidity(e)){

       if($('.uploaded-image img').length==0){
         toastr.error("Please Select atleast 1 Image!", 'Alert !',{timeOut: 8000});
         return false;
       }else if($('.uploaded-image img').length>10){
          toastr.error("Sorry! Image Limit is (5) OR Less  ", 'Alert !',{timeOut: 8000});
          return false;
       }




      var url = "<?php echo base_url($controller.'/command'); ?>";   
       $('button[type="submit"]').prop('disabled',true); 	
         $.ajax({
             url:url,
             type:"post",
             data:new FormData(this),
             processData:false,
             crossOrigin:false,
             contentType:false,
             cache:false,
             async:false,
              success: function(data){
                     var data = JSON.parse(data);
                        if(data.status=='Error'){
                            toastr.error(data.msg, 'Alert !',{timeOut: 8000});
                           $('button[type="submit"]').prop('disabled',false);
                        }else if(data.status=='success'){
                      toastr.success(data.msg, 'Success !');
                      if($('#hidden_id').val()){
                          setTimeout(function () {
                              window.location.href = '<?= base_url($controller) ?>';
                          },3000);
                      }else{
                             $('#product_name').val('');
                             $('#price').val('');
                             $('#expire').val('');
                             $('#description').val('');
                              //clear image src
                             $('#removearray').val('');
                             $('#array').val('');
                             $('.iui-close').trigger('click');
                             $('button[type="submit"]').prop('disabled',false);
                    }
                }
           }
         });

  }e.preventDefault(); 
});
}
submitPR();

$(".submitIRM").validate({
    rules: {
        store_id: {
            required:true,
        },
        cat_id: {
            required:true,
           
        }, 
        product_name: {
            required:true,
           
        },
         price: {
            required:true,
           
        }
       
    },
    messages:{
 
    }
});




let preloaded = [];
var sort='';
var a=0;
var setup = [];
if(Screen['id']>0){
  for (var i=1;i<=10;i++) {
     if(Screen['image'+i]){
      setup[a] = '<?= base_url('') ?>uploads/product/'+Screen['image'+i];
      preloaded[a] = {id: a, src: '<?= base_url('') ?>uploads/product/'+Screen['image'+i]};
      a++;
     }
  }
}

$('#array').val(setup.join(", "));
$('.input-images-1').imageUploader({
    preloaded: preloaded,
    imagesInputName: 'photos',
    preloadedInputName: 'old'
});

removArray=[];
$('.iui-close').on('click',function(){
    thisParent=$(this).closest('.uploaded-image');
    removeImg= thisParent.find('img').attr('src');
    input=$('#array').val();
    old = input.split(', ');
    removArray.push(removeImg);
    removeA(old, removeImg);
    $('#array').val(old.join(", "));
    $('#removearray').val(removArray.join(", "));

    
});

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}



<?php if($view=='view'){ ?>
  $('input[name="photos[]"]').prop('disabled',true);
  $('.delete-image').hide();
<?php }  ?>
// var ary = ['three', 'seven', 'eleven'];
// removeA(ary, 'seven');





function is_approved(obj,Id) { 

  if($(obj).bootstrapSwitch('state')==false){
    return false;
  }
    Swal.fire({
      title: 'Are you sure?',
      text: "You won a Approve This Item!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Approve it!'
    }).then((result) => {
      if (result.isConfirmed) {

        $.ajax({
             url:"<?= base_url('Product/approvedProduct') ?>",
             type:"post",
             data:{id:Id},
              success: function(data){
               
                Swal.fire(
                          'Approved !',
                          'Your file has been Approved.',
                          'success'
                        )
                $(obj).closest('td').html('<i class="text-success">Approved<i>');
              }

            });
           
      }else{
        // $(obj).prop('checked',false);
        $(obj).bootstrapSwitch('state',false,true);
      }
    });

}



 $(".bt-switch input[type='checkbox'], .bt-switch input[type='radio']").bootstrapSwitch();



$(document).ready(function () {
            $('#description').wysihtml5({
     "font-styles": false, //Font styling, e.g. h1, h2, etc.
     "emphasis": true, //Italics, bold, etc.
     "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers.
     "html": false, //Button which allows you to edit the generated HTML.
     "link": false, //Button to insert a link.
     "image": false, //Button to insert an image.
     "color": true, //Button to change color of font
     "quote":false,
});



$('.wysihtml5-toolbar > li').slice(-4).remove();
        });



$(document).ready(function () {
            $('#long_description').wysihtml5({
     "font-styles": false, //Font styling, e.g. h1, h2, etc.
     "emphasis": true, //Italics, bold, etc.
     "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers.
     "html": false, //Button which allows you to edit the generated HTML.
     "link": false, //Button to insert a link.
     "image": false, //Button to insert an image.
     "color": true, //Button to change color of font
     "quote":false,
});
$('.wysihtml5-toolbar > li').slice(-4).remove();

});



</script>