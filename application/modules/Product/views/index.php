
<div class="container-fluid"> 
  <?php if(permission('add')){ ?>
    <div class="text-right mb-3"> 
            <a href="<?php echo base_url() ?>Product/add"><button class="btn btn-outline-success mx-3 ">Add product</button></a> 
     </div> 
   <?php }  ?>
</div>
<div class="container-fluid">
   <?php
                if($this->session->flashdata()){
                    foreach($this->session->flashdata() as $key => $value):
                    if($key == 'update' || $key == 'saved'){
                        $alert_class = 'alert-success';
                    }else{
                        $alert_class = 'alert-danger';
                    }
                ?>
                <div class="alert alert-block <?php echo $alert_class; ?>">
                    <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                    </button>
                    <p>
                        <strong>
                            <i class="icon-ok"></i>
                            <?php echo ucwords(strtolower($key)); ?> !
                        </strong>
                        <?php echo $value; ?>
                    </p>    
                </div>
                <?php
                    endforeach;
                }?>
  <div class="row">
    <div class="col-12 white-box">
     <div class="table-responsive">
        <table id="product" class="table table-bordered table-striped dataTable table_header">
          <thead>
            <tr>
              <th scope="col">Product code</th>
              <th scope="col">Product name</th>
              <th scope="col">description</th> 
              <th scope="col">Retail Price</th> 
              <th scope="col">Current Price</th>
       
              <th scope="col">Sub Category</th>
              <th scope="col">Category</th>
              <th scope="col" class="no-search">Active Item</th>
              <th scope="col" class="no-search">Actions</th>
              <th scope="col" class="no-search">Report</th>

            </tr>
          </thead>
          
      </table>
      </div>
    </div>
  </div>
</div>


   