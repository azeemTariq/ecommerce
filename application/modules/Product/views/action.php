<?php
$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$Category = modules::run('Category/category');
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];

}
$display =($view=='view') ? 'disabled' : '';
?>

<div class="container-fluid col-10 mt-4">

           <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Product/"><button class="btn btn-success mb-4 justify-content-center">show products</button></a></div>
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add product category</h3>
                

              </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitIRM" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="card-body">



                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Store  </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                       
                         <select <?= $display ?> class="form-control select2" onchange="getSubCategory()" name="store_id" id="store" required>
                        <option value="">Select Store</option>
                         
                       </select>
                       <label for="store" class="error" ></label>
                       </div>
                     </div>

                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Category  </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                      <select <?= $display ?> class="form-control select2" name="cat_id" id="category" required>
                        <option value="">Select category</option>
                      </select>

                       </div>
                     </div>
                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Sub Category  </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                      <select <?= $display ?> class="form-control select2" name="subCatId" id="subCatId" required>
                        <option value="">Select Sub category</option>
                      </select>

                       </div>
                     </div>
                     <input type="hidden" name="hideCatgory" id="hideCatgory" value="<?= $row['cat_id'] ?? ''?>">
                     <input type="hidden" name="hideSubCatgory" id="hideSubCatgory" value="<?= $row['subCatId'] ?? ''?>">
                      
                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> product name </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <input <?= $display ?> class="form-control form-control-sm" id="colFormLabelSm" placeholder=" Add product name" name="product_name" value="<?= $row['product_name'] ?? ''?>"  required="">
                       </div>
                     </div>

                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> product price </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <input <?= $display ?> class="form-control form-control-sm" id="colFormLabelSm" placeholder=" Add Price" name="price" value="<?= $row['price'] ?? ''?>"  required="">
                       </div>
                     </div>
                      <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Expired date (Optional) </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <input type="text" id="expire" name="expire" placeholder="Product Expire Date" class="form-control datepicker-autoclose_normal" tabindex="2"  value="<?= $row['expire'] ?? ''?>" />
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                         <!-- <input <?= $display ?>  type="text" class="datepicker-autoclose_normal" id="colFormLabelSm"  name="expire" value=""  > -->
                       </div>
                     </div>

                     
                     <!-- <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Unit </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <input type="email" class="form-control form-control-sm" id="colFormLabelSm" placeholder=" Add Unit">
                       </div>
                     </div> -->

                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Description </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <textarea  <?= $display ?> class="form-control" placeholder="Add Description" name="description"><?=$row['description'] ?? ''?>
                         </textarea> 
                       </div>
                     </div>
                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Long Description </label>
                       <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                         <textarea  <?= $display ?> class="form-control" placeholder="Add Long Description" name="description"><?=$row['long_description'] ?? ''?>
                         </textarea> 
                       </div>
                     </div>                     
             
            

                   <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
                       <div class="col-sm-6">
                         <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
                          <div class="image col-md-12 col-lg-9 col-sm-12">
                            <?php $img= (isset($row['image']) && !empty($row['image']) ) ? base_url().'uploads/product/'.$row['image'] :  base_url().'assets/dist/img/admin.png'  ?>
                            <img id="imgView" width="200" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
                          </div>
                       </div>
                       </div>

                      <div class="form-group row ">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3">file or image                     </label>
                        <div>
                        
                         <div class="input-group ">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Upload</span>
                        </div>
                        <div class="custom-file">
                          <input <?= $display ?> type="file" required class="custom-file-input" name="profile_image" id="profile_image">
                          <label class="custom-file-label col-md-8" for="inputGroupFile01">Choose file</label></div></div>
                        </div>
                      </div>




                     


                    <div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                 
                </div>
                 </form> 
                <!-- /.card-body -->

              
            
            </div>


            
</div>
         