<?php

$controller = $this->uri->segment(1);
$view = $this->uri->segment(3);
$Category = modules::run('Category/category');
$Brands = modules::run('Product/subcategoryAsCat');
$id = $this->uri->segment(4);
$row=[];
if(isset($result_set[0])){
  $row=$result_set[0];

}

$display =($view=='view') ? 'disabled' : '';
?>

<!-- 
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?= base_url('links/css/tini.min.css') ?>">
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link type="text/css" rel="stylesheet" href="<?= base_url('links/css/image-uploader.min.css') ?>">
<!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css"> -->
<div class="container">

           <div class="text-right"> 
                  <a href="<?php echo base_url() ?>Product/"><button class="btn btn-outline-success mb-1 justify-content-center">Product List</button></a></div>
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Add Images</a>
  </li>
</ul>


  



<div class="card card-success">
<div class=" card-body">
  <form  enctype="multipart/form-data" class="form-horizontal form-validate-group submitIRM" id="selectbox_validate" role="form" data-toggle="validator" method="post" accept-charset="utf-8" novalidate="true">
  <input type="hidden" id="hidden_id" name="id" value="<?= $id ?>">
  <input type="hidden" id="array" name="uploadOld">
  <input type="hidden" id="removearray" name="removearray">
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        

                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Category  </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

                      <select <?= $display ?> class="form-control select2"  name="cat_id" id="category" required>
                        <option value="">Select category</option>
                        <?php foreach ($Category as $key => $value) { ?> <!-- for each loop  -->
                            <option <?= (isset($row['cat_id']) && $row['cat_id']==$value['id']) ? 'selected' : '' ?>  value="<?= $value['id'] ?>">
                              <?= $value['category_name'] ?></option>
                        <?php } ?>
                      </select>
                            <label for="category" class="error" ></label>
                       </div>
                     </div>
                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Brands  </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

                      <select <?= $display ?> class="form-control select2" name="subCatId" id="subCatId" >
                        <option value="">Select Brands</option>
                         <?php foreach ($Brands as $key => $value) { ?> <!-- for each loop  -->
                               <option <?= (isset($row['subCatID']) && $row['subCatID']==$value['id']) ? 'selected' : '' ?>  value="<?= $value['id'] ?>"><?= $value['subCategoryName'] ?></option>
                        <?php } ?>
                      </select>
                        <label for="subCatId" class="error" ></label>
                       </div>
                     </div>
                     <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Product name </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                         <input <?= $display ?> type="" class="form-control" id="product_name" placeholder=" Add product name" name="product_name" value="<?= $row['product_name'] ?? ''?>"  required="">
                       </div>
                     </div>
                      <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Product SKU </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                         <input <?= $display ?> type="" class="form-control" id="product_code" placeholder=" Add product SKU" name="product_code" value="<?= $row['product_code'] ?? ''?>"  required="">
                       </div>
                     </div>

                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Retail price </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                         <input <?= $display ?> type="number" class="form-control"  placeholder="Retail  Price" id="RetailPrice" name="retail_price" value="<?= $row['retail_price'] ?? ''?>"  required>
                       </div>
                     </div>
                    <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Current price </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                         <input <?= $display ?> type="number" class="form-control"  placeholder="Current Price" id="price" name="price" value="<?= $row['price'] ?? ''?>"  required>
                       </div>
                     </div>
                      <div class="form-group row">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-3 col-lg-3 col-xs-12">Expired date (Optional) </label>
                       <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                        <input type="" <?= $display ?> id="expire" name="expire" placeholder="Product Expire Date" class="form-control datepicker-autoclose_normal" tabindex="2"  value="<?= $row['expirey_date'] ?? ''?>" />
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                         <!-- <input <?= $display ?>  type="text" class="datepicker-autoclose_normal" id="colFormLabelSm"  name="expire" value=""  > -->
                       </div>
                     </div>

                       <div class="form-group row" style="display: none_;">
                       <label for="colFormLabelSm" class="col-md-3 col-sm-12 col-lg-3" ></label>
                       <div class="col-sm-6">
                         <!-- <textarea class="form-control col-md-12 col-lg-9 col-sm-12"  required="" placeholder="add description" name="description" height="200"></textarea> -->
                          <div class="image col-md-12 col-lg-9 col-sm-12">
                            <?php $img= (isset($row['banner']) && !empty($row['banner']) ) ? base_url().'uploads/banners/'.$row['banner'] :  base_url().'assets/clip.png'  ?>
                            <img id="imgView" width="400" height="200" src="<?= $img ?>" class="img-rounded elevation-2" alt="User Image">
                          </div>
                       </div>
                       </div>


                       <div class="form-group row ">
                         <label for="profile_image" class="col-md-3 col-sm-12 col-lg-3">Product Banner / Cover</label>
                          <div>
                          
                           <div class="input-group " style="padding-left: 17px">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Upload</span>
                          </div>
                          <div class="custom-file">
                          <input <?= $display ?> type="file" class="custom-file-input profile_image" name="profile_image" id="profile_image">
                          <label class="custom-file-label col-md-8" for="inputGroupFile01">Choose file</label></div></div>
                        </div>                        
                      </div>






                   <div class="form-group row">
                     <label for="description" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Description </label>
                     <div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
                      <textarea <?= $display ?>  class="form-control description"  placeholder="Add description" id="description" name="description" 
                        height="1000"><?= isset($row['description']) ? ( ($row['des_status']==1) ? decrypt($row['description']) :$row['description'] ) : '' ?></textarea>
                     </div>
                   </div>

                   <div class="form-group row">
                     <label for="long_description" class="col-md-3 col-sm-3 col-lg-3 col-xs-12"> Long Description </label>
                     <div class="col-lg-7 col-md-9 col-sm-10 col-xs-12">
                      <textarea <?= $display ?> rows="10"  class="form-control description"  placeholder="Add Long Description" id="long_description" name="long_description" 
                        height="1000"><?= isset($row['long_description']) ? ( ($row['des_status']==1) ? $row['long_description'] :$row['long_description'] ) : '' ?></textarea>
                     </div>
                   </div>
                   
                                        
                                  
             

    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

     


 <div class="input-field">
        <label class="active">Photos</label>
        <div class="input-images-1" style="padding-top: .5rem;"></div>
        <div class="validation" style="display:none;"> Upload Max 4 Files allowed </div>
    </div>

    </div>
  </div>

<div class="card-footer" <?= ($view=='view') ? ' style="display:none" ' : ''; ?>>


                      <button type="submit" class="btn btn-outline-success">Submit</button>
                    </div>

</form>     
</div>
</div>
</div>


<script>
 
</script>



