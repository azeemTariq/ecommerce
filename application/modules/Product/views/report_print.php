<html>
<head>
  <title>Grandeur | Perfume Shop</title>
  <!-- styling -->

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="shortcut icon" href="<?= base_url() ?>assets/website/img/favicon.ico" type="image/x-icon" />

  <!-- <title>Admin Panel | Dashboard</title> -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/font/ionic.min.css"> -->
   <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" />

   <link href="<?php echo base_url('assets/plugins/bower_components/bootstrap-switch/bootstrap-switch.min.css')?>" rel="stylesheet">

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">



  
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/adminlte.min.css">

<style>
  .sparkl{
    border:1px solid green;
    margin: 1%;
  }
</style>
</head>
<body>
  


<div class="container-fluid">
  <div class="col-md-12 offset-0">
   <a href="<?= base_url('Product') ?>" style="float:right;" class=" btn btn-success">Back to Product List</a> 
 <h2 class="text-center">Product Detail Report</h2>
   <p class="text-center">Product Status</p>
<div class="row">
  <div class="col-lg-2 sparkl">
                      <div class="white-box analytics-info">
                          <h4 class="box-title">&nbsp;</h4>
                          <ul class="list-inline two-part">
                              <li class=""><span  style="font-size:21px;">
                                Added in cart (<span id="activeUsers">0</span>)</span></li>
                            </ul>
                      </div>
                  </div>
                  <div class="col-lg-2 sparkl">
                      <div class="white-box analytics-info">
                          <h4 class="box-title">&nbsp;</h4>
                          <ul class="list-inline two-part">
                              <li class=""><span  style="font-size:21px;">
                               Added in wish (<span id="activeUsers">0</span>)</span></li>
                            </ul>
                      </div>
                  </div>
                  <div class="col-lg-2 sparkl">
                      <div class="white-box analytics-info">
                          <h4 class="box-title">&nbsp;</h4>
                          <ul class="list-inline two-part">
                              <li class=""><span  style="font-size:21px;">
                                Total Orders (<span id="activeUsers">0</span>)</span></li>
                            </ul>
                      </div>
                  </div>
                  <div class="col-lg-2 sparkl">
                      <div class="white-box analytics-info">
                          <h4 class="box-title">&nbsp;</h4>
                          <ul class="list-inline two-part">
                              <li  ><i  class="icon-people text-danger"></i></li>
                              <li class=""><span  style="font-size:21px;">
                                Product view (<span id="activeUsers">0</span>)</span></li>
                            </ul>
                      </div>
                  </div>

                  <div class="col-lg-2 sparkl">
                      <div class="white-box analytics-info">
                          <h4 class="box-title">&nbsp;</h4>
                          <ul class="list-inline two-part">
                              <li class=""><span  style="font-size:21px;">
                                Customer review (<span id="activeUsers">0</span>)</span></li>
                            </ul>
                      </div>
                  </div>
                  

  </div>





<div class="row">

      <div class=" col-xs-12 col-md-12 col-sm-12" style="height:500px;margin-top:0%;    margin-bottom: 4px;float: left;position: relative;" >
             <div class="white-box" >
                 <h3 class="box-title">Last 12 month summary</h3>
                 <div>
                       <div id="salesVsStock"> </div>
                       <!-- <canvas id="salesVsStock" height="150"></canvas> -->
                    
                 </div>
             </div>
      
         </div>
</div>



<div class="row">

      <div class="col-md-6">
        <h4 class="text-center">Orders</h4>
  <div class="table-responsive">
      <table class="table table-striped table-hover">
         <thead>
           <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Order No</th>
              <th>Qty</th>
              <th>Order Status</th>
           </tr>
         </thead>
         <tbody>
          <?php $statusTitle = array(1=>'New Order',2=>'Confirm',3=>'Dispatched',4=>'Delivered');
          foreach ($orderStatus as $key => $value): ?>
           <tr>
             <td><?= $key+1 ?></td>
             <td><?= $value->name ?></td>
             <td><?= $value->code ?></td>
             <td><?= $value->record ?></td>
             <td><?= $statusTitle[$value->status] ?></td>
           </tr>
           <?php endforeach ?>
         </tbody>
      </table>
  </div>
  </div>
  <div class="col-md-6">
    <h4 class="text-center">Customer Review</h4>
    <div class="table-responsive">
      <table class="table table-striped table-hover">
         <thead>
           <tr>
              <th>#</th>
              <th>Customer Name</th>
              <th>Star</th>
              <th>Comment</th>
              <th>Review Date</th>
           </tr>
         </thead>
         <tbody>
           <?php 
          foreach ($ReviewsStatus as $key => $value): ?>
           <tr>
             <td><?= $key+1 ?></td>
             <td><?= $value->name ?></td>
             <td><?= $value->stars ?></td>
             <td><?= $value->comment ?></td>
             <td><?= $value->created_at ?></td>
           </tr>
           <?php endforeach ?>
         </tbody>
      </table>
    </div>
  </div>
</div>


</div>
</div>









<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>


<!-- Bootstrap Switch -->
<script src="<?php echo base_url('assets/plugins/bower_components/bootstrap-switch/bootstrap-switch.min.js')?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url() ?>assets/plugins/sparklines/sparkline.js"></script>


<script src="<?php echo base_url('assets/js/higherChart.js')?>"></script>
<script src="<?php echo base_url('assets/js/highcharts-3d.js')?>"></script>
<script>
    $(document).ready(function() {

   var sparklineLogin = function() { 

        }
         sparklineLogin();

});

    function BarGraph(dataSet1,dataSet2,dataSet3,dataSet4,labels,title){
var array1=[];
var array2=[];
var array3=[];
var array4=[];
for(var i = 0; i<dataSet1.length; i++) {
   array1[i]={y: parseInt(dataSet1[i]) ,color: 'orange'};
   array2[i]={y: parseInt(dataSet2[i]) ,color: 'purple'};
   array3[i]={y: parseInt(dataSet3[i]) ,color: 'black'};
   array4[i]={y: parseInt(dataSet4[i]) ,color: 'blue'};
}
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'salesVsStock',   //title[0],
        type: 'column'
    },
    title: {
        text: ''
    },
     xAxis: {
            categories:labels,
           
        },
    subtitle: {
        text: ''
    },
    plotOptions: {
        column: {
            depth: 25
        }
    },series: [{
            name:title[0],
            data:array1,
            color:'orange'
        },{
            name:title[1],
            data:array2,
            color:'purpule'
        },{
            name:title[2],
            data:array3,
            color:'black'
        },{
            name:title[3],
            data:array4,
            color:'blue'
        }]
   
});
}



function GraphSumary(){
  $.ajax({
    url:'<?php echo base_url('Product/summary'); ?>',
    type:'POST',
    data:{role_id:1},
    dataType: "json",
    success:function(res){
 
    console.log(res);
                labels = JSON.parse(res.labels);
                dataSet1 = JSON.parse(res.dataSet1);
                dataSet2 = JSON.parse(res.dataSet2);
                dataSet3 = JSON.parse(res.dataSet3);
                dataSet4 = JSON.parse(res.dataSet4);
                BarGraph(dataSet1,dataSet2,dataSet3,dataSet4,labels,['Added in cart','Added in wishlist','Added in order','Product view']);
        
         
        }
         
     }); 
}
GraphSumary();


</script>
</body>
</html>
