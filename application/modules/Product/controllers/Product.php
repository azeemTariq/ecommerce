<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller {

  public function __construct() {
		parent::__construct();
     modules::run('Login/login');
		$this->load->helper('url', 'form');
		 $this->load->model('Mdl_Product');
	}
 public $controller = __CLASS__;
	  public function index(){
      $data['pageTitle']= "Product List";
	  	 $data['result']=$this->getProduct();
        $data['content'] = 'Product/index.php';
        $this->load->view('Template/template',$data);
    }

    public function add(){
       $data['pageTitle']= "Product";
	  	 $data['result']=$this->getProduct();
       $data['content'] = 'Product/tab.php';
        // $data['content'] = 'Product/action';
        $this->load->view('Template/template',$data);
    }

    public function testing(){
        $this->load->view('Product/testing');
    }
      



    public function getProduct($id=''){
      $this->db->select("*");
      $this->db->from('product');
      // $this->db->where('status',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
    }
    public function subcategoryAsCat($id=''){
      $this->db->select("*");
      $this->db->from('sub_category');
       $this->db->where('is_active',1);
      if($id!=''){
      $this->db->where('id',$id);
      }
      return $this->db->get()->result_array();
    }
    public function getList() {
        $controller = $this->router->class;
        $requestData = $_REQUEST;
        $columns = array( 
            'code',
            'product_name',
            'description',
            'retail_price',
            'price',
            'subCategoryName',
            'category_name',
          );
        $this->db->start_cache();
        $this->db->select("product.*,subCategoryName,' ' shop,category_name");
        $this->db->from('product');
        $this->db->join('category ct','product.cat_id = ct.id AND ct.is_active =1','inner');
        $this->db->join('sub_category sct','product.subCatId = sct.id ','left');
        // $this->db->join('login lg','lg.id = product.created_by AND lg.is_active=1','inner');
        $this->db->where('product.is_active<>',2);

        $i = 0;
        $where_filters='';
        foreach ($columns as $key => $value) {
            if (!empty($requestData['columns'][$i]['search']['value'])) {
                  $this->db->like($value, $requestData['columns'][$i]['search']['value']); 
            }
            $i++;
        }
       
        $this->db->stop_cache();
        $totalFiltered = $this->db->count_all_results();
        $this->db->limit($requestData['length'], $requestData['start']);

        $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        $query = $this->db->get()->result_array();
        
        $data = array();
        foreach ($query as $key => $row) {
          $id= encrypt($row['id']);

          $row['action'] = ' <div class="visible-md visible-lg visible-sm visible-xs">';
         
            $row['action'].=' <a class="green" _target="_blank" href="' . base_url($controller . '/action/view/' . $id) . '"><i class="far fa-eye"></i></a>';
         
              $row['action'].=' <a class="green"  _target="_blank" href="' . base_url($controller . '/action/edit/' . $id) . '"><i class="far fa-edit"></i></a>';
         
         
             $row['action'].=' <a class="green"  href="javascript:void(0)" onclick=deleted(this,"'.$id.'","product","Mdl_Product") href="#"><i class="far fa-trash-alt"></i></a>';
            
            
            $row['action'].='</div>';  

            $row['report'] = '<a href="' . base_url($controller . '/report_print/' . $id) . '"><button class="btn btn-info"><i class="fa fa-file"></i></button></a>';
            $is_active =($row['is_active']==1) ? 'checked' : '';
            $row['status']='<div class="col-md-12  bt-switch">
                                 <input type="checkbox" '.$is_active.' data-size="mini" onchange=changeStatus(this,"'.$id.'","Product","product")  data-on-color="success" data-off-color="info"  data-on-text="Yes" data-off-text="No"/>
                              </div>';



            $nestedData = array();
            $nestedData[] = $row['code'];
            $nestedData[] = $row['product_name'];
            $nestedData[] = ($row['des_status']==1) ? decrypt($row['description']) : $row['description'];
            $nestedData[] = $row['retail_price'];
            $nestedData[] = $row['price'];
            $nestedData[] = '<i class="text-success">'.$row['subCategoryName'].'<i>';
            $nestedData[] = '<i class="text-success">'.$row['category_name'].'<i>';
            $nestedData[] =$row['status'];
            $nestedData[] = $row['action'];
            $nestedData[] = $row['report'];
     
            $data[] = $nestedData;
        }
        $json_data = array(
            "draw" => intval($requestData['draw']),
            "recordsTotal" => intval(count($query)),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

     public function command(){
      $this->load->library('form_validation');
      // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
       $post=$this->input->post();


        $id = (!empty($post['id'])) ? decrypt($post['id']) : '';
       if(!empty($post)){ 
          $this->form_validation->set_rules('cat_id', 'Category', 'required|max_length[16]');
          $this->form_validation->set_rules('subCatId', 'Sub Category', 'max_length[16]');
          $this->form_validation->set_rules('product_name', 'Product Name', 'required|is_unique_update[product.product_name@id@' .$id. ']');
          $this->form_validation->set_rules('price', 'Item price', 'required|max_length[16]');
          $this->form_validation->set_rules('description', 'Description', 'max_length[2000]');
          if ($this->form_validation->run() == FALSE){
            $_return['msg'] = validation_errors();
            $_return['status'] = "Error";
            $_return['error'] = 1;
            echo json_encode($_return);
            exit();
          }
           $banner =do_upload($_FILES['profile_image'] ?? '','uploads/banners/',null,'profile_image');
           $Data=array(
            'cat_id'        =>$post['cat_id'],
            'subCatID'      =>$post['subCatId'] ?? 0,
            'product_name'  =>$post['product_name'],
            'retail_price'  =>$post['retail_price'] ?? 0,
            'price'         =>$post['price'] ?? 0,
            'description'   =>encrypt($post['description']),
            'product_code'  =>$post['product_code'],
            'long_description'   =>$post['long_description'],
            'des_status'    => 1,
            'prod_date'     => date('Y-m-d'),
            'year'          => date('Y'),
           );
           if(!empty($banner)){
            $Data['banner'] =$banner;
           }
           $previous = explode(',',$post['uploadOld']); 
           $removeArray = explode(',',$post['removearray']); 
           $rowCount = 0;
           foreach ($previous as $key => $value) {
             if($value){
              $rowCount++;
              $exist = explode('product/',$value);
               $Data['image'.($key+1)]=$exist[1];
             }
           }
            $Data=array_merge($Data,$this->imagesUploader($_FILES,$rowCount+1));
           
           if(!empty($post['id'])){
            foreach ($removeArray as $key => $value) {
             if($value){
               $exist = explode('product/',$value);
               if(file_exists($exist[1])){
                 unlink('././uploads/product/'.$exist[1]);
                }
             }
           }

            $id=decrypt($post['id']);
            $this->db->update('product',['image1'=>'','image2'=>'','image3'=>'','image4'=>'','image5'=>''],'id='.$id);
            $this->Mdl_Product->commandAct($Data,'product',['id'=>$id]);
           $_return=['status'=>'success','msg'=>'successfully Updated !'];
          }else{
            $_return=['status'=>'success','msg'=>'successfully Created !'];
             $this->Mdl_Product->commandAct($Data,'product');
          } 
           echo json_encode($_return);
            exit();
           }
      redirect(base_url($this->controller.'/'));
    } 
    public function action(){
     if($this->uri->segment(3)=='view' || $this->uri->segment(3)=='edit'){
      $id= decrypt($this->uri->segment(4));
      if(!empty($id)){
        $data['result_set'] = $this->getproduct($id);
        $data['content'] = 'Product/tab';
        return $this->load->view('Template/template',$data);
      }
    }
    redirect(base_url($this->controller.'/'));
  } 
  public function validateDate($date, $format = 'Y-m-d')
  {
      $d = DateTime::createFromFormat($format, $date);
      // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
      return $d && $d->format($format) === $date;
  }







// function file_upload()
// {       
//     $config = array();
//     $config['upload_path'] = './uploads/product/';
//     $config['allowed_types'] = 'gif|jpg|png';
//     $config['max_size']      = '0';
//     $config['overwrite']     = FALSE;

//     $this->load->library('upload');

//     $files = $_FILES;

//     for($i=0; $i< count($_FILES['photos']['name']); $i++)
//     {           
//         $_FILES['photos']['name']= $files['photos']['name'][0][$i];
//         $_FILES['photos']['type']= $files['photos']['type'][0][$i];
//         $_FILES['photos']['tmp_name']= $files['photos']['tmp_name'][0][$i];
//         $_FILES['photos']['error']= $files['photos']['error'][0][$i];
//         $_FILES['photos']['size']= $files['photos']['size'][0][$i];    
       
//         $this->upload->initialize($config);
         
//         // $this->upload->do_upload();
//          if(!$this->upload->do_upload('photos')) 
//             {
//                 print_r($this->upload->display_errors());
//             } 
//     }
// }
function imagesUploader($files,$count=0)
{       


    $config = array();
    $config['upload_path'] = './uploads/product/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';

    // $config['max_size']      = '0';
    $config['overwrite']     = FALSE;

    $this->load->library('upload');

    $_FILES = $files;
   $imacCount = $count;
       $array=[];
  $count = count($_FILES['photos']['name']);
    for($i=0; $i< $count; $i++)
    {           
        $_FILES['photos']['name']       = $files['photos']['name'][$i];
        $_FILES['photos']['type']       = $files['photos']['type'][$i];
        $_FILES['photos']['tmp_name']   = $files['photos']['tmp_name'][$i];
        $_FILES['photos']['error']      = $files['photos']['error'][$i];
        $_FILES['photos']['size']       = $files['photos']['size'][$i];
        $config['file_name']            = encrypt($_FILES['photos']['name'].time());    
        $this->upload->initialize($config);
         if(!$this->upload->do_upload('photos')) 
            {
            } else{
                $upload_data    = $this->upload->data();
                $name_array[]   = $upload_data['file_name'];
                $fileName       = $upload_data['file_name'];
                $array['image'.$imacCount] = $fileName; 
                $imacCount++;
           
            }
    }
    return $array;
}




private function getRandomFileName($name)
{
    $random = rand(1, 10000000000000000);
    return hash('sha512', $random.$name . config_item("encryption_key"));
}
public function approvedProduct(){
   $id=decrypt($this->input->post('id'));
   $data=array('is_approved'=>1);
   $this->Mdl_Product->commandAct($data,'product',['id'=>$id]);
}
public function report_print(){
     $data['orderStatus'] = $this->Mdl_Product->OrderStatusByCustomer();
     $data['ReviewsStatus'] = $this->Mdl_Product->ReviewsByCustomer();
     $this->load->view('Product/report_print',$data);
}
   public function summary()
     {   
       echo json_encode($this->getBarGraph());
     }
public function getBarGraph(){

        if($this->session->userdata('role')==2){
                $data['result'] = $this->Mdl_Product->getStoreProduct($this->session->userdata('id'));
        }else{
            $data['result'] = $this->Mdl_Product->getStoreProduct();
        }
        
        $labels = array();
        $count = 0;
        for ($i = 1; $i <= 12; $i++) {
            $labels[] = date("M-Y", strtotime( date( 'Y-m-01' )." -$i months"));
        }

        for ($i=0; $i < 12; $i++) {
            $dataSet1[$i] = rand(10,500);
            $dataSet2[$i] = rand(10,500);
            $dataSet3[$i] = rand(10,500);
            $dataSet4[$i] = rand(10,500);
        }
        
        // $dataSet1 = [0,0,0,0,0,0,0,0];
        // $dataSet2 = [0,0,0,0,0,0,0,0];

       
        return array(
            'labels'=>json_encode($labels),
            'dataSet1'=>json_encode($dataSet1),
            'dataSet2'=>json_encode($dataSet2),
            'dataSet3'=>json_encode($dataSet3),
            'dataSet4'=>json_encode($dataSet4),
        );
    }

}
