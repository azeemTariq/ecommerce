<?php



defined('BASEPATH') OR exit('No direct script access allowed');



 class MY_Form_validation extends CI_Form_validation {



			public function __construct() {

			parent::__construct();

			$this->ci=& get_instance();

			}



		public function is_unique_update($str, $field){

		    

		$CI = & get_instance ();

		$explode			=	explode('@', $field);

		$field_name			=	$explode['0'];

		$field_id_key		=	$explode['1'];

		$field_id_value		=	$explode['2'];

		$field_bg_value		=	$explode['3'] ?? null;

		$is_active			=	$explode['4'] ?? 'is_active';

		sscanf($field_name, '%[^.].%[^.]', $table, $field_name);

	

		if(isset($this->ci->db)){

			$where=array($field_name => $str,$field_id_key.'!='=>$field_id_value,$is_active.'!='=>2);

			if($this->ci->db->limit(1)->get_where($table, $where)->num_rows() === 1){


				$this->ci->form_validation->set_message('is_unique_update', 'The {field} field must contain a unique value.');

				return false;

			}

			

			else

			{

			    return true;

			}

			

			

		}

}



	public function Not_single_zero($number){
		if($number > floatval(0)) 
			 return true;
		   else
		   	 $this->ci->form_validation->set_message('Not_single_zero', 'The {field} must greater then Zero.');
			 return false;
		}
	public function access_code_unique($str, $field) {
	    $explode=explode('@', $field);
	    $field_name=$explode['0'];
	    $field_id_value=$explode['1'];
	    sscanf($field_name, '%[^.].%[^.]', $table, $field_name);
	    if(isset($this->ci->db)){	      
	        if($this->ci->db->limit(1)->select('*')->from($table)->where(json_decode($field_id_value,true))->get()->num_rows() > 0){	  
	            $this->ci->form_validation->set_message('access_code_unique', 'The {field} field must contain a unique value.');
	            return false;
	        }
	        return true;
	    }
	    
	}
	public function update_code_unique($str, $field) {
	    $explode=explode('@', $field);
	    $field_name=$explode['0'];
	    $field_id_value=$explode['1'];
	    sscanf($field_name, '%[^.].%[^.]', $table, $field_name);
	    if(isset($this->ci->db)){
	        if($this->ci->db->limit(1)->select('*')->from($table)->where(json_decode($field_id_value,true))->get()->num_rows() != 1){
	            $this->ci->form_validation->set_message('access_code_unique', 'The {field} field must contain a unique value.');
	            return false;
	        }
	        return true;
	    }
	    
	}
	public function check_date($str, $min)
	{	
		if(strtotime($min) >= strtotime($str)){
			$this->ci->form_validation->set_message('check_date', 'The {field} must be greater than {param}.');
	            return false;
		}
	}

	function alpha_dash_space($str){
	    if (! preg_match('/^[a-zA-Z\s]+$/', $str)) {
	        $this->ci->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha characters & White spaces');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
	function alpha_numerics($str){
	    if (! preg_match('/^[a-zA-Z0-9 \s]+$/', $str)) {
	        $this->ci->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alpha characters & White spaces');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}

	function only_name($str){
	    if (! preg_match('/^[a-zA-Z \\-\\@._()-]*$/', $str)) {
	        $this->ci->form_validation->set_message('only_name', '%s Contain Only Alphabets And Space');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
	function usernameOnly($str){
	    if (! preg_match('/^[a-zA-Z0-9-_]+$/', $str)) {
	        $this->ci->form_validation->set_message('usernameOnly', 'The %s field may only contain alpha characters & dash');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}

		function alpha_func($str){
	    if (! preg_match('/^[a-zA-Z \\-\\_]*$/', $str)) {
	        $this->ci->form_validation->set_message('alpha_func', '%s Contain Only Alphabets And UnderScore');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}

		function number($str){
	    if (! preg_match('/^[0-9]+$/', $str)) {
	        $this->ci->form_validation->set_message('number', '{field} Contain Only Digits');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}

	function amount_type($str){
	    if (! preg_match('/^[0-9.,]+$/', $str)) {
	        $this->ci->form_validation->set_message('amount_type', '{field} Contain Only Digits , Dot And Comma ');
	        return FALSE;
	    } else {
	        return TRUE;
	    }
	}
	function equal_to($str, $val)
	{
		if(is_numeric($str) && ($str == $val)){
			return TRUE;
		}
		$this->ci->form_validation->set_message('equal_to', '{field} Must be equal to '.$val);
		return FALSE;
	}





 }