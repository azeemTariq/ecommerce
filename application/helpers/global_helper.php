<?php


if (! defined('BASEPATH')) exit('No direct script access allowed');
 

function encrypt($pure_string) {
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
    $encrypted_string = base64_encode(json_encode($pure_string));
    return str_replace($dirty, $clean, $encrypted_string);
}

function decrypt($encrypted_string) { 
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
    $decrypted_string = json_decode( base64_decode(str_replace($clean, $dirty, $encrypted_string)) , true );
    return $decrypted_string;
}
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function rangeMonth($datestr) {
   
    date_default_timezone_set(date_default_timezone_get());
    $dt = strtotime($datestr);
    $res['current'] = date(dateResetFormate());
    $res['start'] = date(dateResetFormate(), strtotime('first day of this month', $dt));
    $res['end'] = date(dateResetFormate(), strtotime('last day of this month', $dt));
    $res['startDay'] = date('d', strtotime('first day of this month', $dt));
    $res['LastDay'] = date('d', strtotime('last day of this month', $dt));

    return $res;
}
function dateResetFormate(){
    $CI = & get_instance ();
    return $dateFormate=str_replace("y","Y",preg_replace("/(.)\\1+/", "$1",$CI->session->userdata('date_formate') ?? "Y-m-d"));
}
function dd( $arr, $is_bool = TRUE, $title = '' )
{
 if( $title )
 {
  echo '<strong>'.$title.'</strong>';
 }

 echo '<pre>';
 print_r( $arr );
 echo '</pre>';

 if( $is_bool )
  exit();
}
function pre($returnValue){
	echo '<pre>';
	print_r($returnValue);
	echo '</pre>';
}
function do_upload($upload,$url,$size='20000000',$nameInput='profile_image',$vari='a'){
    $ci =& get_instance();
    if(!empty($upload)){
        $ci->load->library('form_validation');
        if($upload["error"]==0)  {
            $name=$vari.strtotime(date('H:i:s')).".".pathinfo($upload["name"], PATHINFO_EXTENSION);
            $config['upload_path'] =   $url;    //'uploads/CategoryImage/';
            $config['allowed_types']='gif|jpg|jpeg|png';

            $config['max_size']     = $size;
            // $config['max_width'] = '1024';
            // $config['max_height'] = '768';
            $config['file_name'] = $name;

            $ci->load->library('upload', $config);
            $ci->upload->initialize($config);
            if ( ! $ci->upload->do_upload($nameInput)){
                return 'error';
                // return array('error'=>$this->upload->display_errors());
            }else{
                // resize(base_url($url.$name));
                return $image_name=$name;
                }
            }
        }else{
           return false;
        }
    }
function resize($filePath){
    list($width, $height) = getimagesize($filePath);
    $percent = 0.5;
        if($width>350 && $height>545)
        {   $percent = 0.5; }
        else{$percent = 1;}
        $newwidth = $width * $percent;
        $newheight = $height * $percent;
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        $source = null;
        $source = imagecreatefromjpeg($filePath);
        // Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        // Output
        
        $pieces = explode('/', $filePath);
        $url=implode('/', array_slice($pieces, 0, -1));
        $name= substr(md5(str_replace("jpeg","",end($pieces))), 0, 8);
        $files = glob($url.'/*.jpg');
        if ( $files !== false )
        {
            $count = count( $files );
        }
        else
        {
            $count=0;
        }
        $temporary=$url."/".$name."".$count.".jpg";
        imagejpeg($thumb, $temporary, 50);
        unlink($filePath);
        return $name."".$count;
    }

 function permission($btn){
    return  true;
    $ci =& get_instance();
    $id= $ci->session->userdata('id');
    if($id!=1 && $id!='' ){
        $result = $ci->db->query("SELECT * FROM login  WHERE login.".$btn." = 1 AND id =  ".$id."  ")->row();
         return  $result;
    }else{
        return  true;
    }
    
 }
?>